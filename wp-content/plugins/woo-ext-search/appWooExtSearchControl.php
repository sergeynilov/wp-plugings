<?php
if ( ! defined( 'NSN_WooExtSearch' ) ) {       //appWooExtSearchControl    wp-content/plugins/wooExtSearch/appWooExtSearchControl.php
    exit; // Exit if accessed directly
}
abstract class appNSN_WooExtSearchControl    // appNSN_WooExtSearchControl
{
    protected $m_wpdb;
    protected $m_session;
    protected $m_pageUrl;
    protected $m_pluginDir;
    protected $m_pluginUrl;
    protected $m_currentUserId;

    protected $m_defaultLanguage;
    protected $m_containerClass;
    protected $isDebug;
    protected $m_action;
    protected $m_flashMessage;
    protected $m_protectXSSAttacks;
    protected $m_labelNonce;
    protected $m_labelNonceInput;
    protected $m_hasValidationErrors;
    protected $m_validationRules;
    protected $m_parentControl;
    protected $m_pageParametersWithSort;
    protected $m_pageParametersWithoutSort;
    protected $m_is_insert;
    protected $m_is_trimDataBeforeSave;
    protected $m_paged;
    protected $m_sort;
    protected $m_sort_direction;

    //
    public function __construct($containerClass, $wpdb, $session)
    {
        $session_id= session_id();
        if (empty($session_id)) {
            session_start();
        }
        $this->m_containerClass = $containerClass;
        //echo '<pre>$this->m_containerClass::'.print_r($this->m_containerClass,true).'</pre>';
        $this->m_pluginDir = (isset($this->m_containerClass) ? $this->m_containerClass->getPluginDir() : null);
        $this->m_pluginUrl = (isset($this->m_containerClass) ? $this->m_containerClass->getPluginUrl() : null);
        $current_user_id = get_current_user_id();
        $this->m_currentUserId = (isset($current_user_id) ? $current_user_id : null);
        $this->m_defaultLanguage= '';// nsnClass_appLangs::getDefaultLanguage(); //getDefaultLanguage
        $this->m_currentBackendLang= '';// nsnClass_appLangs::getCurrentBackendLanguage(2, __FILE__.' : '.__LINE__ ); //getDefaultLanguage

        $this->m_POSTDataArray = array();
        $this->m_GETDataArray = array();
        $this->validationRules = array();
        $this->m_protectXSSAttacks = true;
        $this->m_validationRules = array();
        $this->isDebug = false;
        $this->m_is_trimDataBeforeSave = true;
        $this->m_wpdb = $wpdb;
        $this->m_session = $session;
        $this->setAppParameters();
        $this->checkInstalledPlugins();
//        require_once( 'models/nsn-voting-reports-db.php');
    }

    protected function setValidationRules($validationRules) {
        $this->m_validationRules= $validationRules;
    }

    protected function setNotValidatedData($editorItem, $fieldsArray)
    {
//        echo '<pre> setNotValidatedData  $editorItem::'.print_r($editorItem,true).'</pre>';
//        die("INSIDE setNotValidatedData");
        foreach( $fieldsArray as $nextItemField=>$nextDataField ) {
            if ( isset($this->m_POSTDataArray[$nextDataField]) ) {
                $editorItem[$nextItemField]= $this->m_POSTDataArray[$nextDataField];
            }
        }
        return $editorItem;
    }


    protected function clearData() {

        $skip_sanitize_fields_array= array();
        if (isset($this->m_validationRules) and is_array($this->m_validationRules) ) {
            foreach( $this->m_validationRules as $rule=>$rulesFields ) {
                if (trim($rule) == 'skip_sanitize') {
                    $skip_sanitize_fields_array = nsnClass_appFuncs::splitStringIntoArray($rulesFields);
                    break;
                }
            }
        }

        /*DebToFile('-5 clearData  $_POST::' .print_r($_POST,true) );
        DebToFile('-51 clearData  $_GET::' .print_r($_GET,true) );
        DebToFile('-52 clearData  $_REQUEST::' .print_r($_REQUEST,true) );  */

        $data_array= array();
        $is_post_request= false;
        if (!empty($_GET) and is_array($_GET)) {
            $data_array= $_GET;
        }
        if (!empty($_POST) and is_array($_POST)) {
            $data_array= $_POST;
            $is_post_request= true;
        }

        /*$data_array= array(
            'action' => 'au<>tocomplete-save-options-as',
            ' logged_user_id ' => 1,
            'ca\'\'tegory_id' => '112  311',
            'price""_from' => 2,
            'price_to' => 44,
            'in_stock' => 0,
            'sort_direction' => 'a<g>sc',
            'attrs_array' => Array (
                Array('field' => 'color', 'value' =>'','field_type' => 'sing<>le_t<>ext' ),
                Array('field' => '_order-in-branches', 'value' => 'no', 'field_type' => 'boolean' ),
                Array('field' => '_width', 'value' => '=', 'field_type' => 'range' ),
                Array('field' => '_height', 'value' => '66=77', 'field_type' => 'range' )
            ),
            'save_options_as' => 'll'
        );

        echo '<pre>$data_array::'.print_r($data_array,true).'</pre>'; */
        /*
        foreach ($_POST as $nextPostKey=>$nextPostValue) {
            if ( in_array($nextPostKey,$skip_sanitize_fields_array) ) {
                $this->m_POSTDataArray[$nextPostKey] = $nextPostValue;
            } else {
                $this->m_POSTDataArray[$nextPostKey] = sanitize_text_field($nextPostValue);
            }
        }                   */

        //$cleared_data_array= array();
        $cleared_data_array= $this->make_sanitize($data_array, $skip_sanitize_fields_array);
//        DebToFile('-11 $options_array $cleared_data_array ::' .print_r($cleared_data_array,true) );

        if ( !$is_post_request ) {
            $this->m_GETDataArray= $cleared_data_array;
        } else {
            $this->m_POSTDataArray= $cleared_data_array;
        }

        reset($this->m_validationRules);
        foreach( $this->m_validationRules as $rule=>$rulesFields ) {
            $rule = trim($rule);
            // echo '<pre>$rule::'.print_r($rule,true).'</pre>';
            /*if (strtolower($rule) == 'email') {
                $rulesFieldsArray = preg_split('/,/', $rulesFields);
                foreach ($rulesFieldsArray as $nextRuleField) {
                    $nextRuleField= trim($nextRuleField);
                    if ( empty($nextRuleField) ) continue;
                    if (isset($this->m_POSTDataArray[$nextRuleField])  ) {
                        // echo '<pre>-$nextRuleField::'.print_r($nextRuleField,true).'</pre>';
                        $this->m_POSTDataArray[$nextRuleField]= sanitize_email($this->m_POSTDataArray[$nextRuleField]);
                    }
                }
            } // if (strtolower($rule) == 'email') {        */

            if (strtolower($rule) == 'filename') {
                $rulesFieldsArray = preg_split('/,/', $rulesFields);
                foreach ($rulesFieldsArray as $nextRuleField) {
                    $nextRuleField= trim($nextRuleField);
                    if ( empty($nextRuleField) ) continue;
                    if (isset($this->m_POSTDataArray[$nextRuleField])  ) {
                        // echo '<pre>-$nextRuleField::'.print_r($nextRuleField,true).'</pre>';
                        $this->m_POSTDataArray[$nextRuleField]= sanitize_file_name($this->m_POSTDataArray[$nextRuleField]);
                    }
                }
            } // if (strtolower($rule) == 'filename') {

            //
        }

    } // protected function clearData() {

    /*protected function showTemplate($templateName, $showHeader= false, $showFooter= false) {
        if ($showHeader) {
            require_once('/views/header.php');
        }
            require_once($templateName);
        if ($showFooter) {
            require_once('/views/footer.php');
        }
    }           */

    protected function make_sanitize($data_array, $skip_sanitize_fields_array= array())
    {
        //echo '<pre>' . count($skip_sanitize_fields_array).'::$skip_sanitize_fields_array::'.print_r($skip_sanitize_fields_array,true).'</pre>';
        $cleared_data_array= array();
//        DebToFile('-DER0 $data_array ::' .print_r($data_array,true) );
        if ( !empty($data_array) and is_array($data_array) ) {
            foreach ($data_array as $nextPostKey => $next_data_value) {
                //echo '<pre>$next_data_value::'.print_r($next_data_value,true).'</pre>';

                if (in_array($nextPostKey, $skip_sanitize_fields_array)) {
                    $cleared_data_array[$nextPostKey] = /*sanitize_text_field(*/
                        $next_data_value/*)*/
                    ;
                } else {
                    if (is_array($next_data_value)) {
//                        DebToFile('-DER $next_data_value ::' .print_r($next_data_value,true) );

                        $ret_array = array();
                        foreach ($next_data_value as $nextKey => $nextValue) {
                            $next_sanitized_value=  $this->make_sanitize($nextValue, $skip_sanitize_fields_array);
//                            DebToFile('-DER3 $next_sanitized_value ::' .print_r($next_sanitized_value,true) );
                            $ret_array[$nextKey] = $this->make_sanitize($nextValue, $skip_sanitize_fields_array);
                        }
//                        DebToFile('-DER5 $ret_array ::' .print_r($ret_array,true) );
                        $cleared_data_array[$nextPostKey] = $ret_array;
                    } else {
                        $cleared_data_array[$nextPostKey] = sanitize_text_field($next_data_value);
                    }
                }

            }
        }
        if ( !empty($data_array) and !is_array($data_array) ) {
            return sanitize_text_field($data_array);
        }
        return $cleared_data_array;
    }

    protected function doPOSTRequest( $action, $editorItem, $validationFields= array() ) {
        $this->isDebug= false;
        if ($this->isDebug) echo '<pre>IN   doPOSTRequest $this->m_POSTDataArray::'.print_r($this->m_POSTDataArray,true).'</pre>';
        //$action= isset($this->m_POSTDataArray['action']) ? $this->m_POSTDataArray['action'] : "list";
        if ( empty($this->m_POSTDataArray)   ) return array('action'=>$action);
        $this->m_hasValidationErrors = false;
        if ( $this->m_protectXSSAttacks and ( empty($this->m_POSTDataArray[ $this->m_labelNonceInput]) or !wp_verify_nonce($this->m_POSTDataArray[ $this->m_labelNonceInput], $this->m_labelNonce)) ) {
            die("NONCE IS SECURE INVALID!");
        }  // check nonce
        //die("-1");

        //$editorItem= isset($params['editorItem']) ? $params['editorItem'] : '';
        if ($this->isDebug) echo '<pre>-Z $action::'.print_r($action,true).'</pre>';

        if ( !empty($this->m_POSTDataArray)   and  isset($this->m_POSTDataArray['bulk_action'] ) ) { // one of bulc actions
            $this->doBulkAction( $this->m_POSTDataArray['bulk_action'] );
            return array('action'=>'list');
        }// if ( isset($this->m_POSTDataArray)   and  isset($this->m_POSTDataArray['bulk_action'] ) ) { // one of bulc actions

        //echo '<pre>BEFORE $this->validationRules::'.print_r($this->validationRules,true).'</pre>';
        if ( !empty($this->m_POSTDataArray) and $action == 'update'  ) { // That is POST update action
            $this->m_hasValidationErrors= $this->doValidation( $this->m_POSTDataArray );
        } //       if ( isset($this->m_POSTDataArray) and $action == 'update'  ) { // That is POST update action

        if ($this->isDebug) echo '<pre>$this->m_hasValidationErrors::'.print_r($this->m_hasValidationErrors,true).'</pre>';
        //die("-HH1");
        if( isset($this->m_hasValidationErrors) and is_array($this->m_hasValidationErrors) and count($this->m_hasValidationErrors) > 0 ) { // there are Validation Errors
            //echo '<pre>-1 $action::'.print_r($action,true).'</pre>';
            $action= $this->m_is_insert ? 'add' : 'edit';
            $editorItem= $this->setNotValidatedData( $editorItem, $validationFields );
            echo '<pre>$editorItem::'.print_r($editorItem,true).'</pre>';
//            die(" doPOSTRequest -1VV");
            if ($this->isDebug) echo '<pre>Is Invalid $editorItem::'.print_r($editorItem,true).'</pre>';
            return array('action'=>$action,'editorItem'=> $editorItem);
        } else {
            if (  $action == 'update'  ) { // That is POST delete action
                $this->beforeRowDataSave($editorItem);
                //die("-1 editorItem editorItem");
                $this->saveRowData($action, $editorItem);
            } //if (  $action == 'update'  ) { // That is POST update action
            if (  $action == 'delete'  ) { // That is POST update action
                $this->deleteRowData();
            } //if (  $action == 'delete'  ) { // That is POST delete action
            //die("-1HERE");

        } // { // if( $Validated OK
        //die("-2 HTRE");

    } // protected function doPOSTRequest() {

    protected function doEditorFlow($m_parentControl) {
        $m_parentControl->m_pageUrl= $this->m_pageUrl;
        $hasActionSpecified= false;
        if ( $this->m_action == 'list' ) {  // show list of items
            $this->showRowsListDataForm( $m_parentControl );
            $hasActionSpecified= true;
            return;
        } // if ( $this->m_action= 'list' ) {  // show list of items

        if ( $this->m_action == 'add' ) {   // show editor for adding of item
            $this->initFieldsAddRow( $m_parentControl );
            $this->addRowDataForm( $m_parentControl );
            $hasActionSpecified= true;
            return;
        } // if ( $this->m_action= 'add' ) {   // show editor for adding of item

        if ( $this->m_action == 'edit' ) {   // show editor for editing of item
            $this->editRowDataForm( $m_parentControl );
            $hasActionSpecified= true;
            return;
        } // if ( $this->m_action= 'edit' ) { // show editor for editing of item

        if ( $this->m_action == 'confirm_delete' ) {   // ask confirmation for confirming of deleting of object
            $this->confirmDeleteRowDataForm( $m_parentControl );
            $hasActionSpecified= true;
            return;
        } // if ( $this->m_action= 'confirm_delete' ) {  // ask confirmation for confirming of deleting of object

        if ( $this->m_action == 'delete' ) {   // ask confirmation for deleting of object
            $this->deleteRowData( $m_parentControl );
            $hasActionSpecified= true;
            return;
        } // if ( $this->m_action= 'delete' ) {  // ask confirmation for deleting of object

        if ( !$hasActionSpecified ) {
            die("Action must be specified for appNSN_WooExtSearchControl->doEditorFlow!");
        }

    } // protected function doEditorFlow($m_parentControl) {

    protected function main()
    {
        $this->clearData();
        //die("-1 AFTER !!");
        $this->readRequestParameters();
    }





    protected function showTemplate($templateFullName= false, $parentControl)
    {
        $m_parentControl= $parentControl;
        require_once($this->m_pluginDir . 'views/admin/header.php');
        require_once($templateFullName);
        require_once($this->m_pluginDir . 'views/admin/footer.php');
    }
    protected function beforeRowDataSave($editorItem)
    {
        if ( $this->m_is_trimDataBeforeSave ) {
            foreach( $this->m_POSTDataArray as $nextKey=>$nextValue ) {
                $this->m_POSTDataArray[$nextKey]= trim($nextValue);
            }
        }
    }

    protected function setLangUpdate( $preparedUpdateDate, $dataArray, $fieldName ) {
        return $preparedUpdateDate;
        if (!is_array($preparedUpdateDate)) $preparedUpdateDate= array();
        if ( !class_exists("nsnClass_appLangs") ) return false;
        $languagesList=  nsnClass_appLangs::getLanguagesList();
        //echo '<pre>$languagesList::'.print_r($languagesList,true).'</pre>';
        $lang_fieldsArray= array();
        foreach( $dataArray as $nextKey=>$nextValue ) {
            $lang= nsnClass_appFuncs::getRSepr($nextKey,$fieldName.'_');
            //echo '<pre>$lang::'.print_r($lang,true).'</pre>';
            if ( in_array( $lang, $languagesList) ) {
                $lang_fieldsArray[$lang]= $nextValue;
            }
        }
        $preparedUpdateDate['lang_fields']= $lang_fieldsArray;
        return $preparedUpdateDate;
    }

    abstract protected function showRowsListDataForm( $parentControl );
    abstract protected function initFieldsAddRow( $parentControl );
    abstract protected function addRowDataForm( $parentControl );
    abstract protected function editRowDataForm( $parentControl );

    abstract protected function doBulkAction( $action );
    abstract protected function saveRowData( $action, $editorItem );
    abstract protected function deleteRowData();
    abstract protected function readRequestParameters();

    protected function doValidation($data)
    {
        $data= nsnClass_appFuncs::getTrimmedArray($data);
        //echo '<pre>$data::'.print_r($data,true).'</pre>';
        $dataTypeValidationFormat= '';//getAppVar('dataTypeValidationFormat');
        // echo '<pre>$this->m_validationRules::'.print_r($this->m_validationRules,true).'</pre>';
        // echo '<pre>$dataTypeValidationFormat::'.print_r($dataTypeValidationFormat,true).'</pre>';
        $errorsArray= array();
        foreach( $this->m_validationRules as $rule=>$rulesFields ) {
            $rule= trim($rule);
            // echo '<pre>$rule::'.print_r($rule,true).'</pre>';
            if ( strtolower($rule) == 'required' ) {
                $rulesFieldsArray = preg_split('/,/', $rulesFields);
                foreach ($rulesFieldsArray as $nextRuleField) {
                    $nextRuleField= trim($nextRuleField);
                    if ( empty($nextRuleField) ) continue;
                    if (empty($data[$nextRuleField]) /*and empty($errorsArray[$nextRuleField])*/ ) {
                        $errorsArray[$nextRuleField]= array('field_name'=>$nextRuleField, 'error_label'=> esc_html__('Required'));
                    }
                }
            } // if ( strtolower($rule) == 'required' ) {

            if ( strtolower($rule) == 'name' /* and empty($errorsArray[$nextRuleField]) */ ) {
                $nameValidationFormat= isset($dataTypeValidationFormat['name']) ? $dataTypeValidationFormat['name'] : "/^[a-zA-Z ]*$/";
                $rulesFieldsArray = preg_split('/,/', $rulesFields);
                foreach ($rulesFieldsArray as $nextRuleField) {
                    $nextRuleField= trim($nextRuleField);
                    if ( empty($nextRuleField) ) continue;
                    if ( !preg_match($nameValidationFormat,$data[$nextRuleField]) and empty($errorsArray[$nextRuleField]) ) {
                        $errorsArray[$nextRuleField]= array('field_name'=>$nextRuleField, 'error_label'=> esc_html__('Must be valid name'));
                    }
                }
            } // if ( strtolower($rule) == 'name' ) {


            if ( strtolower($rule) == 'integer' /* and empty($errorsArray[$nextRuleField])*/ ) {
                $integerValidationFormat= isset($dataTypeValidationFormat['integer']) ? $dataTypeValidationFormat['integer'] : '/^[-]?\d+$/';
                //$integerValidationFormat= isset($dataTypeValidationFormat['integer']) ? $dataTypeValidationFormat['integer'] : '/^[-]?\d+$/';
                //$integerValidationFormat= '/^[-]?\d+$/';
                $rulesFieldsArray = preg_split('/,/', $rulesFields);
                foreach ($rulesFieldsArray as $nextRuleField) {
                    $nextRuleField= trim($nextRuleField);
                    if ( empty($nextRuleField) ) continue;
                    if (empty($data[$nextRuleField])) continue;
                    if ( !preg_match( $integerValidationFormat,$data[$nextRuleField]) and empty($errorsArray[$nextRuleField]) ) {
                        $errorsArray[$nextRuleField]= array('field_name'=>$nextRuleField, 'error_label'=> esc_html__('Must be valid integer'));
                        //echo '<pre>$integerValidationFormat::'.print_r($integerValidationFormat,true).'</pre>';
                        //echo '<pre>$data[$nextRuleField]::'.print_r($data[$nextRuleField],true).'</pre>';
                        //echo '<pre>$nextRuleField::'.print_r($nextRuleField,true).'</pre>';
                        //die("-1 JJ");
                    }
                }
            } // if ( strtolower($rule) == 'integer' ) {

            if ( strtolower($rule) == 'float' /* and empty($errorsArray[$nextRuleField]) */ ) {
                $floatValidationFormat= isset($dataTypeValidationFormat['float']) ? $dataTypeValidationFormat['float'] : '/^[+\-]?(?:\d+(?:\.\d*)?|\.\d+)$/';
                $rulesFieldsArray = preg_split('/,/', $rulesFields);
                foreach ($rulesFieldsArray as $nextRuleField) {
                    $nextRuleField= trim($nextRuleField);
                    if ( empty($nextRuleField) ) continue;
                    if ( !preg_match($floatValidationFormat,$data[$nextRuleField]) and empty($errorsArray[$nextRuleField]) ) {
                        $errorsArray[$nextRuleField]= array('field_name'=>$nextRuleField, 'error_label'=> esc_html__('Must be valid float'));
                    }
                }
            } // if ( strtolower($rule) == 'float' ) {

            if ( strtolower($rule) == 'not_negative' /* and empty($errorsArray[$nextRuleField]) */ ) {
                $rulesFieldsArray = preg_split('/,/', $rulesFields);
                foreach ($rulesFieldsArray as $nextRuleField) {
                    $nextRuleField= trim($nextRuleField);
                    if ( empty($nextRuleField) ) continue;
                    $val= trim($data[$nextRuleField]);
                    if (substr($val,0,1)=='-') {
                        $errorsArray[$nextRuleField]= array('field_name'=>$nextRuleField, 'error_label'=> esc_html__('Can not be negative'));
                    }
                }
            } // if ( strtolower($rule) == 'not_negative' ) {

            // echo '<pre>-1 $errorsArray::'.print_r($errorsArray,true).'</pre>';
            if ( strtolower($rule) == 'email' /*and empty($errorsArray[$nextRuleField])*/ ) {
                // wordpress is_email
                $rulesFieldsArray = preg_split('/,/', $rulesFields);
                // echo '<pre>$rulesFieldsArray::'.print_r($rulesFieldsArray,true).'</pre>';
                foreach ($rulesFieldsArray as $nextRuleField) {
                    $nextRuleField= trim($nextRuleField);
                    if ( empty($nextRuleField) ) continue;
                    if ( !is_email($data[$nextRuleField]) and empty($errorsArray[$nextRuleField]) ) {
                        $errorsArray[$nextRuleField]= array('field_name'=>$nextRuleField, 'error_label'=> esc_html__('Must be valid email'));
                    }
                }

                /*$emailValidationFormat= isset($dataTypeValidationFormat['email']) ? $dataTypeValidationFormat['email'] : "/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/";
                //echo '<pre>$emailValidationFormat::'.print_r($emailValidationFormat,true).'</pre>';
                //die("-1BREAK");
                $rulesFieldsArray = preg_split('/,/', $rulesFields);
                // echo '<pre>$rulesFieldsArray::'.print_r($rulesFieldsArray,true).'</pre>';
                foreach ($rulesFieldsArray as $nextRuleField) {
                    $nextRuleField= trim($nextRuleField);
                    if ( empty($nextRuleField) ) continue;
                    if ( !preg_match( $emailValidationFormat,$data[$nextRuleField]) and empty($errorsArray[$nextRuleField]) ) {
                        $errorsArray[$nextRuleField]= array('field_name'=>$nextRuleField, 'error_label'=> esc_html__('Must be valid email'));
                    }
                }  */
            } // if ( strtolower($rule) == 'email' ) {

            if ( strtolower($rule) == 'website' /* and empty($errorsArray[$nextRuleField]) */ ) {
                $websiteValidationFormat= isset($dataTypeValidationFormat['website']) ? $dataTypeValidationFormat['website'] : "/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i";
                $rulesFieldsArray = preg_split('/,/', $rulesFields);
                foreach ($rulesFieldsArray as $nextRuleField) {
                    $nextRuleField= trim($nextRuleField);
                    if ( empty($nextRuleField) ) continue;
                    if ( !preg_match( $websiteValidationFormat,$data[$nextRuleField]) and empty($errorsArray[$nextRuleField]) ) {
                        $errorsArray[$nextRuleField]= array('field_name'=>$nextRuleField, 'error_label'=> esc_html__('Must be valid website'));
                    }
                }
            } // if ( strtolower($rule) == 'website' ) {

            if ( strtolower($rule) == 'date' /* and empty($errorsArray[$nextRuleField]) */ ) {
                $dateValidationFormat= isset($dataTypeValidationFormat['date']) ? $dataTypeValidationFormat['date'] : '/^[-]?\d+$/';
                $rulesFieldsArray = preg_split('/,/', $rulesFields);
                foreach ($rulesFieldsArray as $nextRuleField) {
                    /*echo '<pre>$data[$nextRuleField]::'.print_r($data[$nextRuleField],true).'</pre>';
                    echo '<pre>-Z $nextRuleField::'.print_r($nextRuleField,true).'</pre>';
                    echo '<pre>-ZZ $errorsArray::'.print_r($errorsArray,true).'</pre>';
                    echo '<pre>---ZZZ::'.print_r(( empty($errorsArray[$nextRuleField]) ),true).'</pre>';  */
                    $nextRuleField= trim($nextRuleField);
                    if ( empty($nextRuleField) ) continue;
                    if ( !preg_match( $dateValidationFormat,$data[$nextRuleField]) and empty($errorsArray[$nextRuleField]) ) {
                        $errorsArray[$nextRuleField]= array('field_name'=>$nextRuleField, 'error_label'=> esc_html__('Must be valid date'));
                    }
                }
            } // if ( strtolower($rule) == 'date' ) {

            // 'email'=>'email', 'website'=> 'site', 'date'=>'birthday', 'registered_at'=> 'registered_at'
        }
        //echo '<pre>END $errorsArray::'.print_r($errorsArray,true).'</pre>';
        //die("-1DDD");
        return $errorsArray;
    } // protected function doValidation($data)

    protected function hasItemChecked($dataArray, $keyName) {
        foreach ( $dataArray as $nextKey=>$nextValue) {
            $A= preg_split('/'.$keyName.'/', $nextKey);
            if ( count($A)  == 2 and strlen($A[0]) == 0 ) return true;
        }
        return false;
    } // protected function hasItemChecked($dataArray, $keyName) {


    protected function setItemsChecked($checkedItemsList, $dataArray, $cbxFieldName, $keyIdName, $keyIdCheckedName) {
        foreach ( $dataArray as $nextKey=>$nextValue) {
            $A= preg_split('/'.$cbxFieldName.'/', $nextKey);
            if ( count($A)  == 2 ) {
                foreach ($checkedItemsList as $nextKey=>$nextItem ) {
                    if ($nextItem[ $keyIdName /*'music_jenre_id' */] == $A[1]) {
                        $checkedItemsList[$nextKey][ $keyIdCheckedName /*'music_jenre_is_checked'*/ ]= 1;
                    }
                }

            }
        }
        return $checkedItemsList;
    } // protected function hasItemChecked($dataArray, $keyName) {

    protected function setAppParameters()
    {
        global $wp_session;
        $app_vars = ! empty( $wp_session['app_vars'] ) ? $wp_session['app_vars'] : array();
        if ( !is_array($app_vars) ) $app_vars= array();
        //$app_vars['pageUrl']= $this->m_pageUrl;
        //echo '<pre>$this->m_containerClass::'.print_r($this->m_containerClass,true).'</pre>';
        //die("-1");
        $plugin_url= (isset($this->m_containerClass) ? $this->m_containerClass->getPluginUrl() : null);
        $plugin_dir= (isset($this->m_containerClass) ? $this->m_containerClass->getPluginDir() : null);
        $app_vars['plugin_url']= $plugin_url;
        $app_vars['plugin_dir']= $plugin_dir;

        $app_vars['dataTypeValidationFormat']= array('name'=> "/^[a-zA-Z0-9 #]*$/", 'integer'=>'/^[-]?\d+$/', 'float'=>'/^[+\-]?(?:\d+(?:\.\d*)?|\.\d+)$/', 'email'=>"/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/", 'website'=> "/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",
            'date'=>"/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", 'datetime'=> "/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/" );  // 1957-07-11



        $wp_session['app_vars'] = $app_vars;
    }

    protected function checkInstalledPlugins() {
        $dependOnActivePlugins= 1;//getAppVar('dependOnActivePlugins');
        if (isset($dependOnActivePlugins) and is_array($dependOnActivePlugins)) {
            foreach ($dependOnActivePlugins as $nextDependOnActivePlugin) {
                if( !is_plugin_active( $nextDependOnActivePlugin ) ) {
                    //die("Plugin $nextDependOnActivePlugin must be installed ans activated!");
                    $this->setFlashMessage( esc_html__("Plugin $nextDependOnActivePlugin must be installed ans activated!"  )  );
                }
            }
        }
    }

    //abstract public function main();

    abstract protected function preparePageParameters($WithPage, $WithSort);

    protected function getParameter($ParameterName, $DefaultValue = '', $parameter_splitter = '')
    {
        // DebToFile('-10 getParameter  $this->m_POSTDataArray::' .print_r($this->m_POSTDataArray,true) );
        /*echo '<pre>$_GET::'.print_r($_GET,true).'</pre>';
        echo '<pre>$_POST::'.print_r($_POST,true).'</pre>';  */
//        DebToFile('-8 getParameter  $ParameterName::' .print_r($ParameterName,true) );
        $is_debug= false;//$ParameterName=='categories_list';
        if ( $is_debug ) DebToFile('-8 getParameter  $_POST::' .print_r($_POST,true) );
        if ( $is_debug ) DebToFile('-9 getParameter  $_GET::' .print_r($_GET,true) );
        if ( $is_debug ) DebToFile('-10 getParameter  $this->m_POSTDataArray::' .print_r($this->m_POSTDataArray,true) );
        if ( $is_debug ) DebToFile('-10 getParameter  $this->m_GETDataArray::' .print_r($this->m_GETDataArray,true) );
        if (!empty($this->m_POSTDataArray)) { // form was submitted
            $ParameterValue = isset($this->m_POSTDataArray[$ParameterName]) ? $this->m_POSTDataArray[$ParameterName] : $DefaultValue;
        } else {
            $ParameterValue = isset($this->m_GETDataArray[$ParameterName]) ? $this->m_GETDataArray[$ParameterName] : $DefaultValue;
        }
        if ( $is_debug ) DebToFile('-10 getParameter  $ParameterName::' .print_r($ParameterName,true) );
        if ( $is_debug ) DebToFile('-11 getParameter  $ParameterValue::' .print_r($ParameterValue,true) );

        if (is_array($ParameterValue)) return $ParameterValue;
        if (!empty($parameter_splitter)) {
            $A = preg_split('/' . $parameter_splitter . '/', (is_string($ParameterValue) ? urldecode($ParameterValue) : $ParameterValue));
            $ResArray = array();
            foreach ($A as $val) {
                if (isset($val)) $ResArray[] = urldecode($val);
            }
            return $ResArray;
        }
        return urldecode($ParameterValue);
    }

    protected function setFlashMessage( $str )
    {
        if (empty($_SESSION['flash_message'])) {
            $_SESSION['flash_message']=  $str;

        } else {
            if ($_SESSION['flash_message'] != $str) {
                $_SESSION['flash_message'] = $_SESSION['flash_message'] . '<br>' . $str;
            }
        }
    }

    protected function getFlashMessage( $defaultValue= '', $clearValue= true )
    {
        if (empty($_SESSION['flash_message'])) return $defaultValue;
        //echo '<pre>--ZZ $_SESSION::'.print_r($_SESSION,true).'</pre>';
        $retValue=  $_SESSION['flash_message'];
        if ( $clearValue ) {
            unset($_SESSION['flash_message']);
        }
        return $retValue;
    }

    protected function makePageNavigation( $paged, $posts_per_page, $max_page, $before = '', $after = '', $echo = true, $jsFuncCoe= '', $showDiv= false, $page_url= '', $classes= array() ) {
        require_once( 'lib/pagenavigation.php');
        return drawNSN_WooExtPageNavigation( $paged, $posts_per_page, $max_page, $before, $after, $echo, $jsFuncCoe, $showDiv, ( !empty($page_url) ? $page_url : $this->m_pageUrl ), $classes );

    }  // protected function makePageNavigation( $paged, $posts_per_page, $max_page, $before = '', $after = '', $echo = true, $jsFuncCoe= '', $showDiv= false ) {




    /*@mysql_query("BEGIN", $wpdb->dbh);

    // Do some expensive/related queries here
    $wpdb->query("DELETE FROM table WHERE form_id = '1' ");
    $wpdb->query("DELETE FROM data WHERE form_id = '1' ");

    if ($error) {
        // Error occured, don't save any changes
        @mysql_query("ROLLBACK", $wpdb->dbh);
    } else {
       // All ok, save the changes
       @mysql_query("COMMIT", $wpdb->dbh);*/

}