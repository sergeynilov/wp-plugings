//alert("::common-funcs.js::")

//showFadingMessage( data.photosCount + " photo(s) were loaded!" /*, "div_fading_dialog", "span_fading_dialog" */);

//function convert_attribute_name( attr_name ) {
//    return attr_name.replace( / /g, '_' ).replace( /%/g, '_' ).replace( /./g, '_' )
//}

function objectToArray(array_data) {
    var locationsList= [];
    var i= 0;
    var array = $.map(array_data, function (value, index) {
        locationsList[i]= value
        i++
    });
    return locationsList;
}


function GetBrowserType() {
    //    alert(navigator.appName);
    //if ( navigator.appName.indexOf("Microsoft") != -1 ) {
    //    return 'MSIE';
    //}
    //if ( navigator.appName.indexOf("Opera") != -1 ) {
    //    return 'Opera';
    //}
    //if ( navigator.appName.indexOf("Netscape") != -1 ) {
    //    return 'Netscape';
    //}
    //
    var sBrowser, sUsrAg = navigator.userAgent;
    //alert( "sUsrAg::"+var_dump(sUsrAg) )
    if(sUsrAg.indexOf("Chrome") > -1) {
        sBrowser = "Chrome";
    } else if (sUsrAg.indexOf("Safari") > -1) {
        sBrowser = "Safari";
    } else if (sUsrAg.indexOf("Opera") > -1) {
        sBrowser = "Opera";
    } else if (sUsrAg.indexOf("Firefox") > -1) {
        sBrowser = "Firefox";
    } else if (sUsrAg.indexOf("MSIE") > -1) {
        sBrowser = "MSIE";
    }
    return sBrowser;
}
/*var sBrowser, sUsrAg = navigator.userAgent;

 if(sUsrAg.indexOf("Chrome") > -1) {
 sBrowser = "Google Chrome";
 } else if (sUsrAg.indexOf("Safari") > -1) {
 sBrowser = "Apple Safari";
 } else if (sUsrAg.indexOf("Opera") > -1) {
 sBrowser = "Opera";
 } else if (sUsrAg.indexOf("Firefox") > -1) {
 sBrowser = "Mozilla Firefox";
 } else if (sUsrAg.indexOf("MSIE") > -1) {
 sBrowser = "Microsoft Internet Explorer";
 }
 */

function GetShowTRMethod() {
    var BrowserType= GetBrowserType();
    //alert("BrowserType:"+BrowserType);
    if ( BrowserType== 'MSIE' ) return 'inline';
    if ( BrowserType== 'Opera' ) return 'table-row';
    if ( BrowserType== 'Netscape' ) return 'table-row';
    return 'table-row';
}

function GetShowCellMethod() {
    var BrowserType= GetBrowserType();
    //alert("BrowserType:"+BrowserType);
    if ( BrowserType== 'MSIE' || BrowserType== 'Safari' ) return 'inline';
    if ( BrowserType== 'Opera' ) return 'table-cell';
    if ( BrowserType== 'Chrome' || BrowserType== 'Firefox' ) return 'table-cell';
    return '';
}

function getLimitedValue(value, limitedValuesArray, defaultValue) {
    if ( typeof limitedValuesArray != "object" ) return value;
    if ( typeof limitedValuesArray.length != "number" ) return value;
    for(i = 0; i< limitedValuesArray.length; i++ ) {
        if (limitedValuesArray[i] == value) return value;
    }
    return defaultValue;
}

function get_splitted(str, splitter, index) { //  get_splitted(next_field.value,'=',0)
    var value_arr = str.split( splitter );
    if (typeof index == "undefined") {
        return value_arr;
    }
    if (typeof value_arr[index] != "undefined" ) {
        return value_arr[index];
    }
    return '';
}

function nl2br2(str) {
    //return preg_replace('/\r\n|\r|\n/', '<br>', $string);
    return str.replace(/\r\n|\r|\n/g, ' <br> ');
}

function showFadingMessage ( message, div_id, span_id ) {
    return;
    //alert( "showFadingMessage message::"+var_dump(message) )
    if ( typeof div_id == "undefined") div_id= "div_fading_dialog";
    if ( typeof span_id == "undefined") span_id= "span_fading_dialog";
    $("#"+span_id).html(message);
    $("#"+div_id).dialog({
        autoOpen: false,
        draggable: false,
        resizable: false,
        show: {
            effect: 'fade',
            duration: 2000
        },
        hide: {
            effect: 'fade',
            duration: 2000
        },
        open: function(){
            $(this).dialog('close');
        },
        close: function(){
            $(this).dialog('destroy');
        }
    });

    $(".ui-dialog-titlebar").remove();

    $("#"+div_id).dialog("open");
    //});
} // function showFadingMessage (div_id, span_id, message) {



function stripSlashes2(str) {
    //       discuss at: http://phpjs.org/functions/stripslashes/
    //      original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    //      improved by: Ates Goral (http://magnetiq.com)
    //      improved by: marrtins
    //      improved by: rezna
    //         fixed by: Mick@el
    //      bugfixed by: Onno Marsman
    //      bugfixed by: Brett Zamir (http://brett-zamir.me)
    //         input by: Rick Waldron
    //         input by: Brant Messenger (http://www.brantmessenger.com/)
    // reimplemented by: Brett Zamir (http://brett-zamir.me)
    //        example 1: stripslashes('Kevin\'s code');
    //        returns 1: "Kevin's code"
    //        example 2: stripslashes('Kevin\\\'s code');
    //        returns 2: "Kevin\'s code"

    return (str + '')
        .replace(/\\(.?)/g, function(s, n1) {
            switch (n1) {
                case '\\':
                    return '\\';
                case '0':
                    return '\u0000';
                case '':
                    return '';
                default:
                    return n1;
            }
        });
}

function addSlashes(str) {
    //  discuss at: http://phpjs.org/functions/addslashes/
    // original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // improved by: Ates Goral (http://magnetiq.com)
    // improved by: marrtins
    // improved by: Nate
    // improved by: Onno Marsman
    // improved by: Brett Zamir (http://brett-zamir.me)
    // improved by: Oskar Larsson Högfeldt (http://oskar-lh.name/)
    //    input by: Denny Wardhana
    //   example 1: addslashes("kevin's birthday");
    //   returns 1: "kevin\\'s birthday"

    return (str + '')
        .replace(/[\\"']/g, '\\$&')
         .replace(/\u0000/g, '\\0');
}

var MAX_DUMP_DEPTH = 20;
function dumpObj(obj, name, indent, depth) {
    if (depth > MAX_DUMP_DEPTH) {
        return indent + name + ": <Maximum Depth Reached>\n";
    }
    if (typeof obj == "object") {
        var child = null;
        var output = indent + name + "\n";
        indent += "\t";
        for (var item in obj)
        {
            try {
                child = obj[item];
            } catch (e) {
                child = "<Unable to Evaluate>";
            }
            if (typeof child == "object") {
                output += dumpObj(child, item, indent, depth + 1);
            } else {
                output += indent + item + ": " + child + "\n";
            }
        }
        return output;
    } else {
        return obj;
    }
}



function setMemoTextToMultyLine(str) {
    return str.replace(/\n/g, '<br>')
}

function var_dump(oElem, from_line, till_line) {
    var sStr = '';
    if (typeof(oElem) == 'string' || typeof(oElem) == 'number')     {
        sStr = oElem;
    } else {
        var sValue = '';
        for (var oItem in oElem) {
            sValue = oElem[oItem];
            if (typeof(oElem) == 'innerHTML' || typeof(oElem) == 'outerHTML') {
                sValue = sValue.replace(/</g, '&lt;').replace(/>/g, '&gt;');
            }
            sStr += 'obj.' + oItem + ' = ' + sValue + '\n';
        }
    }
    //alert( "from_line::"+(typeof from_line) )
    if ( typeof from_line == "number" && typeof till_line == "number" ) {
        return sStr.substr( from_line, till_line );
    }
    if ( typeof from_line == "number" ) {
        return sStr.substr( from_line );
    }
    return sStr;
}


function getDaysInMonth( Year, Month ) {
    switch ( parseInt(Month) ) {
        case 1:
            return 31;
        case 2:
            if ( Year % 4 == 0 )
                return 29; // leap year
            else
                return 28;
        case 3:
            return 31;
        case 4:
            return 30;
        case 5:
            return 31;
        case 6:
            return 30;
        case 7:
            return 31;
        case 8:
            return 31;
        case 9:
            return 30;
        case 10:
            return 31;
        case 11:
            return 30;
        case 12:
            return 31;
    }
    return 0;
}

function ClearDDLBItems(FieldName, ClearFirstElement) {
    var ddlbObj = document.getElementById(FieldName);
    var L = ddlbObj.length;
    for (I = L - 1; I >= 1; I--) {
        ddlbObj.remove(I);
    }
    if (ClearFirstElement) ddlbObj.remove(0);
}

function AddDDLBItem(FieldName, id, text) {
    var ddlbObj = document.getElementById(FieldName);
    var OptObj = document.createElement("OPTION");
    OptObj.value = id;
    OptObj.text = text;
    ddlbObj.options.add(OptObj);
    return OptObj;
}

function SetDDLBActiveItem(FieldName, Value) {
    var ddlbObj = document.getElementById(FieldName);
    for (I = 0; I < ddlbObj.options.length; I++) {
        if (ddlbObj.options[I].value == Value) {
            ddlbObj.options[I].selected = true;
            return;
        }
    }
}

function FillDdlbValue(FieldName, id, text, AddEmpty) {
    var ddlbObj = document.getElementById(FieldName);
    for (I = 0; I < ddlbObj.options.length; I++) {
        if (ddlbObj.options[I].value == id) {
            ddlbObj.options[I].selected = true;
            return;
        }
    }
    AddDDLBItem(FieldName, id, text)
    SetDDLBActiveItem(FieldName, id)
}

function ClearDDLBActiveItem(FieldName) {
    var ddlbObj = document.getElementById(FieldName);
    //  alert("ddlbObj.options.length::"+ddlbObj.options.length);
    for (I = 0; I < ddlbObj.options.length; I++) {
        ddlbObj.options[I].selected = false;
    }
}

function isDifferentDay(dat1, dat2) {
    if ( !(dat1 instanceof Date) || !(dat2 instanceof Date) ) return false;
    if ( dat1.getFullYear() == dat2.getFullYear() && dat1.getMonth() == dat2.getMonth() && dat1.getDate() == dat2.getDate() ) {
        return false;
    }
    return true;
}


//var date = new Date(); //AppointmentDate or date string in case of value retrieved from dom
//var dateTime = setTimeToDate(date, 1, 2, 3);//pass h=m=s=0 for date part only
function addStringTimeToDate(date, stringTime) {
    var newDate;
    if(isNaN(Date.parse(date))) {//instanceof Date = false if you read the value from DOM(i.e string)
        newDate = new Date();
    } else {
        newDate = new Date(date);
    }

    var h= getTimeParamater( stringTime, 'hour', ':' )
    var m= getTimeParamater( stringTime, 'minute', ':' )

    //alert( "newDate::"+newDate.toISOString() + "  h:" + h + "  m:" + m )
    if(!isNaN(h)) {
        newDate.addHours(h);
    }
    if(!isNaN(m) !== NaN) {
        newDate.addMinutes(m);
    }
    //alert( "--newDate::"+newDate.toISOString() + "  h:" + h + "  m:" + m )
    return newDate;
}


//var date = new Date(); //AppointmentDate or date string in case of value retrieved from dom
//var dateTime = setTimeToDate(date, 1, 2, 3);//pass h=m=s=0 for date part only
function addTimeToDate(date, h, m, s) {
    var newDate;
    if(isNaN(Date.parse(date))) {//instanceof Date = false if you read the value from DOM(i.e string)
        newDate = new Date();
    } else {
        newDate = new Date(date);
    }
    //alert( " addTimeToDate newDate::"+newDate.toISOString() + "  h:" + h + "  m:" + m )
    if(!isNaN(h)) {
        newDate.addHours(h);
    }
    if(!isNaN(m) !== NaN) {
        newDate.addMinutes(m);
    }
    if(!isNaN(s) !== NaN) {
        newDate.addSeconds(s);
    }
    //alert( " addTimeToDate --newDate::"+newDate.toISOString() + "  h:" + h + "  m:" + m )
    return newDate;
}


function JSDateToMysql(dat) {
    if(isNaN(Date.parse(dat))) return null;
    return dat.getFullYear() + "-" + padLeft(dat.getMonth() + 1,2,'0') + "-" + padLeft(dat.getDate(),2,'0') + " " +  padLeft(dat.getHours(),2,'0') + ":" + padLeft(dat.getMinutes(),2,'0') + ":" + padLeft(dat.getSeconds(),2,'0');
}

function compareDates(dat1, dat2) {
    dat1.setHours(0);
    dat1.setMinutes(0);
    dat1.setSeconds(0);
    dat1.setMilliseconds(0)
    dat2.setHours(0);
    dat2.setMinutes(0);
    dat2.setSeconds(0);
    dat2.setMilliseconds(0)
    //alert( "compareDates dat1::"+dat1+"   dat2::"+dat2 )
    if ( dat1 == dat2 ) return 0;
    if ( dat1 > dat2 ) return 1;
    if ( dat1 < dat2 ) return -1;
}

function padLeft(nr, n, str){
    return Array(n-String(nr).length+1).join(str||'0')+nr;
}

function stripSlashes(str){
    return str.replace(/\\/g, '');
}

function formatDate(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return date.getMonth()+1 + "/" + date.getDate() + "/" + date.getFullYear() + " " + strTime;
}

function getFormattedTimeOfDate(dat) {
    // alert( "Z1 getFormattedTimeOfDate  dat::"+dat )
    if(isNaN(Date.parse(dat))) return '00:00';//instanceof Date = false if you read the value from DOM(i.e string)
    // alert( "Z2 dat.getUTCHours()::"+dat.getUTCHours())
    return padLeft(dat.getHours(),2,'0') + ':' + padLeft(dat.getMinutes(),2,'0'); // http://stackoverflow.com/questions/5366849/convert-1-to-0001-in-javascript
    //return padLeft(dat.getUTCHours(),2,'0') + ':' + padLeft(dat.getMinutes(),2,'0'); // http://stackoverflow.com/questions/5366849/convert-1-to-0001-in-javascript
} //

var _MS_PER_DAY = 1000 * 60 * 60 * 24;
function dateDiffInDays(a, b) { // http://stackoverflow.com/questions/3224834/get-difference-between-2-dates-in-javascript
    // Discard the time and time-zone information.
    var utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
    var utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());
    var daysCount = Math.floor((utc2 - utc1) / _MS_PER_DAY)
    //alert( "utc1::"+ utc1 + "  utc2::"+ utc2 + "  daysCount::"+daysCount)
    return daysCount;
}


function capitalize(s, replaceUnderline) { // capitalize('this IS THE wOrst string eVeR');
    if ( typeof s == "undefined" ) return "";
    if ( replaceUnderline ) {
        s =  s.replace(/_/g, ' ');
    }
    return s.toLowerCase().replace( /\b./g, function(a){ return a.toUpperCase(); } );
};

/* var now = new Date();
 console.log(now.addMinutes(50)) */

Date.prototype.addSeconds = function(seconds) {
    this.setSeconds(this.getSeconds() + seconds);
    return this;
};

Date.prototype.addMinutes = function(minutes) {
    this.setMinutes(this.getMinutes() + minutes);
    return this;
};

Date.prototype.addHours = function(hours) {
    this.setHours(this.getHours() + hours);
    return this;
};

Date.prototype.addDays = function(days) {
    this.setDate(this.getDate() + days);
    return this;
};

Date.prototype.addWeeks = function(weeks) {
    this.addDays(weeks*7);
    return this;
};

Date.prototype.addMonths = function (months) {
    var dt = this.getDate();

    this.setMonth(this.getMonth() + months);
    var currDt = this.getDate();

    if (dt !== currDt) {
        this.addDays(-currDt);
    }

    return this;
};

Date.prototype.addYears = function(years) {
    var dt = this.getDate();

    this.setFullYear(this.getFullYear() + years);

    var currDt = this.getDate();

    if (dt !== currDt) {
        this.addDays(-currDt);
    }

    return this;
};

function formatMoney(val) {
    return parseFloat(val).toFixed(2)
}

function isInt(value) {
    var er = /^-?[0-9]+$/;
    return er.test(value);
}

function isFloat(value) {
    var er = /^\d+\.\d+$/;
    return er.test(value);
}



// parseFloat(Low).toFixed(..);

function editTranslation(label) {
    // alert( "editTranslation label::"+label)
    $.ajax({
        timeout: 10000,
        url: $ajaxurl + "/editTranslationForm/",
        type: "POST",
        dataType: "json",
        data: { "label": label }
    }).done(function (data) {
        if (data.error) {
            err(data.error, "login");
        } else if (data.html) {
            $("#popupwnd div.popupData").html(data.html);
            $("#popupwnd").arcticmodal();
        } else {
            alert("Please try again later.");
        }
    }).error(function (data) {
        alert("Please try again later.");
    });

}

function convertStrToDateByFormat(str, dateFormat, dateSplitter ) {
    //alert( "convertStrToDateByFormat  str::"+var_dump(str)+"  dateFormat::"+var_dump(dateFormat)+ "  dateSplitter::"+var_dump(dateSplitter))
    var valueArr = str.split( dateSplitter );
    var dateFormatArr = dateFormat.split( dateSplitter );
    var L= valueArr.length
    day= ''; month= ''; year= '';
    for (I = 0; I < L; I++) {
        if ( dateFormatArr[I].toLowerCase() == 'dd' ) {
            day= valueArr[I];
        }
        if ( dateFormatArr[I].toLowerCase() == 'yy' ) {
            year= valueArr[I];
        }
        if ( dateFormatArr[I].toLowerCase() == 'mm' ) {
            month= valueArr[I];
        }

    }
    var retDay = new Date();
    retDay.setYear(parseInt(year));
    retDay.setMonth(parseInt(month)-1);
    retDay.setDate(parseInt(day));
    // alert( "retDay::"+retDay)
    return retDay;
}

// return an array of date objects for start (monday)
// and end (sunday) of week based on supplied
// date object or current date
function startAndEndOfWeek(date) {

    // If no date object supplied, use current date
    // Copy date so don't modify supplied date
    var now = date? new Date(date) : new Date();

    // set time to some convenient value
    now.setHours(0,0,0,0);

    // Get the previous Monday
    var monday = new Date(now);
    monday.setDate(monday.getDate() - monday.getDay() + 1);

    // Get next Sunday
    var sunday = new Date(now);
    sunday.setDate(sunday.getDate() - sunday.getDay() + 7);

    // Return array of date objects
    return [monday, sunday];
}
// Mon Nov 12 2012 00:00:00
// Sun Nov 18 2012 00:00:00
// alert(startAndEndOfWeek(new Date(2012,10,14)).join('\n'));


function convertJSDateTimeToUnix(dat ) {
//	alert( "dat::"+dat)
//	alert( "dat::"+( typeof dat) )
    if(isNaN(Date.parse(dat))) return null;
    var ret= dat.getTime() / 1000
//	alert( "ret::"+ret)
    return ret;
}

