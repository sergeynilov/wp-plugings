//alert( 'backend-scripts.js!' );   // backendFuncs

var this_m_plugin_url = ''
var this_m_plugin_dir = ''
var this_m_plugin_name = ''
var this_m_logged_user_id = ''
var this_m_pageUrl = ''
var this_m_datePickerSelectionFormat = ''

function nsn_woo_ext_search_backendFuncs(Params) {
    this_m_plugin_url = Params.m_plugin_url;
    this_m_plugin_dir = Params.m_plugin_dir;
    this_m_plugin_name = Params.m_plugin_name;
    this_m_logged_user_id = Params.m_logged_user_id;
    this_m_page_url = Params.m_page_url;
    this_m_datePickerSelectionFormat = Params.m_datePickerSelectionFormat;
    //alert( "nsn_woo_ext_search_backendFuncs  this_logged_user_id::" + this_m_logged_user_id + "  this_m_page_url::" + this_m_page_url  + "  this_m_plugin_dir::" + this_m_plugin_dir  + "  this_m_plugin_name::" + this_m_plugin_name )
}

/*backendArtistsEditorFuncs.prototype.onSubmit = function (isReopen) {   // submit form.If isReopen= 1 - editor will be reopened.
 var theForm = $("#form_artist_editor");
 $("#is_reopen").val(isReopen)
 theForm.submit();

 } // backendArtistsEditorFuncs.prototype.onSubmit= function(isReopen) {  // submit form.If isReopen= 1 - editor will be reopened.
 */
nsn_woo_ext_search_backendFuncs.prototype.onInit= function () {
    //alert( "onInit::"+var_dump(1) )

}

nsn_woo_ext_search_backendFuncs.prototype.show_attribute_in_extended_search_prices_box_onChange= function ( attribute_name ) {
    var prices_box= $("#nsn-woo-ext-search-show_attribute_in_extended_search_prices_box").val()
    //alert( "show_attribute_in_extended_search_prices_box_onChange  prices_box::"+prices_box )
    if (prices_box == 'yes') {
        $( "#div_nsn-woo-ext-search-show_attribute_in_extended_search_price_ranges" ).css("display",GetShowCellMethod)
    } else {
        $( "#div_nsn-woo-ext-search-show_attribute_in_extended_search_price_ranges" ).css("display","none")
    }

}
nsn_woo_ext_search_backendFuncs.prototype.show_attribute_in_extended_search_onChange= function ( attribute_name ) {
    var show_attribute_in_extended_search= $("#nsn-woo-ext-search-show_attribute_in_extended_search_"+attribute_name).val()
    //alert( "show_attribute_in_extended_search_onChange  attribute_name::"+var_dump(attribute_name) +"  show_attribute_in_extended_search::"+show_attribute_in_extended_search)
    if ( show_attribute_in_extended_search=="yes" ) {
        //$("#table_attrs_" + attribute_name).css("display","block")
        //alert( "-1 GetShowCellMethod::" )
        $("#td_nsn-woo-ext-search-show_attribute_link_to_post_" + attribute_name).css("display",GetShowCellMethod)
        $("#td_nsn-woo-ext-search-show_attribute_in_post_products_list_" + attribute_name).css("display",GetShowCellMethod)
        $("#nsn-woo-ext-search-show_attribute_link_to_post_" + attribute_name).val("no")
        $("#nsn-woo-ext-search-show_attribute_in_post_products_list_" + attribute_name).val("no")
        //alert( "-2 GetShowCellMethod::" )
    } else {
        //$("#table_attrs_" + attribute_name).css("display","none")
        $("#td_nsn-woo-ext-search-show_attribute_link_to_post_" + attribute_name).css("display","none")
        $("#td_nsn-woo-ext-search-show_attribute_in_post_products_list_" + attribute_name).css("display","none")
        $("#nsn-woo-ext-search-show_attribute_link_to_post_" + attribute_name).val("no")
        $("#nsn-woo-ext-search-show_attribute_in_post_products_list_" + attribute_name).val("no")

    }

}
nsn_woo_ext_search_backendFuncs.prototype.show_attribute_link_to_post_onChange= function ( attribute_name ) {

    var show_attribute_link_to_post= $("#nsn-woo-ext-search-show_attribute_link_to_post_"+attribute_name).val()
    //alert( "show_attribute_link_to_post_onChange attribute_name::"+attribute_name +"  show_attribute_link_to_post::"+show_attribute_link_to_post )

    if ( show_attribute_link_to_post == 'yes' ) {
        $("#table_attrs_" + attribute_name).css("display","block")
        //alert( "-1 GetShowCellMethod::" )
        $("#td_nsn-woo-ext-search-show_attribute_in_post_products_list_" + attribute_name).css("display",GetShowCellMethod)
        $("#nsn-woo-ext-search-show_attribute_in_post_products_list_" + attribute_name).val("no")
        //alert( "-2 GetShowCellMethod::" )
    } else {
        $("#table_attrs_" + attribute_name).css("display","none")
        $("#td_nsn-woo-ext-search-show_attribute_in_post_products_list_" + attribute_name).css("display","none")
        $("#nsn-woo-ext-search-show_attribute_in_post_products_list_" + attribute_name).val("no")
    }

}