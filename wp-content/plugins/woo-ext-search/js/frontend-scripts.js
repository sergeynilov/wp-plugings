//alert("frontend-scripts.js")

var this_m_plugin_url = ''
var this_m_logged_user_id = ''
var this_m_pageUrl = ''
var this_m_plugin_prefix = ''
var this_datePickerSelectionFormat = ''
var this_m_attrs_options_list = ''
var this_m_show_bookmark_alerts = ''
var self_frontendFuncs;

function nsn_woo_ext_search_frontendFuncs(Params, attrs_options_list, show_bookmark_alerts) {
    self_frontendFuncs = this;
    //alert( "  Params::"+var_dump(Params) )
    //alert( "attrs_options_list::"+var_dump(attrs_options_list) )
    this_m_plugin_url= Params.plugin_url;
    this_m_plugin_prefix= Params.plugin_prefix;
    this_m_logged_user_id= Params.logged_user_id;

    //alert( "this_m_logged_user_id::"+this_m_logged_user_id +'  this_m_plugin_prefix::'+this_m_plugin_prefix )
    //alert( "this_m_plugin_url::"+this_m_plugin_url )
    //alert( "self_frontendFuncs::"+var_dump(self_frontendFuncs) )

    this_m_pageUrl = Params.m_pageUrl;
    //alert( "this_m_pageUrl::"+this_m_pageUrl )
    this_datePickerSelectionFormat = Params.datePickerSelectionFormat;
    this_m_attrs_options_list = attrs_options_list;
    this_m_show_bookmark_alerts = show_bookmark_alerts;
    //alert( "this_m_show_bookmark_alerts::"+this_m_show_bookmark_alerts )
}


///////////// PRODUCTS BLOCK START /////////////////

nsn_woo_ext_search_frontendFuncs.prototype.filter_elementClicked = function ( element_type, sub_element_type, element_id ) {
    //alert( "filter_elementClicked element_type::"+element_type + "  sub_element_type::" + sub_element_type + "  element_id::" + element_id )
    //class="{{ plugin_prefix }}cbx_category_selection_filters"
    if ( element_type == "category" ) {
        $("#"+this_m_plugin_prefix+"next_category_"+jQuery.trim(element_id) ).prop('checked',  ( !$("#"+this_m_plugin_prefix+"next_category_"+jQuery.trim(element_id) ).is(':checked') )  );
    }

    if ( element_type == "tag" ) {  // nsn-woo-extended-search-next_tag_43
        $("#"+this_m_plugin_prefix+"next_tag_"+jQuery.trim(element_id) ).prop('checked',  ( !$("#"+this_m_plugin_prefix+"next_tag_"+jQuery.trim(element_id) ).is(':checked') )  );
    }

    if ( element_type == "price" ) { // nsn-woo-extended-search-next_price_5
        var next_id = this_m_plugin_prefix+"next_price_"+jQuery.trim(element_id)
        $( "#" + next_id ).prop('checked',  ( !$("#"+next_id ).is(':checked') )  );
    }

    if ( element_type == "attr" ) { // nsn-woo-extended-search-brand_next_attr_a4tech
        var next_id = this_m_plugin_prefix + sub_element_type + "_next_attr_"+jQuery.trim(element_id)
        $( "#"+next_id ).prop('checked',  ( !$( "#"+next_id ).is(':checked') )  );
    }
    self_frontendFuncs.runFilterSearch( 'loadWooProducts', 1 );
}

nsn_woo_ext_search_frontendFuncs.prototype.loadProductsPage = function (page_number) {
    //alert( "loadProductsPage page_number::"+var_dump(page_number) )
    self_frontendFuncs.runFilterSearch("loadWooProducts", page_number)
}

nsn_woo_ext_search_frontendFuncs.prototype.loadWooProducts = function (page_number, categories_list, attrs_checked_list, prices_list, tags_list, cbx_partial_title, cbx_partial_sku, post_title, sku, rating_min, rating_max, cbx_include_without_rating, only_in_stock, orderby, items_per_page ) {

    // alert( "loadWooProducts page_number "+page_number+"  categories_list::"+var_dump(categories_list) + " attrs_checked_list::"+var_dump(attrs_checked_list)  + "   prices_list::"+var_dump(prices_list) +"   tags_list::"+var_dump(tags_list) + "  cbx_partial_title::"+cbx_partial_title + "  cbx_partial_sku::"+cbx_partial_sku + "  post_title::"+post_title + "  sku::"+sku+ "  rating_min::"+rating_min +  "  rating_max::"+rating_max + "  only_in_stock::"+only_in_stock+"  orderby::"+orderby + "  items_per_page::" + items_per_page )
    //
    //alert( "cbx_include_without_rating::"+cbx_include_without_rating )
    window.location.href = "#nsn_woo_ext_search_product_title"
    $("#div_block_products_list").css( "display", "none" );
    $("#div_block_loading_image").css( "display", "block" );

    var params_attrs_checked_list= Array();
    //alert( "-3  attrs_checked_list.length::"+attrs_checked_list.length )
    for( i = 0; i< attrs_checked_list.length; i++ ) {
        params_attrs_checked_list[i] = new Array(2);
        //alert( "i::"+i+"  "+var_dump(attrs_checked_list[i]) )
        params_attrs_checked_list[i][0]= attrs_checked_list[i].attr_name
        params_attrs_checked_list[i][1]= attrs_checked_list[i].value
    }
    //alert( "::"+var_dump( JSON.stringify( attrs_checked_list ) ) )
    $.ajax({
        url: ajax_object.frontend_ajaxurl,
        dataType: "json",
        //type: 'get',
        type: 'post',
        data: {
            action: 'get-woo-products-by-filter',
            user_id: this_m_logged_user_id,
            categories_list: categories_list,
            //attrs_checked_list: encodeURIComponent(attrs_checked_list),
            //attrs_checked_list: JSON.stringify( attrs_checked_list ),
            attrs_checked_list: params_attrs_checked_list,
            prices_list: prices_list,
            tags_list: tags_list,
            partial_title: ( cbx_partial_title ? '1' : ''),
            partial_sku: ( cbx_partial_sku ? '1' : ''),
            post_title: post_title,
            sku: sku,
            rating_min: rating_min,
            rating_max: rating_max,
            include_without_rating: ( cbx_include_without_rating ? '1' : ''),
            only_in_stock: ( only_in_stock ? '1' : ''),
            orderby: orderby,
            items_per_page : items_per_page,
            page: page_number
        },
        success: function (result) {
            //alert("result::" + var_dump(result))
            if (result.error_code == 0) {
                // alert( "result.args::"+var_dump(result.args) )
                $("#div_products_list").html( result.html );
                $("#div_block_products_list").css( "display", "block" );
                $("#div_block_loading_image").css( "display", "none" );


                /* TODO debuggin - comment on live */
                //$("#div_textarea_args").css( "display", "block" );
                //$("#input_products_count").val( result.products_count +" - " + result.total_products_count);
                //var args_html= var_dump(result.args) + (  ( typeof result.args.meta_query != "undefined" && typeof result.args.meta_query[0] != "undefined" ) ? var_dump(result.args.meta_query[0]) : ''  )
                //$("#textarea_args").val( args_html );


                window.location.href = "#nsn_woo_ext_search_product_title"
            } else {
                alert(result.error_message)
            }
        }
    })

} // nsn_woo_ext_search_frontendFuncs.prototype.loadWooProducts = function (categories_list, attrs_checked_list, prices_list, tags_list, cbx_partial_title, cbx_partial_sku, sku, rating_min, rating_max, only_in_stock, orderby, items_per_page ) {


nsn_woo_ext_search_frontendFuncs.prototype.initRating = function ( rating_min,  rating_max ) {
    if ( typeof rating_min == "undefined" ) {
        rating_min = 1
    }
    if ( typeof rating_max == "undefined" ) {
        rating_max = 5
    }

    $("#slider_rating").slider({
        orientation: "horizontal",
        range: true,
        max: 5,
        min: 1,
        values: [ rating_min, rating_max ],
        slide: function (event, ui) {
            $("#slider_rating_range_info").val( "Selected rating from " + ui.values[0] + " to " + ui.values[1] );
        }
    });
    $("#slider_rating_range_info").val( "Selected rating from " + $("#slider_rating").slider("values", 0) + " to " + $("#slider_rating").slider("values", 1) );
    //alert( "#"+this_m_plugin_prefix+"cbx_include_without_rating" )
    $("#"+this_m_plugin_prefix+"cbx_include_without_rating").prop('checked', false);
}

nsn_woo_ext_search_frontendFuncs.prototype.runFilterSearch = function (run_next, page_number) {
    //alert( "runFilterSearch   run_next::"+run_next + "  page_number::" + page_number )

    var categories_list = {}
    var i= 0
    $('input.'+this_m_plugin_prefix+'cbx_category_selection_filters:checked').each(function () {
        categories_list[i] = $(this).val()
        i++
    });

    var attrs_checked_list= []
    var j= 0
	// alert( "this_attrs_options_flist::"+var_dump(this_m_attrs_options_list) )

    var l= this_m_attrs_options_list.length
    for( i= 0; i< l; i++ ) {
        //alert( "this_m_attrs_options_list[i]::"+var_dump(this_m_attrs_options_list[i]) )
        var attr_name = this_m_attrs_options_list[i]['attr_name']
        //if ( attr_name!= "brand" ) continue;
        //var attrs_list= this_m_attrs_options_list[i].attrs_list
 		//		alert( "attr_name::"+var_dump(attr_name) )
        // class="nsn_woo_ext_search_cbx_{{ attrs_option['attr_name'] }}_selection_filters"
        var next_class_name= 'input.'+this_m_plugin_prefix+'cbx_'+attr_name+'_selection_filters:checked'
        //alert( "next_class_name::"+var_dump(next_class_name) )
        $(next_class_name).each(function () {
            //alert( "INSIDE  attr_name::"+var_dump(attr_name) +" : "+ $(this).val() )
            var attrItem = []
            attrItem['attr_name'] = attr_name;
            attrItem['value'] = $(this).val();
            attrs_checked_list[j]= attrItem
            j++
        });
    }
    //alert( "-3  attrs_checked_list.length::"+attrs_checked_list.length )
    //for( i = 0; i< attrs_checked_list.length; i++ ) {
    //    alert( "i::"+i+"  "+var_dump(attrs_checked_list[i]) )
    //}

    // nsn_woo_ext_search_cbx_attrs_selection_filters
    //alert( "attrs_checked_list::"+var_dump(attrs_checked_list) )
/* attrs_options_list[i]::obj.attr_name = brandobj.show_link_to_post = yesobj.show_in_post_products_list = yesobj.attrs_list = [object Object],[object Object],[object Object],[object Object] */

    var prices_list = {}
    var i= 0
    $('input.'+this_m_plugin_prefix+'cbx_price_selection_filters:checked').each(function () {
        prices_list[i] = $(this).val()
        i++
    });

    var tags_list = {}
    var i= 0
    $('input.'+this_m_plugin_prefix+'cbx_tag_selection_filters:checked').each(function () {
        tags_list[i] = $(this).val()
        i++
    });

    var cbx_partial_title= $("#"+this_m_plugin_prefix+"cbx_partial_title").is(':checked')
    var cbx_partial_sku= $("#"+this_m_plugin_prefix+"cbx_partial_sku").is(':checked')
    var sku= jQuery.trim($("#"+this_m_plugin_prefix+"input_sku").val())
    var post_title= jQuery.trim($("#"+this_m_plugin_prefix+"post_title").val())

    var slider_values = $( "#slider_rating" ).slider( "option", "values" );
    var rating_min= ''
    var rating_max= ''
    if ( typeof slider_values[0] != "undefined" && typeof slider_values[1] != "undefined" ) {
        rating_min= slider_values[0]
        rating_max= slider_values[1]
    }
    var cbx_include_without_rating= $("#"+this_m_plugin_prefix+"cbx_include_without_rating").is(':checked')


    var orderby= $("#"+this_m_plugin_prefix+"orderby").val()
    var items_per_page= $("#"+this_m_plugin_prefix+"items_per_page").val()
    var only_in_stock= $("#"+this_m_plugin_prefix+"cbx_only_in_stock").is(':checked')
    //alert( "-1 only_in_stock::"+only_in_stock +"  items_per_page::"+items_per_page )
    if ( run_next == "saveBookmark" ) {
        self_frontendFuncs.saveBookmark(categories_list, attrs_checked_list, prices_list, tags_list, cbx_partial_title, cbx_partial_sku, post_title, sku, rating_min, rating_max, cbx_include_without_rating, only_in_stock, orderby, items_per_page)
    }
    if ( run_next == "loadWooProducts" )   {
        self_frontendFuncs.loadWooProducts( page_number, categories_list, attrs_checked_list, prices_list, tags_list, cbx_partial_title, cbx_partial_sku, post_title, sku, rating_min, rating_max, cbx_include_without_rating, only_in_stock, orderby, items_per_page)
    }
} // nsn_woo_ext_search_frontendFuncs.prototype.runFilterSearch = function () {

///////////// PRODUCTS BLOCK END /////////////////


///////////// BOOKMARK BLOCK START /////////////////
nsn_woo_ext_search_frontendFuncs.prototype.loadAndFillControls = function (bookmark_title, run_filter_search) {

    // alert( "loadAndFillControls bookmark_title::"+var_dump(bookmark_title)  )
    $.ajax({
        url: ajax_object.frontend_ajaxurl,
        dataType: "json",
        type: 'get',
        //type: 'post',
        data: {
            action: 'load_bookmark_by_title',
            user_id: this_m_logged_user_id,
            bookmark_title: bookmark_title
        },
        success: function (result) {
            // alert("result::" + var_dump(result))
            if (result.error_code == 0) {
                self_frontendFuncs.clearAllInputs()
                //alert( "loadAndFillControls result.bookmark.items_per_page::"+var_dump(result.bookmark.items_per_page) )
                //alert( "loadAndFillControls result.bookmark::"+var_dump(result.bookmark) )

                $('#'+this_m_plugin_prefix+'cbx_partial_title').prop( 'checked', ( typeof result.bookmark.partial_title != "undefined" ? result.bookmark.partial_title : false ) );
                $('#'+this_m_plugin_prefix+'cbx_partial_sku').prop( 'checked', ( typeof result.bookmark.partial_sku != "undefined" ? result.bookmark.partial_sku : false ) );
                $("#"+this_m_plugin_prefix+"input_sku").val( ( typeof result.bookmark.sku != "undefined" ? result.bookmark.sku : "" ) )
                self_frontendFuncs.sku_onChange()

                $("#"+this_m_plugin_prefix+"post_title").val( ( typeof result.bookmark.post_title != "undefined" ? result.bookmark.post_title : "" ) )
                self_frontendFuncs.post_title_onChange()

                $("#"+this_m_plugin_prefix+"orderby").val( ( typeof result.bookmark.orderby != "undefined" ? result.bookmark.orderby : "" ) )
                if (result.bookmark.items_per_page != "") {
                    $("#" + this_m_plugin_prefix + "items_per_page").val(( typeof result.bookmark.items_per_page != "undefined" ? result.bookmark.items_per_page : "" ))
                }
                $('#'+this_m_plugin_prefix+'cbx_only_in_stock').prop( 'checked', ( typeof result.bookmark.only_in_stock != "undefined" ? result.bookmark.only_in_stock : false ) );

                //alert( " items_per_page::"+$("#"+this_m_plugin_prefix+"items_per_page").val()  )
                var categories_str= result.bookmark.categories_list.toString()
                //alert( "typeof categories_str::"+(typeof categories_str) )
                categories_list= get_splitted(categories_str,',')

                //alert( "categories_list::"+var_dump(categories_list) )

                for( i = 0; i< categories_list.length; i++ ) {
                    var next_attr_name= this_m_plugin_prefix + "next_category_" + categories_list[i]
                    //alert( "next_attr_name::"+next_attr_name + "    categories_list[i]::"+categories_list[i])
                    if (  $("#"+next_attr_name)  ) {
                        $("#"+next_attr_name).prop('checked', true);
                        //alert( "FOUND!  next_attr_checked::" )
                    }
                }


                var prices_str= result.bookmark.prices_list.toString()
                prices_list= get_splitted(prices_str,',')
                //alert( "typeof prices_list::"+(typeof prices_list) )
                //alert( "prices_list::"+var_dump(prices_list) )

                for( i = 0; i< prices_list.length; i++ ) {
                    // nsn-woo-extended-search-next_price_200.01_1000
                    var next_price= parseInt( prices_list[i] )
                    //var next_price= prices_list[i]
                    var next_attr_name= this_m_plugin_prefix + "next_price_" + next_price
                    //alert( "next_attr_name::"+next_attr_name  )
                    if (  $( "#"+next_attr_name ) ) {
                        $( "#"+next_attr_name ).prop( 'checked', true );
                        //alert( "FOUND!  next_attr_checked::" )
                    }
                }

                //alert( "result.bookmark.tags_list::"+var_dump(result.bookmark.tags_list) +"  typeof::" +(typeof result.bookmark.tags_list) )
                if ( typeof result.bookmark.tags_list != "undefined" ) {
                    var tags_str = result.bookmark.tags_list.toString()
                    tags_list = get_splitted(tags_str, ',')
                    //alert("typeof tags_list::" + (typeof tags_list))
                    //alert("tags_list::" + var_dump(tags_list))

                    for (i = 0; i < tags_list.length; i++) {
                        // nsn-woo-extended-search-next_tag_200.01_1000
                        var next_tag = parseInt(tags_list[i])
                        //var next_tag= tags_list[i]
                        var next_attr_name = this_m_plugin_prefix + "next_tag_" + next_tag
                        //alert( "next_attr_name::"+next_attr_name  )
                        if ($("#" + next_attr_name)) {
                            $("#" + next_attr_name).prop('checked', true);
                            //alert( "FOUND!  next_attr_checked::" )
                        }
                    }
                }

                var attrs_checked_list= result.bookmark.attrs_checked_list
                for( i = 0; i< attrs_checked_list.length; i++ ) {
                    //alert( "attrs_checked_list::"+var_dump(attrs_checked_list[i]) )
                    var attr_name= attrs_checked_list[i][0]
                    var attr_slug= attrs_checked_list[i][1]
                    var next_attr_slug= this_m_plugin_prefix + attr_name + "_next_attr_" + attr_slug
                    //alert( "next_attr_slug::"+next_attr_slug + "    attr_slug::"+attr_slug)
                    //alert( "$(next_attr_slug)::"+var_dump(  $("#"+next_attr_slug)  ) )
                    if (  $("#"+next_attr_slug)  ) {
                        $("#"+next_attr_slug).prop('checked', true);
                        //alert( "FOUND!  next_attr_checked::" )
                    }
                }

                self_frontendFuncs.initRating(result.bookmark.rating_min,result.bookmark.rating_max)

                //alert( "AFTER::"+var_dump(1) )
                $( "#"+this_m_plugin_prefix+"show_existing_bookmarks_dialog" ).modal('hide');
                if ( this_m_show_bookmark_alerts == 'yes' ) {
                    $.alert({
                        title: 'Bookmarks',
                        icon: 'glyphicon glyphicon-bookmark',
                        content: 'Bookmark loaded !',
                        confirmButton: 'OK',
                        keyboardEnabled: true
                    });
                }

                // alert( "run_filter_search  ::"+run_filter_search )
                if ( run_filter_search ) {
                    self_frontendFuncs.runFilterSearch("loadWooProducts", 1)
                }


            } else {
                alert(result.error_message)
            }
        }
    })

} // nsn_woo_ext_search_frontendFuncs.prototype.loadAndFillControls = function (bookmark_title) {

nsn_woo_ext_search_frontendFuncs.prototype.onChange_show_existing_select_bookmark = function (obj) {
    //alert( "onChange_show_existing_select_bookmark  obj::"+var_dump(obj) )
    if ( obj.value == "" ) {
        $("#button_"+this_m_plugin_prefix+"remove_selected_bookmark").css("display","none")
        //$("#span_"+this_m_plugin_prefix+"run_filter_search").css("display","none")
    } else {
        $("#button_"+this_m_plugin_prefix+"remove_selected_bookmark").css("display","inline")
        //$("#span_"+this_m_plugin_prefix+"run_filter_search").css("display","block")
    }
}

nsn_woo_ext_search_frontendFuncs.prototype.removeBookmarks = function () {



    $.confirm({
        icon: 'glyphicon glyphicon-signal',
        title: 'Confirm!',
        content: "Do you want to <b>remove</b> selected bookmark ?",
        confirmButton: 'YES',
        cancelButton: 'Cancel',
        confirmButtonClass: 'btn-info',
        cancelButtonClass: 'btn-danger',
        keyboardEnabled: true,
        confirm: function(){


            var bookmark_title= $("#"+this_m_plugin_prefix+"show_existing_select_bookmark").val()
            $.ajax({
                url: ajax_object.frontend_ajaxurl,
                dataType: "json",
                type: 'post',
                data: {
                    action: 'remove_bookmark',
                    user_id: this_m_logged_user_id,
                    bookmark_title: bookmark_title
                },
                success: function (result) {
                    if (result.error_code == 0) {
                        self_frontendFuncs.loadBookmarks();
                        $( "#"+this_m_plugin_prefix+"show_existing_bookmarks_dialog" ).modal('hide');
                        if ( this_m_show_bookmark_alerts == 'yes' ) {
                            $.alert({
                                title: 'Bookmarks',
                                icon: 'glyphicon glyphicon-bookmark',
                                content: 'Bookmark was removed !',
                                confirmButton: 'OK',
                                keyboardEnabled: true
                            });
                        }
                    } else {
                        alert(result.error_message)
                    }
                }
            })


        }
    });



}


nsn_woo_ext_search_frontendFuncs.prototype.clearAllInputs = function () {
    //alert( "clearAllInputs::"+var_dump( 1) )



    $('input.'+this_m_plugin_prefix+'cbx_category_selection_filters:checked').each(function () {
        $(this).prop('checked', false);
        //alert( "CATEGORY INSIDE  ::"+ $(this).text() )
    });

    var l= this_m_attrs_options_list.length
    for( i= 0; i< l; i++ ) {
        var attr_name = this_m_attrs_options_list[i]['attr_name']
        var next_class_name= 'input.'+this_m_plugin_prefix+'cbx_'+attr_name+'_selection_filters:checked'
        $(next_class_name).each(function () {
            //alert( "INSIDE  attr_name::"+var_dump(attr_name) +" : "+ $(this).val() )
            $(this).prop('checked', false);
        });
    }

    $('input.'+this_m_plugin_prefix+'cbx_price_selection_filters:checked').each(function () {
        $(this).prop('checked', false);
        //alert( "INSIDE  price::" + $(this).val() )
    });

    $('input.'+this_m_plugin_prefix+'cbx_tag_selection_filters:checked').each(function () {
        $(this).prop('checked', false);
        //alert( "INSIDE  tag::" + $(this).val() )
    });

    $("#"+this_m_plugin_prefix+"input_sku").val("")
    $("#"+this_m_plugin_prefix+"cbx_partial_sku").prop('checked', false);
    $("#"+this_m_plugin_prefix+"cbx_partial_sku").prop("disabled","disabled");
    $("#"+this_m_plugin_prefix+"post_title").val("")
    $("#"+this_m_plugin_prefix+"cbx_partial_title").prop('checked', false);
    $("#"+this_m_plugin_prefix+"cbx_partial_title").attr("disabled","disabled");

    $("#"+this_m_plugin_prefix+"orderby").val("menu_order")
    $("#"+this_m_plugin_prefix+"items_per_page").val("")
    $("#"+this_m_plugin_prefix+"cbx_only_in_stock").prop('checked', false);

    //var ratingSlider = $("#"+this_m_plugin_prefix+"rating_slider").bootstrapSlider(); ////https://github.com/seiyria/bootstrap-slider
    //var ratingSlider = $("#"+this_m_plugin_prefix+"rating_slider").bootstrapSlider();
    //alert( "ratingSlider::"+var_dump(ratingSlider) )

    self_frontendFuncs.initRating(1,5)
    //$("#slider_rating").slider({
    //    orientation: "horizontal",
    //    range: true,
    //    max: 5,
    //    min: 0,
    //    values: [ filter_rating_min, filter_rating_max ],
    //    slide: function (event, ui) {
    //        //$("#slider_rating_range_info").val("Range: " + ui.values[0] + "% to " + ui.values[1] + "%");
    //        $("#slider_rating_range_info").val( "Selected rating from " + ui.values[0] + " to " + ui.values[1] );
    //    }
    //});


    //ratingSlider.slider('setValue', 1).slider('setValue', 5);
    //ratingSlider
    //    .bootstrapSlider('setValue', 1)
    //    .bootstrapSlider('setValue', 5);

} // nsn_woo_ext_search_frontendFuncs.prototype.clearAllInputs = function () {

nsn_woo_ext_search_frontendFuncs.prototype.showNewBookmarkDialog = function () {
    $("#"+this_m_plugin_prefix+"bookmark_title").val("")
    //$("#"+this_m_plugin_prefix+"select_bookmark").val("")
    //
    //ClearDDLBItems(this_m_plugin_prefix+"select_bookmark", false)
    //if ( typeof nsn_woo_ext_search_bookmarks_list.length != "undefined" ) {
    //    var l = nsn_woo_ext_search_bookmarks_list.length
    //    for (i = 0; i < l; i++) {
    //        if ( nsn_woo_ext_search_bookmarks_list[i].title!= "" ) {
    //            AddDDLBItem(this_m_plugin_prefix + "select_bookmark", nsn_woo_ext_search_bookmarks_list[i].title, nsn_woo_ext_search_bookmarks_list[i].title)
    //        }
    //    }
    //}

    $( "#"+this_m_plugin_prefix+"show_new_bookmark_dialog" ).modal(  {
        "backdrop": "static",
        "keyboard": true,
        "show": true
    }  );


} // showNewBookmarkDialog

nsn_woo_ext_search_frontendFuncs.prototype.makeSaveBookmark = function () {
    var bookmark_title= jQuery.trim( $("#"+this_m_plugin_prefix+"bookmark_title").val() )
    var is_default= jQuery.trim( $("#"+this_m_plugin_prefix+"is_default").val() )
    //alert( "is_default::"+is_default )
    //var select_bookmark= jQuery.trim( $("#"+this_m_plugin_prefix+"select_bookmark").val() )
    if ( bookmark_title == ""  ) {
        alert( "Enter new bookmark title !" )
        $("#"+this_m_plugin_prefix+"bookmark_title").focus()
        return;
    }
    self_frontendFuncs.runFilterSearch("saveBookmark")
} // makeSaveBookmark

nsn_woo_ext_search_frontendFuncs.prototype.fillBookmarkInControls = function () {
    var selected_bookmark= jQuery.trim( $("select#"+this_m_plugin_prefix+"show_existing_select_bookmark").val() )
    var run_filter_search= $( "#"+this_m_plugin_prefix+"run_filter_search" ).is(':checked')

    // alert( "fillBookmarkInControls selected_bookmark::"+selected_bookmark + "  run_filter_search::"+run_filter_search)

    //  elem_id::nsn-woo-extended-search-run_filter_search
    //           nsn-woo-extended-search-run_filter_search
    if ( selected_bookmark == ""  ) {
        alert( "Select bookmark !" )
        $("#"+this_m_plugin_prefix+"show_existing_select_bookmark").focus()
        return;
    }
    self_frontendFuncs.loadAndFillControls(selected_bookmark, run_filter_search)
    // if ( run_filter_search ) {
    //     self_frontendFuncs.runFilterSearch("loadWooProducts", 1)
    // }

} // fillBookmarkInControls


nsn_woo_ext_search_frontendFuncs.prototype.saveBookmark = function ( categories_list, attrs_checked_list, prices_list, tags_list, cbx_partial_title, cbx_partial_sku, post_title, sku, rating_min, rating_max, cbx_include_without_rating, only_in_stock, orderby, items_per_page ) {

    var bookmark_title= jQuery.trim( $("#"+this_m_plugin_prefix+"bookmark_title").val() )
    var is_default= jQuery.trim( $("#"+this_m_plugin_prefix+"is_default").val() )
    //alert( "is_default::"+is_default )

    //var select_bookmark= jQuery.trim( $("#"+this_m_plugin_prefix+"select_bookmark").val() )

    //alert( "saveBookmark categories_list::"+var_dump(categories_list) + " attrs_checked_list::"+var_dump(attrs_checked_list)  + "   prices_list::"+var_dump(prices_list) + "   tags_list::"+var_dump(tags_list) + "  cbx_partial_title::"+cbx_partial_title + "  cbx_partial_sku::"+cbx_partial_sku + "  sku::"+sku + "  post_title::"+post_title+ "  rating_min::"+rating_min +  "  rating_max::"+rating_max + "  only_in_stock::"+only_in_stock+"  orderby::"+orderby + "  items_per_page::"+items_per_page+"  bookmark_title::" + bookmark_title  )
    //
    //alert( "-3  attrs_checked_list.length::"+attrs_checked_list.length )
    var params_attrs_checked_list= Array();
    for( i = 0; i< attrs_checked_list.length; i++ ) {
        params_attrs_checked_list[i] = new Array(2);
        params_attrs_checked_list[i][0]= attrs_checked_list[i].attr_name
        params_attrs_checked_list[i][1]= attrs_checked_list[i].value
        //alert( "params_attrs_checked_list[i]::"+var_dump(params_attrs_checked_list[i]) )
    }


    //alert( "::"+var_dump( JSON.stringify( attrs_checked_list ) ) )
    //alert( "this_m_logged_user_id::"+this_m_logged_user_id )
    //return
    $.ajax({
        url: ajax_object.frontend_ajaxurl,
        dataType: "json",
        //type: 'get',
        type: 'post',
        data: {
            action: 'save_bookmark',
            user_id: this_m_logged_user_id,
            categories_list: categories_list,
            bookmark_title: bookmark_title,
            is_default: is_default,
            //select_bookmark: select_bookmark,
            //attrs_checked_list: encodeURIComponent(attrs_checked_list),
            //attrs_checked_list: JSON.stringify( attrs_checked_list ),
            //attrs_checked_list: attrs_checked_list,
            attrs_checked_list: params_attrs_checked_list,
            prices_list: prices_list,
            tags_list: tags_list,
            partial_title: ( cbx_partial_title ? '1' : ''),
            partial_sku: ( cbx_partial_sku ? '1' : ''),
            sku: sku,
            post_title: post_title,


            rating_min: rating_min,
            rating_max: rating_max,
            include_without_rating: ( cbx_include_without_rating ? '1' : ''),
            only_in_stock: ( only_in_stock ? '1' : ''),
            orderby: orderby,
            items_per_page: items_per_page,
        },
        success: function (result) {
            //alert("result::" + var_dump(result))
            if (result.error_code == 0) {
                //alert( "#"+this_m_plugin_prefix+"show_new_bookmark_dialog" )
                self_frontendFuncs.loadBookmarks();
                $( "#"+this_m_plugin_prefix+"show_new_bookmark_dialog" ).modal('hide');
                if ( this_m_show_bookmark_alerts == 'yes' ) {
                    $.alert({
                        title: 'Bookmarks',
                        icon: 'glyphicon glyphicon-bookmark',
                        content: 'Bookmark was saved !',
                        confirmButton: 'OK',
                        keyboardEnabled: true
                    });
                }
            } else {
                alert(result.error_message)
            }
        }
    })

}
nsn_woo_ext_search_frontendFuncs.prototype.showBookmarks = function () {
    var l= nsn_woo_ext_search_bookmarks_list.length
    ClearDDLBItems(this_m_plugin_prefix+"show_existing_select_bookmark", false)
    for(i= 0; i< l;i++) {
        if ( nsn_woo_ext_search_bookmarks_list[i].title!= "" ) {
            var bookmark_key= nsn_woo_ext_search_bookmarks_list[i].title.replace( / \( default \) /g, '' ) // ' ( default ) '
            // alert( "bookmark_key::"+bookmark_key )
            AddDDLBItem(this_m_plugin_prefix + "show_existing_select_bookmark",bookmark_key , nsn_woo_ext_search_bookmarks_list[i].title)
        }
    }
    $("#button_"+this_m_plugin_prefix+"remove_selected_bookmark").css("display","none")
    $( "#"+this_m_plugin_prefix+"show_existing_bookmarks_dialog" ).modal(  {  // nsn-woo-extended-search-show_existing_bookmarks_dialog
        "backdrop": "static",
        "keyboard": true,
        "show": true
    }  );
}

nsn_woo_ext_search_frontendFuncs.prototype.loadBookmarks = function () {
    $.ajax({
        url: ajax_object.frontend_ajaxurl,
        dataType: "json",
        type: 'post',
        data: {
            action: 'load_bookmarks',
            user_id: this_m_logged_user_id
        },
        success: function (result) {
            // alert("loadBookmarks result::" + var_dump(result))
            if (result.error_code == 0) {
                nsn_woo_ext_search_bookmarks_list= Array.from( objectToArray(result.bookmarks_array) )
                if ( typeof nsn_woo_ext_search_bookmarks_list == "undefined" || nsn_woo_ext_search_bookmarks_list.length == 0 ) {
                    $("#span_have_bookmarks").html("You have no saved Bookmarks yet" );
                    $("#button_show_existing_bookmarks").css("display","none")

                } else {
                    if (nsn_woo_ext_search_bookmarks_list.length > 0) {
                        $("#button_show_existing_bookmarks").css("display","inline")
                        $("#span_have_bookmarks").html("You have " + nsn_woo_ext_search_bookmarks_list.length + " saved Bookmark" + ( nsn_woo_ext_search_bookmarks_list.length > 1 ? 's' : '' ));

                        if ( result.default_bookmark_title != "" && result.default_bookmark_index >= 0  ) {
                            // alert( "result.default_bookmark_title::"+var_dump(result.default_bookmark_title) )
                            self_frontendFuncs.loadAndFillControls(result.default_bookmark_title, true)
                        }
                    }
                }
            } else {
                alert(result.error_message)
            }
        }
    })

}

nsn_woo_ext_search_frontendFuncs.prototype.post_title_onChange = function () {
    var post_title= jQuery.trim(  $("#"+this_m_plugin_prefix+"post_title").val()  )
    if ( post_title == "" ) {
        $("#"+this_m_plugin_prefix+"cbx_partial_title").prop('checked', false);
        $("#" + this_m_plugin_prefix + "cbx_partial_title").attr("disabled", "disabled");
    } else {
        $("#" + this_m_plugin_prefix + "cbx_partial_title").removeAttr("disabled");
    }
}


nsn_woo_ext_search_frontendFuncs.prototype.sku_onChange = function () {
    var sku= jQuery.trim(  $("#"+this_m_plugin_prefix+"input_sku").val()  )
    if ( sku == "" ) {
        $("#"+this_m_plugin_prefix+"cbx_partial_sku").prop('checked', false);
        $("#" + this_m_plugin_prefix + "cbx_partial_sku").attr("disabled", "disabled");
    } else {
        $("#" + this_m_plugin_prefix + "cbx_partial_sku").removeAttr("disabled");
    }
}

nsn_woo_ext_search_frontendFuncs.prototype.block_minusonClick = function (block_id, is_minus) {
    if ( is_minus ) {
        $( "#span_" + block_id+"_filter_minus" ).css( "display", "none" )
        $( "#span_" + block_id+"_filter_plus" ).css( "display", "inline" )
        $( "#section_block_" + block_id ).css( "display", "none" )
    } else {
        $( "#span_" + block_id+"_filter_minus" ).css( "display", "inline" )
        $( "#span_" + block_id+"_filter_plus" ).css( "display", "none" )
        $( "#section_block_" + block_id ).css( "display", "block" )
    }
    //<section class="nsn_woo_ext_search_block_content {{ row_class }} " id="section_block_{{ block_id }}">

}

///////////// BOOKMARK BLOCK END /////////////////

