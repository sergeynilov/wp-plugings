<?php
//echo '<pre> - site-control.php::'.print_r(-321,true).'</pre>';
if ( ! defined( 'NSN_WooExtSearch' ) ) {
//    echo '<pre>NSN_WooExtSearchEXIT::'.print_r(-1,true).'</pre>';
    exit; // Exit if accessed directly
}
//echo '<pre> - site-control.php::'.print_r(-322,true).'</pre>';
require_once( 'appWooExtSearchControl.php'); //wp-content/plugins/wooExtSearch/appWooExtSearchControl.php
//echo '<pre> - site-control.php::'.print_r(-323,true).'</pre>';


    //286, 274, 280
class siteControl extends  appNSN_WooExtSearchControl
{
    protected $m_attrParams;
    protected $m_action;
    protected $m_currentPost;

    public function __construct($containerClass, $wpdb, $attrAparams, $action, $currentPost)
    {
        global $session;
//        echo '<pre> - site-control.php::'.print_r(-324,true).'</pre>';
        $this->m_labelNonce= 'site-editor-nonce';
        $this->m_labelNonceInput= 'site-editor-nonce-input';
//        $this->m_pageUrl = $containerClass->get_plugin_name();
        $this->m_pageUrl= '/wp-admin/admin.php?page=NSN_WooExtSearch-editor&';
        $this->m_plugin_url= $containerClass->getPluginUrl();
        $this->m_plugin_dir= $containerClass->getPluginDir();
        $this->m_plugin_name= $containerClass->getPluginName();
        $this->m_plugin_prefix= $containerClass->getPluginPrefix();
        $this->m_prices_list= $containerClass->getPricesList();
        $this->m_currency_symbol= get_woocommerce_currency_symbol( get_woocommerce_currency() );
//p://local-wp-plugings.com/wp-admin/admin.php?page=NSN_WooExtSearch-editor
        $this->m_attrParams= $attrAparams;
        $this->m_action= $action;
        $this->m_currentPost= $currentPost;
        parent::__construct($containerClass, $wpdb, $session);
//        echo '<pre>$ZZZ::'.print_r($this->m_pluginDir . '/lib/app-nsn-woo-tools-data.php',true).'</pre>';
        require_once( $this->m_pluginDir . '/lib/app-nsn-woo-tools-data.php'); //wp-content/plugins/wooExtSearch/lib/app-nsn-woo-tools-data.php
        $this->isDebug= true;
        $this->m_is_insert= false;
    }

    protected function readRequestParameters() {  // before main
//        if ($this->isDebug) echo '<pre>$this->m_action::'.print_r($this->m_action,true).'</pre>';
    } // protected function readRequestParameters() {

    public function main() {


//        parent::main();
//        $this->isDebug= false;
//
//        if ($this->isDebug) echo '<pre>$_REQUEST::'.print_r($_REQUEST,true).'</pre>';
//        if ($this->isDebug) echo '<pre>$this->m_GETDataArray::'.print_r($this->m_GETDataArray,true).'</pre>';
//        if ($this->isDebug) echo '<pre>$this->m_POSTDataArray::'.print_r($this->m_POSTDataArray,true).'</pre>';
//        if ($this->isDebug) echo '<pre>$this->m_attrParams::'.print_r($this->m_attrParams,true).'</pre>';
//        if ($this->isDebug) echo '<pre>$this->m_action::'.print_r($this->m_action,true).'</pre>';
//        if ($this->isDebug) echo '<pre>$this->m_currentPost::'.print_r($this->m_currentPost,true).'</pre>';
        $m_parentControl= $this;
//        echo '<pre>$this->m_action::'.print_r($this->m_action,true).'</pre>';

        DebToFile('---main  $this->m_plugin_url::' . print_r($this->m_plugin_url,true));
        DebToFile('---main  $this->m_plugin_dir::' . print_r($this->m_plugin_dir,true));
        DebToFile('---main  $this->m_action::' . print_r($this->m_action,true));


        if ( $this->m_action == 'siteNSN_WooExtSearchForm' ) { // Show NSN Woo Extended Search Form
            $args = array(
                's'                   => '',
                'post_type'           => 'product',
                'post_status'         => 'publish',
                'ignore_sticky_posts' => 1,
                'numberposts'         => -1, // show all
                'posts_per_page'      => -1,
                'suppress_filters'    => false,

            );
            $products_list = get_posts( $args );
            $woo_ext_search_options = get_option( 'nsn-woo-ext-search-' );
//            echo '<pre>$woo_ext_search_options::'.print_r($woo_ext_search_options,true).'</pre>';
            $app_params= nsnClass_appFuncs::prepareParams( true,  array( 'plugin_prefix'=> $this->m_plugin_prefix ) );
            if ( empty($woo_ext_search_options) ) {
                echo '<p class="text-info" id="nsn_woo_ext_search_product_title">Set Options in backend part!</p>';
                return;
            }
//            echo '<pre>$woo_ext_search_options::'.print_r($woo_ext_search_options,true).'</pre>';
            $show_number_of_products= !empty($woo_ext_search_options['show_in_extended_search_show_number_of_products']) ? $woo_ext_search_options['show_in_extended_search_show_number_of_products'] : 'no';
//            echo '<pre>$show_number_of_products::'.print_r($woo_ext_search_options['show_in_extended_search_show_number_of_products'],true).'</pre>';
//            echo '<pre>$products_list::'.print_r($products_list,true).'</pre>';

            $categories_list= nsnWooToolsData::get_categories_list( array( 'taxonomy' => 'product_cat') );

            if ( $show_number_of_products == 'yes' ) {
                foreach( $categories_list as $next_key=>$next_category ) {
                    foreach( $products_list as $next_product ) {

                        $category_array = nsnWooToolsData::get_category_by_post_id( $next_product->ID, 'product_cat', array('id'=>'name', 'get_all_keys'=>1));
//                    echo '<pre>$category_array::'.print_r($category_array,true).'</pre>';
                        if ( !empty($category_array['ret_keys']) and is_array($category_array['ret_keys']) ) {
                            foreach( $category_array['ret_keys'] as $next_ref_category_id ) {
                                if ( $next_ref_category_id == $next_category['ID'] ) {
                                    if ( empty($categories_list[$next_key]['products_count']) ) {
                                        $categories_list[$next_key]['products_count']= 1;
                                    } else {
                                        $categories_list[$next_key]['products_count'] = $categories_list[$next_key]['products_count'] + 1;
                                    }
                                }
                            }
                        }

                    }
                }
            } //             if ( $show_number_of_products == 'yes' )

        reset($products_list);

//            echo '<pre>$categories_list::'.print_r($categories_list,true).'</pre>';
//            $products_tags_list= array();
            $tags_list = nsnWooToolsData::get_products_tags_list(true); //http://stackoverflow.com/questions/31478603/get-woocommerce-products-tags-for-array-of-products
//            echo '<pre>$tags_list::'.print_r($tags_list,true).'</pre>';
//            die("-1 XXZ");
            if ( $show_number_of_products == 'yes' )
            foreach( $tags_list as $next_tag_key=>$next_tag ) {
                foreach ($products_list as $next_key => $next_product) {
//                $next_product_tags= get_the_terms($next_product, 'product_tag', '', ',' );
                    $next_product_tags= nsnWooToolsData::get_tags_list_by_product($next_product);
//                    if (empty($next_product_tags)) {
//                        echo '<pre>$next_product->id::'.print_r($next_product->ID,true).'</pre>';
//                    }
//                    echo '<pre>$next_product_tags::'.print_r($next_product_tags,true).'</pre>';
//                    die("-1 XXZ");
                    foreach( $next_product_tags as $next_product_tag_key=>$next_product_tag ) {
                        if ($next_product_tag['slug'] == $next_tag['slug']) {
                            if (empty($tags_list[$next_tag_key]['products_count'])) {
                                $tags_list[$next_tag_key]['products_count'] = 1;
                            } else {
                                $tags_list[$next_tag_key]['products_count'] = $tags_list[$next_tag_key]['products_count'] + 1;
                            }
                        }
                    }
                }
            }
            reset($products_list);
//            echo '<pre>$tags_list::'.print_r($tags_list,true).'</pre>';
//            die("-1 XXZ");
            $attrs_options_list= array();
            if (  !empty($woo_ext_search_options['attribute_option_values']) and is_array($woo_ext_search_options['attribute_option_values'])  ) {
                foreach( $woo_ext_search_options['attribute_option_values'] as $next_attr ) { // any attribute for products

//                    echo '<pre>$next_attr::'.print_r($next_attr,true).'</pre>';
//                    die("-1 XXZ");
                    if ( strtolower($next_attr['show_attribute_in_extended_search']) != 'yes' ) continue;
                    $attrs_list = nsnWooToolsData::get_products_attrs_list($next_attr['attr_name'], true);




                    if ( $show_number_of_products == 'yes' ) {
//            echo '<pre>000 $attrs_list::'.print_r($attrs_list,true).'</pre>';
                        foreach ($products_list as $next_key => $next_product) {
//                if ($next_product->ID != 28) continue;
//                echo '<pre>$::'.print_r($next_product,true).'</pre>';
                            $next_attr_values = wc_get_product_terms($next_product->ID, 'pa_'.$next_attr['attr_name'], array('fields' => 'names'));
//                echo '<pre>$next_attr_values::'.print_r($next_attr_values,true).'</pre>';
//                die("-1 XXZ---------");
                            foreach ($next_attr_values as $next_next_attr_value) {
                                foreach ($attrs_list as $next_next_attrs_list_key => $next_next_attrs_list_value) {
                                    if ($next_next_attr_value == $next_next_attrs_list_value['name']) {
//                            die("-1 XXZ 000000000");
                                        if (empty($attrs_list[$next_next_attrs_list_key]['products_count'])) {
                                            $attrs_list[$next_next_attrs_list_key]['products_count'] = 1;
                                        } else {
                                            $attrs_list[$next_next_attrs_list_key]['products_count'] = $attrs_list[$next_next_attrs_list_key]['products_count'] + 1;
                                        }
                                    }
                                }
                            }
                        }
                    } //             if ( $show_number_of_products == 'yes' ) {


//                    echo '<pre>$attrs_list::'.print_r($attrs_list,true).'</pre>';
                    $attrs_options_list[]= array( 'attr_name'=> $next_attr['attr_name'], 'show_attribute_link_to_post'=> $next_attr['show_attribute_link_to_post'], 'show_attribute_in_post_products_list'=> $next_attr['show_attribute_in_post_products_list'], 'attrs_list'=>$attrs_list );
                    /*                  [show_link_to_post] => yes
                    [show_in_post_products_list] => yes */
                } //foreach( $woo_ext_search_options['attribute_option_values'] as $next_attr ) { // any attribute for products
            } // if (  !empty($woo_ext_search_options['attribute_option_values']) and is_array($woo_ext_search_options['attribute_option_values'])  ) {

//            echo '<pre>$attrs_options_list::'.print_r($attrs_options_list,true).'</pre>';
//            die("-1 XXZ");

//                array('min'=>200.01, 'max'=>1000),   );


            if ( $show_number_of_products == 'yes' ) {
                foreach( $products_list as $next_key=>$next_product ) {
                    $_product = wc_get_product( $next_product->ID );
//                echo '<pre>--- $_product::'.print_r($_product,true).'</pre>';
                    $sale_price= $_product->get_sale_price();
//                echo '<pre>--- $sale_price::'.print_r($sale_price,true).'</pre>';
                    if (empty($sale_price) or (int)($sale_price) < 0 ) continue;

                    foreach( $this->m_prices_list as $next_price_key=>$next_price ) {
                        if ( $sale_price >= $next_price['min'] and $sale_price < $next_price['max'] ) {
                            if ( empty($this->m_prices_list[$next_price_key]['products_count']) ) {
                                $this->m_prices_list[$next_price_key]['products_count']= 1;
                            } else {
                                $this->m_prices_list[$next_price_key]['products_count'] = $this->m_prices_list[$next_price_key]['products_count'] + 1;
                            }
                        }

                    }
                }
            } //             if ( $show_number_of_products == 'yes' ) {
//            echo '<pre>$this->m_prices_list::'.print_r($this->m_prices_list,true).'</pre>';
//            die("-1 XXZ");

            $woo_ext_search_options['run_search_by_click']= 'both'; // both / link /button
            $products_pagination_per_page_list= array();
            if ( !empty($woo_ext_search_options['products_pagination_per_page_list']) ) {
                $products_pagination_per_page_list = nsnWooToolsData::preg_split('/,/', $woo_ext_search_options['products_pagination_per_page_list']);
            }
            reset($products_list);

            $data= array('categories_list'=>$categories_list, 'tags_list'=> $tags_list,  'attrs_options_list'=> $attrs_options_list, 'prices_list'=> $this->m_prices_list, 'app_params'=> $app_params, 'plugin_prefix'=> $this->m_plugin_prefix, 'woo_ext_search_options'=> $woo_ext_search_options, 'currency_symbol' => $this->m_currency_symbol, 'products_pagination_per_page_list'=> $products_pagination_per_page_list, 'plugin_url'=> $this->m_plugin_url , 'plugin_dir'=> $this->m_plugin_dir );
            twigpress_render_twig_template( $data, "white-simple/woo_ext_search_form.twig" );
            return;
        } // if ( $this->m_action == 'siteNSN_WooExtSearchForm' ) { // Show NSN Woo Extended Search Form


        if ($this->isDebug) echo '<hr><hr><hr>';


    } // public function main() {

    private  function exitWithMessage($text='') {
        if (empty($text)) {
            $text= ob_get_contents();
        }
        ob_end_clean ();
        return $text;
    }

    protected function saveRowData( $action, $editorItem ) {
    } // protected function saveRowData( $action, $editorItem ) {

    protected function showRowsListDataForm( $parentControl )
    {
    } // protected function showRowsListDataForm( $parentControl )   {

    protected function addRowDataForm( $parentControl )   {
    }

    protected function initFieldsAddRow( $parentControl )
    {
    }


    protected function editRowDataForm( $parentControl ) {

    } // protected function editRowDataForm( $parentControl ) {

    protected function deleteRowData()
    {
    }


    protected function doBulkAction( $action) {
    } // protected function doBulkAction() {

    protected function doValidation($data)
    {
    } // protected function doValidation($data) {

    protected function setNotValidatedData($editorItem, $fieldsArray) {
    } // protected function setNotValidatedData($editorItem, $fieldsArray) {

    protected function preparePageParameters($WithPage, $WithSort) // Prepare url for pagination with all fields in filter and sort and sort_direction
    {
   } // protected function preparePageParameters($WithPage, $WithSort) // Prepare url for pagination with all fields in filter and sort and sort_direction

}