<?php
function drawNSN_WooExtPageNavigation( $paged, $posts_per_page, $max_page, $before = '', $after = '', $echo = true, $jsFuncCoe= '', $showDiv= false, $pageUrl= '', $classes= array() ) {
    /* ================ Настройки ================ */
    $text_num_page = ''; // Текст перед пагинацией. {current} - текущая; {last} - последняя (пр. 'Страница {current} из {last}' получим: "Страница 4 из 60" )
    $num_pages = 10; // сколько ссылок показывать
    $stepLink = 10; // ссылки с шагом (значение - число, размер шага (пр. 1,2,3...10,20,30). Ставим 0, если такие ссылки не нужны.
    $dotright_text = '…'; // промежуточный текст "до".
    $dotright_text2 = '…'; // промежуточный текст "после".
    $backtext = '0';  // '« назад'; // текст "перейти на предыдущую страницу". Ставим 0, если эта ссылка не нужна.
    $nexttext = '0'; //'forward »'; // текст "перейти на следующую страницу". Ставим 0, если эта ссылка не нужна.
    $first_page_text = '« to top'; // текст "к первой странице". Ставим 0, если вместо текста нужно показать номер страницы.
    $last_page_text = 'to end »'; // текст "к последней странице". Ставим 0, если вместо текста нужно показать номер страницы.
    /* ================ Конец Настроек ================ */

    global $wp_query;

    //$posts_per_page = (int) $wp_query->query_vars['posts_per_page'];
    //$paged = (int) $wp_query->query_vars['paged'];
    //$max_page = $wp_query->max_num_pages;

    //проверка на надобность в навигации
    if( $max_page <= 1 )
        return false;

    if( empty($paged) || $paged == 0 )
        $paged = 1;

    $pages_to_show = intval( $num_pages );
    $pages_to_show_minus_1 = $pages_to_show-1;

    $half_page_start = floor( $pages_to_show_minus_1/2 ); //сколько ссылок до текущей страницы
    $half_page_end = ceil( $pages_to_show_minus_1/2 ); //сколько ссылок после текущей страницы

    $start_page = $paged - $half_page_start; //первая страница
    $end_page = $paged + $half_page_end; //последняя страница (условно)

    if( $start_page <= 0 )
        $start_page = 1;
    if( ($end_page - $start_page) != $pages_to_show_minus_1 )
        $end_page = $start_page + $pages_to_show_minus_1;
    if( $end_page > $max_page ) {
        $start_page = $max_page - $pages_to_show_minus_1;
        $end_page = (int) $max_page;
    }

    if( $start_page <= 0 )
        $start_page = 1;

    //выводим навигацию
    $out = '';
//    $out = '<pre>$classes::'.print_r($classes,true).'</pre>';;

    // создаем базу чтобы вызвать get_pagenum_link один раз

    $link_base = get_pagenum_link(99999999); // 99999999 будет заменено
    //echo '<pre>$link_base::'.print_r($link_base,true).'</pre>';
    if ( !empty($_POST) ) {
        //$link_base = get_pagenum_link(99999999); // 99999999 будет заменено
        $urlsStr= '';
        foreach( $_POST as $nextKey=>$nextValue ) {
            $a = preg_split('/filter_/', $nextKey);
            if ( count($a)==2 and !empty($nextValue) ) {
                $urlsStr.= $nextKey.'='.$nextValue.'&';
            }
        }
        //echo '<pre>$urlsStr::'.print_r($urlsStr,true).'</pre>';
        //echo '<pre>$pageUrl::'.print_r($pageUrl,true).'</pre>';
        $link_base = get_home_url() . $pageUrl . 'action=list&' . $urlsStr . '&paged=99999999'; // 99999999 будет заменено
    }
    //echo '<pre>+++$link_base::'.print_r($link_base,true).'</pre>';
    //die("-1");

    $link_base = str_replace( 99999999, '___', $link_base);
    //DebToFile('-2 $link_base  ::' .print_r($link_base,true) );


    $first_url = get_pagenum_link( 1 );
    $out .= $before . "<" . ( $showDiv ? "div" : "span")." class='" . (!empty($classes['external_div']) ? $classes['external_div'] : '') . "'>\n";

    if( $text_num_page ){
        $text_num_page = preg_replace( '!{current}|{last}!', '%s', $text_num_page );
        $out.= sprintf( "<span class='pages'>$text_num_page</span> ", $paged, $max_page );
    }
    // назад
    if ( $backtext && $paged != 1 ) {
        if ( empty($jsFuncCoe) ) {  // str.replace(/\n/g, '<br>')
            $out .= '<a class="prev ' . (!empty($classes['next_link_page']) ? $classes['next_link_page'] : '') . '" href="' . str_replace('___', ($paged - 1), $link_base) . '">' . $backtext . '</a> ';
        } else {
            $out .= '<a class="prev ' . (!empty($classes['next_link_page']) ? $classes['next_link_page'] : '') . '" style="cursor:pointer" onclick="javascript:'.str_replace('#page#',($paged - 1),$jsFuncCoe).' " >' . $backtext . '</a> ';
        }
    }
    // в начало
    if ( $start_page >= 2 && $pages_to_show < $max_page ) {
        if ( empty($jsFuncCoe) ) {
            $out .= '<a class="first ' . (!empty($classes['next_link_page']) ? $classes['next_link_page'] : '') . '" href="' . $first_url . '">' . ($first_page_text ? $first_page_text : 1) . '</a> ';
        } else {
            $out .= '<a class="first ' . (!empty($classes['next_link_page']) ? $classes['next_link_page'] : '') . '"  style="cursor:pointer" onclick="javascript:'.str_replace('#page#',1,$jsFuncCoe).' ">' . ($first_page_text ? $first_page_text : 1) . '</a> ';
        }
        if( $dotright_text && $start_page != 2 ) $out .= '<span class="extend">'. $dotright_text .'</span> ';
    }
    // пагинация                            next_link_page
    for( $i = $start_page; $i <= $end_page; $i++ ) {
        if( $i == $paged )
            $out .= '<span class="' . (!empty($classes['current_page']) ? $classes['current_page'] : '') . '">'.$i.'</span> ';
        elseif( $i == 1 ) {
            if ( empty($jsFuncCoe) ) {
                $out .= '<a class="' . (!empty($classes['next_link_page']) ? $classes['next_link_page'] : '') . '" href="' . $first_url . '">1</a> ';
            } else {
                $out .= '<a  class="' . (!empty($classes['next_link_page']) ? $classes['next_link_page'] : '') . ' style="cursor:pointer" onclick="javascript:'.str_replace('#page#',$i,$jsFuncCoe).'" >1</a> ';
            }
        }
        else {
            if ( empty($jsFuncCoe) ) {
                $out .= '<a  class="' . (!empty($classes['next_link_page']) ? $classes['next_link_page'] : '') . ' href="' . str_replace('___', $i, $link_base) . '">' . $i . '</a> ';
            } else {
                $out .= '<a  class="' . (!empty($classes['next_link_page']) ? $classes['next_link_page'] : '') . ' style="cursor:pointer" onclick="javascript:'.str_replace('#page#',$i,$jsFuncCoe).'" >' . $i . '</a> ';
            }
        }
    }

    //ссылки с шагом
    $dd = 0;
    if ( $stepLink && $end_page < $max_page ){
        for( $i = $end_page+1; $i<=$max_page; $i++ ) {
            if( $i % $stepLink == 0 && $i !== $num_pages ) {
                if ( ++$dd == 1 )
                    $out.= '<span class="extend">'. $dotright_text2 .'</span> ';
                if ( empty($jsFuncCoe) ) {
                    $out .= '<a  class="' . (!empty($classes['next_link_page']) ? $classes['next_link_page'] : '') . ' href="' . str_replace('___', $i, $link_base) . '">' . $i . '</a> ';
                } else {
                    $out .= '<a  class="' . (!empty($classes['next_link_page']) ? $classes['next_link_page'] : '') . ' style="cursor:pointer" onclick="javascript:'.str_replace('#page#',$i,$jsFuncCoe).'" >' . $i . '</a> ';
                }
            }
        }
    }
    // в конец
    if ( $end_page < $max_page ) {
        if( $dotright_text && $end_page != ($max_page-1) )
            $out.= '<span class="extend">'. $dotright_text2 .'</span> ';
        if ( empty($jsFuncCoe) ) {
            $out .= '<a class="last ' . (!empty($classes['next_link_page']) ? $classes['next_link_page'] : '') . '" href="' . str_replace('___', $max_page, $link_base) . '">' . ($last_page_text ? $last_page_text : $max_page) . '</a> ';
        } else {
            $out .= '<a class="last ' . (!empty($classes['next_link_page']) ? $classes['next_link_page'] : '') . '"  style="cursor:pointer" onclick="javascript:'.str_replace('#page#',$max_page,$jsFuncCoe).'" >' . ($last_page_text ? $last_page_text : $max_page) . '</a> ';
        }
    }
    // вперед
    if ( $nexttext && $paged != $end_page ) {
        if ( empty($jsFuncCoe) ) {
            $out .= '<a class="next ' . (!empty($classes['next_link_page']) ? $classes['next_link_page'] : '') . '" href="' . str_replace('___', ($paged + 1), $link_base) . '">' . $nexttext . '</a> ';
        } else {
            $out .= '<a class="next"  style="cursor:pointer" onclick="javascript:'.str_replace('#page#',($paged + 1),$jsFuncCoe).'" >' . $nexttext . '</a> ';
        }
    }

    $out .= "</" . ( $showDiv ? "div " . (!empty($classes['next_link_page']) ? $classes['next_link_page'] : '') . "" : "span").">". $after ."\n";

    if ( ! $echo )
        return $out;
    echo $out;
} // protected function makePageNavigation( $paged, $posts_per_page, $max_page, $before = '', $after = '', $echo = true, $jsFuncCoe= '', $showDiv= false )