<?php
if (!defined('NSN_WooExtSearch')) {
    exit; // Exit if accessed directly
}

class nsnWooToolsData
{

    //286, 274, 280
    public static function insert_test_users( $users_count= 10, $users_role= 'contributor' ) {
        $ret_users_id= '';
        for($i= 1; $i<= $users_count; $i++) {
            $username= 'test_user_'.$i.'_';
            $user_email= $username . '@mail.com';
//            if (null == username_exists($user_email)) {
            if ( username_exists($username) == null and email_exists($user_email) == null ) {

                $password = $username;
                $user_id = wp_create_user($username, $password, $user_email);
//                echo '<pre>$user_id::'.print_r($user_id,true).'</pre>';
                if ( !$user_id ) return false;

                // Set the nickname
                wp_update_user(
                    array(
                        'ID' => $user_id,
                        'nickname' =>  $username.'_nick',
                        'user_url' =>  'http://' . $username . '_url.com',
                        'user_status' => 0
                    )
                );

                // Set the role
                $user = new WP_User($user_id);
                $users_role= 'contributor';
                $a= array('editor', 'author', 'contributor', 'subscriber');
                $i= rand(0,count($a)-1);
                if (!empty($a[$i])) {
                    $users_role= $a[$i];
                }

                $user->set_role($users_role);

                // Email the user
//                wp_mail($user_email, 'Welcome!', 'Your Password: ' . $password);

            } // end if
        }
    }
    public static function delete_product_comments($skip_comment_id= '') {
//            $comment_id = comment_ID();
        $args = array ('post_type' => 'product', 'orderby'=> 'comment_ID', 'order'=> 'ASC' );
        $product_comments_list = get_comments( $args );
//        echo '<pre>++$skip_comment_id::'.print_r($skip_comment_id,true).'</pre>';
//        echo '<pre>'.count($product_comments_list).'::$product_comments_list::'.print_r($product_comments_list,true).'</pre>';
        foreach( $product_comments_list as $next_key=> $next_product_comment ) {
//            echo '<pre>$next_product_comment->comment_ID::'.print_r($next_product_comment->comment_ID,true).'</pre>';
            if ($next_product_comment->comment_ID <= (int)$skip_comment_id ) continue;
            wp_delete_comment( $next_product_comment->comment_ID, true );
        }
//        die("-1 XXZ _product_comments");
    }

    public static function insert_product_comments($post_id, $users_list, $add_rating = true, $rand_max= 1)
    {
        echo '<pre>insert_product_comments  $post_id::'.print_r($post_id,true).'</pre>';
//        if ($post_id <= 286) return false; // already filled !
        $ret_array= array();
        $time = current_time('mysql');
        $post= get_post($post_id);
        if (empty($post)) return false;

//        $current_user= wp_get_current_user();
//        echo '<pre>$current_user::'.print_r($current_user,true).'</pre>';
//        echo '<pre>$_SERVER::'.print_r($_SERVER,true).'</pre>';

        $rand= rand(1,$rand_max);
//        echo '<pre>!!$rand::'.print_r($rand,true).'</pre>';
        for ($i= 1; $i<= $rand; $i++) {
//            echo '<pre>$i::'.print_r($i,true).'</pre>';

            $user_index= rand( 0, count($users_list) - 1 );
//            echo '<pre>$user_index::'.print_r($user_index,true).'</pre>';
            if (!empty( $users_list[$user_index] ) and is_object($users_list[$user_index])) {
                $current_user= $users_list[$user_index];
            } else {
                $current_user= wp_get_current_user();
            }

            $user_nicename= ( !empty($current_user->user_nicename) ? $current_user->user_nicename: 'admin' );
            $user_email= ( !empty($current_user->user_email) ? $current_user->user_email: 'admin@site.com' );
            $data = array(
                'comment_post_ID' => $post_id,
                'comment_author' => $user_nicename ,
                'comment_author_email' => $user_email,
                'comment_author_url' => '',
                'comment_content' => ' Review # '.$i." for product '".$post->post_title."' by <b>".$user_nicename."</b>(".$user_email.") : ".nsnWooToolsData::loremIpsumText(),
                'comment_type' => '',
                'comment_parent' => 0,
                'user_id' => ( !empty($current_user->ID) ? $current_user->ID : 1 ),
                'comment_author_IP' => (!empty($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR']: '' ),
                'comment_agent' => (!empty($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT']: '' ),
                'comment_date' => $time,
                'comment_approved' => 1,
            );


//            echo '<pre>$data::'.print_r($data,true).'</pre>';
            $comment_id= wp_insert_comment($data);
//            echo '<pre>$comment_id::'.print_r($comment_id,true).'</pre>';
            $rating_count= rand(0,5);
            if ( $rating_count > 0 and $add_rating ) {
                $ret_comment= add_comment_meta($comment_id, 'rating', $rating_count);
//                echo '<pre>$ret_comment::'.print_r($ret_comment,true).'</pre>';

            }


            if ( $comment_id > 0 ) {
                $parent_rand= rand(1,$rand_max);
//                echo '<pre>$parent_rand::'.print_r($parent_rand,true).'</pre>';
                for ($j= 1; $j<= $parent_rand; $j++) {
//                    echo '<pre>$j::' . print_r($j, true) . '</pre>';
                    $user_index= rand( 0, count($users_list) - 1 );
//                    echo '<pre>$user_index::'.print_r($user_index,true).'</pre>';
                    if (!empty( $users_list[$user_index] ) and is_object($users_list[$user_index])) {
                        $current_user= $users_list[$user_index];
                    } else {
                        $current_user= wp_get_current_user();
                    }
                    $user_nicename= ( !empty($current_user->user_nicename) ? $current_user->user_nicename: 'admin' );
                    $user_email= ( !empty($current_user->user_email) ? $current_user->user_email: 'admin@site.com' );
                    $child_data = array(
                        'comment_post_ID' => $post_id,
//                        'comment_author' => ( !empty($current_user->user_nicename) ? $current_user->user_nicename: 'admin' ),
//                        'comment_author_email' => ( !empty($current_user->user_email) ? $current_user->user_email: 'admin@admin.com' ),
                        'comment_author' => $user_nicename ,
                        'comment_author_email' => $user_email,
                        'comment_author_url' => '',
                        'comment_content' => 'Feedback for review '.$j.' Review # '.$i." ( #".$comment_id.") for product '".$post->post_title . "' by <b>".$user_nicename."</b>(".$user_email.") : ".nsnWooToolsData::loremIpsumText(),
                        'comment_type' => '',
                        'comment_parent' => $comment_id,
                        'user_id' => ( !empty($current_user->ID) ? $current_user->ID : 1 ),
                        'comment_author_IP' => (!empty($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR']: '' ),
                        'comment_agent' => (!empty($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT']: '' ),
                        'comment_date' => $time,
                        'comment_approved' => 1,
                    );


//                    echo '<pre>$child_data::'.print_r($child_data,true).'</pre>';
                    $child_comment_id= wp_insert_comment($child_data);
//                    echo '<pre>$child_comment_id::'.print_r($child_comment_id,true).'</pre>';
                    $rating_count= rand(0,5);
                    if ( $rating_count > 0 and $add_rating ) {
//                        echo '<pre>INSIDE $rating_count::'.print_r($rating_count,true).'</pre>';
                        $ret_child_comment= add_comment_meta($child_comment_id, 'rating', $rating_count);
//                        echo '<pre>$ret_child_comment::'.print_r($ret_child_comment,true).'</pre>';
                    }

                }
            }


        }
        return $ret_array;
    }


    public static function loremIpsumText($number = 1, $p_wrapping= false, $initial_text= '')
    {
        if ($number<= 1 or !is_numeric($number)) {
            return ( $p_wrapping ? '<p>' : '' ) . $initial_text . 'Lorem ipsum dolor sit amet,<br> consectetur adipiscing elit, sed do eiusmod  tempor<br> incididunt ut labore.';
        }

        $ret_str= '';
        if ( is_numeric($number) and $number > 1) {
            for($i=0; $i< $number;$i++) {
                $ret_str.= ( $p_wrapping ? '<p>' : '' ) . ($i==0? $initial_text : '') . 'L,<br> consectetur adipiscing elit, sed do eiusmod  tempor<br> incididunt ut labore.'. ( $p_wrapping ? '</p>' : '' );
            }
        }
        return $ret_str;
    }

    public static function preg_split($splitter, $string_items, $skip_empty = true)
    {
        $ret_array = array();
        $a = preg_split($splitter, $string_items);
        foreach ($a as $next_key => $next_value) {
            if ($skip_empty and empty($next_value)) continue;
            $ret_array[] = $next_value;
        }
        return $ret_array;
    }

    public static function add_products_reviews($repeat_count= 5) {
//        nsnWooToolsData::delete_product_comments(41);
//        nsnWooToolsData::insert_test_users(10);
//        nsnWooToolsData::add_products_reviews(6);

        $users_list= get_users( array( 'orderby'    => 'ID', 'order'        => 'ASC' ) );
//        echo '<pre>$users_list::'.print_r($users_list,true).'</pre>';
        $categories_list= nsnWooToolsData::get_categories_list( array( 'taxonomy' => 'product_cat') );
        foreach( $categories_list as $next_category ) {
//            echo '<pre>$next_category::'.print_r($next_category,true).'</pre>';
            $posts_lists= nsnWooToolsData::get_woo_commerce_products_by_category_id($next_category['ID']);
//            echo '<pre>$posts_lists::'.print_r($posts_lists,true).'</pre>';
            foreach( $posts_lists as $next_post ) {
                $ret= nsnWooToolsData::insert_product_comments($next_post->ID, $users_list, true, $repeat_count);
            }
        }

    }








    public static function prices_string_into_list($show_in_extended_search_price_ranges) {
        $list= preg_split( '/,/', $show_in_extended_search_price_ranges );
        $is_first= true;
        $min_value= '';
        $ret_array= array();
        foreach( $list as $next_key=> $next_value ) {
            if ( empty($next_value) ) continue;
            if ( $is_first ) {
                $min_value = 0.01;
            } else {
                $min_value = $min_value + 0.01;
            }
            $ret_array[]= array('min'=> $min_value, 'max'=> $next_value );
            $min_value= $next_value;
            $is_first= false;
        }
        return $ret_array;
    }

    public static function is_post($post) {
        return ( !empty($post) and is_a ( $post , "WP_Post" ) ) ;

    }

    public static function changeSubmitKey($str) {
        return str_replace(' ','_',$str) ;
    }

    public static function WWW($attr_name) {
        active_plugins();
    }

    public static function get_attr_value_slug_by_value($attr_name, $attr_value)
    {
        $products_attrs_list= nsnWooToolsData::get_products_attrs_list();
        foreach( $products_attrs_list as $next_product_attr ) {
            if ( $next_product_attr['attribute_name'] == $attr_name ) {
                $attrs_list= ( !empty($next_product_attr['attrs']) and is_array($next_product_attr['attrs']) ) ? $next_product_attr['attrs'] : array() ;
//                echo '<pre>$attrs_list::'.print_r($attrs_list,true).'</pre>';
                foreach( $attrs_list as $next_key=>$next_attr ) {
                    if ( $next_attr['name'] == trim($attr_value)) {
                        return $next_attr['slug'];
                    }
                }
            }
        }
        return '';
    }

    public static function get_attr_label_by_name($attr_name)
    {
        $products_attrs_list= nsnWooToolsData::get_products_attrs_list();
        foreach( $products_attrs_list as $next_product_attr ) {
            if ( $next_product_attr['attribute_name'] == $attr_name ) {
                return $next_product_attr['attribute_label'];
            }
        }
        return '';
    }

    public static function get_tags_list_by_product( $product, $return_only_name= true )
    {
        $tags_list= get_the_terms($product, 'product_tag', '', ',' );
        if (empty($tags_list) or !is_array($tags_list)) return array();

        if (!$return_only_name) return $tags_list;
        $ret_array= array();
        foreach( $tags_list as $next_key=>$next_product ) {
            $ret_array[]= array( 'name'=> $next_product->name, 'slug'=> $next_product->slug, 'id'=> $next_product->term_id );
        }
        return $ret_array;
    }

    public static function get_products_tags_list( $return_only_name= true )
    {
        $terms = get_terms( 'product_tag' );
        $term_array = array();
        if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
            foreach ( $terms as $term ) {
                if ($return_only_name) {
                    $term_array[] =  array( 'id'=>$term->term_id, 'name'=>$term->name, 'slug'=> $term->slug );
                }
                else {
                    $term_array[] =  $term;
                }
            }
        }
        return $term_array;
    }

    public static function get_products_attrs_list($attribute_name= '', $get_attrs= false) {
//        echo '<pre>$attribute_name::'.print_r($attribute_name,true).'</pre>';
        $attribute_taxonomies = wc_get_attribute_taxonomies();  //  Get attribute taxonomies.
        $ret_products_attrs_list = array();
        if ( $attribute_taxonomies ) :
            foreach ($attribute_taxonomies as $key=>$tax) :
                if (taxonomy_exists(wc_attribute_taxonomy_name($tax->attribute_name))) :
                    $next_tax_struct= get_object_vars( $tax );
                    $attrs_objects_list= get_terms( wc_attribute_taxonomy_name($tax->attribute_name), 'orderby=name&hide_empty=0' );
                    $attrs_list= array();
                    foreach( $attrs_objects_list as $next_attr_object ) {
                        $attrs_list[]= get_object_vars($next_attr_object);
                    }
                    $next_tax_struct['attrs'] = $attrs_list;
                    $ret_products_attrs_list[] = $next_tax_struct;
                endif;
            endforeach;
        endif;
        if( !empty($attribute_name) ) {
            foreach( $ret_products_attrs_list as $next_key=> $next_products_attr ) {
                if ( $next_products_attr['attribute_name']  ==  $attribute_name ) {
//                    echo '<pre>+++$next_products_attr::'.print_r($next_products_attr,true).'</pre>';
                    if ( $get_attrs and !empty($next_products_attr['attrs']) and is_array($next_products_attr['attrs']) ) {
                        $ret_array= array();
                        foreach($next_products_attr['attrs'] as $next_attr) {
                            $ret_array[] = array('ID'=>$next_attr['term_id'], 'name'=>$next_attr['name'], 'slug'=>$next_attr['slug']);
                        }
                        return $ret_array;
                    }
                    return $next_products_attr;
                }
            }
            //and !empty($ret_products_attrs_list[$attribute_name]) ) {
            return ( !empty($ret_products_attrs_list[$attribute_name]) ? $ret_products_attrs_list[$attribute_name] : array() );
        }
        return $ret_products_attrs_list;
    }

    public static function get_posts_by_attrs()
    {

        $paged= '';
        $meta_query= array($key="color", "value"=>"bluTTe");
        $query_color = array('key' => $key, 'value' => "blue");
//        $meta_query[] = $query_color;
        $args=array('meta_query'=>$meta_query/*,'tax_query'=>array($query_tax)*/,'posts_per_page' => 10,'post_type' => 'product', /*'orderby'=>$orderby,'order'=>$order ,'paged'=>$paged*/);

//where "$meta_query" is:

//$key="your_custom_key"; //custom_color for example
//$value="blue";//or red or any color
//$query_color = array('key' => $key, 'value' => $value);
//$meta_query[] = $query_color;
//
//and after that:

        $posts = query_posts($args);

//        $posts = get_posts(array(
//            'numberposts'	=> -1,
//            'post_type'		=> 'product',
//            'meta_key'		=> 'color',
//            'meta_value'	=> 'pa_blue'
//        ));
//        $posts = get_posts(array(
//            'numberposts'	=> -1,
////            'post_type'		=> 'product',
////            'meta_query'	=> array(
//                'relation'		=> 'AND',
//                array(
//                    'key'	 	=> 'color',
//                    'value'	  	=> array('Blue', 'Green'),
//                    'compare' 	=> 'IN',
//                ),
////                array(
////                    'key'	  	=> 'featured',
////                    'value'	  	=> '1',
////                    'compare' 	=> '=',
////                ),
//            ),
//        ));
//        echo '<pre>get_posts_by_attrs $posts::'.print_r($posts,true).'</pre>';
    }
    public static function get_posts_of_category($category_id, $only_headers= true, $post_type = 'post') {

        //query_posts( array( 'category__and' => array(1,3), 'posts_per_page' => 2, 'orderby' => 'title', 'order' => 'DESC' ) );

        $orderby= 'post_title';
        $order  = 'ASC';//'DESC';
        $post_status = 'publish';
        query_posts('category__and='.$category_id.'&post_type='.$post_type.'&showposts=-1&orderby='.$orderby.'&order=' . $order . '&post_status='.$post_status);//&term=' . $nextCatMusicJenre['category_id']); // ;post_type=post TODO
        $posts_array= array();
        while (have_posts()) :      // https://codex.wordpress.org/Class_Reference/WP_Query
            the_post();
            $postId = get_the_ID();
            //echo '<pre>$postId::'.print_r($postId,true).'</pre>';
            $nextPost= get_post($postId);
            if ( !empty($nextPost) ) {
                if ( $only_headers ) {
                    $posts_array[] = array('ID' => $nextPost->ID, 'title' => $nextPost->post_title);
                } else {
                    $posts_array[] = $nextPost;
                }
            }
        endwhile;
//        echo '<pre>$posts_array::'.print_r($posts_array,true).'</pre>';

        return $posts_array;
//        $args = array(
//            'posts_per_page'   => -1,
//            'category'         => $category_id,
//            'orderby'          => 'post_title',
//            'order'            => 'DESC',
//            'post_type'        => 'post',
//            'post_status'      => 'publish',
//            'suppress_filters' => false
//        );
////        define("get_posts_category_debugging","true");
//        $posts_array = get_posts( $args );
//        echo '<pre>get_posts_of_category $category_id::'.print_r($category_id,true).'</pre>';
//        echo '<pre>get_posts_of_category $posts_array::'.print_r($posts_array,true).'</pre>';
//        die("-1 XXZ");
//        return $posts_array;
    }

    public static function get_category_by_post_id($post_id, $taxonomy, $args= array(), $fields_saparator= ',')
    {          // get_all_keys
        $post_categories = wp_get_object_terms($post_id, $taxonomy, $args);
//        echo '<pre>$post_categories::'.print_r($post_categories,true).'</pre>';
        $rows_count= count($post_categories);
        if (empty($args)) return $post_categories;
        $ret_keys= array();
        $ret_titles= array();
        $ret_slugs= array();
        $field_values= '';
        if (!empty($args['get_all_keys'])) {
            for( $row= 0; $row< $rows_count; $row++ ) {
                $ret_keys[] = $post_categories[$row]->term_id;
                $ret_titles[] = $post_categories[$row]->name;
                $ret_slugs[] = $post_categories[$row]->slug;
            }
        }
        if (!empty($args['id'])) {
            for( $row= 0; $row< $rows_count; $row++ ) {
                $next_post_category = get_object_vars($post_categories[$row]);
                if ( !empty($next_post_category[$args['id']]) ) {
                    $field_values.= $next_post_category[$args['id']] .( $row< $rows_count-1 ? $fields_saparator : "" ) ;
                }
            }
        }
        return array('field_values'=>$field_values, 'ret_keys'=>$ret_keys, 'ret_titles'=> $ret_titles, 'ret_slugs'=> $ret_slugs );
    }

    public static function get_categories_list($args= array()) {
        $args = array(
            'type'                     => 'post',
            'child_of'                 => 0,
            'parent'                   => '',
            'orderby'                  => 'name',
            'order'                    => 'ASC',
            'hide_empty'               => 0,
            'hierarchical'             => 0,
            'exclude'                  => '',
            'include'                  => '',
            'number'                   => '',
            'taxonomy'                 =>  !empty( $args['taxonomy'] ) ? $args['taxonomy'] : 'category',
//            'taxonomy'                 => 'product_cat',
            'pad_counts'               => false
        );
        $categories_list= get_categories($args);
        // echo '<pre>+++$categories_list::'.print_r($categories_list,true).'</pre>';
        foreach( $categories_list as $next_category ) {
            $ret_array[]= array('ID'=>$next_category->term_id, 'name'=> $next_category->name);
        }
        return $ret_array;
    } // function categories_list($args= array()) {

    public static function get_woo_commerce_products_by_category_id($category_id)
    {
        $args = array(
            'post_type'             => 'product',
            'post_status'           => 'publish',
            'ignore_sticky_posts'   => -1,
            'posts_per_page'        => -1,
            'meta_query'            => array(
                array(
                    'key'           => '_visibility',
                    'value'         => array('catalog', 'visible'),
                    'compare'       => 'IN'
                )
            ),
            'tax_query'             => array(
                array(
                    'taxonomy'      => 'product_cat',
                    'field' => 'term_id', //This is optional, as it defaults to 'term_id'
                    'terms'         => $category_id,
                    'operator'      => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
                )
            )
        );
        $products_list = get_posts($args);
        return $products_list;
    }

    public static function get_woo_commerce_categories($args= array()) {
        $ret_array= array();
        $args = array(
            'type'                     => 'product',
            'child_of'                 => 0,
            'parent'                   => '',
            'orderby'                  => 'name',
            'order'                    => 'ASC',
            'hide_empty'               => 0,
            'hierarchical'             => 0,
            'exclude'                  => '',
            'include'                  => '',
            'number'                   => '',
            'taxonomy'                 => 'product_cat',
            'pad_counts'               => false

        );
        $categories_list= get_categories($args);
        // echo '<pre>+++$categories_list::'.print_r($categories_list,true).'</pre>';
        foreach( $categories_list as $next_category ) {
            $ret_array[]= array('ID'=>$next_category->term_id, 'name'=> $next_category->name);
        }
        return $ret_array;
    } // function get_woo_commerce_categories($args= array()) {

    //$next_product->ID, array( 'image', 'categories_list', 'sale_price', 'regular_price', '_sku', 'is_in_stock', '_purchase_note'), array('brand')
    public static function  get_woo_product_props($product, $props_list, $attrs_list= array(), $additive_attrs= array() )
    {
        //                $additive_attrs= array( 'woocommerce_price_format'=> get_woocommerce_price_format(), 'woocommerce_currency_symbol'=> get_woocommerce_currency_symbol( get_woocommerce_currency() ) );
        if ( empty($additive_attrs['woocommerce_price_format']) ) {
            $additive_attrs['woocommerce_price_format']= get_woocommerce_price_format();
        }
        if ( empty($additive_attrs['woocommerce_currency_symbol']) ) {
            $additive_attrs['woocommerce_currency_symbol']= get_woocommerce_currency_symbol( get_woocommerce_currency() );
        }
        if ( empty($additive_attrs['decimal_separator']) ) {
            $additive_attrs['decimal_separator']= wc_get_price_decimal_separator();
        }
        if ( empty($additive_attrs['thousand_separator']) ) {
            $additive_attrs['thousand_separator']= wc_get_price_thousand_separator();
        }
        if ( empty($additive_attrs['decimals']) ) {
            $additive_attrs['decimals']= wc_get_price_decimals();
        }

        if ( empty($additive_attrs['date_format']) ) {
            $additive_attrs['date_format']= get_option('date_format');
        }

        if ( empty($additive_attrs['time_format']) ) {
            $additive_attrs['time_format']= get_option('time_format');
        }

//        $decimal_separator  = wc_get_price_decimal_separator();
        //$thousand_separator = wc_get_price_thousand_separator();
//        $decimals           = wc_get_price_decimals();

//        echo '<pre>00$product::'.print_r(gettype($product),true).'</pre>';
        if ( gettype($product) == "integer" or gettype($product) == "string"  ) {
            $product = wc_get_product( $product );
        }

//        $ret_array = array();
//        echo '<pre>$product::'.print_r(gettype($product),true).'</pre>';
//        echo '<pre>++$product::'.print_r( $product,true).'</pre>';
//        typeof "next_woo_product"
        $ret_array=  (array)$product;
//        echo '<pre>-- $ret_array::'.print_r($ret_array,true).'</pre>';
        if (in_array('image',$props_list)) {
            $ret_array['image'] = $product->get_image();
            $image_id = $product->get_image_id();
            $attachment= get_post($image_id);
//            echo '<pre>$attachment::'.print_r($attachment,true).'</pre>';

//global $_wp_additional_image_sizes;
//echo '<pre>';
//print_r( $_wp_additional_image_sizes );
//echo '</pre>';

            //echo '<pre>self::get_global_image_sizes(::'.print_r(self::get_global_image_sizes(),true).'</pre>';
            if ( !empty($attachment->guid) ) {
//                $ret_array['image_url'] = $attachment->guid;
                $file_image_sizes_struct= self::get_file_image_sizes_struct( 'shop_thumbnail', $attachment->guid);
//                echo '<pre>$file_image_sizes_struct::'.print_r($file_image_sizes_struct,true).'</pre>';
                $ret_array['image_url'] = $file_image_sizes_struct['new_filename'];
//                die("-1 XXZ");
            }
        }

        foreach( $attrs_list as $next_attr ) {
            $next_attrs_list = wc_get_product_terms($product->id, 'pa_'.$next_attr, array('fields' => 'names'));
            $ret_array[$next_attr] = !empty($next_attrs_list[0]) ? $next_attrs_list[0] : '';
        }

        if (in_array('categories_list',$props_list)) {
            $categories_list = nsnWooToolsData::get_category_by_post_id( $product->id, 'product_cat', array('id'=>'name', 'get_all_keys'=>1));
            $ret_array['categories_list'] = $categories_list;
        }

        if (in_array('tags_list',$props_list)) {
            //get_tags_list_by_product( $product, $return_only_name= true )
            $tags_list = nsnWooToolsData::get_tags_list_by_product( $product->id );
            $ret_array['tags_list'] = $tags_list;
        }

        if (in_array('sale_price',$props_list)) {
            $ret_array['sale_price'] = $product->get_sale_price();

            $ret_array['formatted_sale_price'] = sprintf( $additive_attrs['woocommerce_price_format'], '<span class="woo_products_currencySymbol">'.$additive_attrs['woocommerce_currency_symbol'].'</span>', number_format( (float)$ret_array['sale_price'], $additive_attrs['decimals'], $additive_attrs['decimal_separator'], $additive_attrs['thousand_separator'] ) );

        }

        if (in_array('regular_price',$props_list)) {
            $ret_array['regular_price'] = $product->get_regular_price();
            $ret_array['formatted_regular_price'] = '<strike>' . sprintf( $additive_attrs['woocommerce_price_format'], '<span class="woo_products_currencySymbol">'.$additive_attrs['woocommerce_currency_symbol'].'</span>', number_format( (float)$ret_array['regular_price'], $additive_attrs['decimals'], $additive_attrs['decimal_separator'], $additive_attrs['thousand_separator'] ) ) . '</strike>';
        }

        if (in_array('sku',$props_list)) {
            $sku = get_post_meta($product->id, '_sku');
            $ret_array['sku'] = !empty($sku[0]) ? $sku[0] : '';
        }

        if (in_array('rating',$props_list)) {
//            $sku = get_post_meta($product->id, '_sku');
            $average = $product->get_average_rating();
//            echo '<pre>$average::'.print_r($average,true).'</pre>';
            $ret_array['rating_average'] = ceil( $average );
            $ret_array['star_rating'] = ( $average / 5 ) * 100;

            $args = array ('post_type' => 'product', 'post_id' => $product->id);
            $comments = get_comments( $args );
            $ret_array['comments_count'] = count($comments);

//            die("-1 XXZ");
        } /*$average = $product->get_average_rating();

    echo '<div class="star-rating"><span style="width:'.( ( $average / 5 ) * 100 ) . '%"><strong itemprop="ratingValue" class="rating">'.$average.'</strong> '.__( 'out of 5', 'woocommerce' ).'</span></div>';
}*/

        if (in_array('is_in_stock',$props_list)) {
            $ret_array['is_in_stock'] = $product->is_in_stock();
        }

        if (in_array('purchase_note',$props_list)) {
            $_purchase_note = get_post_meta($product->id, '_purchase_note');
            $ret_array['purchase_note'] = !empty($_purchase_note[0]) ? $_purchase_note[0] : '';;
        }

//        the_date ()
//'date_format'=> get_option('date_format'), 'time_format'=> get_option('time_format')
//echo '<pre>0date_format]::'.print_r($additive_attrs['date_format'],true).'</pre>';
//echo '<pre>-1::'.print_r($product->post->post_date,true).'</pre>';
//echo '<pre>-2::'.print_r(mysql2date( 'G',$product->post->post_date),true).'</pre>';
//        $tmp_post_date= mysql2date( 'G',$product->post->post_date);
//echo '<pre>-3 $tmp_post_date::'.print_r($tmp_post_date,true).'</pre>';
//
//
//        $formatted_post_date= strftime($additive_attrs['date_format'], $tmp_post_date);
//echo '<pre>-4 $formatted_post_date::'.print_r($formatted_post_date,true).'</pre>';
//echo '======';
//die("-1 XXZ");
//        $ret_array['post_date'] = $product->post->post_date;
//        $ret_array['formatted_post_date'] = strftime($additive_attrs['date_format'], mysql2date( 'G', $product->post->post_date ) ) .' '.strftime($additive_attrs['time_format'],
//                mysql2date( 'G', $product->post->post_date ) );
////        echo '<pre>++ $ret_array::'.print_r($ret_array,true).'</pre>';
        return $ret_array;
    }


    public static function get_file_image_sizes_struct( $size_icon, $source_filename )
    {
        $global_image_sizes_array= self::get_global_image_sizes();
        //непонятно откуда 1-2-180×180.jpg ?
//        echo '<pre>$global_image_sizes_array::'.print_r($global_image_sizes_array,true).'</pre>';
//        echo '<pre>$source_filename::'.print_r($source_filename,true).'</pre>';
        foreach( $global_image_sizes_array as $next_image_size_key => $next_image_size_struct ) {
            if ( $next_image_size_key == $size_icon ) {

                $ext= nsnClass_appFuncs::getFileNameExt($source_filename);
//                echo '<pre>$ext::'.print_r($ext,true).'</pre>';
                $base= nsnClass_appFuncs::getFileNameBase($source_filename);
//                echo '<pre>$base::'.print_r($base,true).'</pre>';

                $new_filename= $base . '-' . $next_image_size_struct['width'] . 'x' . $next_image_size_struct['height'] . '.'. $ext;
//                echo '<pre>$new_filename::'.print_r($new_filename,true).'</pre>';

                return array( 'width'=>$next_image_size_struct['width'], 'height'=>$next_image_size_struct['height'], 'new_filename' =>$new_filename );
            }
        }
        return false;
        /*rray
(
    [shop_thumbnail] => Array
        (
            [width] => 180
            [height] => 180
            [crop] => 1
        )  */
    }

    public static function get_global_image_sizes( $size = '' ) {

        global $_wp_additional_image_sizes;

        $sizes = array();
        $get_intermediate_image_sizes = get_intermediate_image_sizes();

        // Create the full array with sizes and crop info
        foreach( $get_intermediate_image_sizes as $_size ) {
            if ( in_array( $_size, array( 'thumbnail', 'medium', 'large' ) ) ) {
                $sizes[ $_size ]['width'] = get_option( $_size . '_size_w' );
                $sizes[ $_size ]['height'] = get_option( $_size . '_size_h' );
                $sizes[ $_size ]['crop'] = (bool) get_option( $_size . '_crop' );
            } elseif ( isset( $_wp_additional_image_sizes[ $_size ] ) ) {
                $sizes[ $_size ] = array(
                    'width' => $_wp_additional_image_sizes[ $_size ]['width'],
                    'height' => $_wp_additional_image_sizes[ $_size ]['height'],
                    'crop' =>  $_wp_additional_image_sizes[ $_size ]['crop']
                );
            }
        }

        // Get only 1 size if found
        if ( $size ) {
            if( isset( $sizes[ $size ] ) ) {
                return $sizes[ $size ];
            } else {
                return false;
            }
        }
        return $sizes;
    }


    public static function  get_woo_commerce_products($args= array()) {

        //echo '<pre>$args::'.print_r($args,true).'</pre>';
        $ret_array= array();
        $currency_symbol= get_woocommerce_currency_symbol( get_woocommerce_currency() );

        $products_args     = array( 'post_type' => 'product', 'post_status' => 'publish', 'numberposts'=>-1 );
        if ( !empty($args['sort']) ) {
            $products_args['orderby']= $args['sort'];
        }
        if ( !empty($args['sort_direction']) ) {
            $products_args['order']= $args['sort_direction'];
        }
/* 		'category' => 0, 'orderby' => 'date',
		'order' => 'DESC', 'include' => array(), */
        //echo '<pre>$products_args::'.print_r($products_args,true).'</pre>';
        $products_list = get_posts( $products_args );
        //echo '<pre>$products_list::'.print_r($products_list,true).'</pre>';

        if ( !empty( $args['type_of_selection']) and $args['type_of_selection'] == 'grouped' ) {
            $woo_commerce_categories= self::get_woo_commerce_categories();
            //echo '<pre>'.count($woo_commerce_categories).'::$woo_commerce_categories::'.print_r($woo_commerce_categories,true).'</pre>';
            foreach( $woo_commerce_categories as $next_woo_commerce_category ) {
                $related_posts_list = array();
                reset($products_list);
                foreach( $products_list as $nextProduct ) {
                    $post_categories = wp_get_object_terms($nextProduct->ID, 'product_cat');
                    $is_found= false;
                    foreach( $post_categories as $next_post_category ) {
                        if ( $next_post_category->term_id== $next_woo_commerce_category['ID'] ) {
                            $is_found= true;
                            break;
                        }
                    }
                    if ($is_found) {
                        $price = nsnWooToolsData::get_price_by_type(get_post_meta( $nextProduct->ID, '_sale_price'));
                        // echo '<pre>---$price::'.print_r($price,true).'</pre>';

                        $_regular_price = nsnWooToolsData::get_price_by_type(get_post_meta( $nextProduct->ID, '_regular_price'));
                        if (!empty($_regular_price) and empty($price)) {
                            $price= $_regular_price;
                        }
                        $formatted_price= sprintf( get_woocommerce_price_format(), $currency_symbol, $price );

                        $next_category_name= '';
                        $category_array = nsnWooToolsData::get_category_by_post_id( $nextProduct->ID, 'product_cat', array('id'=>'name', 'get_all_keys'=>1));
                        //echo '<pre>$category_array::'.print_r($category_array,true).'</pre>';
                        if ( !empty($category_array['field_values']) ) {
                            $next_category_name= $category_array['field_values'];
                        }

                        $related_posts_list[$nextProduct->ID] = $nextProduct->post_title . ', ' . $formatted_price . ' (' . $next_category_name . ')' ;
                    }
                }
                //echo '<pre>$related_posts_list::'.print_r($related_posts_list,true).'</pre>';
                $ret_array[] = array( 'group_id'=> $next_woo_commerce_category['ID'], 'group_name'=> $next_woo_commerce_category['name'], 'data_sub_array'=>$related_posts_list );

            }
            return $ret_array;
        } //             if ( !empty( $args['type_of_selection']) and $args['type_of_selection'] == 'grouped' ) {

        if ( empty( $args['type_of_selection']) or $args['type_of_selection'] == 'dropdown' ) {
            //echo '<pre>'.count($products_list).'::$products_list::'.print_r($products_list,true).'</pre>';
            foreach( $products_list as $nextProduct ) {
                $price = nsnWooToolsData::get_price_by_type(get_post_meta( $nextProduct->ID, '_sale_price'));
                // echo '<pre>---$price::'.print_r($price,true).'</pre>';

                $_regular_price = nsnWooToolsData::get_price_by_type(get_post_meta( $nextProduct->ID, '_regular_price'));
                if (!empty($_regular_price) and empty($price)) {
                    $price= $_regular_price;
                }
                $formatted_price= sprintf( get_woocommerce_price_format(), $currency_symbol, $price );

                $next_category_name= '';
                $category_array = nsnWooToolsData::get_category_by_post_id( $nextProduct->ID, 'product_cat', array('id'=>'name', 'get_all_keys'=>1));
                //echo '<pre>$category_array::'.print_r($category_array,true).'</pre>';
                if ( !empty($category_array['field_values']) ) {
                    $next_category_name= $category_array['field_values'];
                }

                $ret_array[] = array( 'ID' => $nextProduct->ID, 'title' => $nextProduct->post_title . ', ' . $formatted_price . ' (' . $next_category_name . ')' );
            }
            //echo '<pre>$ret_array::'.print_r($ret_array,true).'</pre>';
            return $ret_array;
        } //             if ( !empty( $args['type_of_selection']) and $args['type_of_selection'] == 'grouped' ) {


        query_posts('post_type=product&showposts=-1&');//&term=' . $nextCatMusicJenre['category_id']); // ;post_type=post TODO
        $ret_array= array();
        while (have_posts()) :
            the_post();
            $postId = get_the_ID();
            //echo '<pre>$postId::'.print_r($postId,true).'</pre>';
            $nextPost= get_post($postId);
            if ( !empty($nextPost) ) {
                $ret_array[] = array( 'ID'=> $nextPost->ID, 'title'=> $nextPost->post_title );
            }
        endwhile;
        return $ret_array;
    } // function get_woo_commerce_products($args= array()) {


    public static function get_price_by_type($price) {
        //echo '<pre>???$price::'.print_r($price,true).'</pre>';
        if (is_scalar($price) ) {
            return $price;
        }
        if (is_array($price)) {
            $zero_key_value= '';
            $name_key_value= '';
            foreach( $price as $next_key=>$next_value ) {
                // echo '<pre>$next_key::'.print_r($next_key,true).'</pre>';
                // echo '<pre>$next_value::'.print_r($next_value,true).'</pre>';
                if ($next_key== 0) {
                    $zero_key_value= $next_value;
                }
                if ( is_string($next_key) and $next_key == 'price') {
                    //die("-1 INSU");
                    $name_key_value= $next_value;
                }
            }
        }
        // echo '<pre>$zero_key_value::'.print_r($zero_key_value,true).'</pre>';
        // echo '<pre>$name_key_value::'.print_r($name_key_value,true).'</pre>';
        if ( !empty($name_key_value) and empty($zero_key_value)) {
            return $name_key_value;
        }
        if ( !empty($zero_key_value ) and empty($name_key_value)) {
            return $zero_key_value;
        }
        if ( !empty($zero_key_value ) and !empty($name_key_value)) {
            return $name_key_value;
        }

    }

}// class AppUtils {

?>