<?php
if (!defined('NSN_WooExtSearch')) {
    exit; // Exit if accessed directly
}

class nsnClass_appFuncs
{

    private static $DATE_FORMAT = '%B %d, %Y';
    private static $MONTH_AS_TEXT_YEAR_DATE_FORMAT = '%B %Y';
    private static $DateTimeFormat = '%Y-%m-%d %H:%M';
    private static $DateTimeAsTextFormat = '%B %d, %Y %H:%M%p';
    private static $DateAsTextFormat = '%B %d, %Y';
    private static $DateTimeMySqlFormat = '%Y-%m-%d %H:%M:%S';
    private static $YesNoValueArray = array(0 => 'No', 1 => 'Yes');
    private static $MinValidYear = 2000;
    private static $MaxValidYear = 2050;
    private static $DatePickerSelectionFormat = 'mm-dd-yy';
    private static $ShortDateFormat = '%Y-%m-%d';


    public static function showAdminMessages($message, $errormsg = false)
    {
        showAdminMessages($message, $errormsg);
    }

    public static function getJenreIdByJenreName($jenreName, $jenresList)
    {
        foreach ($jenresList as $nextJenreKey => $nextJenreValue) {
            if (strtoupper(trim($nextJenreValue)) == strtoupper(trim($jenreName))) {
                return $nextJenreKey;
            }
        }
        return '';
    } // public static function cutSecondsFromDateTimeString( $value )

    public static function cutSecondsFromDateTimeString($value)
    { // "2015-06-20 10:05:06"
        $a = preg_split('/\:/', $value);
        if (count($a) == 3) {
            return $a[0] . ':' . $a[1];
        }
        return $value;
    } // public static function cutSecondsFromDateTimeString( $value )

    // $sort_direction = nsnClass_appFuncs::getLimitedValue( $sort_direction, array('asc', 'desc'), 'asc' );
    // if $value not in 1 elements of $limitedValuesArray then return $defaultValue
    public static function getLimitedValue($value, $limitedValuesArray, $defaultValue)
    {
        $value = trim($value);
        /*echo '<pre>$value::'.print_r($value,true).'</pre>';
        echo '<pre>$limitedValuesArray::'.print_r($limitedValuesArray,true).'</pre>';
        echo '<pre>$defaultValue::'.print_r($defaultValue,true).'</pre>';  */
        if (!is_array($limitedValuesArray)) return $value;
        if (!in_array($value, $limitedValuesArray)) return $defaultValue;
        //die("-1LAST");
        return $value;
    }

    public static function stripErrorCode($error_text)
    {
        //echo '<pre>$error_text::'.print_r($error_text,true).'</pre>';
        if (empty($error_text)) return '';
        $error_text = htmlspecialchars_decode(strip_tags($error_text));
        //echo '++<pre>$error_text::'.print_r($error_text,true).'</pre>';
        $error_text = str_replace(array("&amp;", "\r", "\n"/*, "\t" */), "", $error_text);
        $error_text = str_replace(array("\t"), " ", $error_text);
        $error_text = str_replace(array("&#039;"), "'", $error_text);
        return $error_text;
    }

    public static function addLog($error_text, $error_from, $error_type, $wpdb)
    {

        $current_user_id = get_current_user_id();
        $newLog = array('user_id' => isset($current_user_id) ? $current_user_id : null, 'error_text' => $error_text, 'error_from' => $error_from, 'error_type' => $error_type);
        $LogsTblSource = new Logs($wpdb);
        $LogsTblSource->updateLog('', $newLog);
        return $error_text;
    }

    public static function generateTimestampAmplitude( $fromTime = null, $dayAamplitude = 30, $returnTimestamp = false, $returnOnlyDate= false )
    {
        if (empty($fromTime) /* or ($fromTime instanceof DateTime) */ ) {
            $fromTime = time();
        }
        if ($fromTime == "add_1" ) {
            $day = date('d');
            $year = date('Y');
            $month = date('m');
        } else {
            $day = date('d', $fromTime);
            $year = date('Y', $fromTime);
            $month = date('m', $fromTime);
        }
        //DebToFileSQL('--generateTimestampAmplitude $fromTime! ::' .print_r($fromTime,true) );
        //DebToFileSQL('--generateTimestampAmplitude $day! ::' .print_r($day,true) );
        if ( $fromTime == "add_1" ) {
            //DebToFileSQL('-++INSIDE generateTimestampAmplitude $day! ::' .print_r($day,true) );
            $day = (int)$day + 1;
        } else {
        }
        //DebToFileSQL('-++generateTimestampAmplitude $day! ::' .print_r($day,true) );
        $retTime = mktime(rand(-23, 23), rand(-59, 59), rand(-59, 59), $month, $day + rand(-$dayAamplitude, $dayAamplitude), $year);
        if (!$returnOnlyDate) {
            return ($returnTimestamp ? $retTime : strftime(self::$DateTimeMySqlFormat, $retTime));
        } else {
            return ($returnTimestamp ? $retTime : strftime(self::$ShortDateFormat, $retTime));
        }
    }

    //             $app_params= nsnClass_appFuncs::prepareParams( true,  array(  ) );
    public static function prepareParams($commonParams = true, $paramsArray = array(), $return_array= true)
    {
        $resArray = array();
        if ($commonParams) {
            $resArray['logged_user_id'] = get_current_user_id();
            $resArray['datePickerSelectionFormat'] = getAppVar('datePickerSelectionFormat');
            $resArray['plugin_url'] = getAppVar('plugin_url');

        }
        if (isset($paramsArray) and is_array($paramsArray)) {
            foreach ($paramsArray as $nextParamKey => $nextParamValue) {
                $resArray[$nextParamKey] = $nextParamValue;
            }
        }
        if ( $return_array ) return $resArray;
        $resCode = '';
        foreach ($resArray as $nextParamKey => $nextParamValue) {
            $resCode .= '  ' . $nextParamKey . ' : ' . "'" . $nextParamValue . "', ";
        }

        return $resCode;
    }

    /**
     * form string $S trim substring at the right end if it equals $Substr
     */
    public static function TrimRightSubString($S, $Substr)
    {
        $Res = preg_match('/(.*?)(' . preg_quote($Substr, "/") . ')$/si', $S, $A);
        if (isset($A[1]))
            return $A[1];
        return $S;
    }

    public static function htmlspecialcharsArray($arr)
    {
        $resArray = array();
        foreach ($arr as $nextKey => $nextValue) {
            $resArray[$nextKey] = htmlspecialchars($nextValue);
        }
        return $resArray;
    }

    public static function concatStr($str, $max_length = '', $add_str = '...', $strip_tags = true)
    {
        if ($strip_tags) {
            $str = self::removeNewLineMore1Space(strip_tags($str));  // !!!check    strip_tags
        }
        $str = self::limitChars($str, $max_length, $add_str);
        return $str;

    }


    public static function getRightSubstring($S, $count)
    {
        return substr($S,strlen($S)-$count,$count);
    }


    public static function concat_conditional_values($values_array, $splitter= '', $default_value='')
    {
        $ret= '';
        $have_values = false;
//        echo '<pre>$values_array::'.print_r($values_array,true).'</pre>';
        foreach( $values_array as $next_key=>$next_value ) {
            if ( $next_value['condition'] ) {
                $have_values = true;
                $ret.= $next_value['value'] . $splitter;
            }
        }
        if ( empty($have_values) ) {
            $ret = $default_value;
        }
        $ret= nsnClass_appFuncs::trimRightSubString($ret, $splitter);
        return $ret;
    }

    public static function removeNewLineMore1Space($text)   { // ????  Например, нам необходимо "сжать" текст, убрав из него все лишние пробелы и символы перевода строки:
        return $text;
        //$text= preg_replace(array( "/\s\s+/i"  /*, "/\n/i", "/\r/i"*/ ), " ", $text);
        //$text= preg_replace( "/\s\s+/i", " ", $text);
        //echo '<pre>$text::'.print_r($text,true).'</pre>';
        return $text;
    }

    public static function strip_tags_content($text, $tags = '', $invert = FALSE) {

        preg_match_all('/<(.+?)[\s]*\/?[\s]*>/si', trim($tags), $tags);
        $tags = array_unique($tags[1]);

        if(is_array($tags) AND count($tags) > 0) {
            if($invert == FALSE) {
                return preg_replace('@<(?!(?:'. implode('|', $tags) .')\b)(\w+)\b.*?>.*?</\1>@si', '', $text);
            }
            else {
                return preg_replace('@<('. implode('|', $tags) .')\b.*?>.*?</\1>@si', '', $text);
            }
        }
        elseif($invert == FALSE) {
            return preg_replace('@<(\w+)\b.*?>.*?</\1>@si', '', $text);
        }
        return $text;
    }

    public static function removeHTMLCodes($document= '')
    {
        $search = array("'<script[^>]*?>.*?</script>'si",  // Strip out javascript
            "'<[\/\!]*?[^<>]*?>'si",           // Strip out html tags
            "'([\r\n])[\s]+'",                 // Strip out white space
            "'&(quot #34);'i",                 // Replace html entities
            "'&(amp #38);'i",
            "'&(lt #60);'i",
            "'&(gt #62);'i",
            "'&(nbsp #160);'i",
            "'&(iexcl #161);'i",
            "'&(cent #162);'i",
            "'&(pound #163);'i",
            "'&(copy #169);'i",
            "'&#(\d+);'e");                    // evaluate as php
        $replace = array("",
            "",
            "\\1",
            "\"",
            "&",
            "<",
            ">",
            " ",
            chr(161),
            chr(162),
            chr(163),
            chr(169),
            "chr(\\1)");
        $text = preg_replace($search, $replace, $document);
        echo $text;
    }


    public static function concatArrayAsString($arrData, $splitter = ', ', $skipEmptyItems = true, $hideLastSplitter = true)
    {
        $l = count($arrData);
        $retStr = '';
        for ($i = 0; $i < $l; $i++) {
            if ($skipEmptyItems and empty($arrData[$i])) continue;
            $retStr .= $arrData[$i] . (($i == $l - 1 and $hideLastSplitter) ? '' : $splitter);
        }
        return $retStr;
    }

    public static function getLSepr($str, $splitter = ',')
    {
        $A = preg_split('/' . $splitter . '/', $str);
        if (count($A) == 2) return $A[0];
        return $str;
    }

    public static function getRSepr($str, $splitter = ',')
    {
        $A = preg_split('/' . $splitter . '/', $str);
        //echo '<pre>$A::'.print_r($A,true).'</pre>';
        if (count($A) == 2) return $A[1];
        return '';
    }

    public static function splitStringIntoArray($str, $splitter = ',', $keySplitter = '', $skip_empty = true)
    {
        $A = preg_split('/' . $splitter . '/', $str);
        // echo '<pre>00$A::'.print_r($A,true).'</pre>';
        if (empty($keySplitter)) return $A;
        $ResArray = array();
        foreach ($A as $key => $value) {
            if (empty($value) and $skip_empty) continue;
            //echo '<pre>$keySplitter::'.print_r($keySplitter,true).'</pre>';
            if (empty($keySplitter)) {
                $ResArray[] = trim($value);
            } else {
                $A_1 = preg_split('/' . $keySplitter . '/', $value);
                //echo '<pre>$A_1::'.print_r($A_1,true).'</pre>';
                if (count($A_1) == 2) {
                    $ResArray[trim($A_1[0])] = trim($A_1[1]);
                }
            }
        }
        return $ResArray;
    }


    public static function clearLastChar($s, $splitter = ', ')
    {
        $s = trim($s);
        $l = strlen($s);
        if (substr($s, $l - 1, 1) == $splitter) {
            $s = substr($s, 0, $l - 1);
        }
        return $s;
    }

    public static function showArrayAsString($dataArray, $splitter = ', ')
    {
        if (!is_array($dataArray)) return '';
        $ret = '';
        foreach ($dataArray as $nextValue) {
            // echo '<pre>$nextValue::'.print_r($nextValue,true).'</pre>';
            $nextValue = trim($nextValue);
            if (isset($nextValue)) {
                $ret .= $nextValue . $splitter;
            }
        }
        return $ret;
    }// showArrayAsString

    public static function getTrimmedArray($dataArray)
    {
        if (!is_array($dataArray)) return false;
        $resArray = array();
        foreach ($dataArray as $key => $value) {
            $resArray[$key] = trim($value);
        }
        return $resArray;
    }

    public static function getMySqlInsertId($link)
    {
        return mysqli_insert_id($link);
    }

    public static function getNextParameter($UriArray, $ParameterName)
    {
        $is_get_next_key = false;
        foreach ($UriArray as $key => $value) {
            if ($is_get_next_key) {
                return $key;
            }
            if (strtolower(trim($key)) == strtolower(trim($ParameterName))) {
                return $value;
            }
            if (strtolower(trim($value)) == strtolower(trim($ParameterName))) {
                $is_get_next_key = true;
            }
        }

    }

    //    $sorting_link= nsnClass_appFuncs::getLinkParameter( $this->uri->uri_to_assoc(1), $this->config->config['base_url'], array('page'), 'sorting', '' );
    public static function getLinkParameter($UriArray, $base_url, $params_to_exclude, $param_name_toadd = '', $param_value_toadd = '')
    {
        $result_uri = '';
        foreach ($UriArray as $param_name => $param_value) {
            if (in_array($param_name, $params_to_exclude)) continue;
            $result_uri .= $param_name . '/' . $param_value . '/';
        }
        if (isset($param_name_toadd)) $result_uri .= $param_name_toadd . '/' . $param_value_toadd;
        return $base_url . '/' . $result_uri;
    }


    public static function getPageLink($uri, $next_page, $NumberOfPages, $base_url)
    {
        $Pattern = '/\/page\//xsi';
        $A = preg_split($Pattern, $uri);
        if (count($A) == 1) {
            $current_page = 1;
        } else {
            $current_page = $A[1];
        }
        if ($next_page) {
            $current_page++;
            if ($current_page > $NumberOfPages) $current_page = $NumberOfPages;
        } else {
            $current_page--;
            if ($current_page < 1) $current_page = 1;
        }
        if (count($A) == 1) {
            return $base_url . '/' . $uri . '/page/' . $current_page;
        } else {
            return $base_url . '/' . $A[0] . '/page/' . $current_page;
        }
    }

    public static function getDateFormat()
    {
        return self::$DATE_FORMAT;
    }

    public static function getDateTimeMySqlFormat()
    {
        return self::$DateTimeMySqlFormat;
    }

    public static function getDatePickerSelectionFormat()
    {
        return self::$DatePickerSelectionFormat;
    }

    public static function convirtDateToMySqlFormat($stringDate)
    { // 12-14-2012
        $A = preg_split("/-/", $stringDate);
        if (count($A) == 3) {
            return self::getFormattedDateTime(mktime(null, null, null, $A[0], $A[1], $A[2]), "MYSQL");
        }
    }

    public static function convertFromMySqlToStamp($stringDate, $splitter = '-', $outputFormat = '')
    { // 	2012-12-28
        if (empty($stringDate)) return time();
        $A = preg_split("/" . $splitter . "/", $stringDate);
        if (count($A) == 3) { // 2016-09-13
            //DebToFile('++22 convertFromMySqlToStamp $outputFormat::' . print_r($outputFormat, true));
            if (empty($outputFormat)) {
                //DebToFile('++22 convertFromMySqlToStamp $A::' . print_r($A, true));
                return mktime(null, null, null, $A[2], $A[1], $A[0]);
                //return $A[1] . '/' . $A[2] . '/' . $A[0];
            }
        }
    }

    public static function IsValidDate($StringDate)
    { // 12-14-2012
        $A = preg_split("/\//", $StringDate);
        if (count($A) != 3)
            return false;
        $Day = (int)$A[1];
        $Month = (int)$A[0];
        $Year = (int)$A[2];
        if (!($Year >= self::$MinValidYear and $Year <= self::$MaxValidYear))
            return false;
        if (!($Month >= 1 and $Month <= 12))
            return false;
        if (!($Day >= 1 and $Day <= 31))
            return false;
        $DaysInMonth = self::DaysInMonth($Year, $Month);
        if (!($DaysInMonth > 0 and $Day <= $DaysInMonth))
            return false;
        return true;
    }

    public static function unixDateTimeAddDay($dat, $DaysToAdd) {
        $StartYear = (int) strftime('%Y', $dat);
        $StartMonth = (int) strftime('%m', $dat);
        $StartDay = (int) strftime('%d', $dat);
        $StartHour = (int) strftime("%H", $dat);
        $StartMinutes = (int) strftime("%M", $dat);
        $StartSeconds = (int) strftime("%S", $dat);

        $end_dat = mktime($StartHour, $StartMinutes, $StartSeconds, $StartMonth, $StartDay + $DaysToAdd, $StartYear);
        return $end_dat;
    }


    public static function toDateAddTime($dat, $StrHour) { // 1250719200  20:55 {
        if (strlen($dat) == 0)
            return null;
        $year = strftime("%Y", $dat);
        $month = strftime("%m", $dat);
        $day = strftime("%d", $dat);
        $hour = 0;
        $minute = 0;
        //Util::deb($dat,'ToDateAddTime( $dat::');
        //Util::deb($StrHour,'$StrHour::');
        if (strlen($StrHour) == 4 and (!strpos($StrHour, ':') )) {
            $StrHour = substr($StrHour, 0, 2) . ":" . substr($StrHour, 2, 2);
        }
        //Util::deb($StrHour,'$StrHour::');
        $A = split(':', $StrHour);
        if (Count($A) == 2) {
            $hour = (int) $A[0];
            $minute = (int) $A[1];
        }
        //Util::deb($hour,'$hour::');
        //Util::deb($minute,'$minute::');
        return mktime($hour, $minute, 0, $month, $day, $year);
    }

    public static function DaysInMonth($Year, $Month)
    {
        switch ($Month) {
            case 1:
                return 31;
            case 2:
                if (ceil($Year % 4) == 0)
                    return 29; // високосный год
                else
                    return 28;
            case 3:
                return 31;
            case 4:
                return 30;
            case 5:
                return 31;
            case 6:
                return 30;
            case 7:
                return 31;
            case 8:
                return 31;
            case 9:
                return 30;
            case 10:
                return 31;
            case 11:
                return 30;
            case 12:
                return 31;
        }
        return 0;
    }

    public static function WasFieldChanged($DataRow, $PostArray, $FieldsToSkip = array())
    {
        foreach ($DataRow as $Key => $Value) {
            if (in_array($Key, $FieldsToSkip))
                continue;
            if (!isset($PostArray[$Key]))
                continue;
            if (trim($Value) != trim($PostArray[$Key]))
                return $Key;
        }
        return '';
    }


    public static function compareDatesByDay($date1, $date2) // 0 - dates are equal, 1 - $date1 > $date2, -1 - $date1< $date2,
    {
        $year1 = strftime('%Y', $date1);
        $month1 = strftime('%m', $date1);
        $day1 = strftime('%d', $date1);

        $year2 = strftime('%Y', $date2);
        $month2 = strftime('%m', $date2);
        $day2 = strftime('%d', $date2);
        $day1 = mktime(null, null, null, $month1, $day1, $year1);
        $day2 = mktime(null, null, null, $month2, $day2, $year2);
        if ($day1 == $day2) return 0;
        if ($day1 > $day2) return 1;
        if ($day1 < $day2) return -1;
    }

    public static function getDatesDifference($date1, $date2) {
        $datediff = $date1 - $date2;
        return floor($datediff/(60*60*24));
    }

    public static function getFormattedDateTime($DateTimeLabel = '', $format = '')
    {
        //echo '<pre>$DateTimeLabel::'.print_r($DateTimeLabel,true).'</pre>';
        // echo '<pre>$format::'.print_r($format,true).'</pre>';

        // <pre>$DateTimeLabel::1976-07-21</pre><pre>$format::ShortDate</pre>yy-mm-dd
        if (empty($DateTimeLabel))
            $DateTimeLabel = time();
        if (!is_numeric($DateTimeLabel)) {
            $DateTimeLabel = strtotime($DateTimeLabel);
            // echo '<pre>-1  ++ $DateTimeLabel::'.print_r($DateTimeLabel,true).'</pre>';
        }
        //echo '<pre>$DateTimeLabel::'.print_r($DateTimeLabel,true).'</pre>';
        //echo '<pre>-2 $format::'.print_r($format,true).'</pre>';
        if (empty($DateTimeLabel)) return false;
        if (empty($format) or strtoupper($format) == "COMMON") {
            return strftime(self::$DateTimeFormat, $DateTimeLabel);
        }
        if (strtoupper($format) == "ASTEXT") {
            return strftime(self::$DateTimeAsTextFormat, $DateTimeLabel);
        }
        if (strtoupper($format) == "DATEASTEXT") {
            return strftime(self::$DateAsTextFormat, $DateTimeLabel);
        }

        if (strtoupper($format) == "SHORTDATE") {
            return strftime(self::$ShortDateFormat, $DateTimeLabel);
        }

        if (strtoupper($format) == "MONTHASTEXTYEAR") {
            return strftime(self::$MONTH_AS_TEXT_YEAR_DATE_FORMAT, $DateTimeLabel);
        }
        if (strtoupper($format) == "MYSQL") {
            return strftime(self::$DateTimeMySqlFormat, $DateTimeLabel);
        }


    }


    public static function  is_empty_array($A)
    {
        foreach ($A as $value) {
            if (isset($value)) return false;
        }
        return true;
    }

    public static function SetArrayHeader($HeaderArray, $DataArray)
    {
        if (empty($HeaderArray) or !is_array(($HeaderArray))) {
            die("-1VV");
            return $DataArray;
        }
        $ResArray = array();
        foreach ($HeaderArray as $HeaderItem) {
            $ResArray[] = $HeaderItem;
            if (is_array($DataArray)) {
                foreach ($DataArray as $DataItem) {
                    $ResArray[] = $DataItem;
                }
            }
        }
        return $ResArray;
    }

    public static function getYesNoValueArray()
    {
        return self::$YesNoValueArray;
    }

    /*public static function getParameter($Controller, $UriArray, $PostArray, $ParameterName, $DefaultValue = '', $parameter_splitter = '')
    {
        if (isset($PostArray)) { // form was submitted
            $ParameterValue = $Controller->input->post($ParameterName);
        } else {
            $ParameterValue = isset($UriArray[$ParameterName]) ? $UriArray[$ParameterName] : $DefaultValue;
        }
        //nsnClass_appFuncs::deb($ParameterValue, '$ParameterValue::');
        if (is_array($ParameterValue)) return $ParameterValue;
        if (!empty($parameter_splitter)) {
            $A = preg_split('/' . $parameter_splitter . '/', (is_string($ParameterValue) ? urldecode($ParameterValue) : $ParameterValue));
            $ResArray = array();
            foreach ($A as $val) {
                if (isset($val)) $ResArray[] = urldecode($val);
            }
            return $ResArray;
        }
        return urldecode($ParameterValue);
    } */

    public static function isDeveloperComp()
    {
        // return true;
        if (empty($_SERVER["HTTP_HOST"]))
            return false;
        if (!(strpos($_SERVER["HTTP_HOST"], 'local-ci22.com') === false)) {
            return true;
        }
        return false;
    }

    public static function deb($Var, $Descr = '', $isSql = false)
    {
        if (!self::isDeveloperComp()) {
            return;
        }
        //self::DebToFile('<pre>'.$Descr.'::'.print_r($Var,true).'<pre>', false);
        ///Console::log('Hey, this is really cool');
        if (ENVIRONMENT == 'development' and class_exists('Console')) {
            Console::log($Descr . '::' . print_r($Var, true));
        }
        //echo '<pre>'.$Descr.'::'.var_dump($Var).'<pre>';
        return;

        include_once("dbug.php");
        if (isset($Descr)) {
            echo "<h5><b><u>" . '<font color= green>' . HtmlSpecialChars($Descr) . " </font></u></b>";
        }
        if (is_array($Var))
            new dBug($Var);
        else if (is_object($Var))
            echo var_dump($Var);
        else {
            if (gettype($Var) == 'string') {
                if ($isSql) {
                    echo '<b><I>' . gettype($Var) . "</I>" . '&nbsp;$Var:=<font color= red>&nbsp;' . AppUtil::showFormattedSql($Var) . "</font></b></h5>";
                } else {
                    echo '<b><I>' . gettype($Var) . "</I>" . '&nbsp;$Var:=<font color= red>&nbsp;' . HtmlSpecialChars($Var) . "</font></b></h5>";
                }
            } else {
                echo '<b><I>' . gettype($Var) . "</I>" . '&nbsp;$Var:=<font color= red>&nbsp;' . $Var . "</font></b></h5>";
            }
        }
    }

    public static function showFormattedSql($Sql, $isHtml = true)
    {
        $BreakLine = $isHtml ? '<br>' : "\r";
        $Sql = preg_replace("/insert into/i", ($isHtml ? "&nbsp;&nbsp;<B>INSERT INTO</B>" : "  INSERT INTO"), $Sql);
        $Sql = preg_replace("/insert/i", ($isHtml ? "&nbsp;<B>INSERT</B>" : "  INSERT"), $Sql);
        $Sql = preg_replace("/delete/i", ($isHtml ? "&nbsp;<B>DELETE</B>" : "  DELETE"), $Sql);
        $Sql = preg_replace("/values/i", ($isHtml ? "&nbsp;&nbsp;<B>VALUES</B>" : "  VALUES"), $Sql);
        $Sql = preg_replace("/update/i", ($isHtml ? "&nbsp;<B>UPDATE</B>" : "  UPDATE"), $Sql);
        $Sql = preg_replace("/straight_join/i", $BreakLine . ($isHtml ? "<B>&nbsp;&nbsp;STRAIGHT_JOIN</B>" : "  STRAIGHT_JOIN"), $Sql);
        $Sql = preg_replace("/left join/i", $BreakLine . ($isHtml ? "&nbsp;&nbsp;<B>LEFT JOIN</B>" : "  LEFT JOIN"), $Sql);
        $Sql = preg_replace("/select/i", ($isHtml ? "&nbsp;<B>SELECT</B>" : " SELECT"), $Sql);
        $Sql = preg_replace("/from/i", $BreakLine . ($isHtml ? "&nbsp;&nbsp;<B>FROM</B>" : "  FROM"), $Sql);
        $Sql = preg_replace("/where/i", $BreakLine . ($isHtml ? "&nbsp;&nbsp;<B>WHERE</B>" : " WHERE"), $Sql);
        $Sql = preg_replace("/group by/i", $BreakLine . ($isHtml ? "&nbsp;&nbsp;<B>GROUP BY</B>" : "  GROUP BY"), $Sql);
        $Sql = preg_replace("/having/i", $BreakLine . ($isHtml ? "&nbsp;&nbsp;<B>HAVING</B>" : "  HAVING"), $Sql);
        $Sql = preg_replace("/order by/i", $BreakLine . ($isHtml ? "&nbsp;&nbsp;<B>ORDER BY</B>" : "  ORDER BY"), $Sql);
        return $Sql;
    }


    public static function tbUrlDecode($Url)
    {
        $Url = str_replace('ZZZZZ', '/', $Url);
        $Url = str_replace('XXXXX', '.', $Url);
        $Url = str_replace('YYYYY', '-', $Url);
        $Url = str_replace('WWWWW', '_', $Url);
        return $Url;
    }

    public static function tbUrlEncode($Url)
    {
        $Url = str_replace('/', 'ZZZZZ', $Url);
        $Url = str_replace('.', 'XXXXX', $Url);
        $Url = str_replace('-', 'YYYYY', $Url);
        $Url = str_replace('_', 'WWWWW', $Url);
        return $Url;
    }

    public static function WriteToFileName($FileName = '', $contents, $IsTruncateData = false)
    {
        try {
            // if( $config_array['log_path'] ) return;
            //$CurrentDir = str_replace('\\', '/', sfConfig::get('sf_web_dir'));
            //Util::deb($CurrentDir,'$CurrentDir::');
            if (empty($FileName))
                $FileName = '/_wwwroot/props-ci/log/deb.txt';
            $fd = fopen($FileName, ($IsTruncateData ? "w+" : "a+"));
            fwrite($fd, $contents);
            fclose($fd);
            return true;
        } catch (Exception $lException) {
            return false;
        }
    }

    public static function PrepareRandomWord($Length = 6)
    {
        $alphabet = "abcdefghijklmnopqrstuvwxyz";
        $Res = '';
        for ($I = 0; $I < $Length; $I++) {
            $Index = rand(0, strlen($alphabet) - 1);
            $Res .= substr($alphabet, $Index, 1);
        }
        return $Res;
    }

    public static function GeneratePassword($Length = 8)
    {
        $I = 0;
        while (true) {
            $Password = nsnClass_appFuncs::PreparePassword($Length);
            $User = get_instance()->muser->getRowByPassword(md5($Password));
            if (empty($User))
                return $Password;
        }
        return '';
    }

    /**
     * Prepare Password with given length($Length)
     *
     */
    public static function PreparePassword($Length)
    {
        $alphabet = "0123456789abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $Res = '';
        for ($I = 0; $I < $Length; $I++) {
            $Index = rand(0, strlen($alphabet) - 1);
            $Res .= substr($alphabet, $Index, 1);
        }
        return $Res;
    }

    // getSeparatedString( array( $Row['country'], $Row['zip'], $Row['state'], $Row['county'], $Row['town'], $Row['street'], $Row['apartment'], $Row['house_name'], $Row['house_number'], ),',' );
    //getSeparatedString( array( $Row['country'], $Row['zip'], $Row['state'], $Row['county'], $Row['town'], $Row['street'], $Row['apartment'], $Row['house_name'], $Row['house_number'], ),',' );
    public static function getSeparatedString($DataArray, $Separator = ',', $Space = ' ', $SkipLastSeparator = true)
    {
        $ResStr = '';
        $L = count($DataArray);
        for ($I = 0; $I < $L; $I++) {
            if (empty($DataArray[$I]))
                continue;
            $ResStr .= $DataArray[$I];
            if ($I < $L - 1) { //
                $ResStr .= $Separator . $Space;
            }
        }
        if ($SkipLastSeparator) {
            $ResStr = trim($ResStr);
            if (substr($ResStr, strLen($ResStr) - 1, 1) == $Separator) {
                $ResStr = substr($ResStr, 0, strLen($ResStr) - 1);
            }
        }
        return $ResStr;
    }

    public static function SaveFormHTML($DataArray)
    {
        if (empty($DataArray) or !is_array($DataArray))
            return $DataArray;
        foreach ($DataArray as $key => $value) {
            $DataArray[$key] = form_prep($value);
        }
        return $DataArray;
    }

    // nsnClass_appFuncs::getNextParameters( $UriArray, 'redirect_url', true );
    public static function getNextParameters($UriArray, $ParamName, $return_string = true)
    {
        $ResStr = '';
        $HasParam = false;
        foreach ($UriArray as $key => $value) {
            if ($key == $ParamName) {
                $HasParam = true;
            }
            if ($HasParam) {
                $ResStr .= ($key == $ParamName ? '' : $key . '/') . $value . ($value ? '/' : '');

            }
        }
        return $ResStr;
    }

    public static function ReadCSVFileAsArray($filename, $skip_first_row = true)
    {
        if (!file_exists($filename)) {
            echo '<b> File ' . $filename . ' not found</b>';
        }
        $file = fopen($filename, "r");
        $ResArray = array();
        $I = 1;
        while (!feof($file)) {
            $A = fgetcsv($file, null, ';');
            if (!($I == 1 and $skip_first_row)) {
                $ResArray[] = $A;
            }
            $I++;
        }
        echo '<b>  ' . count($ResArray) . ' rows imported.</b><br>';
        fclose($file);
        return $ResArray;

    }

    public static function getFileNameExt($FileName)
    {
        $K = strrpos($FileName, ".");
        $Ext = '';
        if ($K > 0) { // File has an extension
            $Ext = substr($FileName, $K + 1);
        }
        return trim($Ext);
    }

    public static function getFileNameBase($FileName)
    {
        $K = strrpos($FileName, ".");
        $Ext = '';
        if ($K > 0) { // File has an extension
            $Ext = substr($FileName, 0, $K/* - 1*/);
        }
        return trim($Ext);
    }

    public static function getAppUrl($show_end_separator = true)
    {
        // echo '<pre>AppUtils::isDeveloperComp()::'.print_r(AppUtils::isDeveloperComp(),true).'<pre>';
        if (nsnClass_appFuncs::isDeveloperComp()) {
            $url = 'http://' . $_SERVER['HTTP_HOST'] . ($show_end_separator ? '/' : '');
            //echo '<pre>-1::'.print_r($url,true).'<pre>';
            return $url;
        }
        $url = 'http://' . $_SERVER['HTTP_HOST'] . '/ci22' . ($show_end_separator ? '/' : '');
        // echo '<pre>-2::'.print_r($url,true).'<pre>';
        //die("-ZZZ1");
        return $url;
    }

    public static function readFileAsText($fileName)
    {
        try {
            $fd = fopen($fileName, "r");
            $contents = fread($fd, filesize($fileName));
            fclose($fd);
            return $contents;
        } catch (Exception $lException) {
            return false;
        }
    }


    public static function sendEmail($emailTo, $subjectText, $messageText, $emailFrom = '', $attachedFiles = array())
    {
        $CIObj =& get_instance();
        $CIObj->email->clear();
        if (!empty($emailFrom)) {
            $CIObj->email->from($emailFrom);
        } else {
            $CIObj->email->from($CIObj->config->item('admin_email', 'ion_auth'), $CIObj->config->item('site_title', 'ion_auth'));
        }
        $CIObj->email->to($emailTo);
        $CIObj->email->subject($subjectText);
        $CIObj->email->message($messageText);
        $CIObj->email->send();
    }


    public static function createDir($directories_list = array(), $removeExistingFiles= false, $mode = 0755)
    {
        $l= count($directories_list); $i= 1;
        foreach ($directories_list as $dir) {
            $lastDir= $l==$i;
            if (!file_exists($dir)) {
                mkdir($dir, $mode);
            } else {
                if ($removeExistingFiles and $lastDir ) {
                    nsnClass_appFuncs::deleteDirectory($dir,  null, true);
                }
            }
            $i++;
        }
    }


    public static function getFileSizeAsString($FileSize)
    {
        /*if ((int)$FileSize < 1024)
            return $FileSize . ' b';
        if ((int)$FileSize < 1024 * 1024)
            return floor($FileSize / 1024) . ' kb';
        return floor($FileSize / (1024 * 1024)) . ' mb';  */
        if ((int)$FileSize < 1024)
            return $FileSize . ' b';
        if ((int)$FileSize < 1024 * 1024)
            return floor($FileSize / 1024) . ' kb';

        if ((int)$FileSize < 1024 * 1024 * 1024)
            return floor($FileSize / (1024 * 1024)) . ' mb';
        return floor($FileSize / (1024 * 1024 * 1024)) . ' gb';
    }

    public static function deleteDirectory($DirectoryName, $hour_label = null, $remove_only_files = false)
    {
        if (!file_exists($DirectoryName)) return;
        $H = OpenDir($DirectoryName);
        while ($NextFile = readdir($H)) { // All files in dir
            if ($NextFile == "." or $NextFile == "..")
                continue;
            $DeleteFile = true;
            if ($hour_label != null) {
                $filemtime = filemtime($DirectoryName . DIRECTORY_SEPARATOR . $NextFile);
                if ($filemtime > $hour_label) $DeleteFile = false;
            }
            if ($DeleteFile) {
                unlink($DirectoryName . DIRECTORY_SEPARATOR . $NextFile);
            }
        }
        closedir($H);
        if (!$remove_only_files) rmdir($DirectoryName);
    }

    public static function getImageShowSize($ImageFileName, $orig_width, $orig_height)
    {
        $ResArray = array('Width' => 0, 'Height' => 0, 'OriginalWidth' => 0, 'OriginalHeight' => 0);
        $FileArray = @getimagesize($ImageFileName);
        if (empty($FileArray))
            return $ResArray;

        $width = (int)$FileArray[0];
        $height = (int)$FileArray[1];

        $ResArray = array('Width' => 0, 'Height' => 0, 'OriginalWidth' => 0, 'OriginalHeight' => 0);
        $FileArray = @getimagesize($ImageFileName);
        if (empty($FileArray))
            return $ResArray;

        $width = (int)$FileArray[0];
        $height = (int)$FileArray[1];
        $ResArray['OriginalWidth'] = $width;
        $ResArray['OriginalHeight'] = $height;
        $ResArray['Width'] = $width;
        $ResArray['Height'] = $height;

        $Ratio = round($width / $height, 3);

        if ($width > $orig_width) {
            $ResArray['Width'] = (int)($orig_width);
            $ResArray['Height'] = (int)($orig_width / $Ratio);
            if ($ResArray['Width'] <= (int)$orig_width and $ResArray['Height'] <= (int)$orig_height) {
                return $ResArray;
            }
            $width = $ResArray['Width'];
            $height = $ResArray['Height'];
        }
        if ($height > $orig_height and ((int)($orig_height / $Ratio)) <= $orig_width) {
            $ResArray['Width'] = (int)($orig_height * $Ratio);
            $ResArray['Height'] = (int)($orig_height);
            return $ResArray;
        }
        if ($height > $orig_height and ((int)($orig_height / $Ratio)) > $orig_width) {
            $ResArray['Width'] = (int)($orig_height * $Ratio);
            $ResArray['Height'] = (int)($ResArray['Width'] / $Ratio);
            return $ResArray;
        }
        return $ResArray;

    }

    /*
    public static function getThumbnailWidth() {
        return self::$thumbnailWidth;
    }

    public static function getThumbnailHeight() {
        return self::$thumbnailHeight;
    }         */


    //nsnClass_appFuncs::makeFileUpload($_FILES, 'artist_image', $ret_artist_id, $ArtistsTblSource,$this->isDebug);
    public static function makeFileUpload($filesArray, $fieldName, $product_id, $TblSource, $isDebug)
    {
        if (isset($filesArray[$fieldName]['tmp_name']) and isset($filesArray[$fieldName]['name'])) {
            $src_filepath = $filesArray[$fieldName]['tmp_name']; // path of loaded file
            $src_filename = $filesArray[$fieldName]['name'];  // name of loaded file
            $filenameExt = nsnClass_appFuncs::getFileNameExt($src_filename);
            if ($isDebug) echo '<pre>$filenameExt::' . print_r($filenameExt, true) . '</pre>';

            $upload_dir = wp_upload_dir();
            if ($isDebug) echo '<pre>$upload_dir::' . print_r($upload_dir, true) . '</pre>';

            //$dest_dirname = $upload_dir['basedir'] . DIRECTORY_SEPARATOR . 'products-images/product-image-' . $product_id;
            $dest_dirname = $upload_dir['basedir'] . DIRECTORY_SEPARATOR . $fieldName . 's/' . $fieldName . '_' . $product_id;
            // ($artist_id, $filenameExt='', $returnUrl= true) { //basedir
            $dest_filename = $TblSource->getImage($product_id, $filenameExt, false);

            if ($isDebug) echo '<pre>$dest_filename::' . print_r($dest_filename, true) . '</pre>';
            //DebToFile('upload_state_image $tmp_dest_filename::' .print_r($tmp_dest_filename,true) );
            $productImagesDirs = array($upload_dir['basedir'], $upload_dir['basedir'] . DIRECTORY_SEPARATOR . $fieldName . 's/', $dest_dirname);
            if ($isDebug) echo '<pre>$productImagesDirs::' . print_r($productImagesDirs, true) . '</pre>';
            nsnClass_appFuncs::createDir($productImagesDirs,true);
            $ret = move_uploaded_file($src_filepath, $dest_filename);
            return $ret;
            //die("-1CC");
        }

    } //     public static function getThumbnailHeight() {

    /**
     * Limits a phrase to a given number of characters.
     *
     * @param   string   phrase to limit characters of
     * @param   integer  number of characters to limit to
     * @param   string   end character or entity
     * @param   boolean  enable or disable the preservation of words while limiting
     * @return  string
     */
    public static function limitChars($str, $limit = 100, $endChar = null, $preserveWords = false)
    {
        $endChar = ($endChar === null) ? '&#8230;' : $endChar;

        $limit = (int)$limit;

        if (trim($str) === '' OR strlen($str) <= $limit)
            return $str;

        if ($limit <= 0)
            return $endChar;

        if ($preserveWords == false) {
            return rtrim(substr($str, 0, $limit)) . $endChar;
        }
        // TO FIX AND DELETE SPACE BELOW
        preg_match('/^.{' . ($limit - 1) . '}\S* /us', $str, $matches);

        return rtrim($matches[0]) . (strlen($matches[0]) == strlen($str) ? '' : $endChar);
    }

    /**
     * Limits a phrase to a given number of words.
     *
     * @param   string   phrase to limit words of
     * @param   integer  number of words to limit to
     * @param   string   end character or entity
     * @return  string
     */
    public static function limitWords($str, $limit = 100, $endChar = null)
    {
        $limit = (int)$limit;
        $endChar = ($endChar === null) ? '&#8230;' : $endChar;

        if (trim($str) === '')
            return $str;

        if ($limit <= 0)
            return $endChar;

        preg_match('/^\s*+(?:\S++\s*+){1,' . $limit . '}/u', $str, $matches);

        // Only attach the end character if the matched string is shorter
        // than the starting string.
        return rtrim($matches[0]) . (strlen($matches[0]) === strlen($str) ? '' : $endChar);
    }

    public static function isWindows()
    {
        return (strpos(php_uname(), 'Linux ') === false);
    }

    public static function getUploadDir()
    {
        $wp_upload_dir = wp_upload_dir();
        if (nsnClass_appFuncs::isWindows()) {
            $wp_upload_dir['path'] = str_replace('/', '\\', $wp_upload_dir['path']);
            $wp_upload_dir['basedir'] = str_replace('/', '\\', $wp_upload_dir['basedir']);
        }
        return $wp_upload_dir;
    }


    public static function get_splitted_clauses($str= '')    { //  clause  like " arg1  :  val1 arg2="val2   = \'A\'" arg3 : val3' " arrays

        $a= preg_split('/ /', $str, 2);
        //echo '<pre>'.count($a).'::$a::'.print_r($a,true).'</pre>';
        $field_name= '';
        if (count($a)== 2) {
            $field_name= $a[0];
        }
        $ret_array= array();
        if ( 1 ) {
            //$re = '/([a-z\d]+)\s*[:=]\s*("[^"]+"|[^ ]+)/i';
            $re = '/([\w]+)\s*[:=]\s*("[^"]+"|[^ ]+)/i';
            //$re = '/(\S+?)\s*[:=]\s*(?:"([^"]*)"|(\S+))\s*/';
            //$re = '/(\S+?)\s*[:=]\s*(?:"([^"]*)"|(\S+))\s*/';
            preg_match_all($re, $str, $m);
            // echo '<pre>$m::'.print_r($m,true).'</pre>';
            $options_list= array();
            if ( count($m[2] == 3) ) {
                //echo '<pre>$m[1]::'.print_r($m[1],true).'</pre>';
                foreach ($m[1] as $key => $val) {
                    //echo '<pre>$key::'.print_r($key,true).'</pre>';
                    //echo '<pre>$val::'.print_r($val,true).'</pre>';
                    $options_list[]= array( 'attr'=>$m[1][$key], 'code'=> $m[2][$key] );
                }
            }
            $ret_array['options']= $options_list;
        }
        $ret_array['field_name']= $field_name;
        return $ret_array;

        //$str =  "id=artist-id ; source_function= nsn_getArtistsList; source_args=is_active = 'A'; sort= name; type= group_selection; option_value_field= ID;option_label_field= name; select_label= Artist; select_class= ' class=\"wpcf7-form-control-wrap @id@' \" ";

        //$str = "123;23;345;456;567";
        //$result = preg_match_all('/\d{3}/',$str,$found);
        $res_array= array();
        $result = preg_match_all('/([^;]+)/',$str,$found);
        //echo "Matches: $result<br>";
        //echo '<pre>'.count($found).'::$found::'.print_r($found,true).'</pre>';
        if ($result> 0 and !empty($found) and is_array($found)) {
            foreach ($found[1] as $next_key => $next_item) {
                $a = preg_split('/=/', $next_item, 2);
                //echo '<pre>$a::' . print_r($a, true) . '</pre>';
                if (count($a) == 2) {
                    $res_array[] = array('attr' => trim($a[0]), 'code' => trim($a[1]));
                }
            }
        }
        //echo '<pre>'.count($res_array).'::$res_array::'.print_r($res_array,true).'</pre>';
        return $res_array;
    }

    public static function set_session_value( $variable_name, $message ) {
        if (!session_id()) {
            session_start();
        }
        $_SESSION[$variable_name] = $message;
    }

    public static function get_session_value( $variable_name, $default_message= '', $clear_variable= false ) {
        if (!session_id()) {
            session_start();
        }
        $ret= $default_message;
        if ( !empty($_SESSION[$variable_name]) ) {
            $ret= $_SESSION[$variable_name];
            if ($clear_variable) {
                $_SESSION[$variable_name]= "";
                unset($_SESSION[$variable_name]);
            }
        }
        return $ret;
    }

    public static function check_session_active( $session_key_name/* , $seconds_period= 0 */ ) {
        if (!session_id()) {
            session_start();
        }
        if ( empty( $_SESSION[$session_key_name]) ) return false;
        if ( !is_numeric($_SESSION[$session_key_name]) )  return false;
        $session_time = $_SESSION[$session_key_name];
        //DebToFile('$session_time:::'.print_r(strftime('%Y-%m-%d %H:%M:%S',$session_time),true));
        //DebToFile('++time() ::'.print_r(  strftime('%Y-%m-%d %H:%M:%S',time() )  ,true));
        if ($session_time >= ( time() )) return true;
        return false;
    }

    public static function add_session_value( $session_value_key_name, $id= '', $separator= ';' ) {
        if ( empty( $id) ) return;
        if (!session_id()) {
            session_start();
        }
        if ( empty( $_SESSION[$session_value_key_name]) ) {
            $_SESSION[$session_value_key_name]= $id . $separator;
            return;
        }
        $_SESSION[$session_value_key_name]= $_SESSION[$session_value_key_name] . $separator . $id;
        DebToFile('!!!!  $_SESSION[$session_value_key_name]:::'.print_r($_SESSION[$session_value_key_name],true));
    }

}

// class nsnClass_appFuncs {
?>