<?php
if (!function_exists("DebToFile")) {   // woo-ext-search-plugin.php
    function DebToFile($contents, $IsClearText = false, $FileName = '')
    {
        //return;
        try {
            if (empty($FileName)) {
                $FileName = ABSPATH . DIRECTORY_SEPARATOR . "log" . DIRECTORY_SEPARATOR . "logging_deb.txt";
            }
            $IsClearText = false;
            $fd = fopen($FileName, ($IsClearText ? "w+" : "a+"));
            fwrite($fd, $contents . chr(13));
            fclose($fd);
            return true;
        } catch (Exception $lException) {
            return false;
        }
    }
}

/*
Plugin Name: NSN Woo Extended Search
Description: NSN Woo Extended Search for wooCommerce products.
Version: 1.0
Author: Nilov Serge
Author URI: http://home.com/*/
//ini_set('display_errors', '1');
//ini_set('error_reporting', E_ALL);
// Stop direct call


if(preg_match('#' . basename(__FILE__) . '#', $_SERVER['PHP_SELF'])) { die('You are not allowed to call this page directly.'); }

if (!class_exists('NSN_WooExtSearch')) {    // woo-ext-search-plugin.php
    class NSN_WooExtSearch {
        protected $m_plugin_name;
        protected $m_plugin_dir;
        protected $m_plugin_url;
        protected $m_prices_list;
        protected $m_appTwig;
        protected $m_plugin_prefix= 'nsn-woo-ext-search-'; // nsn-woo-ext-search-bookmark-

        public function __construct()
        {

            DEFINE('NSN_WooExtSearch', true); // Pluging is inited
            $woo_ext_search_options = get_option('nsn-woo-ext-search-');
            if ( !empty($woo_ext_search_options['meta_info_key']) ) {
                $this->m_plugin_prefix = $woo_ext_search_options['meta_info_key'];
            }

            require_once( 'lib/app-funcs.php');
            require_once( WP_CONTENT_DIR.'/Twig/Autoloader.php');  //wp-content/Twig/Autoloader.php
            require_once( 'lib/app-twig.php');

            $this->m_plugin_name = "/wp-admin/options-general.php?page=". plugin_basename(__FILE__);

            $this->m_plugin_dir = trailingslashit(WP_PLUGIN_DIR.'/'.dirname(plugin_basename(__FILE__)));
            $this->m_plugin_url = trailingslashit(WP_PLUGIN_URL.'/'.dirname(plugin_basename(__FILE__)));


//            DebToFile('-NSN_WooExtSearch __construct  $this->m_plugin_dir::' .print_r($this->m_plugin_dir,true) );
//            DebToFile('-NSN_WooExtSearch __construct  $this->m_plugin_url::' .print_r($this->m_plugin_url,true) );

            require_once( $this->m_plugin_dir . '/lib/app-nsn-woo-tools-data.php'); //wp-content/plugins/wooExtSearch/lib/app-nsn-woo-tools-data.php
            $this->m_prices_list= nsnWooToolsData::prices_string_into_list( $woo_ext_search_options['show_in_extended_search_price_ranges'] );

            register_activation_hook( $this->m_plugin_name, array(&$this, 'activate') );
            register_deactivation_hook( $this->m_plugin_name, array(&$this, 'deactivate') );

            if ( is_admin() ) { // That is admin page
                add_action('wp_print_scripts', array(&$this, 'woo_ext_search_admin_load_scripts'));
                add_action('admin_enqueue_scripts', array(&$this, 'woo_ext_search_admin_load_styles'));
                add_action( 'admin_menu', array(&$this, 'woo_ext_search_add_admin_menu') );

            } else {  // That is NOT admin page
                add_action('wp_print_scripts', array(&$this, 'woo_ext_search_load_scripts'));
                add_action('wp_enqueue_scripts', array(&$this, 'woo_ext_search_site_load_styles'));

                //add_action('wp_print_styles', array(&$this, 'site_load_styles'));
                add_action('wp_footer', array(&$this, 'frontendNSN_WooExtSearchFooterHTML') );
                add_shortcode('ext_search_form', array (&$this, 'siteNSN_WooExtSearchForm'));    // https://codex.wordpress.org/Function_Reference/add_shortcode
            }
        } // function NSN_WooExtSearch() // constructor of plugin class

        function woo_ext_search_load_scripts()
        {
            wp_register_script('NSN_WooExtSearch_jquery', $this->m_plugin_url . 'js/jquery/jquery-1.11.1.min.js' );
            wp_register_script('NSN_WooExtSearch_jquery_ui', $this->m_plugin_url . 'js/jquery/jquery-ui-1.11.1.min.js' );
            wp_register_script('NSN_WooExtSearch_jquery_confirm', $this->m_plugin_url . 'js/jquery/jquery-confirm.min.js' );

            wp_enqueue_script('NSN_WooExtSearch_jquery');
            wp_enqueue_script('NSN_WooExtSearch_jquery_ui');
            wp_enqueue_script('NSN_WooExtSearch_jquery_confirm');

            wp_register_script('CommonFuncsJs', $this->m_plugin_url . 'js/common-funcs.js' );
            wp_enqueue_script('CommonFuncsJs');

            wp_register_script('FrontendNSN_WooExtSearchJs', $this->m_plugin_url . 'js/frontend-scripts.js' );
            wp_enqueue_script('FrontendNSN_WooExtSearchJs');

            $get_message = get_option('mand_message_custom');
            if ( empty( $get_message ) )
                $get_message = 'The Fields Marked In Red Are Mandatory';

            wp_localize_script( 'FrontendNSN_WooExtSearchJs', 'ajax_object', array( 'errorMessage' =>  $get_message,
                'frontend_ajaxurl' => $this->m_plugin_url.'ajax-frontend-control.php',
                'post_type' => get_post_type()) );
        }

        function woo_ext_search_admin_load_scripts()
        {

            global $post_type;
            global $post;
            $querySting= '';
            if ( isset($_SERVER['QUERY_STRING']) ) {
                $querySting= $_SERVER['QUERY_STRING'];
            }
//            DebToFile('admin_load_scripts  $querySting::'.print_r($querySting,true));

            wp_register_script('NSN_WooExtSearch_jquery', $this->m_plugin_url . 'js/jquery/jquery-1.11.1.min.js' );
            wp_register_script('NSN_WooExtSearch_jquery_ui', $this->m_plugin_url . 'js/jquery/jquery-ui-1.11.1.min.js' );

            wp_enqueue_script('NSN_WooExtSearch_jquery');
            wp_enqueue_script('NSN_WooExtSearch_jquery_ui');

            wp_register_script('CommonFuncsJs', $this->m_plugin_url . 'js/common-funcs.js');
            wp_register_script('BackendNSN_WooExtSearchJs', $this->m_plugin_url . 'js/backend-scripts.js');
//
//            // Add scripts on page
            wp_enqueue_script('CommonFuncsJs');
            wp_enqueue_script('BackendNSN_WooExtSearchJs');

            if ( !(strpos($querySting,'page=NSN_WooExtSearch-editor')=== false) ) { // that is backend editor
                wp_enqueue_script('BackendNSN_WooExtSearch-editor');
            } // if ( !(strpos($querySting,'page=NSN_WooExtSearch-editor')=== false) ) { // that is backend-editor

        }

        function woo_ext_search_admin_load_styles()
        {
            wp_register_style( 'NSN_WooExtSearch_admin_style', $this->m_plugin_url . 'css/nsn_woo_ext_search_admin_style.css' );
            wp_enqueue_style( 'NSN_WooExtSearch_admin_style' );
        }

        function woo_ext_search_add_admin_menu()
        {
            global $wpdb;
            require_once( $this->m_plugin_dir . 'options-control.php');
            $wooExtSearchControlObj = new appNSN_WooExtSearchOptionsControl($this,$wpdb);

//            add_menu_page('NSN Woo Extended Search Module', 'NSN Woo Extended Search plugin', 'manage_options', 'NSN_WooExtSearch-main-editor', array(&$this,
//                'admin_NSN_WooExtSearch_plugin_info'));


            add_menu_page( 'NSN Woo Extended Search Module', 'NSN Woo Extended Search plugin', 'manage_options', 'NSN_WooExtSearch-editor', array(&$wooExtSearchControlObj,'main'));
//            add_submenu_page( 'NSN_WooExtSearch-main-editor', 'Plugin Options', ' Plugin Options', 'manage_options', 'NSN_WooExtSearch-editor', array(&$wooExtSearchControlObj,'main'));

        }


        /**
         * Show NSN Woo Extended Search Form
         */
        public function siteNSN_WooExtSearchForm($attrParams)
        {
            global $wpdb, $post;
            require_once( $this->m_plugin_dir . 'site-control.php');
            $siteNSN_WooExtSearchFormControlObj = new siteControl($this,$wpdb,$attrParams, 'siteNSN_WooExtSearchForm', $post);
            return $siteNSN_WooExtSearchFormControlObj->main();
        } // public function siteNSN_WooExtSearchForm($attrParams)



        /**
         * HTML code which will be added in frontend footer
         */
        public function frontendNSN_WooExtSearchFooterHTML()
        {
            global $wpdb, $post;
            require_once( $this->m_plugin_dir . 'site-control.php');
            $sitefrontendNSN_WooExtSearchFooterHTMLControlObj = new siteControl($this,$wpdb,'', 'frontendNSN_WooExtSearchFooterHTML', $post);
            echo $sitefrontendNSN_WooExtSearchFooterHTMLControlObj->main();
        } // public function siteShowArtistsRating($atts)


        /**
         * Static info page
         */
//        public function admin_NSN_WooExtSearch_plugin_info()
//        {
//            include_once('artists--info.php');
//        }
//

        function woo_ext_search_site_load_styles ()
        {
//            wp_register_style( 'NSN_WooExtSearch_common_styles', $this->m_plugin_url . 'css/nsn_woo_ext_search_style.css' );
//            wp_enqueue_style( 'NSN_WooExtSearch_common_styles' );
/*
<link rel='stylesheet' id='parent-style-css' href='http://local-wp-plugings.com/wp-content/themes/ultrabootstrap/style.css?ver=4.6.1' type='text/css' media='all' /><link rel='stylesheet' id='child-style-css' href='http://local-wp-plugings.com/wp-content/themes/ultrabootstrap-child/style.css?ver=1.0.0' type='text/css' media='all' /><link rel='stylesheet' id='ultrabootstrap-bootstrap-css' href='http://local-wp-plugings.com/wp-content/themes/ultrabootstrap/css/bootstrap.css?ver=4.6.1' type='text/css' media='all' /><link rel='stylesheet' id='ultrabootstrap-fontawesome-css' href='http://local-wp-plugings.com/wp-content/themes/ultrabootstrap/css/font-awesome.css?ver=4.6.1' type='text/css' media='all' /><link rel='stylesheet' id='ultrabootstrap-googlefonts-css' href='//fonts.googleapis.com/css?family=Roboto%3A400%2C300%2C700&#038;ver=4.6.1' type='text/css' media='all' /><link rel='stylesheet' id='ultrabootstrap-style-css' href='http://local-wp-plugings.com/wp-content/themes/ultrabootstrap-child/style.css?ver=4.6.1' type='text/css' media='all' />


<script type='text/javascript' src='http://local-wp-plugings.com/wp-content/themes/ultrabootstrap/js/bootstrap.js?ver=1.0.0'></script><script type='text/javascript' src='http://local-wp-plugings.com/wp-content/themes/ultrabootstrap/js/script.js?ver=1.0.0'></script>

  */


            wp_register_style( 'NSN_WooExtSearch_bootstrap', 'http://local-wp-plugings.com/wp-content/themes/ultrabootstrap/css/bootstrap.css' );
            wp_enqueue_style( 'NSN_WooExtSearch_bootstrap' );


            wp_register_style( 'NSN_WooExtSearch_font_awesome', 'http://local-wp-plugings.com/wp-content/themes/ultrabootstrap/css/font-awesome.css' );
            wp_enqueue_style( 'NSN_WooExtSearch_font_awesome' );



            wp_register_style( 'NSN_WooExtSearch_jquery_ui', $this->m_plugin_url . 'css/jquery-ui.min-1.11.1.css' );
            wp_enqueue_style( 'NSN_WooExtSearch_jquery_ui' );

            wp_register_style( 'NSN_WooExtSearch_common_styles_less', $this->m_plugin_url . 'less/nsn_woo_ext_search_styles.less' );
            wp_enqueue_style( 'NSN_WooExtSearch_common_styles_less' );

//            wp_register_style( 'NSN_WooExtSearch_bootstrap_slider', $this->m_plugin_url . 'css/bootstrap-slider.min.css' );
//            wp_enqueue_style( 'NSN_WooExtSearch_bootstrap_slider' );
            wp_register_style( 'NSN_WooExtSearch_jquery_confirm', $this->m_plugin_url . 'css/jquery-confirm.min.css' );
            wp_enqueue_style( 'NSN_WooExtSearch_jquery_confirm' );
        }



        function activate()
        {
            echo '<pre>activate::'.print_r(1,true).'</pre>';
            die("-1 XXZ");
            global $wpdb;
            ob_start();
            $NSN_WooExtSearchDBObj = new NSN_WooExtSearchDB( $wpdb, $this->m_plugin_dir, $this->m_plugin_url );
            echo '<pre>activate $NSN_WooExtSearchDBObjs::'.print_r($NSN_WooExtSearchDBObj,true).'</pre>';
            die("-1 INSIDE activate");
            //DebToFile('-0 activate  $NSN_WooExtSearchDBObj::' .print_r($NSN_WooExtSearchObj,true) );
            $ret= $NSN_WooExtSearchDBObj->InitDBStructure(true);
            $output = ob_get_contents ();
            ob_end_clean ();
            //DebToFile('-InitDBStructure $output ::' .print_r($output,true) );
            return $ret;
        }

        function deactivate()
        {
            echo '<pre>deactivate::'.print_r(1,true).'</pre>';
            die("-1 X0000");
            global $wpdb;
            $NSN_WooExtSearchDBObj = new NSN_WooExtSearchDB($wpdb);
            $ret= $NSN_WooExtSearchDBObj->ClearDBStructure();
            nsnClass_appData::deactivatePlugins( array('nsn-source-selection/nsn-source-selection-plugin.php', 'nsn-voting-reports/nsn-voting-reports-plugin.php' ) );
            return $ret;
        }


        /*function uninstall()
        {
            //global $wpdb;
            //$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}adv_reviews");
        } */


        public function getPluginPrefix() {
            return $this->m_plugin_prefix;
        }

        public function getPluginDir() {
            return $this->m_plugin_dir;
        }

        public function getPluginUrl() {
            return $this->m_plugin_url;
        }


        public function getPluginName() {
            return $this->m_plugin_name;
        }

        public function getPricesList() {
            return $this->m_prices_list;
        }



    } // class NSN_WooExtSearch {

} // if (!class_exists('NSN_WooExtSearch')) {

if (!function_exists("getAppVar"))  {
    function getAppVar($varName= '') {
        global $wp_session;
        $app_vars = ! empty( $wp_session['app_vars'] ) ? $wp_session['app_vars'] : array();
        //echo '<pre>$varName::'.print_r($varName,true).'</pre>';
        //echo '<pre>$app_vars::'.print_r($app_vars,true).'</pre>';
        if ( empty($varName) )return $app_vars;
        if ( isset($app_vars[$varName]) ) return $app_vars[$varName];
        return '';
    }
} // if (!function_exists("getAppVar"))  {


if (!function_exists("showErrorMessage")) {
    function showErrorMessage( $postFieldName, $dbFieldName,  $hasValidationErrors, $returnClass= false ) {
//        echo '<pre>$hasValidationErrors::'.print_r($hasValidationErrors,true).'</pre>';
//        exit;
//        die("-1 XXZ");
        if(empty($hasValidationErrors) or !is_array($hasValidationErrors)) return '';
        foreach( $hasValidationErrors as $nextErrorInfo ) {
            if ( $postFieldName == $nextErrorInfo['field_name'] ) {
                if (!$returnClass) {
                    return "<span id='span_error_field_" . $nextErrorInfo['field_name'] . "' >*&nbsp;" . $nextErrorInfo['error_label'] . "</span>";
                } else {
                    return ' form-invalid ';
                }
            }
        }
        return '';
    }
}


if (!function_exists("showNotice")) {
    function showNotice($message, $errormsg = false)
    {
        if ($errormsg) {
            echo '<div id="div_showNotice">';
        } else {
            echo '<div id="div_showNotice">';
        }
        echo $message . "</div>";
    }
}

if (!function_exists("showAdminNotice")) {
    function showAdminNotice($message)
    {
        showNotice($message, true);
    }
}
add_action('admin_notices', 'showAdminNotice');


//echo '<pre>-Z1::'.print_r(12,true).'</pre>';
global $NSN_WooExtSearchObj;
$NSN_WooExtSearchObj = new NSN_WooExtSearch();


//echo '<pre>-Z1::'.print_r(13,true).'</pre>';
