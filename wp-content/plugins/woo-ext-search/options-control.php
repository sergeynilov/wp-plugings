<?php
if (!defined('NSN_WooExtSearch')) {
    exit; // Exit if accessed directly
}

require_once('appWooExtSearchControl.php');

class appNSN_WooExtSearchOptionsControl extends appNSN_WooExtSearchControl // appVotingReportsControl
{
    public $m_products_attrs_list;
    public function __construct($containerClass, $wpdb)
    {
        global $session;
        require_once( $containerClass->getPluginDir() . '/lib/app-nsn-woo-tools-data.php'); //wp-content/plugins/wooExtSearch/lib/app-nsn-woo-tools-data.php
        $this->m_pageUrl= '/wp-admin/admin.php?page=NSN_WooExtSearch-editor&';
        $this->m_plugin_url= $containerClass->getPluginUrl();
        $this->m_plugin_dir= $containerClass->getPluginDir();
        $this->m_plugin_name= $containerClass->getPluginName();
        $this->m_labelNonce = 'nsn-woo-ext-search-options-editor-nonce';
        $this->m_labelNonceInput = 'nsn-woo-ext-search-options-editor-nonce-input';
        $this->m_categories_list = nsnWooToolsData::get_categories_list();
        $this->m_posts_of_service_category_list = array();
        $this->m_optionEditorItem = array();
        $this->m_fields_to_show = array( 'title', 'image', 'brand' , 'categories' , 'sale_price' , 'regular_price' , 'is_in_stock', 'purchase_note' , 'post_date'/*, 'sku' */);
        parent::__construct($containerClass, $wpdb, $session); //option_NextGEN_Gallery-shortcode-props

        $validationRules= ' nsn-woo-ext-search-meta_info_key, nsn-woo-ext-search-show_bookmark_functionality,  nsn-woo-ext-search-show_in_extended_search_rating, nsn-woo-ext-search-show_in_extended_search_sku, nsn-woo-ext-search-fields_to_show_rating, nsn-woo-ext-search-fields_to_show_sku, nsn-woo-ext-search-fields_to_show_tags, nsn-woo-ext-search-products_per_page, nsn-woo-ext-search-show_bookmark_alerts,   nsn-woo-ext-search-show_in_extended_search_post_title, nsn-woo-ext-search-show_in_extended_search_price_ranges,   nsn-woo-ext-search-show_in_extended_search_tag,   nsn-woo-ext-search-show_in_extended_search_categories_box,   nsn-woo-ext-search-show_in_extended_search_prices_box,    nsn-woo-ext-search-show_in_extended_search_hide_with_zero_products,   nsn-woo-ext-search-show_in_extended_search_show_number_of_products, ';
        // nsn-woo-ext-search-service_category_for_attrs,      nsn-woo-ext-search-service_post_for_help_text,
        /* nsn-woo-ext-search-products_pagination_per_page_list, */
        $this->m_products_attrs_list= nsnWooToolsData::get_products_attrs_list();
        foreach( $this->m_products_attrs_list as $next_products_attr ) {
            $validationRules.= ' nsn-woo-ext-search-show_attribute_in_extended_search_'.$next_products_attr['attribute_name'].',';
        }

        $this->setValidationRules(array('required' => $validationRules, 'integer'=> 'nsn-woo-ext-search-products_per_page, ' )); // must be called AFTER __construct

        $this->isDebug = false;
        // wp-content/plugins/wooExtSearch/views/woo_ext_search_form.php
        require_once( $containerClass->getPluginDir() . '/lib/app-nsn-woo-tools-data.php'); //wp-content/plugins/wooExtSearch/lib/app-nsn-woo-tools-data.php
    }

    protected function readRequestParameters()
    {
        $this->m_action = $this->getParameter('action', 'edit');
        $this->m_flashMessage = $this->getFlashMessage();   // TODO
    }


    public function main()
    {
        parent::main();

//        nsnWooToolsData::delete_product_comments(41);
//        nsnWooToolsData::insert_test_users(10);
        nsnWooToolsData::add_products_reviews(3);



        if ($this->isDebug) echo '<pre>$this->m_GETDataArray::' . print_r($this->m_GETDataArray, true) . '</pre>';
        if ($this->isDebug) echo '<pre>$this->m_POSTDataArray::' . print_r($this->m_POSTDataArray, true) . '</pre>';
        if (!in_array($this->m_action, array('list', 'edit', 'add', 'delete', 'update'))) $this->m_action = 'edit';  // List is default

        if ($this->isDebug) echo '<pre>$this->m_action::' . print_r($this->m_action, true) . '</pre>';

        echo '<pre>BEFORE get_option $this->m_hasValidationErrors::'.print_r($this->m_hasValidationErrors,true).'</pre>';
//        if ( ! ( !empty($this->m_hasValidationErrors) and is_array($this->m_hasValidationErrors) and count($this->m_hasValidationErrors) > 0 ) ) {  // there are NO submit error
        if (empty($_POST)) { // do not get data is the form issubmitted
//            die("INSIDE get_option");
            $this->m_optionEditorItem = get_option('nsn-woo-ext-search-');
            if (empty($this->m_optionEditorItem)) {
                $this->m_optionEditorItem = $this->getDefaultValues();
            }
        } /// if ( ! ( !empty($this->m_hasValidationErrors) and is_array($this->m_hasValidationErrors) and count($this->m_hasValidationErrors) > 0 ) ) {  // there are NO submit error

        if ( empty($this->m_optionEditorItem['meta_info_key']) ) {
//            $this->m_optionEditorItem['meta_info_key']= 'nsn-woo-ext-search-';
        }

        $postReturnData = $this->doPOSTRequest($this->m_action, $this->m_optionEditorItem, array(

            'meta_info_key'=> 'nsn-woo-ext-search-meta_info_key', 'show_bookmark_functionality'=> 'nsn-woo-ext-search-show_bookmark_functionality',
            'service_category_for_attrs'=> 'nsn-woo-ext-search-service_category_for_attrs', 'show_in_extended_search_rating'=>'nsn-woo-ext-search-show_in_extended_search_rating',
            'show_in_extended_search_sku'=> 'nsn-woo-ext-search-show_in_extended_search_sku', 'fields_to_show_rating'=>'nsn-woo-ext-search-fields_to_show_rating',
            'fields_to_show_sku'=>'nsn-woo-ext-search-fields_to_show_sku', 'fields_to_show_tags'=>'nsn-woo-ext-search-fields_to_show_tags',
            'products_per_page'=>'nsn-woo-ext-search-products_per_page', 'show_bookmark_alerts'=>'nsn-woo-ext-search-show_bookmark_alerts',
            'service_post_for_help_text'=>'nsn-woo-ext-search-service_post_for_help_text', 'service_post_for_help_text'=>'nsn-woo-ext-search-service_post_for_help_text',
            'show_in_extended_search_post_title'=>'nsn-woo-ext-search-show_in_extended_search_post_title', 'show_in_extended_search_price_ranges'=>'nsn-woo-ext-search-show_in_extended_search_price_ranges',
            'show_in_extended_search_tag'=>'nsn-woo-ext-search-show_in_extended_search_tag', 'show_in_extended_search_categories_box'=>'nsn-woo-ext-search-show_in_extended_search_categories_box',  'show_in_extended_search_prices_box'=> 'nsn-woo-ext-search-show_in_extended_search_prices_box',  'show_in_extended_search_hide_with_zero_products'=> 'nsn-woo-ext-search-show_in_extended_search_hide_with_zero_products', 'show_in_extended_search_show_number_of_products'=> 'nsn-woo-ext-search-show_in_extended_search_show_number_of_products'
        ));

        $this->m_optionEditorItem= isset($postReturnData['editorItem']) ? $postReturnData['editorItem'] : $this->m_optionEditorItem;
        echo '<pre>AFTER $postReturnData$this->m_optionEditorItem::'.print_r($this->m_optionEditorItem,true).'</pre>';
//        die("-1 XXZ -10");

        if ( !empty($this->m_optionEditorItem['service_category_for_attrs'] ) and is_numeric($this->m_optionEditorItem['service_category_for_attrs']) ) {
            $this->m_posts_of_service_category_list= nsnWooToolsData::get_posts_of_category($this->m_optionEditorItem['service_category_for_attrs'], false);
        }
        $this->m_action = isset($postReturnData['action']) ? $postReturnData['action'] : $this->m_action;

        if ($this->isDebug) echo '<pre>$this->m_action::' . print_r($this->m_action, true) . '</pre>';

        ob_start();
        $this->doEditorFlow($this);

        $output = ob_get_contents();
        ob_end_clean ();
        echo $output;
    } // public function main() {

    protected function setNotValidatedData($editorItem, $fieldsArray)
    {
        echo '<pre> setNotValidatedData  $editorItem::'.print_r($editorItem,true).'</pre>';
        echo '<pre> setNotValidatedData  $fieldsArray::'.print_r($fieldsArray,true).'</pre>';
//        die("INSIDE setNotValidatedData");
//        foreach( $fieldsArray as $nextItemField=>$nextDataField ) {

        $attribute_option_values= array();
        foreach( $this->m_POSTDataArray as $next_post_key=>$next_post_value ) {
            $a= preg_split('/nsn-woo-ext-search-show_attribute_in_extended_search_/',$next_post_key);
            if ( count($a)==2 and $next_post_value) {
                $attr_name = $a[1];
                $show_attribute_in_extended_search = $next_post_value;
                $attribute_service_category_attr= '';
                $show_attribute_in_post_products_list= '';

                if (!empty($this->m_POSTDataArray['nsn-woo-ext-search-show_attribute_link_to_post_' . $attr_name])) {
                    $show_attribute_link_to_post = $this->m_POSTDataArray['nsn-woo-ext-search-show_attribute_link_to_post_' . $attr_name];
                    if ($show_attribute_link_to_post== 'yes') {
                        if (!empty($this->m_POSTDataArray['nsn-woo-ext-search-attribute_service_category_attr_' . $attr_name])) {
                            $attribute_service_category_attr= $this->m_POSTDataArray['nsn-woo-ext-search-attribute_service_category_attr_' . $attr_name];
                        }
                    }
                }
                if (!empty($this->m_POSTDataArray['nsn-woo-ext-search-show_attribute_in_post_products_list_' . $attr_name])) {
                    $show_attribute_in_post_products_list = $this->m_POSTDataArray['nsn-woo-ext-search-show_attribute_in_post_products_list_' . $attr_name];
                }
                $attribute_option_values[]= array( 'attr_name'=>$attr_name, 'show_attribute_in_extended_search'=> $show_attribute_in_extended_search, 'show_attribute_link_to_post'=> $show_attribute_link_to_post, 'attribute_service_category_attr'=> $attribute_service_category_attr,  'show_attribute_in_post_products_list'=> $show_attribute_in_post_products_list );
                echo '<pre>$attribute_option_values::'.print_r($attribute_option_values,true).'</pre>';
//            if ( isset($this->m_POSTDataArray[$nextDataField]) ) {
//                $editorItem[$nextItemField]= $this->m_POSTDataArray[$nextDataField];
/*       [attribute_option_values] => Array
        (
            [0] => Array
                (
                    [attr_name] => brand
                    [show_attribute_in_extended_search] => yes
                    [show_attribute_link_to_post] => yes
                    [attribute_service_category_attr] =>
                    [show_attribute_in_post_products_list] => no


     [nsn-woo-ext-search-attribute_service_category_attr_brand_A4Tech] =>
                    [nsn-woo-ext-search-attribute_service_category_attr_brand_Firstblood] =>
                    [nsn-woo-ext-search-attribute_service_category_attr_brand_IBM] =>
                    [nsn-woo-ext-search-attribute_service_category_attr_brand_Idea] =>
                    [nsn-woo-ext-search-attribute_service_category_attr_brand_Logitech] =>
                    [nsn-woo-ext-search-attribute_service_category_attr_brand_Media_Singles] =>
                    [nsn-woo-ext-search-attribute_service_category_attr_brand_MSI] =>
                    [nsn-woo-ext-search-attribute_service_category_attr_brand_zero._brand*/
            }  // if ( count($a)==2 and $next_post_value) {
            else {
//                $editorItem[$nextItemField]= $this->m_POSTDataArray[$nextDataField];
                foreach( $fieldsArray as $next_field => $next_prefix_field ) { // all fields in turn


                    if ( $next_prefix_field == $next_post_key and isset($this->m_POSTDataArray[$next_post_key]) ) {
                        $editorItem[$next_field]= $this->m_POSTDataArray[$next_post_key];
                    }
                }  // foreach( $fieldsArray as $next_field=>$next_prefix_field ) { // all fields in turn
            }
        }
        $editorItem['attribute_option_values']= $attribute_option_values;
        echo '<pre>$editorItem::'.print_r($editorItem,true).'</pre>';
//        die("END setNotValidatedData");
        return $editorItem;
/*
          $a= preg_split('/nsn-woo-ext-search-show_attribute_in_extended_search_/',$next_post_key);
            if ( count($a)==2 and $next_post_value) {
                $attr_name = $a[1];
                $show_attribute_in_extended_search = $next_post_value;

                if (!empty($this->m_POSTDataArray['nsn-woo-ext-search-show_attribute_link_to_post_' . $attr_name])) {
                    $show_attribute_link_to_post = $this->m_POSTDataArray['nsn-woo-ext-search-show_attribute_link_to_post_' . $attr_name];
                    if ($show_attribute_link_to_post== 'yes') {
                        if (!empty($this->m_POSTDataArray['nsn-woo-ext-search-attribute_service_category_attr_' . $attr_name])) {
                            $attribute_service_category_attr= $this->m_POSTDataArray['nsn-woo-ext-search-attribute_service_category_attr_' . $attr_name];
                        }
                    }
                }
                if (!empty($this->m_POSTDataArray['nsn-woo-ext-search-show_attribute_in_post_products_list_' . $attr_name])) {
                    $show_attribute_in_post_products_list = $this->m_POSTDataArray['nsn-woo-ext-search-show_attribute_in_post_products_list_' . $attr_name];
                }
//                echo '<pre>$attribute_service_category_attr_::'.print_r($attribute_service_category_attr,true).'</pre>';
                $attribute_option_values[]= array( 'attr_name'=>$attr_name, 'show_attribute_in_extended_search'=> $show_attribute_in_extended_search, 'show_attribute_link_to_post'=> $show_attribute_link_to_post, 'attribute_service_category_attr'=> $attribute_service_category_attr,  'show_attribute_in_post_products_list'=> $show_attribute_in_post_products_list );
            }
 */
    }

//    protected function setNotValidatedData($editorItem, $fieldsArray)
//    {
//
//
//        echo '<pre> setNotValidatedData  $editorItem::'.print_r($editorItem,true).'</pre>';
//        echo '<pre> setNotValidatedData  $fieldsArray::'.print_r($fieldsArray,true).'</pre>';
////        die("INSIDE setNotValidatedData");
//        foreach( $fieldsArray as $nextItemField=>$nextDataField ) {
//            if ( isset($this->m_POSTDataArray[$nextDataField]) ) {
//                $editorItem[$nextItemField]= $this->m_POSTDataArray[$nextDataField];
//            }
//        }
//        return $editorItem;
//    }
//
    private function getDefaultValues() {
        die("INSIDE  getDefaultValues!!");
        return array( 'option_values' => Array(),
                 'fields_to_show_array' => Array('title' => 'no', 'image' => 'no', 'brand' => 'no', 'categories' => 'no', 'sale_price' => 'no', 'regular_price' => 'no', 'is_in_stock' => 'no', 'purchase_note' => 'no', 'post_date' => 'no', 'rating' => 'no', 'sku' => 'no'   ),
                 'show_in_extended_search_rating' => 'no', 'show_in_extended_search_sku' => 'no', 'fields_to_show_rating' => 'no', 'fields_to_show_sku' => 'no','fields_to_show_tags' => 'no', 'show_bookmark_functionality' => 'no', 'service_category_for_attrs_array' => 'no',

            'products_per_page' => 10, 'products_pagination_per_page_list' => '', 'show_bookmark_alerts' => 'no', 'service_post_for_help_text' => 'no', 'show_in_extended_search_post_title' => 'no', 'show_in_extended_search_price_ranges' => 'no', 'show_in_extended_search_tag' => 'no', 'show_in_extended_search_categories_box' => 'no', 'show_in_extended_search_prices_box' => 'no', 'show_in_extended_search_hide_with_zero_products' => 'yes' , 'show_in_extended_search_show_number_of_products' => 'yes' );
    }

    protected function preparePageParameters($WithPage, $WithSort) // Prepare url for pagination with all fields in filter and sort and sort_direction
    {
    } // protected function preparePageParameters($WithPage, $WithSort) // Prepare url for pagination with all fields in filter and sort and sort_direction

    protected function showRowsListDataForm($parentControl)
    {

    }

    protected function addRowDataForm($parentControl)
    {
    }

    protected function initFieldsAddRow( $parentControl )
    {
    }


    protected function editRowDataForm($parentControl)
    {
        $m_parentControl = $parentControl;
        require_once($this->m_pluginDir . 'views/admin/header.php');
        require_once($this->m_pluginDir . 'views/admin/options-editor.php');
        require_once($this->m_pluginDir . 'views/admin/footer.php');
    }


    protected function doBulkAction($action, $params = array(), $isInsert = 0)
    {
    } // protected function doBulkAction() {




    protected function doValidationWWW($data)
    {

//        $this->m_hasValidationErrors= parent::doValidation($data);
//        return $this->m_hasValidationErrors;

//        $this->m_hasValidationErrors= parent::doValidation($data);
        echo '<pre> ++ doValidation$this->m_hasValidationErrors::'.print_r($this->m_hasValidationErrors,true).'</pre>';
        if ( count($this->m_hasValidationErrors) > 0 ) {
            echo '<pre>$data::'.print_r($data,true).'</pre>';
            echo '<pre> ++$this->m_optionEditorItem::'.print_r($this->m_optionEditorItem,true).'</pre>';


            foreach( $data as $next_data_key=>$next_data_value ) {
//                if ( $next_data_key != 'nsn-woo-ext-search-products_per_page') continue;
                echo '<pre>$next_data_key::'.print_r($next_data_key,true).'</pre>';
                echo '<pre>$next_data_value::'.print_r($next_data_value,true).'</pre>';
                if ( !is_array($next_data_value) ) {
                    echo '<pre>INSIDE1::' . print_r(1, true) . '</pre>';
//                    if (isset($data['nsn-woo-ext-search-'.$next_key])) {
                    $a = preg_split('/nsn-woo-ext-search-/', $next_data_key);
                    if (count($a) == 2) {
                        echo '<pre>INSIDE2::' . print_r(2, true) . '</pre>';
                        $this->m_optionEditorItem[$next_data_key] = $next_data_value;
                    }
//                    }
                } // if ( !is_array($next_optionEditorItem) ) {
                else {

                }
            }
            echo '<pre> ++AFTERR $this->m_optionEditorItem::'.print_r($this->m_optionEditorItem,true).'</pre>';
//            die("-1 XXZ");

//            foreach( $this->m_optionEditorItem as $next_key=>$next_optionEditorItem ) {
////                if ( $next_key != 'show_bookmark_functionality') continue;
//                echo '<pre>$next_key::'.print_r($next_key,true).'</pre>';
//                echo '<pre>$next_optionEditorItem::'.print_r($next_optionEditorItem,true).'</pre>';
//                if ( !is_array($next_optionEditorItem) ) {
//                    echo '<pre>INSIDE1::'.print_r(1,true).'</pre>';
//                    if (isset($data['nsn-woo-ext-search-'.$next_key])) {
//                        echo '<pre>INSIDE2::'.print_r(2,true).'</pre>';
//                        $this->m_optionEditorItem[$next_key]= $data['nsn-woo-ext-search-'.$next_key];
//                    }
//                } // if ( !is_array($next_optionEditorItem) ) {
//                else {
//
//                }
//            }
//
//
//            die("-1 XXZ");
        }

//        die("-1 XXZ");
        return $this->m_hasValidationErrors;
    } // protected function doValidation($data) {

    protected function saveRowData($action, $editorItem)
    {
//        echo '<pre>$this->m_POSTDataArray::'.print_r($this->m_POSTDataArray,true).'</pre>';
//        echo '<pre>$_POST::'.print_r($_POST,true).'</pre>';

        $meta_info_key= '';
        $show_bookmark_functionality= '';
        $service_category_for_attrs= '';
        $service_post_for_help_text= '';
        $show_bookmark_alerts= '';
        $products_per_page= '';
        $products_pagination_per_page_list= '';
        $attribute_option_values= array();
        $fields_to_show_array= array();

        $show_in_extended_search_rating= '';
        $show_in_extended_search_tag= '';
        $show_in_extended_search_prices_box= '';
        $show_in_extended_search_categories_box= '';
        $show_in_extended_search_hide_with_zero_products= '';
        $show_in_extended_search_show_number_of_products= '';
        $show_in_extended_search_price_ranges= '';
        $show_in_extended_search_post_title= '';
        $show_in_extended_search_sku= '';
        $fields_to_show_rating= '';
        $fields_to_show_sku= '';
        $fields_to_show_tags= '';
        $show_attribute_link_to_post= '';
        $attribute_service_category_attr= '';
        $show_attribute_in_post_products_list= '';

        foreach( $this->m_POSTDataArray as $next_post_key=>$next_post_value ) {
            $a= preg_split('/nsn-woo-ext-search-fields_to_show_/',$next_post_key);
            if ( count($a)==2 and $next_post_value) {
                $fields_to_show_array[$a['1']] = $next_post_value;
            }

            $a= preg_split('/nsn-woo-ext-search-show_in_extended_search_rating/',$next_post_key);
            if ( count($a)==2 and $next_post_value) {
                $show_in_extended_search_rating = $next_post_value;
            }

            $a= preg_split('/nsn-woo-ext-search-show_in_extended_search_tag/',$next_post_key);
            if ( count($a)==2 and $next_post_value) {
                $show_in_extended_search_tag = $next_post_value;
            }

            $a= preg_split('/nsn-woo-ext-search-show_in_extended_search_prices_box/',$next_post_key);
            if ( count($a)==2 and $next_post_value) {
                $show_in_extended_search_prices_box = $next_post_value;
            }

            $a= preg_split('/nsn-woo-ext-search-show_in_extended_search_categories_box/',$next_post_key);
            if ( count($a)==2 and $next_post_value) {
                $show_in_extended_search_categories_box = $next_post_value;
            }

            $a= preg_split('/nsn-woo-ext-search-show_in_extended_search_hide_with_zero_products/',$next_post_key);
            if ( count($a)==2 and $next_post_value) {
                $show_in_extended_search_hide_with_zero_products = $next_post_value;
            }

            $a= preg_split('/nsn-woo-ext-search-show_in_extended_search_show_number_of_products/',$next_post_key);
            if ( count($a)==2 and $next_post_value) {
                $show_in_extended_search_show_number_of_products = $next_post_value;
            }

            $a= preg_split('/nsn-woo-ext-search-show_in_extended_search_price_ranges/',$next_post_key);
            if ( count($a)==2 and $next_post_value) {
                $show_in_extended_search_price_ranges = $next_post_value;
            }

            $a= preg_split('/nsn-woo-ext-search-show_in_extended_search_post_title/',$next_post_key);
            if ( count($a)==2 and $next_post_value) {
                $show_in_extended_search_post_title = $next_post_value;
            }

            $a= preg_split('/nsn-woo-ext-search-show_in_extended_search_sku/',$next_post_key);
            if ( count($a)==2 and $next_post_value) {
                $show_in_extended_search_sku = $next_post_value;
            }

            $a= preg_split('/nsn-woo-ext-search-fields_to_show_rating/',$next_post_key);
            if ( count($a)==2 and $next_post_value) {
                $fields_to_show_rating = $next_post_value;
            }

            $a= preg_split('/nsn-woo-ext-search-fields_to_show_sku/',$next_post_key);
            if ( count($a)==2 and $next_post_value) {
                $fields_to_show_sku = $next_post_value;
            }

            $a= preg_split('/nsn-woo-ext-search-fields_to_show_tags/',$next_post_key);
            if ( count($a)==2 and $next_post_value) {
                $fields_to_show_tags = $next_post_value;
            }

            $a= preg_split('/nsn-woo-ext-search-meta_info_key/',$next_post_key);
            if ( count($a)==2 and $next_post_value) {
                $meta_info_key = $next_post_value;
            }

            $a= preg_split('/nsn-woo-ext-search-show_bookmark_functionality/',$next_post_key);
            if ( count($a)==2 and $next_post_value) {
                $show_bookmark_functionality = $next_post_value;
            }

            $a= preg_split('/nsn-woo-ext-search-service_category_for_attrs/',$next_post_key);
            if ( count($a)==2 and $next_post_value) {
                $service_category_for_attrs = $next_post_value;
            }

            $a= preg_split('/nsn-woo-ext-search-products_per_page/',$next_post_key);
            if ( count($a)==2 and $next_post_value) {
                $products_per_page = $next_post_value;
            }

            $a= preg_split('/nsn-woo-ext-search-products_pagination_per_page_list/',$next_post_key);
            if ( count($a)==2 and $next_post_value) {
                $products_pagination_per_page_list = $next_post_value;
            }

            $a= preg_split('/nsn-woo-ext-search-show_bookmark_alerts/',$next_post_key);
            if ( count($a)==2 and $next_post_value) {
                $show_bookmark_alerts = $next_post_value;
            }

            $a= preg_split('/nsn-woo-ext-search-service_post_for_help_text/',$next_post_key);
            if ( count($a)==2 and $next_post_value) {
                $service_post_for_help_text = $next_post_value;
            }

            $a= preg_split('/nsn-woo-ext-search-show_attribute_in_extended_search_/',$next_post_key);
            if ( count($a)==2 and $next_post_value) {
                $attr_name = $a[1];
                $show_attribute_in_extended_search = $next_post_value;

                if (!empty($this->m_POSTDataArray['nsn-woo-ext-search-show_attribute_link_to_post_' . $attr_name])) {
                    $show_attribute_link_to_post = $this->m_POSTDataArray['nsn-woo-ext-search-show_attribute_link_to_post_' . $attr_name];
                    if ($show_attribute_link_to_post== 'yes') {
                        if (!empty($this->m_POSTDataArray['nsn-woo-ext-search-attribute_service_category_attr_' . $attr_name])) {
                            $attribute_service_category_attr= $this->m_POSTDataArray['nsn-woo-ext-search-attribute_service_category_attr_' . $attr_name];
                        }
                    }
                }
                if (!empty($this->m_POSTDataArray['nsn-woo-ext-search-show_attribute_in_post_products_list_' . $attr_name])) {
                    $show_attribute_in_post_products_list = $this->m_POSTDataArray['nsn-woo-ext-search-show_attribute_in_post_products_list_' . $attr_name];
                }
//                echo '<pre>$attribute_service_category_attr_::'.print_r($attribute_service_category_attr,true).'</pre>';
                $attribute_option_values[]= array( 'attr_name'=>$attr_name, 'show_attribute_in_extended_search'=> $show_attribute_in_extended_search, 'show_attribute_link_to_post'=> $show_attribute_link_to_post, 'attribute_service_category_attr'=> $attribute_service_category_attr,  'show_attribute_in_post_products_list'=> $show_attribute_in_post_products_list );
            }
        } // foreach( $this->m_POSTDataArray as $next_post_key=>$next_post_value ) {

        $service_category_for_attrs_array= array();
        foreach( $this->m_products_attrs_list as $next_products_attr ) {
            $next_attribute_name= $next_products_attr['attribute_name'];


//            echo '<pre>$next_attribute_name::'.print_r($next_attribute_name,true).'</pre>';
            if (empty($next_products_attr['attrs']) or !is_array($next_products_attr['attrs'])) continue;
            $attribute_attrs_array= array();
            foreach( $next_products_attr['attrs'] as $next_attr ) {
                $next_attr_changed_name= nsnWooToolsData::changeSubmitKey($next_attr['name']);
//                echo '<pre>$next_attr_changed_name::'.print_r($next_attr_changed_name,true).'</pre>';
                $next_attr_name= 'nsn-woo-ext-search-attribute_service_category_attr_' . $next_attribute_name . '_'. $next_attr_changed_name;
//                echo '<pre>$next_attr_name::'.print_r($next_attr_name,true).'</pre>';
                $attribute_attrs_array[$next_attr_name]= isset( $this->m_POSTDataArray[$next_attr_name] ) ? $this->m_POSTDataArray[$next_attr_name] : "";
            }
            $service_category_for_attrs_array[$next_attribute_name]= $attribute_attrs_array;
        }
//        echo '<pre>$service_category_for_attrs_array::'.print_r($service_category_for_attrs_array,true).'</pre>';


        $this->setFlashMessage( esc_html__("Options")." " . esc_html__("modified") );

//        echo '<pre>$show_in_extended_search_price_ranges::'.print_r($show_in_extended_search_price_ranges,true).'</pre>';
//        echo '<pre>$products_per_page::'.print_r($products_per_page,true).'</pre>';
//        echo '<pre>$products_pagination_per_page_list::'.print_r($products_pagination_per_page_list,true).'</pre>';

        update_option('nsn-woo-ext-search-', array( 'attribute_option_values'=> $attribute_option_values, 'fields_to_show_array'=> $fields_to_show_array,
            'show_in_extended_search_rating'=> $show_in_extended_search_rating, 'show_in_extended_search_tag'=> $show_in_extended_search_tag,'show_in_extended_search_categories_box'=> $show_in_extended_search_categories_box,'show_in_extended_search_prices_box'=> $show_in_extended_search_prices_box, 'show_in_extended_search_hide_with_zero_products'=> $show_in_extended_search_hide_with_zero_products, 'show_in_extended_search_show_number_of_products'=> $show_in_extended_search_show_number_of_products,

            'show_in_extended_search_post_title' => $show_in_extended_search_post_title, 'show_in_extended_search_price_ranges'=> $show_in_extended_search_price_ranges, 'show_in_extended_search_sku'=> $show_in_extended_search_sku, 'fields_to_show_rating'=> $fields_to_show_rating, 'fields_to_show_sku'=> $fields_to_show_sku,'fields_to_show_tags'=> $fields_to_show_tags,
            'meta_info_key'=> $meta_info_key, 'show_bookmark_functionality'=> $show_bookmark_functionality, 'service_category_for_attrs'=> $service_category_for_attrs, 'service_category_for_attrs_array' => $service_category_for_attrs_array, 'products_per_page'=> $products_per_page, 'products_pagination_per_page_list'=> $products_pagination_per_page_list, 'show_bookmark_alerts'=> $show_bookmark_alerts, 'service_post_for_help_text' => $service_post_for_help_text ) );

//        echo '<pre>$attribute_option_values::'.print_r($attribute_option_values,true).'</pre>';
//        die("-1 XXZ");

//        $this->setFlashMessage(esc_html__("Options") . " " . esc_html__("updated"));
        $redirectUrl = get_site_url() . $this->m_pageUrl;
        wp_safe_redirect($redirectUrl);

    }

    protected function deleteRowData()
    {
    }
}