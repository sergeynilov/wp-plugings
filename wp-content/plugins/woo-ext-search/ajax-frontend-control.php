<?php
define( 'DOING_AJAX', true );

require_once( '../../../wp-load.php' );

if ( ! defined( 'NSN_WooExtSearch' ) ) {
    exit; // Exit if accessed directly
}
require_once( 'appWooExtSearchControl.php');


class ajaxFrontendControl extends  appNSN_WooExtSearchControl
{
    /*  private $isDebug; */

    private $m_service_category_for_attrs_array= array();
    private $m_fields_to_show = array();
    private $m_attrs_to_show = array();
    private $m_woo_ext_search_options = '';
    private $m_sample_prices_list = array();
    public function __construct($containerClass, $wpdb)
    {
        global $session;
        parent::__construct($containerClass, $wpdb, $session);
        if ( !empty($containerClass) ) {
            $this->m_plugin_url = $containerClass->getPluginUrl();
            $this->m_plugin_dir = $containerClass->getPluginDir();
        }
        $this->setValidationRules( array() ); // must be called AFTER __construct
        //         require_once( $this->m_pluginDir . '/lib/app-nsn-woo-tools-data.php'); //wp-content/plugins/wooExtSearch/lib/app-nsn-woo-tools-data.php
        $this->m_woo_ext_search_options = get_option('nsn-woo-ext-search-');
//        DebToFile('-10  $this->m_woo_ext_search_options ::' .print_r($this->m_woo_ext_search_options,true) );
        $this->isDebug= true;
    }

    protected function readRequestParameters()
    {
        //$this->m_action = isset($_REQUEST[ 'action' ]) ? $_REQUEST[ 'action' ] : $_REQUEST[ 'action' ];
        $this->m_action = $this->getParameter( 'action' );
        if ( isset($_GET['action']) and $_GET['action'] == 'upload-artist-photo' ) {
            $this->m_action= 'upload-artist-photo';
        }
    } // protected function readRequestParameters() {


    public function main() {
//        $this->m_plugin_dir = trailingslashit(WP_PLUGIN_DIR.'/'.dirname(plugin_basename(__FILE__)));
//        DebToFile('-1 $ajaxFrontendControlObj $_POST ::' .print_r($_POST,true) );
//        DebToFile('-1 $ajaxFrontendControlObj $_REQUEST ::' .print_r($_REQUEST,true) );

        require_once(  'lib/app-nsn-woo-tools-data.php');
        parent::main();
        $this->isDebug= false;
//        if ($this->isDebug) echo '<pre>$_POST::'.print_r($_POST,true).'</pre>';
        if ($this->isDebug) echo '<pre>$_REQUEST::'.print_r($_REQUEST,true).'</pre>';
        if ($this->isDebug) echo '<pre>$this->m_GETDataArray::'.print_r($this->m_GETDataArray,true).'</pre>';
        if ($this->isDebug) echo '<pre>$this->m_POSTDataArray::'.print_r($this->m_POSTDataArray,true).'</pre>';
        DebToFile('-1 $ajaxFrontendControlObj $_REQUEST ::' .print_r($_REQUEST,true) );
        DebToFile('-1 $ajaxFrontendControlObj $_FILES ::' .print_r($_FILES,true) );
//        DebToFile('-1 $ajaxFrontendControlObj $this->m_GETDataArray ::' .print_r($this->m_GETDataArray,true) );
//        DebToFile('-1 $ajaxFrontendControlObj $this->m_POSTDataArray ::' .print_r($this->m_POSTDataArray,true) );
//        DebToFile('-1 $ajaxFrontendControlObj $this->m_action ::' .print_r($this->m_action,true) );
        $action = $this->getParameter( 'action' );
        DebToFile('-1 $ajaxFrontendControlObj $action ::' .print_r($action,true) );
        //echo '<pre>$action::'.print_r($action,true).'</pre>';
        //if ( !in_array($action,array('list','edit', 'add', 'delete', 'update')) ) $action= 'list';  // List is default

        $m_parentControl = $this;

        if ( $this->m_is_trimDataBeforeSave ) {
            foreach( $_REQUEST as $nextKey=>$nextValue ) {
                if ( !is_array($nextValue) ) {
                    $_REQUEST[$nextKey] = trim($nextValue);
                }
            }
        }

        if (empty($_REQUEST['action']) ) {
            echo json_encode( array( 'error_code' => 1, 'error_message'=>'action parameter must be!', 'current_user_id'=> $m_parentControl->m_currentUserId ) );
            exit;
        }
//
        if ($_REQUEST['action'] == 'get-woo-products-by-filter') {  // get list of products by filters
            $fields_to_show_array = ((!empty($this->m_woo_ext_search_options['fields_to_show_array']) and is_array($this->m_woo_ext_search_options['fields_to_show_array'])) ? $this->m_woo_ext_search_options['fields_to_show_array'] : array());
//            DebToFile( '-1 $fields_to_show_array::' .print_r($fields_to_show_array, true) );

            $this->m_service_category_for_attrs_array = ((!empty($this->m_woo_ext_search_options['service_category_for_attrs_array']) and is_array($this->m_woo_ext_search_options['service_category_for_attrs_array'])) ? $this->m_woo_ext_search_options['service_category_for_attrs_array'] : array());

            foreach ($fields_to_show_array as $next_fields_to_show_name => $next_fields_to_show_value) {
                if ($next_fields_to_show_value == 'yes' and !empty($next_fields_to_show_name)) {
                    $this->m_fields_to_show[$next_fields_to_show_name] = true;
                }
            }


            $this->m_attrs_to_show = ((!empty($this->m_woo_ext_search_options['attribute_option_values']) and is_array($this->m_woo_ext_search_options['attribute_option_values'])) ? $this->m_woo_ext_search_options['attribute_option_values'] : array());
            $attrs_to_show_name_array= array();
            foreach( $this->m_attrs_to_show as $next_key => $next_attrs_to_show ) {
                if ( $next_attrs_to_show['show_attribute_in_extended_search'] == 'yes' ) {
                    $this->m_fields_to_show[$next_attrs_to_show['attr_name']]= 1;
                    $attrs_to_show_name_array[]= $next_attrs_to_show['attr_name'];
                }
            }
//            DebToFile( '-1 $this->m_fields_to_show::' .print_r($this->m_fields_to_show, true) );
//            DebToFile( '-22 $this->m_attrs_to_show::' .print_r($this->m_attrs_to_show, true) );

            $orderby = $this->getParameter('orderby', 'date');
            $items_per_page = $this->getParameter('items_per_page', '');
            $partial_sku = $this->getParameter('partial_sku', '');
            $sku = $this->getParameter('sku', '');
            $partial_title = $this->getParameter('partial_title', '');
            $post_title = $this->getParameter('post_title', '');
            $rating_min = $this->getParameter('rating_min', 1);
            $rating_max = $this->getParameter('rating_max', 5);
            $include_without_rating = $this->getParameter('include_without_rating', '');
            $only_in_stock = $this->getParameter('only_in_stock', 1);
            $page = $this->getParameter('page', '1');

            $filter_categories_list = $this->getParameter('categories_list', array());
            $attrs_checked_list = $this->getParameter('attrs_checked_list', array());

            if ( !empty($this->m_woo_ext_search_options['show_in_extended_search_price_ranges']) ) { //=> 5, 10, 20, 50, 100
                $this->m_sample_prices_list= nsnWooToolsData::prices_string_into_list( $this->m_woo_ext_search_options['show_in_extended_search_price_ranges'] );
            }
//            DebToFile( '-1 RUN FILTER $this->m_sample_prices_list::' .print_r($this->m_sample_prices_list, true) );
            $filter_prices_list = $this->getParameter('prices_list',array());
//            DebToFile('-1 RUN FILTER $filter_prices_list::' .print_r($filter_prices_list,true) );
            $prices_conditions= array();
            $query_array= array();

            $prices_query_sub_array= array('relation' => 'OR');
            foreach( $filter_prices_list as $next_key => $next_filter_price ) {
                foreach( $this->m_sample_prices_list as $next_sample_price ) {
                    if ( (int)$next_filter_price == (int)$next_sample_price['max'] ) {
                        $prices_conditions[]= $next_sample_price;
                        $prices_query_sub_array[] = array(
                            'key' => '_price',
                            'value' => array(floatval($next_sample_price['min']), floatval($next_sample_price['max'])),
                            'type' => 'DECIMAL',
                            'compare' => 'BETWEEN',
                        );
                        break;
                    }
                }
            }
//            DebToFile( '-1 $prices_query_sub_array::' .print_r($prices_query_sub_array, true) );
            if ( count($prices_query_sub_array) > 1 ) { // there are price conditions
                $query_array[]= $prices_query_sub_array;
            } // if ( count($prices_query_sub_array) > 0 ) { // there are price conditions


            $attrs_checked_tax_query_array= array(/*'relation' => 'OR'*/);
            foreach( $attrs_checked_list as $next_key=>$next_attrs_checked ) {
                $attrs_checked_tax_query_array[] =  array(
                    'taxonomy' => 'pa_' . $next_attrs_checked[0],
                    'field'    => 'slug',
                    'terms'    => $next_attrs_checked[1],
                );
            }
//            DebToFile( '-1 $attrs_checked_tax_query_array::' .print_r($attrs_checked_tax_query_array, true) );
            $result_attrs_checked_tax_query= array();
            $result_attrs_checked_tax_sub_query= array('relation' => 'OR');
            $is_first_row= true;
            $is_last_row_added= false;
            $current_taxonomy= '';
            foreach( $attrs_checked_tax_query_array as $next_key=>$next_attrs_checked_tax_query ) {
                $is_last_row_added= false;
                if ($is_first_row) {
                    $current_taxonomy= $next_attrs_checked_tax_query['taxonomy'];
                    $result_attrs_checked_tax_sub_query[]= $next_attrs_checked_tax_query;
                }
                if (!$is_first_row) {
                    if ( $current_taxonomy!= $next_attrs_checked_tax_query['taxonomy'] ) { // find new taxonomy - write current subarray and start new subarray
                        $result_attrs_checked_tax_query[]= array('relation' => 'AND',$result_attrs_checked_tax_sub_query);
                        $result_attrs_checked_tax_sub_query= array('relation' => 'OR', $next_attrs_checked_tax_query);
                        $is_last_row_added= true;
                        $current_taxonomy= $next_attrs_checked_tax_query['taxonomy'];
                    } else {
                        $result_attrs_checked_tax_sub_query[]/*[0][]*/= $next_attrs_checked_tax_query;
                    }
                }
                $is_first_row= false;
            }
//            DebToFile( 'END OF LOOP $is_last_row_added::' .print_r($is_last_row_added, true) );
//            DebToFile( 'END OF LOOP $is_first_row::' .print_r($is_first_row, true) );
//            DebToFile( 'END OF LOOP $result_attrs_checked_tax_sub_query::' .print_r($result_attrs_checked_tax_sub_query, true) );
            if ( /*!$is_last_row_added and  */ !empty($result_attrs_checked_tax_sub_query)  ) {
//                DebToFile( 'END OF LOOP INSIDE $result_attrs_checked_tax_sub_query[0]::' .print_r( $result_attrs_checked_tax_sub_query, true) );
//                $result_attrs_checked_tax_sub_query[]= $next_attrs_checked_tax_query;
                $result_attrs_checked_tax_query[0][] = $result_attrs_checked_tax_sub_query;
//                $result_attrs_checked_tax_query[0][] = array('relation' => 'AND', $result_attrs_checked_tax_sub_query);
//                $result_attrs_checked_tax_query[ count($result_attrs_checked_tax_query)-1 ][0][] = array('relation' => 'AND', $result_attrs_checked_tax_sub_query);
            }
//            if ( !$is_last_row_added and !empty($result_attrs_checked_tax_sub_query[0])  ) {
//                DebToFile( 'END OF LOOP INSIDE $result_attrs_checked_tax_sub_query[0]::' .print_r( $result_attrs_checked_tax_sub_query[0], true) );
//                $result_attrs_checked_tax_sub_query[]= $next_attrs_checked_tax_query;
//                $result_attrs_checked_tax_query[ count($result_attrs_checked_tax_query)-1 ][0][] = array('relation' => 'AND', $result_attrs_checked_tax_sub_query[0]);
//            }
//            if ( count($result_attrs_checked_tax_sub_query) > 1 ) {
//                $result_attrs_checked_tax_query[] = array('relation' => 'AND', $result_attrs_checked_tax_sub_query);
//            }
//            DebToFile( '-001 $result_attrs_checked_tax_query::' .print_r($result_attrs_checked_tax_query, true) );




            $filter_tags_list = $this->getParameter('tags_list',array());
//            DebToFile('-103 RUN FILTER $filter_tags_list::' .print_r($filter_tags_list,true) );
            $tags_list= array();
            if (!empty($filter_tags_list) and is_array($filter_tags_list)) {
                foreach( $filter_tags_list as $next_tag ) {
                    if ( !empty($next_tag) ) {
                        $tags_list[]= $next_tag;
                    }
                }
            }
//            DebToFile('-1031 RUN FILTER $tags_list::' .print_r($tags_list,true) );

//            DebToFile('-13 RUN FILTER $query_array::' .print_r($query_array,true) );
            $categories_list= array();
            if (!empty($filter_categories_list) and is_array($filter_categories_list)) {
                foreach( $filter_categories_list as $next_category ) {
//                    echo '<pre>$next_category::'.print_r($next_category,true).'</pre>';
                    if ( !empty($next_category) ) {
                        $categories_list[]= $next_category;
                    }
                }
            }

//            DebToFile('-120 $partial_title ::' .print_r($partial_title,true) );
//            DebToFile('-12 $partial_sku ::' .print_r($partial_sku,true) );
//            DebToFile('-11 $categories_list ::' .print_r($categories_list,true) );
            $meta_query_array= array();
            if ( count($query_array) > 0 /*) or (0) */ ) {
//                $meta_query_array = array('relation' => 'AND');
                $meta_query_array = array('relation' => 'AND');
                foreach( $query_array as $next_key => $next_query ) {
                    $meta_query_array[]= $next_query;
                }
            }


//            $sku= 'battery';
            if ( !empty($sku) ) {
                $meta_query_array[]= array(
                    'key' => '_sku',
                    'value'   => $sku,
                    'compare' => ( $partial_sku ? 'like' : '=' ),
                );
            }

            if ( $only_in_stock ) {
                $meta_query_array[]= array(
                    'key' => '_stock_status',
                    'value' => 'instock'
                );
            }

            $args = array(         //$filter_categories_list
                's'                   => $post_title,
                'post_type'           => 'product',
                'post_status'         => 'publish',
                'ignore_sticky_posts' => -1,
                'numberposts'         => -1, // show all
                'orderby'             => $orderby,
                'posts_per_page'      => -1,
                'suppress_filters'    => false,
                'tax_query'           => array(),
            );
            if ( $partial_title ) {
                $args['sentence'] = true;
            } else {
                $args['exact'] = true;
            }

            if ( count($meta_query_array) > 0 ) {
                $args['meta_query'] = $meta_query_array;
            }

            if ( count($result_attrs_checked_tax_query) > 0 ) {
                $args['tax_query'][]= $result_attrs_checked_tax_query;
            }

            if ( count($categories_list) > 0 ) {
                $args['tax_query'][]= array(
                    array(
                        'taxonomy'  => 'product_cat',
                        'field'     => 'id',
                        'terms'     => $categories_list
                    )
                );
            }



            $products_list = array();

            $page_rows_start = -1;
            $page_rows_end = -1;

            $posts_per_page = (int)$this->m_woo_ext_search_options['products_per_page'];
            if ( !empty($items_per_page) and (int)$items_per_page > 0 ) {
                $posts_per_page= $items_per_page;
            }

//            $offset = ($page - 1) * $posts_per_page;
            if ( !empty($posts_per_page) and (int)$posts_per_page > 0) {
                $page_rows_start = ($page - 1) * $posts_per_page;
                $page_rows_end = $page_rows_start + $posts_per_page;
            }
            if (empty($posts_per_page) or !is_numeric($posts_per_page)) $posts_per_page= 10;

            $navigation_classes= array('current_page'=> 'nsn_woo_ext_search_current_page', 'next_link_page'=> 'nsn_woo_ext_search_next_link_page', 'external_div'=> 'nsn_woo_ext_search_external_div');

            DebToFile('-1 $ajaxFrontendControlObj $args ::' .print_r($args,true) );

            $raw_products = get_posts( $args );
//            DebToFile('-012 $raw_products ::' .print_r($raw_products,true) );
            $additive_attrs= array( 'woocommerce_price_format'=> get_woocommerce_price_format(), 'woocommerce_currency_symbol'=> get_woocommerce_currency_symbol( get_woocommerce_currency() ), 'decimal_separator' => wc_get_price_decimal_separator(), 'thousand_separator' => wc_get_price_thousand_separator(), 'decimals' => wc_get_price_decimals(), 'date_format'=> get_option('date_format'), 'time_format'=> get_option('time_format')   );
            $products= array();
            foreach( $raw_products as $next_key => $next_raw_product_post ) { // all rows from db in for circle
//                if ( !in_array($next_raw_product_post->ID, array( 99, 96, 93, 90, 87 ) ) ) continue;
//                if ( !in_array($next_raw_product_post->ID, array( 297 ) ) ) continue;

                $woo_next_product = nsnWooToolsData::get_woo_product_props( $next_raw_product_post->ID /*$next_product->ID*/, array('image', 'categories_list','tags_list', 'sale_price', 'regular_price', 'sku', 'rating', 'is_in_stock', 'purchase_note'), $attrs_to_show_name_array, $additive_attrs);
                $woo_next_product['guid'] = $next_raw_product_post->guid;
                $woo_next_product['post_title'] = $next_raw_product_post->post_title;
                $woo_next_product['image_c'] = htmlspecialchars($next_raw_product_post->image);


                $add_product_to_results= true;
                if ( !empty($tags_list) and is_array($tags_list) and count($tags_list) > 0 ) {
                    $product_tags_list = nsnWooToolsData::get_tags_list_by_product($next_raw_product_post, false);
                    DebToFile('-0123 $product_tags_list ::' .print_r($product_tags_list,true) );
                    if ( count($product_tags_list) == 0 ) { // this product has no tags at all
                        $add_product_to_results = false;
                    }
//                DebToFile('-11 $ajaxFrontendControlObj $next_raw_product_post ::' .print_r($next_raw_product_post,true) );
//                DebToFile('-11 $ajaxFrontendControlObj $woo_next_product ::' .print_r($woo_next_product,true) );
                    $add_product_to_results = false;
                    foreach ($product_tags_list as $next_key_product_tag => $next_product_tag) {
                        DebToFile('-01233 $next_product_tag->term_id ::' .print_r($next_product_tag->term_id,true) );
                        if (in_array($next_product_tag->term_id, $tags_list)) {
                            $add_product_to_results = true;
                            break;
                        }
                    }
                } // if ( !empty($tags_list) and is_array($tags_list) and count($tags_list) > 0 ) {

//                if ( !$only_in_stock or $woo_next_product['is_in_stock'] ) {
//                    $skip_product= false;
//                }

/*             $ret_array['rating_average'] = ceil( $average );
            $ret_array['star_rating'] = ( $average / 5 ) * 100;
 */
                $woo_next_product_rating= (float)!empty($woo_next_product['rating_average']) ? $woo_next_product['rating_average'] : 0;
//                DebToFile('-012321 $include_without_rating ::' .print_r($include_without_rating,true) );
//                DebToFile('-012321 $woo_next_product_rating ::' .print_r($woo_next_product_rating,true) );
//                DebToFile('-012321 $rating_min ::' .print_r($rating_min,true) );
//                DebToFile('-012321 $rating_max ::' .print_r($rating_max,true) );
                if ( empty($woo_next_product_rating) ) { // product HAS NO rating
//                    DebToFile('EMPTY 1' .print_r(1,true) );
                    if (!$include_without_rating) { // flag include_without_rating is not set
                        $add_product_to_results = false;
//                        DebToFile('EMPTY 2' .print_r(2,true) );
                    }
                }
                else {  // product HAS rating
                    if (!empty($rating_min) and !empty($rating_max)) {
                        if ($woo_next_product_rating < $rating_min or $woo_next_product_rating > $rating_max) {
                            $add_product_to_results = false;
                        }
                    }
                }
//                DebToFile('-01234 $add_product_to_results ::' .print_r($add_product_to_results,true) );
                if ($add_product_to_results) {
                    $products[] = $woo_next_product;
                }
            } // foreach( $raw_products as $next_key => $next_raw_product_post ) { // all rows from db in for circle
            /*             $rating_min = $this->getParameter('rating_min', '');
            $rating_max = $this->getParameter('rating_max', '1');  */

            $paginationMaxPage = (int)( count($products) / $posts_per_page ) + ( count($products) % $posts_per_page == 0 ? 0 : 1 );
            if ($page > $paginationMaxPage) $page = 1;
//            DebToFile('-1 $paginationMaxPage ::' .print_r($paginationMaxPage,true) );

            $navigationHTML = $this->makePageNavigation( $page, $posts_per_page, $paginationMaxPage, '', '', false, " nsn_woo_ext_search_frontendFuncsObj.loadProductsPage( '#page#' ) ", false, '', $navigation_classes );
//            DebToFile('-12 RUN FILTER $navigationHTML::' .print_r($navigationHTML,true) );

            $index= 0;
            foreach( $products as $next_woo_product ) {
                if ( $page_rows_start >= 0 and $page_rows_end > 0 and $page_rows_start<= $index and $page_rows_end > $index ) {
//                    $woo_next_product = nsnWooToolsData::get_woo_product_props($next_product->ID, array('image', 'categories_list', 'sale_price', 'regular_price', 'sku', 'rating', 'is_in_stock', 'purchase_note'), array('brand', 'discount'), $additive_attrs);
//                    $woo_next_product['guid'] = $next_product->guid;
//                    $woo_next_product['post_title'] = $next_product->post_title;
//                    $woo_next_product['image_c'] = htmlspecialchars($woo_next_product['image']);
                    $products_list[] = $next_woo_product;
                }
                $index++;
            }


//            echo '<pre>$products_list::'.print_r($products_list,true).'</pre>';
//            die("-1 XXZ");
            $data= array('products_list'=>$products_list, 'page'=> $page, 'total_products_count'=> count($products), 'filter_categories_list'=>$filter_categories_list,  'filter_prices_list'=> $filter_prices_list, 'fields_to_show' => $this->m_fields_to_show, 'service_category_for_attrs_array'=> $this->m_service_category_for_attrs_array, 'attrs_to_show'=> $this->m_attrs_to_show, 'navigationHTML' => $navigationHTML /*, 'plugin_url'=> $this->m_plugin_url , 'plugin_dir'=> $this->m_plugin_dir*/ );
            $ret= twigpress_render_twig_template( $data, "white-simple/woo_ext_products_list.twig", false );
//            echo '<pre>$ret::'.print_r(htmlspecialchars($ret),true).'</pre>';
            echo json_encode( array( 'error_code' => 0, 'error_message'=>'', 'html'=> $ret , 'html'=> $ret , 'args'=> $args, 'products_count'=> count($products_list ) , 'total_products_count'=> count($products ) ) );
            return;
        }   // if ($_REQUEST['action'] == 'get-woo-products-by-filter') {  // get list of products by filters
/////////////////////////////////////////////

        if ($_REQUEST['action'] == 'load_bookmark_by_title') {  // save bookmark
            $user_id = (int)$this->getParameter('user_id', '');
            if (empty($user_id) or $user_id < 0 ) {
                echo json_encode( array( 'error_code' => 1, 'error_message'=>'You must login to system to make this operation !', 'current_user_id'=> $user_id ) );
                exit;
            }
            $bookmark_title = $this->getParameter('bookmark_title', '');
            $bookmarks_array = get_option($this->m_woo_ext_search_options['meta_info_key'].'bookmark-'.$user_id);
            foreach( $bookmarks_array as $next_bookmark ) {
                if ( strtolower(trim($next_bookmark['title'])) == strtolower(trim($bookmark_title)) ) {
                    echo json_encode( array( 'error_code' => '0', 'error_message'=> "", 'bookmark'=> $next_bookmark ) );
                    return;
                }
            }
//            echo '<pre>$bookmarks_array::'.print_r($bookmarks_array,true).'</pre>';
//            echo '<pre>gettype($bookmarks_array)::'.print_r( gettype($bookmarks_array) ,true).'</pre>';
//            dump($bookmarks_array);
            echo json_encode( array( 'error_code' => '1', 'error_message'=>"Bookmark '" . $bookmark_title . "' was not found !" ) );
            return;

        }
        if ($_REQUEST['action'] == 'load_bookmarks') {  // load bookmarks by user_id
            $user_id = $this->getParameter('user_id', '');
            if (empty($user_id) or $user_id < 0 ) {
                echo json_encode( array( 'error_code' => 1, 'error_message'=>'You must login to system to make this operation !', 'current_user_id'=> $user_id ) );
                exit;
            }
            $bookmarks_array = get_option($this->m_woo_ext_search_options['meta_info_key'].'bookmark-' . $user_id, array());

            $default_bookmark_title= '';
            $default_bookmark_index= -1;
            $index= 0;
            foreach( $bookmarks_array as $next_key => $next_bookmark ) {
                if ( $next_bookmark['is_default'] == ("yes") ) {
                    $default_bookmark_title= $next_bookmark['title'];
                    $default_bookmark_index= $index;
                    $bookmarks_array[$next_key]['title']= $next_bookmark['title'] .' ( default ) ';
                    break;
                }
                $index++;
            }
            echo json_encode( array( 'error_code' => '0', 'error_message'=>'', 'bookmarks_array'=> $bookmarks_array, 'default_bookmark_title'=> $default_bookmark_title,       'default_bookmark_index' => $default_bookmark_index ) );
            return;

        } // if ($_REQUEST['action'] == 'load_bookmarks') {  // load bookmarks by user_id


        if ($_REQUEST['action'] == 'remove_bookmark') {  // remove bookmark
            $user_id = $this->getParameter('user_id', '');
            $bookmark_title = $this->getParameter('bookmark_title', '');
            $bookmarks_array = get_option($this->m_woo_ext_search_options['meta_info_key'].'bookmark-'.$user_id, array());
            $saved_bookmarks_array= array();
            foreach( $bookmarks_array as $next_key=>$next_bookmark_struct ) {
                if ( strtolower($next_bookmark_struct['title']) == strtolower($bookmark_title) ) continue;
                $saved_bookmarks_array[$next_key]= $next_bookmark_struct;
            }
            $ret = update_option($this->m_woo_ext_search_options['meta_info_key'].'bookmark-'.$user_id, $saved_bookmarks_array);
            echo json_encode( array( 'error_code' => ($ret?0:1), 'error_message'=>'', 'html'=> '' ) );
        } // if ($_REQUEST['action'] == 'remove_bookmark') {  // remove bookmark


        if ($_REQUEST['action'] == 'save_bookmark') {   // save bookmark

            $user_id = $this->getParameter('user_id', '');
            if (empty($user_id) or $user_id < 0 ) {
                echo json_encode( array( 'error_code' => 1, 'error_message'=>'You must login to system to make this operation !', 'current_user_id'=> $user_id ) );
                exit;
            }
            DebToFile('-1 save_bookmark $user_id ::' .print_r($user_id,true) );
            $bookmark_title = $this->getParameter('bookmark_title', '');
            $is_default = $this->getParameter('is_default', 'no');
//            $select_bookmark = $this->getParameter('select_bookmark', '');
            $orderby = $this->getParameter('orderby', '');
            $items_per_page = $this->getParameter('items_per_page', '');
//            $sale_price = $this->getParameter('sale_price', '');
            $partial_sku = $this->getParameter('partial_sku', '');
            $partial_title = $this->getParameter('partial_title', '');
            $sku = $this->getParameter('sku', '');
            $post_title = $this->getParameter('post_title', '');
            $rating_min = $this->getParameter('rating_min', '');
            $rating_max = $this->getParameter('rating_max', '');
            $only_in_stock = $this->getParameter('only_in_stock', '');

            $categories_list = $this->getParameter('categories_list', array());
            $prices_list = $this->getParameter('prices_list', array());
            $attrs_checked_list = $this->getParameter('attrs_checked_list', array());
//            $categories_list= array(10,22);
//            if ($this->isDebug) {
//                echo '<pre>$categories_list::' . print_r($categories_list, true) . '</pre>';
//                echo '<pre>$prices_list::' . print_r($prices_list, true) . '</pre>';
//                echo '<pre>$attrs_checked_list::' . print_r($attrs_checked_list, true) . '</pre>';
//                echo '<pre>$partial_sku::' . print_r($partial_sku, true) . '</pre>';
//                echo '<pre>$sku::' . print_r($sku, true) . '</pre>';
//                echo '<pre>$rating_min::' . print_r($rating_min, true) . '</pre>';
//                echo '<pre>$rating_max::' . print_r($rating_max, true) . '</pre>';
//                echo '<pre>$only_in_stock::' . print_r($only_in_stock, true) . '</pre>';
//
//            }
            $bookmarks_array = get_option($this->m_woo_ext_search_options['meta_info_key'].'bookmark-'.$user_id, array());
//            $is_insert_bookmark = true;
            if ( strtolower($is_default) == "yes" ) {
                foreach ($bookmarks_array as $next_key => $next_bookmark_struct) {
                    $bookmarks_array[$next_key]['is_default']= "no";
                }
            }

            $options_array= array( 'user_id'=> $user_id, 'title'=> $bookmark_title, 'is_default' => $is_default, 'orderby'=> $orderby, 'items_per_page'=> $items_per_page, 'partial_sku'=> $partial_sku, 'partial_title'=> $partial_title, 'sku'=> $sku , 'post_title'=> $post_title, 'rating_min'=> $rating_min, 'rating_max'=> $rating_max, 'only_in_stock'=> $only_in_stock, /*'sale_price'=> $sale_price, */ 'categories_list'=> $categories_list, 'prices_list'=> $prices_list, 'attrs_checked_list'=>$attrs_checked_list );
            DebToFile('-1 $options_array $user_id ::' .print_r($options_array,true) );
//            DebToFile('-1 $is_insert_bookmark $user_id ::' .print_r($is_insert_bookmark,true) );
            DebToFile('-1 attrs_checked_list ::' .print_r($attrs_checked_list,true) );

//            if ( $is_insert_bookmark ) {
//                $bookmarks_array[$bookmark_title]= $options_array;
//            } else {
            $bookmarks_array[$bookmark_title]= $options_array;
//            }
            $ret = update_option($this->m_woo_ext_search_options['meta_info_key'].'bookmark-' . $user_id, $bookmarks_array);
            echo json_encode( array( 'error_code' => ($ret?0:1), 'error_message'=>'', 'html'=> '' ) );
            return;

        } //        if ($_REQUEST['action'] == 'save_bookmark') {  // save bookmark







    } // public function main() {



    protected function showRowsListDataForm( $parentControl ) {

    }
    protected function addRowDataForm( $parentControl ) {
    }

    protected function initFieldsAddRow( $parentControl )
    {
    }


    protected function editRowDataForm( $parentControl ) {

    }

    protected function doBulkAction($action) {
    } // protected function doBulkAction() {

    protected function saveRowData( $action, $editorItem ) {
    }

    protected function deleteRowData()
    {
    }


    protected function preparePageParameters($WithPage, $WithSort) // Prepare url for pagination with all fields in filter and sort and sort_direction
    {
        return '';
    } // protected function preparePageParameters($WithPage, $WithSort) // Prepare url for pagination with all fields in filter and sort and sort_direction


}

global $wpdb;
$ajaxFrontendControlObj = new ajaxFrontendControl(null,$wpdb);
//echo '<pre>$ajaxFrontendControlObj::'.print_r($ajaxFrontendControlObj,true).'</pre>';
$ajaxFrontendControlObj->main();

//echo '<pre>AFTER::'.print_r(-98,true).'</pre>';
//function get_price_by_type($price) {
//    //echo '<pre>???$price::'.print_r($price,true).'</pre>';
//    if (is_scalar($price) ) {
//        return $price;
//    }
//    if (is_array($price)) {
//        $zero_key_value= '';
//        $name_key_value= '';
//        foreach( $price as $next_key=>$next_value ) {
//            // echo '<pre>$next_key::'.print_r($next_key,true).'</pre>';
//            // echo '<pre>$next_value::'.print_r($next_value,true).'</pre>';
//            if ($next_key== 0) {
//                $zero_key_value= $next_value;
//            }
//            if ( is_string($next_key) and $next_key == 'price') {
//                //die("-1 INSU");
//                $name_key_value= $next_value;
//            }
//        }
//    }
//    // echo '<pre>$zero_key_value::'.print_r($zero_key_value,true).'</pre>';
//    // echo '<pre>$name_key_value::'.print_r($name_key_value,true).'</pre>';
//    if ( !empty($name_key_value) and empty($zero_key_value)) {
//        return $name_key_value;
//    }
//    if ( !empty($zero_key_value ) and empty($name_key_value)) {
//        return $zero_key_value;
//    }
//    if ( !empty($zero_key_value ) and !empty($name_key_value)) {
//        return $name_key_value;
//    }
//
//}
