<?php 
class Woo_wooexim_export {

	public $enable_export;
	
	public function __construct(){	
		add_action( 'admin_menu', array( $this, 'add_menu_link' ) );
		add_action( 'wp_ajax_save_product', 'wooexim_save_woo_products' );
		add_action( 'wp_ajax_save_category', 'wooexim_save_woo_category' );
		add_action( 'wp_ajax_save_tags', 'wooexim_save_woo_tags' );
		add_action( 'wp_ajax_save_orders', 'wooexim_save_woo_orders' );
		add_action( 'wp_ajax_save_customers', 'wooexim_save_woo_customers' );
	}
	
	public function add_menu_link() {
		add_submenu_page('wooexim-import','Export Product', 'Export Product','manage_options','wooexim-export',	array( $this, 'settings_page' ));
		add_submenu_page('wooexim-import','Export - Import Orders', 'Orders','manage_woocommerce','wooexim-archive',	array( $this, 'settings_page_pro' ));
		add_submenu_page('wooexim-import','Export - Import Categories', 'Categories','manage_woocommerce','wooexim-archive',	array( $this, 'settings_page_pro' ));
		add_submenu_page('wooexim-import','Export - Import Customers', 'Customers','manage_woocommerce','wooexim-archive',	array( $this, 'settings_page_pro' ));
		add_submenu_page('wooexim-import','Export - Import Coupons', 'Coupons','manage_woocommerce','wooexim-archive',	array( $this, 'settings_page_pro' ));
		add_submenu_page('wooexim-import','WOOEXIM Product Archive', 'Archive','manage_woocommerce','wooexim-archive',	array( $this, 'settings_page_pro' ));
		add_submenu_page('wooexim-import','WOOEXIM Schedule Export', 'Schedule','manage_woocommerce','wooexim-archive',	array( $this, 'settings_page_pro' ));
		add_submenu_page('wooexim-import','WOOEXIM Settings', 'Settings','manage_woocommerce','wooexim-settings',	array( $this, 'settings_page3' ));
	}
	
	public function settings_page( $tab = false ){
		ini_set('max_execution_time', 3000);
		$this->wxe_set_archive_table();
		$show_settings = ''; $show_archive='';$show_export='';
		if( $this->wxe_generate_data_for_export())
			$tab = 'archive';
			
		if( ! empty( $tab ) )
		{
			if( $tab == 'export')
				$show_export = 'nav-tab-active';
			if( $tab == 'archive')
				$show_archive = 'nav-tab-active';
			if( $tab == 'settings')
				$show_settings = 'nav-tab-active';			
		}
		else
		{
			if( isset( $_REQUEST['tab'] ) )
			{
				$tab = $_REQUEST['tab'];
				if( $tab == 'export')
					$show_export = 'nav-tab-active';
				if( $tab == 'archive')
					$show_archive = 'nav-tab-active';
				if( $tab == 'settings')
					$show_settings = 'nav-tab-active';
			}
			else
			{
				$tab = 'export';
				$show_export = 'nav-tab-active';
			}
		}
		$html ='';
		$html .=   '<link href="'.WOOEXIM_PATH.'css/style.css" rel="stylesheet" />';
		$html .=   '<link href="'.WOOEXIM_PATH.'css/colorbox.css" rel="stylesheet" />';
		$html .=   '<script type="text/javascript">var admin_url = "'.admin_url('admin-ajax.php').'";var pluginpath = "'.WOOEXIM_PATH.'";</script>';
		
		if( $this->is_woocommerce_activated() ) {
		$html .=  '<div class="wrap" style="border: 1px solid #e3e3e3;padding: 10px;background: #f6f6f6;">
				<div id="icon-woocommerce" class="icon32 icon32-woocommerce-importer"><br></div>
					
				<div style="background: #9b5c8f;min-height: 92px;padding: 10px;color: #fff;">
					<img style="border: 1px solid #e3e3e3;padding: 5px;float: left;margin-right: 10px; "src="' . plugin_dir_url(__FILE__) . 'thumb.jpg">			
					<h2 style="color: #fff;">WOOEXIM &raquo; Export Product</h2>
					<p style="color: #fff;line-height: 0.5;">Developed by <a style="color: #fff;" href="http://WooEXIM.com" target="_blank">WooEXIM.com</a> Version: 1.0.0</p>
					<p style="color: #fff;line-height: 0.5;">Quick and easy plugin for WooCommerce product export-import.</p>
				</div>
					
					
				<div id="content">
		<div class="overview-left">';
		$tab_html = $this->get_export_tab_data();
		$html .= $tab_html;
		$html .= '</div><!-- .overview-left -->
				</div><!-- #content --></div>';
		}
		else
			$html = '<span>Please activate woocommerce plugin to use this feature</span>';
			
		echo $html;
		echo '<script src="'.WOOEXIM_PATH.'js/custom.js"></script>';
		echo '<script src="'.WOOEXIM_PATH.'js/jquery.bpopup.min.js"></script>';
		echo '<script src="'.WOOEXIM_PATH.'js/jquery.colorbox.js"></script>';
	}
	
	public function settings_page_pro( $tab = false ){
		ini_set('max_execution_time', 3000);
		$this->wxe_set_archive_table();
		$show_settings = ''; $show_archive='';$show_export='';
		if( $this->wxe_generate_data_for_export())
			$tab = 'archive';
			
		if( $this->is_woocommerce_activated() ) {
		$html .=  '<div class="wrap" style="border: 1px solid #e3e3e3;padding: 10px;background: #f6f6f6;">
				<div id="icon-woocommerce" class="icon32 icon32-woocommerce-importer"><br></div>
					
				<div style="background: #9b5c8f;min-height: 92px;padding: 10px;color: #fff;">
					<img style="border: 1px solid #e3e3e3;padding: 5px;float: left;margin-right: 10px; "src="' . plugin_dir_url(__FILE__) . 'thumb.jpg">			
					<h2 style="color: #fff;">WOOEXIM &raquo; This Feature Available in WOOEXIM Pro Version</h2>
					<p style="color: #fff;line-height: 0.5;">Developed by <a style="color: #fff;" href="http://WooEXIM.com" target="_blank">WooEXIM.com</a> Version: 1.0.0</p>
					<p style="color: #fff;line-height: 0.5;">Quick and easy plugin for WooCommerce product export-import.</p>
				</div>
					
					
				<div id="content">
					
		<div class="overview-left">';
		$html .= '</div><!-- .overview-left -->';
		
		$html .='<div class="user-html">
		<h2>Get WOOEXIM Premium Version <a href="http://WooEXIM.com">WOOEXIM Pro</a></h2>
		<p style="font-size: 16px;">WOOEXIM Pro is an easy, quick and advanced Import & Export your store data. All type of your WooCommerce Products, Orders, Users, Product Categories and Coupons import/export in just one click. This plugin has inbuilt functionality are Multiple Powerful Filters, Export Management, Field Management And Scheduled Management.</p>

				
		<h3><strong>General Features</strong></h3>
		<ul>
			 <li> Easy Installation and Detailed Documentation.</li>
			<li> Easy Import and Export Interface.</li>
			<li> Easy, Quick and Advanced Import-Export System.</li>
			<li> Ajax Products, Orders and Users Import System.</li>
			<li> Export Data to CSV File.</li>
			<li> Import Data from CSV File.</li>
			<li> Multi Language support.</li>
			<li> Fully responsive and 100% customizable.</li>
			<li> Dedicated Support.</li>
		</ul>

		<h3><strong>Products Import Export</strong></h3>
		<ul>
			<li> Multiple Powerful Filters Available.</li>
			<li> All Product Types Supported.</li>
			<li> Almost All Products fields are used.</li>
			<li> Custom field supported.</li>
			<li> Custom Attributes supported.</li>
			<li> Export Data preview available.</li>
			<li> Export CSV data Management.</li>
			<li> Product Fields Management.</li>
			<li> Product Export Scheduled Management (Automatic Export).</li>
			<li> Import Products by URL.</li>
			<li> Import Products with new Categories.</li>
			<li> Import Product Create / Update / Skip option available when already product exist.</li>
			<li> Imported Product preview available.</li>

		</ul>

		<h3><strong>Orders Import Export</strong></h3>
		<ul>
			<li> Woocommerce Update (Woocommerce 2.1.X to Woocommerce 2.X.X).</li>
			<li> Multiple Powerful Filters Available.</li>
			<li> All new and old Order Status Supported.</li>
			<li> Almost All Order fields are used.</li>
			<li> Custom field supported.</li>
			<li> Custom Attributes supported.</li>
			<li> Export Data preview available.</li>
			<li> Export CSV data Management.</li>
			<li> Order Fields Management.</li>
			<li> Order Export Scheduled Management(Automatic Export).</li>
			<li> Import Order by URL.</li>
			<li> Import Order Create / Update / Skip option available when already Order exist.</li>
			<li> Imported Order preview available.</li>

		</ul>

		<h3><strong>Users Import Export</strong></h3>
		<ul>
			<li> Multiple Powerful Filters Available.</li>
			<li> Almost All User fields are used.</li>
			<li> All Usermeta supported.</li>
			<li> All Capabilities supported.</li>
			<li> Custom Capabilities supported.</li>
			<li> Export Data preview available.</li>
			<li> Export CSV data Management.</li>
			<li> User Fields Management.</li>
			<li> User Export Scheduled Management(Automatic Export).</li>
			<li> Import User by URL.</li>
			<li> Import User Update / Skip option available when already User exist.</li>
			<li> Imported User preview available.</li>

		</ul>
		<h3><strong>Product Categories Import Export</strong></h3>
		<ul>
			<li> Multiple Powerful Filters Available.</li>
			<li> Almost All Category fields are used.</li>
			<li> Category images are exported and auto set.</li>
			<li> Category parent auto detected.</li>
			<li> WooCommerce categories meta are exported.</li>
			<li> Export Data preview available.</li>
			<li> Export CSV data Management.</li>
			<li> Categories Fields Management.</li>
			<li> Categories Export Scheduled Management(Automatic Export).</li>
			<li> Import Categories by URL.</li>
			<li> Import Categories Update / Skip option available when already Product Categories exist.</li>
			<li> Imported Categories preview available.</li>

		</ul>
		<h3><strong>Coupons Import Export</strong></h3>
		<ul>
			<li> Multiple Powerful Filters Available.</li>
			<li> Almost All Coupons fields are used.</li>
			<li> Coupons meta are exported.</li>
			<li> Export Data preview available.</li>
			<li> Export CSV data Management.</li>
			<li> Coupons Fields Management.</li>
			<li> Coupons Export Scheduled Management(Automatic Export).</li>
			<li> Import Coupons by URL.</li>
			<li> Import Coupons Update / Skip option available when already Coupons exist.</li>
			<li> Imported Coupons preview available.</li>

		</ul>

		<h3><strong>Scheduled Management (Automatic Export)</strong></h3>
		<p>You can create scheduled in just one click. Click on ‘Save Scheduled’ Button.</p>
		<p>You can also create multiple scheduled with filter data. You can see all Scheduled list in ‘Manage Scheduled’ menu and if you want to delete than just click ‘DELETE’ link of scheduled.</p>

		<h3><strong>Export Field Management</strong></h3>
		<p>You can set you field name just goto ‘Setting’ menu.</p>
		<p>you can set each field name of any export file. Just Enter Field name and click ‘Save’ button for save field name and you are done.</p>

		<h3><strong>Supported Product Types for Import Export Products</strong></h3>
		<ul>
			 <li>Simple Product</li>
			<li>Downloadable Product</li>
			<li>Virtual Product</li>
			<li>Variable Product</li>
			<li>External/Affiliate Product</li>
			<li>Grouped Product</li>
		</ul>
		</div>';
		
		$html .='</div><!-- #content --></div>';
		}			
		echo $html;

	}
	
	public function settings_page3( $tab = false ){
		ini_set('max_execution_time', 3000);

		$html ='';
		$html .=   '<link href="'.WOOEXIM_PATH.'css/style.css" rel="stylesheet" />';
		$html .=   '<link href="'.WOOEXIM_PATH.'css/colorbox.css" rel="stylesheet" />';
		$html .=   '<script type="text/javascript">var admin_url = "'.admin_url('admin-ajax.php').'";var pluginpath = "'.WOOEXIM_PATH.'";</script>';
		
		if( $this->is_woocommerce_activated() ) {
		$html .=  '<div class="wrap" style="border: 1px solid #e3e3e3;padding: 10px;background: #f6f6f6;">
				<div id="icon-woocommerce" class="icon32 icon32-woocommerce-importer"><br></div>
					
				<div style="background: #9b5c8f;min-height: 92px;padding: 10px;color: #fff;">
					<img style="border: 1px solid #e3e3e3;padding: 5px;float: left;margin-right: 10px; "src="' . plugin_dir_url(__FILE__) . 'thumb.jpg">			
					<h2 style="color: #fff;">WOOEXIM &raquo; Settings</h2>
					<p style="color: #fff;line-height: 0.5;">Developed by <a style="color: #fff;" href="http://wooexim.com" target="_blank">WOOEXIM.com</a> Version: 1.0.0</p>
					<p style="color: #fff;line-height: 0.5;">Quick and easy plugin for WooCommerce product export-import.</p>
				</div>
					
					
				<div id="content">
					
		<div class="overview-left">';
		$tab_html = $this->wxe_get_setting_tab_data();
		$html .= $tab_html;
		$html .= '</div><!-- .overview-left -->
				</div><!-- #content --></div>
				';
		}
		else
			$html = '<span>Please activate woocommerce plugin to use this feature</span>';
			
		echo $html;
		echo '<script src="'.WOOEXIM_PATH.'js/custom.js"></script>';
		echo '<script src="'.WOOEXIM_PATH.'js/jquery.bpopup.min.js"></script>';
		echo '<script src="'.WOOEXIM_PATH.'js/jquery.colorbox.js"></script>';
	}
	
	
	
	
	function is_woocommerce_activated(){
		if ( is_plugin_active( "woocommerce/woocommerce.php" ) )
			return 1;
		else
			return 0;
	}	
	
	function wxe_set_archive_table(){
		global $wpdb;
		$sql = 'CREATE TABLE IF NOT EXISTS '.$wpdb->prefix.'wooexim_export_archive 
					( 
					 	archive_id INT PRIMARY KEY AUTO_INCREMENT,
						archive_name VARCHAR(50),
						archive_type VARCHAR(30),
						author VARCHAR(30),
						category VARCHAR(30),
						date_created TIMESTAMP DEFAULT CURRENT_TIMESTAMP
					)';
		$wpdb->query( $sql ); 
	}
	function get_tab_data( $tab )
	{
		$output_data = '';
		
		if( ! empty( $tab ) )
		{
			if(  $tab == 'export' )
			{
				$output_data = $this->get_export_tab_data();
			}
			else if(  $tab == 'archive' )
			{
				$output_data = $this->get_archive_tab_data();
			}	
			else if(  $tab == 'settings' )
			{
				$output_data = $this->wxe_get_setting_tab_data();
			}	
		}
		
		return $output_data;
	}
	
	function get_export_tab_data(){
		$data = '
			<div class="overview-left">
			
	<div style="width: 47.5%; margin-right: 0.5%; display: block;float: left;background: #9b5c8f;margin: 15px 0px;padding: 1%;color: #fff;">
		<h2 style="color: #FFF;">Export All Products</h2>
		<p>From here you can export all the products with all the meta informations of your store out of WooCommerce into a CSV Spreadsheet file. And you can use that file for import purpose in future.</p>
		<a class="button-primary all_export_btn export_btn" href="'.WOOEXIM_EXPORT_ADMIN_URL.'&amp;tab=export&export=all_products">Export All Products</a>
			<div class="all_product_exporting_loader">
				<img src="'.WOOEXIM_PATH.'img/animated_loading.gif" /><span>Exporting All Product Data Please Wait ... </span>
			</div>
	</div>
	
	<div style="width: 47.5%; margin-left: 0.5%; display: block;float: right;background: #9b5c8f;margin: 15px 0px;padding: 1%;color: #fff;">
		<h2 style="color: #FFF;">Export Products by Category</h2>
		<p>From here you can export products of your store from desired category into a CSV Spreadsheet file. And you can use it for import purpose in future. For this first you need to <b>Choose Categories</b>.</p>
		<a class="button-primary  cat_export_btn export_btn" href="'.WOOEXIM_EXPORT_ADMIN_URL.'&amp;tab=export&export=selected_category">Export Category Products</a>
			
			<a href="#export-products" class="button export_category_settings">Choose Categories</a>
			<div class="cat_product_exporting_loader">
				<img src="'.WOOEXIM_PATH.'img/animated_loading.gif" /><span>Exporting Please Wait ... </span>
			</div>
			<div class="category_settings">
				'.$this->wxe_get_all_woo_categories().'
			<div>
		
	</div>
</div>
		';
		return $data;
	}
	
	function get_archive_tab_data(){
		global $wpdb;
		if( isset( $_REQUEST['action'] ) )
		{
			if( $_REQUEST['action'] == 'delete' )
			{
				$id = $_REQUEST['archid'];
				if( ! empty( $id ) )
				{
					$sql = 'delete from '.$wpdb->prefix.'wooexim_export_archive where archive_id = '.$id;
					$wpdb->query( $sql );
					$name = $_REQUEST['arch_name'];
					unlink ( WOOEXIM_EXPORT_PATH.'/'. $name . ".csv");
					echo '<div class="success_mgs"><span>Archive Deleted</span></div>';
				}
			}
		}
		
		
		$sql = 'select * from '.$wpdb->prefix.'wooexim_export_archive order by date_created desc';
		$result = $wpdb->get_results( $sql );
		//echo "<pre>"; print_r($result); echo "</pre>";
		$data = '
		<table class="widefat fixed media archive" cellspacing="0">
		<thead>

			<tr>
				<th scope="col" id="icon" class="manage-column column-icon"></th>
				<th scope="col" id="title" class="manage-column column-title">Filename</th>
				<th scope="col" class="manage-column column-type">Type</th>
				<th scope="col" class="manage-column column-catgegory">Category</th>
				<th scope="col" class="manage-column column-size">Size</th>
				<th scope="col" class="manage-column column-author">Author</th>
				<th scope="col" id="title" class="manage-column column-title">Date</th>
			</tr>

		</thead>
		<tfoot>

			<tr>
				<th scope="col" class="manage-column column-icon"></th>
				<th scope="col" class="manage-column column-title">Filename</th>
				<th scope="col" class="manage-column column-type">Type</th>
				<th scope="col" class="manage-column column-catgegory">Category</th>
				<th scope="col" class="manage-column column-size">Size</th>
				<th scope="col" class="manage-column column-author">Author</th>
				<th scope="col" class="manage-column column-title">Date</th>
			</tr>

		</tfoot><tbody id="the-list">';
		
		foreach ($result as $arch )
		{
			$file_url =   WOOEXIM_EXPORT_PATH.'/'.$arch->archive_name.'.csv' ;
			$size = $this->wxe_formatSizeUnits( filesize ( $file_url ) );
			
			$data .= '
				<tr class="ddd" id="post-404" class="author-self status-inherit" valign="top">
				<td class="column-icon media-icon">
					<img width="48" height="64" src="'.WOOEXIM_PATH.'img/excelimg.png" class="attachment-80x60" alt="woo-export_products-2014_05_07.csv">				</td>
				<td class="post-title page-title column-title">
					<strong>'.$arch->archive_name.'.csv</strong>
					<div class="row-actions">
						<span class="view"><a href="'.WOOEXIM_DOWNLOAD_PATH.$arch->archive_name.'.csv" title="download">Download</a></span> | 
						<span class="trash"><a href="'.WOOEXIM_EXPORT_ADMIN_URL.'&action=delete&tab=archive&archid='.$arch->archive_id.'&arch_name='.$arch->archive_name.'" title="Delete Permanently">Delete</a></span>
					</div>
				</td>
				<td class="title">Products</td>
				<td class="title">'.$arch->category.'</td>
				<td class="title">'.$size.'</td>
				<td class="author column-author">'.$arch->author.'</td>
				<td class="date column-date">'.$arch->date_created.'</td>
			</tr>
	
		';
		}
		if( empty ( $result ) )
		{
			$data .= '<tr>
				<td colspan= "6" scope="col" class="manage-column column-icon"> 
				<span class="no_rec">	No Archive Found </span>
				</th>
			</tr>';
		}
	$data .= '</tbody></table>';
		return $data;
	}
	
	function wxe_get_all_woo_products(){
		$args = array( 
				'post_type' => 'product', 
				'posts_per_page' => 1000, 
		);
		$db_checked_products = array();
		$db_checked_products = get_option( 'wooexim_selected_products' );
		$query = new WP_Query( $args );
		$data = '';
		if( $query->have_posts() ){
			$data .= '
				<span class="setting_header">Select the product you want to export</span>
				<form action=" " method="post" >
				<ul class="table_setting">';
			while ( $query->have_posts() ) {
				$query->the_post();
				$data .= '<li>';
				$data .= '<div class="checkbx" onclick="changechk(this)">';
				
				if( !empty( $db_checked_products ) )
				{
					if( in_array( $query->post->ID, $db_checked_products ) )
						$data .= '<img class="checkimg" src="'.WOOEXIM_PATH.'img/checked.png" />
							<input class="checkbox checkeddone"  type="checkbox" name="checked_product[]" value = "'.$query->post->ID.'" checked="checked" /></div>';
					else
						$data .= '<img class="checkimg" src="'.WOOEXIM_PATH.'img/unchecked.png" />
								<input class="checkbox"  type="checkbox" name="checked_product[]" value = "'.$query->post->ID.'" /></div>';
				}
				else
					$data .= '<img class="checkimg" src="'.WOOEXIM_PATH.'img/unchecked.png" />
								<input class="checkbox"  type="checkbox" name="checked_product[]" value = "'.$query->post->ID.'" /></div>';
				
				//the_post_thumbnail( array('class'	=> "image"));
				$data .= get_the_post_thumbnail($query->post->ID, 'thumbnail', array('class'	=> "image"));
				$data .= '<span>'.get_the_title().'</span>';
				$data .= '</li>';
			}
			$data .= '<div class="clr"></div></ul>
			<input style="margin-left: 5px;width:50px;" type="submit" name="submit" value="Save" class="button button-primary save_button" onclick="return save_product()">
			</form><div class="save_loader"><img src="'.WOOEXIM_PATH.'/img/loading.gif" alt="" /><span>Saving...</span></div>';
		}
		else
		{
			$data .= '<span class="setting_header">No product has beed added yet.</span>';
		}
			return $data;
	}
	
	function wxe_get_all_woo_categories(){
		$taxonomies = array( 'product_cat' );
		$args = array( 'orderby' => 'name', 'order' => 'ASC', 'hide_empty' => false );  
		$list =  get_terms( $taxonomies, $args ) ;
		
		$db_checked_categories = array();
		$db_checked_categories = get_option( 'wooexim_selected_categories' );
		$data = '';
		if( ! empty( $list ) )
		{	
			$data .= '<span class="setting_header">Export the product on the basis of their category</span>
			<ul class="table_setting">';
			foreach ( $list as $cat )
			{
				$data .= '<li>';
				$data .= '<div class="checkbx" onclick="changechk(this)">';
				if( !empty( $db_checked_categories ) )
				{
					if( in_array( $cat->term_id, $db_checked_categories ) )
						$data .= '<img class="checkimg" src="'.WOOEXIM_PATH.'img/checked.png" />
							<input class="checkbox checkeddone"  type="checkbox" name="checked_category[]" value = "'.$cat->term_id.'" checked="checked" />';
					else
						$data .= '<img class="checkimg" src="'.WOOEXIM_PATH.'img/unchecked.png" />
								<input class="checkbox"  type="checkbox" name="checked_category[]" value = "'.$cat->term_id.'" />';
				}
				else
					$data .= '<img class="checkimg" src="'.WOOEXIM_PATH.'img/unchecked.png" />
						<input class="checkbox"  type="checkbox" name="checked_category[]" value = "'.$cat->term_id.'" />';
				
				$data .= '<div><div>'.$cat->name.'</div></li>';
			}
			$data .=  '<div class="clr"></div></ul>
			<input style="margin-left: 5px;width:50px;" type="submit" name="submit" value="Save" class="button button-primary save_button" onclick="return save_category()">
			<div class="save_loader"><img src="'.WOOEXIM_PATH.'/img/loading.gif" alt="" /><span>Saving...</span></div>';
		}
		else
			$data .= '<span class="setting_header">No category has beed added yet.</span>';
			
		return $data;
	}
	function wxe_get_setting_tab_data(){
		if( isset( $_REQUEST['save_settings'] ) )
		{
			$prefix_name = $_REQUEST['prefix_name'];
			$author_name = $_REQUEST['author_name'];
			$subject_name = $_REQUEST['subject_name'];
			$description_archive = $_REQUEST['description_archive'];
			$field_separator = $_REQUEST['field_separator'];
			$hierarchy_separator = $_REQUEST['hierarchy_separator'];
			
			if($field_separator == ''){ $field_separator = ',';}
			if($hierarchy_separator == ''){ $hierarchy_separator = '/';}
			if($prefix_name == ''){ $prefix_name = 'wooexim_export_';}
			if($author_name == ''){ $author_name = 'by Woo EXIM';}
			
			
			update_option( 'wxe_archive_prefix', $prefix_name  );
			update_option( 'wooexim_author_name', $author_name );
			update_option( 'wooexim_subject_name', $subject_name );
			update_option( 'wooexim_description_archive', $description_archive );
			update_option( 'wooexim_field_separator', $field_separator );
			update_option( 'wooexim_hierarchy_separator', $hierarchy_separator );
			echo '<div class="success_mgs"><span>Settings Updated</span></div>';
		}
			$prefix_name = get_option( 'wxe_archive_prefix' );
			$author_name = get_option( 'wooexim_author_name' );
			$subject_name = get_option( 'wooexim_subject_name' );
			$description_archive = get_option( 'wooexim_description_archive' );
			$field_separator = get_option( 'wooexim_field_separator' );
			$hierarchy_separator = get_option( 'wooexim_hierarchy_separator' );
		
		$upload_dir = wp_upload_dir();
		$html = '
			<div class="settings_tab">
				<form action="" method="post" >
				<table>
					<tr>
						<td>Path to Your <strong>uploads</strong> Folder</td>
						<td><input type="text" name="upload_folder" value="'.$upload_dir['basedir'].'" />
							<span>Directory of your WordPress upload folder.</span>
						</td>
					</tr>
					<tr>
						<td>CSV field separator</td>
						<td><input type="text" name="field_separator" value="'.$field_separator.'" />
							<span>Enter the character used to separate each field in your CSV. The default is the comma (,) character. Some formats use a semicolon (;) instead.</span>
						</td>
					</tr>
					<tr>
						<td>Category hierarchy separator</td>
						<td><input type="text" name="hierarchy_separator" value="'.$hierarchy_separator.'" />
							<span>Enter the character used to separate categories in a hierarchical structure. The default is the forward-slash (/) character.</span>
						</td>
					</tr>
					<tr>
						<td>Prefix for archive</td>
						<td><input type="text" name="prefix_name" value="'.$prefix_name.'" />
							<span>Tags can be used: %YEAR%, %MONTH%, %DATE%, %HOUR%, %MINUTE%, %SEC%.<br/> Please use \'_\' or space to separate the letters.</span>
						</td>
					</tr>
					<tr>
						<td>Archive\'s Author Name</td>
						<td><input type="text" name="author_name" value="'.$author_name.'" />
							<span>Author name to be added with your archive.</span>
						</td>
						
					</tr>
					
					<tr>
						<td>Archive Subject</td>
						<td><input type="text" name="subject_name" value="'.$subject_name.'" />
							<span>Subject to be added with your archive.</span>
						</td>
					</tr>
					
					<tr>
						<td>Description for your Archive</td>
						<td><textarea name="description_archive">'.$description_archive.'</textarea>
							<span>Description to be added with your archive.</span>
						</td>
					</tr>
					<tr>
						<td></td>
						<td><input type="submit" name="save_settings" value="Save" class="button-primary save_settings"></td>
					</tr>
				</table>
				</form>
			</div>
		';
		return $html;
	}
	function wxe_generate_data_for_export(){
		if( isset( $_REQUEST['export']) )
		{
			if( $_REQUEST['export'] == 'all_products' )
			{
				
				$args = array(
					'posts_per_page' => 10000,
					//'product_cat' => 'category-slug-here',
					'post_type' => 'product',
					'orderby' => 'title',
				);
				$the_query = new WP_Query( $args );
				// The Loop
				$full_data = array();
				global $wpdb;
				if( $the_query->have_posts() )
				{
                    $export_xls_name = $this->wxe_get_archive_name();
					while ( $the_query->have_posts() ) {
						$the_query->the_post();
						
						$single_product = array();
//						$product = get_product($the_query->post->ID);
						$product = wc_get_product($the_query->post->ID);
						$single_product['name'] = $product->post->post_title ;//. ' src ID:'.$the_query->post->ID;
						$single_product['post_content'] = $product->post->post_content;
						$single_product['post_excerpt'] = $product->post->post_excerpt;
						$single_product['post_status'] = $product->post->post_status;
						$single_product['permalink'] = get_permalink();
						
						$post_categories = wp_get_post_terms($the_query->post->ID, $taxonomy = 'product_cat');			
						$cat = ''; $ii = 0;
						foreach((array)$post_categories as $post_category):
							if($ii > 0){$cat .= '|';}
							$cat .= $post_category->name;
							$ii++;
						endforeach;
						$single_product['product_categories'] = $cat;
						
						$post_tags = wp_get_post_terms($the_query->post->ID, $taxonomy = 'product_tag');
						$tag = ''; $ii = 0;
						foreach((array)$post_tags as $post_tag):
							if($ii > 0){$tag .= '|';}
							$tag .= $post_tag->name;
							$ii++;
						endforeach;
						$single_product['product_tags'] = $tag;
						
						$single_product['sku'] = get_post_meta($the_query->post->ID, '_sku', true);
						$single_product['sale_price'] = get_post_meta($the_query->post->ID, '_sale_price', true);
						$single_product['visibility'] = get_post_meta($the_query->post->ID, '_visibility', true);
						$single_product['stock_status'] = get_post_meta($the_query->post->ID, '_stock_status', true);
						$single_product['price'] = get_post_meta($the_query->post->ID, '_price', true);
						$single_product['regular_price'] = get_post_meta($the_query->post->ID, '_regular_price', true);
						$single_product['total_sales'] = get_post_meta($the_query->post->ID, 'total_sales', true);
						$single_product['downloadable'] = get_post_meta($the_query->post->ID, '_downloadable', true);
						$single_product['virtual'] = get_post_meta($the_query->post->ID, '_virtual', true);
						$single_product['purchase_note'] = get_post_meta($the_query->post->ID, '_purchase_note', true);
						$single_product['weight'] = get_post_meta($the_query->post->ID, '_weight', true);
						$single_product['length'] = get_post_meta($the_query->post->ID, '_length', true);
						$single_product['width'] = get_post_meta($the_query->post->ID, '_width', true);
						$single_product['height'] = get_post_meta($the_query->post->ID, '_height', true);
						$single_product['sold_individually'] = get_post_meta($the_query->post->ID, '_sold_individually', true);
						$single_product['_manage_stock'] = get_post_meta($the_query->post->ID, '_manage_stock', true);
						$single_product['stock'] = get_post_meta($the_query->post->ID, '_stock', true);
						$single_product['backorders'] = get_post_meta($the_query->post->ID, '_backorders', true);
						$single_product['featured'] = get_post_meta($the_query->post->ID, '_featured', true);
						$single_product['tax_status'] = get_post_meta($the_query->post->ID, '_tax_status', true);
						$single_product['tax_class'] = get_post_meta($the_query->post->ID, '_tax_class', true);
						
						$thumbnailid = get_post_meta($the_query->post->ID, '_thumbnail_id', true);
						$upload_dir = wp_upload_dir();
						$timg = $upload_dir['baseurl'] . '/' . get_post_meta($thumbnailid, '_wp_attached_file', true);
						$imggalls = get_post_meta($the_query->post->ID, '_product_image_gallery', true);
						$imggall = explode(',', $imggalls); $kk = 0;
						foreach((array)$imggall as $nimg ){
							if( $timg != '' ){ $timg .= '|';}
							$timg .= $upload_dir['baseurl'] . '/' . get_post_meta($imggall[$kk], '_wp_attached_file', true);
							$kk++;
						}
						$single_product['images'] = $timg;
						
						$single_product['download_limit'] = get_post_meta($the_query->post->ID, '_download_limit', true);
						$single_product['download_expiry'] = get_post_meta($the_query->post->ID, '_download_expiry', true);
						$single_product['file_path'] = get_post_meta($the_query->post->ID, '_file_path', true);
						$single_product['product_url'] = get_post_meta($the_query->post->ID, '_product_url', true);
						$single_product['product_type'] = get_post_meta($the_query->post->ID, '_product_type', true);
						
						$post_sclass = wp_get_post_terms($the_query->post->ID, $taxonomy = 'product_shipping_class');
						$sclass = ''; $ii = 0;
						foreach((array)$post_sclass as $post_class):
							if($ii > 0){$sclass .= '|';}
							$sclass .= $post_class->name;
							$ii++;
						endforeach;
						$single_product['shipping_class'] = $sclass;
						
						$mydata = $wpdb->get_row("SELECT * FROM " . $wpdb->posts . " WHERE id = " . $the_query->post->ID . "");
						$single_product['comment_status'] = $mydata->comment_status;
						$single_product['ping_status'] = $mydata->ping_status;

						$full_data[] = $single_product;
					}
					//echo "<pre>"; print_r($full_data); echo "</pre>";
					$headers = array(
								'Name', 'Description', 'Short Description',  'Product Status', 'Permalink', 'Categories', 'Tags', 'SKU', 'Sale Price',
								'Visibility', 'Stock Status', 'Price', 'Regular Price', 'Total Sales',	'Downloadable',	'Virtual', 'Purchase Note', 'Weight', 'Length', 'Width', 
								'Height', 'Sold Individually', 'Manage Stock', 'Stock', 'Backorders', 'Featured', 'Tax Status', 'Tax Class', 'Images', 'Download Limit', 'Download Expiry', 'File Path', 'Product URL','Product Type',
								'Shipping Class', 'Comment Status', 'Ping Status', 'result_comments_list', 'result_comment_rating_average'
							);
					
					$records = $full_data;
					$sheet_data = new Woo_ExIm_spreadsheet();
					$sheet_data->set_filename( $export_xls_name );
					$sheet_data->set_header( $headers );
					$sheet_data->set_records( $records );
					$sheet_data->do_export();
					
					$current_user = wp_get_current_user();
					$author = $current_user->user_login;
					$category = 'All';
					
					$sql = 'insert into '.$wpdb->prefix.'wooexim_export_archive 
						(archive_name, archive_type, author, category) values 
						( "'.$export_xls_name.'", "product", "'.$author.'", "'.$category.'" )';
					$wpdb->query( $sql );
					unset( $_REQUEST );
					return true;
				}
				else
				{
					echo '<span class="error_mgs">No product to export.</span>';
					unset( $_REQUEST );
					return false;
				}
				
				
			}
			else if ( $_REQUEST['export'] == 'selected_category' )
			{
				$all_cat_names= '';
				$ids = get_option( 'wooexim_selected_categories' );
				//echo "<pre>"; print_r( $ids ); echo "</pre>";
				$args = array(
					'posts_per_page' => 10000,
					'tax_query' => array(
					'relation' => 'AND',
						array(
							'taxonomy' => 'product_cat',
							'field' => 'id',
							'terms' => $ids,
							'operator' => 'IN'
						)
					),
					'post_type' => 'product',
					'orderby' => 'title'
				);
				$the_query = new WP_Query( $args );
                $products_attrs_list= nsnWooToolsData::get_products_attrs_list();  // get_products_attributes list
                $attributes_list= serialize($products_attrs_list);
                $attributes_count= count($products_attrs_list);

                $users_list_for_product= array();
                $users_list= get_users( array( 'orderby'    => 'ID', 'order'        => 'ASC' ) );
                foreach( $users_list as $next_key=>$next_user ) { // get all users in system
                    $next_user_roles= '';
                    foreach( $next_user->roles as $next_role ) {
                        $next_user_roles.= $next_role.'|';
                    }
                    $users_list_for_product[]= array( 'ID' => $next_user->ID, 'user_login' => $next_user->user_login, 'display_name' => $next_user->display_name, 'user_email' => $next_user->user_email, 'user_url' => $next_user->user_url, 'user_registered' => $next_user->user_registered, 'user_nicename' => $next_user->user_nicename, 'user_pass' => $next_user->user_pass, 'user_status' => $next_user->user_status, 'roles' => $next_user_roles  );
                }
				$full_data = array();
                $full_data[] = array( 'type'=>'attributes_list', 'data' => $attributes_list );
                $full_data[] = array( 'type'=>'users_list', 'data' => serialize($users_list_for_product ));
				global $wpdb;
				if( $the_query->have_posts() )
				{
                    $export_xls_name = $this->wxe_get_archive_name();
                    $products_exported_count= 0;
                    $images_exported_count= 0;
                    $product_attributes_count= 0;
                    $upload_dir = wp_upload_dir();
					while ( $the_query->have_posts() ) {
						$the_query->the_post();
						$single_product = array();
						$product = wc_get_product($the_query->post->ID);

						$single_product['name'] = $product->post->post_title;
						$single_product['post_content'] = $product->post->post_content;
						$single_product['post_excerpt'] = $product->post->post_excerpt;
						$single_product['post_status'] = $product->post->post_status;
						$single_product['permalink'] = get_permalink();
						
						$post_categories = wp_get_post_terms($the_query->post->ID, $taxonomy = 'product_cat');			
						$cat = ''; $ii = 0;
						foreach((array)$post_categories as $post_category):
							if($ii > 0){$cat .= '|';}
							$cat .= $post_category->name;
							$ii++;
						endforeach;
						$single_product['product_categories'] = $cat;
						
						$post_tags = wp_get_post_terms($the_query->post->ID, $taxonomy = 'product_tag');
						$tag = ''; $ii = 0;
						foreach((array)$post_tags as $post_tag):
							if($ii > 0){$tag .= '|';}
							$tag .= $post_tag->name;
							$ii++;
						endforeach;
						$single_product['product_tags'] = $tag;
						
						$single_product['sku'] = get_post_meta($the_query->post->ID, '_sku', true);
						$single_product['sale_price'] = get_post_meta($the_query->post->ID, '_sale_price', true);
						$single_product['visibility'] = get_post_meta($the_query->post->ID, '_visibility', true);
						$single_product['stock_status'] = get_post_meta($the_query->post->ID, '_stock_status', true);
						$single_product['price'] = get_post_meta($the_query->post->ID, '_price', true);
						$single_product['regular_price'] = get_post_meta($the_query->post->ID, '_regular_price', true);
						$single_product['total_sales'] = get_post_meta($the_query->post->ID, 'total_sales', true);
						$single_product['downloadable'] = get_post_meta($the_query->post->ID, '_downloadable', true);
						$single_product['virtual'] = get_post_meta($the_query->post->ID, '_virtual', true);
						$single_product['purchase_note'] = get_post_meta($the_query->post->ID, '_purchase_note', true);
						$single_product['weight'] = get_post_meta($the_query->post->ID, '_weight', true);
						$single_product['length'] = get_post_meta($the_query->post->ID, '_length', true);
						$single_product['width'] = get_post_meta($the_query->post->ID, '_width', true);
						$single_product['height'] = get_post_meta($the_query->post->ID, '_height', true);
						$single_product['sold_individually'] = get_post_meta($the_query->post->ID, '_sold_individually', true);
						$single_product['_manage_stock'] = get_post_meta($the_query->post->ID, '_manage_stock', true);
						$single_product['stock'] = get_post_meta($the_query->post->ID, '_stock', true);
						$single_product['backorders'] = get_post_meta($the_query->post->ID, '_backorders', true);
						$single_product['featured'] = get_post_meta($the_query->post->ID, '_featured', true);
						$single_product['tax_status'] = get_post_meta($the_query->post->ID, '_tax_status', true);
						$single_product['tax_class'] = get_post_meta($the_query->post->ID, '_tax_class', true);

                        $_uploaded_directory= str_replace( ' ','_', $export_xls_name.'_uploaded' );
                        $uploaded_sub_dir= WOOEXIM_EXPORT_PATH . '/'  . $_uploaded_directory;
						$thumbnailid = get_post_meta($the_query->post->ID, '_thumbnail_id', true);
                        $src_filename= get_post_meta($thumbnailid, '_wp_attached_file', true);
						$timg = $upload_dir['baseurl'] . '/' . get_post_meta($thumbnailid, '_wp_attached_file', true);

                        if ( !file_exists($uploaded_sub_dir) ) { // create directory with images
                            mkdir($uploaded_sub_dir,0775);
                        }

                        create_subdirs_of_file($src_filename, $uploaded_sub_dir);
						$images_subdirs = $_uploaded_directory . '/' . $src_filename;;

                        $src_full_filename = $upload_dir['basedir'] . '/' . $src_filename;
                        if ( file_exists($src_full_filename) and !is_dir($src_full_filename) ) {
                            $ret= copy( $src_full_filename, $uploaded_sub_dir . '/' . $src_filename);  // copy thumbnail of product to subdirectory
                            if ($ret) $images_exported_count++;
                        }

						$imggalls = get_post_meta($the_query->post->ID, '_product_image_gallery', true);
						$imggall = explode(',', $imggalls); $kk = 0;
						foreach((array)$imggall as $nimg ){
							if( $timg != '' ){ $timg .= '|';}
							if( $images_subdirs != '' ){ $images_subdirs .= '|';}


                            $src_filename= get_post_meta($imggall[$kk], '_wp_attached_file', true);
                            create_subdirs_of_file($src_filename, $uploaded_sub_dir);

							$timg .= $upload_dir['baseurl'] . '/' . $src_filename;
                            $images_subdirs .= $_uploaded_directory . '/' . $src_filename;
							$src_full_filename = $upload_dir['basedir'] . '/' . $src_filename;
//                            echo '<pre>$src_full_filename::'.print_r($src_full_filename,true).'</pre>';
                            if ( file_exists($src_full_filename) and !is_dir($src_full_filename) ) {
//                                echo '<pre>$INSIDE::'.print_r(1,true).'</pre>';
                                $ret= copy( $src_full_filename, $uploaded_sub_dir . '/' . $src_filename);
                                if ($ret) $images_exported_count++;
//                                echo '<pre>$ret::'.print_r($ret,true).'</pre>';
                            }
//                            die("-1 XXZ");
							$kk++;
						}
//                        echo '<pre>$timg::'.print_r($timg,true).'</pre>';
//                        echo '<pre>$images_subdirs::'.print_r($images_subdirs,true).'</pre>';
						$single_product['images'] = $timg;
						$single_product['product_image_by_path'] = $images_subdirs;

//                        $users_list_for_product= array();
//                        $users_list= get_users( array( 'orderby'    => 'ID', 'order'        => 'ASC' ) );
//                        foreach( $users_list as $next_key=>$next_user ) {
//                            $next_user_roles= '';
//                            foreach( $next_user->roles as $next_role ) {
//                                $next_user_roles.= $next_role.'|';
//                            }
//                            $users_list_for_product[]= array( 'ID' => $next_user->ID, 'user_login' => $next_user->user_login, 'display_name' => $next_user->display_name, 'user_email' => $next_user->user_email, 'user_url' => $next_user->user_url, 'user_registered' => $next_user->user_registered, 'user_nicename' => $next_user->user_nicename, 'user_status' => $next_user->user_status, 'roles' => $next_user_roles  );
//                        }
////                        echo '<pre>$users_list_for_product::'.print_r($users_list_for_product,true).'</pre>';
////                        die("-1 XXZ");
//                        $single_product['users_list'] = serialize($users_list_for_product);

                        $product_attributes_list= array();
                        foreach( $products_attrs_list as $next_attribute ) {
                            $next_attributes_list = wc_get_product_terms($product->id, 'pa_'.$next_attribute['attribute_name'], array('fields' => 'names'));
                            $product_attributes_list[$next_attribute['attribute_name']] = !empty($next_attributes_list[0]) ? $next_attributes_list[0] : '';
                            $product_attributes_count+= !empty($next_attributes_list[0]) ? count($next_attributes_list[0]) : 0;
//                            echo '<pre>NEXT $product_attributes_count::'.print_r($product_attributes_count,true).'</pre>';
                        }

//                      echo '<pre>$product_attributes_list::'.print_r($product_attributes_list,true).'</pre>';
						$single_product['product_attributes_list'] = serialize($product_attributes_list);
						$single_product['post_date'] = $product->post->post_date;
//                        die("-1 XXZ");

						$single_product['download_limit'] = get_post_meta($the_query->post->ID, '_download_limit', true);
						$single_product['download_expiry'] = get_post_meta($the_query->post->ID, '_download_expiry', true);
						$single_product['file_path'] = get_post_meta($the_query->post->ID, '_file_path', true);
						$single_product['product_url'] = get_post_meta($the_query->post->ID, '_product_url', true);
						$single_product['product_type'] = get_post_meta($the_query->post->ID, '_product_type', true);
						
						$post_sclass = wp_get_post_terms($the_query->post->ID, $taxonomy = 'product_shipping_class');
						$sclass = ''; $ii = 0;
						foreach((array)$post_sclass as $post_class):
							if($ii > 0){$sclass .= '|';}
							$sclass .= $post_class->name;
							$ii++;
						endforeach;
						$single_product['shipping_class'] = $sclass;
						
						$mydata = $wpdb->get_row("SELECT * FROM " . $wpdb->posts . " WHERE id = " . $the_query->post->ID . "");
						$single_product['comment_status'] = $mydata->comment_status;
						$single_product['ping_status'] = $mydata->ping_status;

//                        echo '<pre>$the_query->post->ID::'.print_r($the_query->post->ID,true).'</pre>';
                        $comments_list= get_comments( array('post_id' => $the_query->post->ID, 'orderby' => 'comment_ID', 'order'=> 'ASC' ) );
//                        echo '<pre>'.count($comments_list).'::$comments_list::'.print_r($comments_list,true).'</pre>';
                        $result_comments_list= array();
                        foreach( $comments_list as $next_key=>$next_comment ) {
                            $rating = intval( get_comment_meta( $next_comment->comment_ID, 'rating', true ) );
                            $result_comments_list[]= array( 'comment_ID'=> $next_comment->comment_ID, 'comment_author'=> $next_comment->comment_author, 'comment_author_email'=> $next_comment->comment_author_email, 'comment_author_url'=> $next_comment->comment_author_url, 'comment_author_IP'=> $next_comment->comment_author_IP, 'comment_date'=> $next_comment->comment_date, 'comment_date_gmt'=> $next_comment->comment_date_gmt, 'comment_content'=> $next_comment->comment_content, 'comment_karma'=> $next_comment->comment_karma, 'comment_approved'=> $next_comment->comment_approved, 'comment_agent'=> $next_comment->comment_agent, 'comment_type'=> $next_comment->comment_type, 'comment_parent'=> $next_comment->comment_parent, 'user_id'=> $next_comment->user_id, 'rating'=> $rating  );

                        }

                        $result_comments_list_length= get_arr_length($result_comments_list);
//                        $single_product['name'] .= /*$product->post->post_title . */' src ID:'.$the_query->post->ID.' '.count($result_comments_list).' @@@'.$result_comments_list_length.'@@@ ';

                        $single_product['result_comments_list']= serialize($result_comments_list);

                        $rating_average = $product->get_average_rating();
                        $single_product['result_comment_rating_average']= $rating_average;

						$full_data[] = $single_product;
                        $products_exported_count++;
					}
					$headers = array(
								'Name', 'Description', 'Short Description',  'Product Status', 'Permalink', 'Categories', 'Tags', 'SKU', 'Sale Price',
								'Visibility', 'Stock Status', 'Price', 'Regular Price', 'Total Sales',	'Downloadable',	'Virtual', 'Purchase Note', 'Weight', 'Length', 'Width', 
								'Height', 'Sold Individually', 'Manage Stock', 'Stock', 'Backorders', 'Featured', 'Tax Status', 'Tax Class', 'Images', 'product_image_by_path', /*'users_list', */'product_attributes_list', 'post_date',  'Download Limit', 'Download Expiry', 'File Path', 'Product URL','Product Type',
								'Shipping Class', 'Comment Status', 'Ping Status', 'result_comments_list', 'result_comment_rating_average'
							);
//                    echo '<pre>$full_data::'.print_r($full_data,true).'</pre>';
//                    die("-1 XXZ");
					$records = $full_data;
					$sheet_data = new Woo_ExIm_spreadsheet();
					$sheet_data->set_filename( $export_xls_name );
					$sheet_data->set_header( $headers );
					$sheet_data->set_records( $records );
					$sheet_data->do_export();
					
					
					$all_cat_names = str_replace(" ","",$all_cat_names ); 
					$all_cat_names = substr($all_cat_names, 0, -1);
					$current_user = wp_get_current_user();
					$author = $current_user->user_login;
					$category = implode(", ", array_unique( explode(",", $all_cat_names) ) );
					
					$sql = 'insert into '.$wpdb->prefix.'wooexim_export_archive 
						(archive_name, archive_type, author, category) values 
						( "'.$export_xls_name.'", "product", "'.$author.'", "'.$category.'" )';
					$wpdb->query( $sql );
                    global $_SESSION;
                    $_SESSION['products_exported_count']= $products_exported_count;
                    $_SESSION['images_exported_count'] = $images_exported_count;

                    $_SESSION['attributes_count']= $attributes_count;
                    $_SESSION['product_attributes_count'] = $product_attributes_count;
//                    echo '<pre>+1  $_SESSION::'.print_r($_SESSION,true).'</pre>';

                    unset( $_REQUEST );
					return true;
				}
				else
				{
					echo '<span class="error_mgs">No products in the category to export.</span>';
					unset( $_REQUEST );
					return false;
				}
			}
		}
	}
	
	function wxe_get_archive_name(){
		$prefix_name = get_option( 'wxe_archive_prefix' );
		if( empty( $prefix_name ) )
		{
			$today = date("Y_m_d H_i_s");
			$prefix_name = 'wooexim_export_'.$today;
		}
		else
		{
			$real_name = '';
			if (strpos($prefix_name,'%') === false) {
				$prefix_name .= "".date("Y_m_d H_i_s");
			}
			else
			{
				$temp = explode( '%', $prefix_name );
				//echo "<pre>"; print_r($temp); echo "</pre>";
				foreach ( $temp as $val )
				{
					if(strcasecmp ( $val , 'YEAR') == 0)
						$real_name .= date("Y"); 
					else if(strcasecmp ( $val , 'MONTH') == 0)
						$real_name .= date("m"); 
					else if(strcasecmp ( $val , 'DATE') == 0)
						$real_name .= date("d"); 
					else if(strcasecmp ( $val , 'HOUR') == 0)
						$real_name .= date("H"); 
					else if(strcasecmp ( $val , 'MINUTE') == 0)
						$real_name .= date("i"); 
					else if(strcasecmp ( $val , 'SEC') == 0)
						$real_name .= date("s"); 
					else 
						$real_name .= $val;
				}
				$prefix_name = $real_name;
			}
		}
		return $prefix_name;
	}
	
	function wxe_formatSizeUnits($bytes)
    {
        if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }

        return $bytes;
	}
}

function create_subdirs_of_file($src_filename, $uploaded_sub_dir) {
//    echo '<pre>$uploaded_sub_dir::'.print_r($uploaded_sub_dir,true).'</pre>';
//    die("-1 XXZ");
    $src= str_replace('/',',',$src_filename);
//    echo '<pre>$src::'.print_r($src,true).'</pre>';
    $a= preg_split('/,/',$src);
//    echo '<pre>$a::'.print_r($a,true).'</pre>';
    if ( is_array($a) and count($a) > 1 ) {
        $l= count($a);
        $dst_dir= $uploaded_sub_dir;
        for( $i= 0; $i< $l-1;$i++ ) {
            $dst_dir.= '/' . $a[$i];
            if ( !file_exists($dst_dir) ) {
                mkdir($dst_dir, 0775);
            }
        }
    }
}
//function createDir($directories_list = array(), $removeExistingFiles= false, $mode = 0755)
//{
//    $l= count($directories_list); $i= 1;
//    foreach ($directories_list as $dir) {
//        $lastDir= $l==$i;
//        if (!file_exists($dir)) {
//            mkdir($dir, $mode);
//        } else {
//            if ($removeExistingFiles and $lastDir ) {
//                nsnClass_appFuncs::deleteDirectory($dir,  null, true);
//            }
//        }
//        $i++;
//    }
//}


function get_products_attrs_list($attribute_name= '', $get_attrs= false) {
//        echo '<pre>$attribute_name::'.print_r($attribute_name,true).'</pre>';
    $attribute_taxonomies = wc_get_attribute_taxonomies();  //  Get attribute taxonomies.
    $ret_products_attrs_list = array();
    if ( $attribute_taxonomies ) :
        foreach ($attribute_taxonomies as $key=>$tax) :
            if (taxonomy_exists(wc_attribute_taxonomy_name($tax->attribute_name))) :
                $next_tax_struct= get_object_vars( $tax );
                $attrs_objects_list= get_terms( wc_attribute_taxonomy_name($tax->attribute_name), 'orderby=name&hide_empty=0' );
                $attrs_list= array();
                foreach( $attrs_objects_list as $next_attr_object ) {
                    $attrs_list[]= get_object_vars($next_attr_object);
                }
                $next_tax_struct['attrs'] = $attrs_list;
                $ret_products_attrs_list[] = $next_tax_struct;
            endif;
        endforeach;
    endif;
    if( !empty($attribute_name) ) {
        foreach( $ret_products_attrs_list as $next_key=> $next_products_attr ) {
            if ( $next_products_attr['attribute_name']  ==  $attribute_name ) {
//                    echo '<pre>+++$next_products_attr::'.print_r($next_products_attr,true).'</pre>';
                if ( $get_attrs and !empty($next_products_attr['attrs']) and is_array($next_products_attr['attrs']) ) {
                    $ret_array= array();
                    foreach($next_products_attr['attrs'] as $next_attr) {
                        $ret_array[] = array('ID'=>$next_attr['term_id'], 'name'=>$next_attr['name'], 'slug'=>$next_attr['slug']);
                    }
//                        echo '<pre>$::'.print_r($ret_array,true).'</pre>';
//                        die("-1 XXZZZZZZZ ");
                    return $ret_array;
                }
                return $next_products_attr;
            }
        }
        //and !empty($ret_products_attrs_list[$attribute_name]) ) {
        return ( !empty($ret_products_attrs_list[$attribute_name]) ? $ret_products_attrs_list[$attribute_name] : array() );
    }
    return $ret_products_attrs_list;
}
function get_arr_length($arr) {
    if (!is_array($arr) or empty($arr)) return 0;
    $ret= 0;
    foreach( $arr as $next_key=>$next_arr ) {
        if (is_array($next_arr)) {
            $ret += get_arr_length($next_arr);

        } else {
            $ret += count($next_arr);
        }
    }
    return $ret;
}

?>