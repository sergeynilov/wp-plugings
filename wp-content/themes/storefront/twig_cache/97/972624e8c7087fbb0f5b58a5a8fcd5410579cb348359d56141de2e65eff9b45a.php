<?php

/* white-simple/woo_ext_products_list.twig */
class __TwigTemplate_0f8dcc9454b42a8f0eba5d464fe7238231ae30af0a3387d858dd22a10cb4ae0e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 220
        echo "  ";
        // line 221
        echo "
<h2 class=\"site_content_title margin_top_lg\">
\t<span class=\"fa fa-building-o fa-2x middle_icon\" ></span>&nbsp;Found Products (";
        // line 223
        echo twig_length_filter($this->env, (isset($context["products_list"]) ? $context["products_list"] : $this->getContext($context, "products_list")));
        echo ")
</h2>

attrs_to_show::";
        // line 226
        echo twig_var_dump($this->env, $context, (isset($context["attrs_to_show"]) ? $context["attrs_to_show"] : $this->getContext($context, "attrs_to_show")));
        echo "
";
        // line 227
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["products_list"]) ? $context["products_list"] : $this->getContext($context, "products_list")));
        foreach ($context['_seq'] as $context["_key"] => $context["nextProduct"]) {
            // line 228
            echo "\t<div class=\"col-xs-12 col-sm-6 next_hostel\">
\t\t<ul class=\"products ul_multiline\">

                        <span class=\"visible-xs\">
\t                        ";
            // line 233
            echo "\t                        ";
            // line 234
            echo "\t                    </span>
\t                    <span class=\"visible-sm\">
\t                        ";
            // line 237
            echo "\t                    </span>
\t                    <span class=\"visible-md\">
\t                        ";
            // line 240
            echo "\t                    </span>
\t                    <span class=\"visible-lg\">
\t                        ";
            // line 242
            echo $this->getAttribute($this, "show_next_product_info", array(0 => $context["nextProduct"], 1 => (isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")), 2 => (isset($context["attrs_to_show"]) ? $context["attrs_to_show"] : $this->getContext($context, "attrs_to_show")), 3 => "_md"), "method");
            echo "
\t                    </span>
\t\t</ul>

\t</div>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['nextProduct'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    // line 1
    public function getshow_next_product_info($__nextProduct__ = null, $__fields_to_show__ = null, $__attrs_to_show__ = null, $__field_param__ = null, ...$__varargs__)
    {
        $context = $this->env->mergeGlobals(array(
            "nextProduct" => $__nextProduct__,
            "fields_to_show" => $__fields_to_show__,
            "attrs_to_show" => $__attrs_to_show__,
            "field_param" => $__field_param__,
            "varargs" => $__varargs__,
        ));

        $blocks = array();

        ob_start();
        try {
            // line 2
            echo "
\t";
            // line 4
            echo "\t<li class=\"post-";
            echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "id", array(), "array");
            echo " \">
\t";
            // line 5
            if (($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "title", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")), "title", array()))) {
                // line 6
                echo "\t\t<div class=\"row\">
\t\t<h3>
\t\t\t";
                // line 8
                if (($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "image", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")), "image", array()))) {
                    // line 9
                    echo "\t\t\t\t<span class=\"fa fa-info-circle \" data-toggle=\"tooltip\" data-placement=\"top\" title=\"";
                    echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "purchase_note", array(), "array");
                    echo "\"></span>&nbsp;
\t\t\t";
                }
                // line 11
                echo "\t\t\t<a href=\"";
                echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "guid", array(), "array");
                echo "\" class=\"woo_products_title a_link\">";
                echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "id", array(), "array");
                echo "->";
                echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "post_title", array(), "array");
                echo "</a>
\t\t</h3>
\t\t</div>
\t";
            }
            // line 15
            echo "
\t";
            // line 16
            if (($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "image", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")), "image", array()))) {
                // line 17
                echo "\t\t<div class=\"row\">
\t\t\t<a href=\"";
                // line 18
                echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "guid", array(), "array");
                echo "\" class=\"woo_products_image a_link\">";
                echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "image_url", array(), "array");
                echo "</a>
\t\t</div>
\t";
            }
            // line 21
            echo "
\t\t";
            // line 23
            echo "\t\t\t";
            // line 24
            echo "\t\t";
            // line 25
            echo "

\t";
            // line 28
            echo "\t\t";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["attrs_to_show"]) ? $context["attrs_to_show"] : $this->getContext($context, "attrs_to_show")));
            foreach ($context['_seq'] as $context["_key"] => $context["next_attr_to_show"]) {
                // line 29
                echo "\t\t\t";
                if (($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), $context["next_attr_to_show"], array(), "array", true, true) && ($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), $context["next_attr_to_show"], array(), "array") != ""))) {
                    // line 30
                    echo "\t\t\t<div class=\"row\">
\t\t\t\t<span class=\"woo_products_label\">";
                    // line 31
                    echo $context["next_attr_to_show"];
                    echo ":</span>
\t\t\t\t<span class=\"woo_products_";
                    // line 32
                    echo $context["next_attr_to_show"];
                    echo "\">
\t\t\t\t\t";
                    // line 33
                    echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), $context["next_attr_to_show"], array(), "array");
                    echo "
\t\t\t\t</span>
\t\t\t</div>
\t\t\t";
                }
                // line 37
                echo "\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['next_attr_to_show'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 38
            echo "\t";
            // line 39
            echo "

\t";
            // line 42
            echo "\t\t";
            // line 43
            echo "\t\t\t";
            // line 44
            echo "\t\t\t";
            // line 45
            echo "\t\t\t\t";
            // line 46
            echo "\t\t\t";
            // line 47
            echo "\t\t";
            // line 48
            echo "\t";
            // line 49
            echo "

\t";
            // line 51
            if (($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "sku", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")), "sku", array()))) {
                // line 52
                echo "\t\t<div class=\"row\">
\t\t\t<span class=\"woo_products_label\">Sku:</span>
\t\t\t<span class=\"woo_products_sku\">
\t\t\t\t";
                // line 55
                echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "sku", array(), "array");
                echo "
\t\t\t</span>
\t\t</div>
\t";
            }
            // line 59
            echo "

\t";
            // line 61
            if (($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "categories", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")), "categories", array()))) {
                // line 62
                echo "\t\t<div class=\"row\">
\t\t\t<span class=\"woo_products_label\">Categor";
                // line 63
                if ((twig_length_filter($this->env, $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "categories_list", array(), "array")) > 1)) {
                    echo "ies";
                } else {
                    echo "y";
                }
                echo ":</span>
\t\t\t";
                // line 64
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "categories_list", array(), "array"), "ret_titles", array(), "array"));
                $context['loop'] = array(
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                );
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["_key"] => $context["next_category_title"]) {
                    // line 65
                    echo "\t\t\t<span class=\"woo_products_categories\">
\t\t\t\t<a href=\"";
                    // line 66
                    echo call_user_func_array($this->env->getFilter('make_woo_product_category_url')->getCallable(), array($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "categories_list", array(), "array"), "ret_slugs", array(), "array"), $this->getAttribute($context["loop"], "index0", array()), array(), "array")));
                    echo "\" class=\"a_link\">";
                    echo $context["next_category_title"];
                    echo "</a>
\t\t\t\t";
                    // line 67
                    if ( !$this->getAttribute($context["loop"], "last", array())) {
                        echo ", ";
                    }
                    // line 68
                    echo "\t\t\t</span>
\t\t\t";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['next_category_title'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 70
                echo "\t\t</div>
\t";
            }
            // line 72
            echo "

\t";
            // line 74
            if ((($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "sale_price", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")), "sale_price", array())) || ($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "regular_price", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")), "regular_price", array())))) {
                // line 75
                echo "\t\t<div class=\"row\">
\t\t\t<span class=\"woo_products_label\">Price:</span>


\t\t\t<span class=\"woo_products_sale_price\">

\t\t\t\t";
                // line 81
                if (($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), "regular_price", array(), "array", true, true) && ($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "regular_price", array(), "array") > 0))) {
                    // line 82
                    echo "\t\t\t\t<strike><span class=\"woo_products_currencySymbol\">\$";
                    echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "regular_price", array(), "array");
                    echo "</span></strike>
\t\t\t\t";
                }
                // line 84
                echo "
\t\t\t\t";
                // line 85
                if ((($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), "regular_price", array(), "array", true, true) && ($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "regular_price", array(), "array") > 0)) && ($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), "sale_price", array(), "array", true, true) && ($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "sale_price", array(), "array") > 0)))) {
                    // line 86
                    echo "\t\t\t\t&nbsp;-&nbsp;
\t\t\t\t";
                }
                // line 88
                echo "
\t\t\t\t";
                // line 89
                if (($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), "sale_price", array(), "array", true, true) && ($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "sale_price", array(), "array") > 0))) {
                    // line 90
                    echo "\t\t\t\t<span class=\"woo_products_currencySymbol\">\$</span>";
                    echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "sale_price", array(), "array");
                    echo "
\t\t\t\t";
                }
                // line 92
                echo "\t\t\t</span>
\t\t</div>
\t";
            }
            // line 95
            echo "

\t\t";
            // line 97
            if (($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "is_in_stock", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")), "is_in_stock", array()))) {
                // line 98
                echo "\t\t\t<div class=\"row\">
\t\t\t\t<span class=\"woo_products_label\">In Stock:</span>
\t\t\t<span class=\"woo_products_is_in_stock\">
\t\t\t\t<b>";
                // line 101
                if ($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "is_in_stock", array(), "array")) {
                    echo "Yes";
                } else {
                    echo "No";
                }
                echo "</b>
\t\t\t</span>
\t\t\t</div>
\t\t";
            }
            // line 105
            echo "

\t\t";
            // line 125
            echo "


\t\t";
            // line 129
            echo "\t</li>

\t";
            // line 178
            echo "
\t";
            // line 180
            echo "\t\t";
            // line 181
            echo "\t";
            // line 182
            echo "
\t";
            // line 184
            echo "\t\t";
            // line 185
            echo "\t\t\t";
            // line 186
            echo "\t\t";
            // line 187
            echo "\t";
            // line 188
            echo "
\t";
            // line 190
            echo "\t\t";
            // line 191
            echo "\t\t";
            // line 192
            echo "\t\t\t";
            // line 193
            echo "\t\t";
            // line 194
            echo "\t";
            // line 195
            echo "

\t";
            // line 198
            echo "\t\t";
            // line 199
            echo "\t\t\t";
            // line 200
            echo "\t\t\t";
            // line 201
            echo "\t\t";
            // line 202
            echo "\t";
            // line 203
            echo "
\t";
            // line 205
            echo "\t\t";
            // line 206
            echo "\t\t\t";
            // line 207
            echo "\t\t";
            // line 208
            echo "\t";
            // line 209
            echo "
\t";
            // line 211
            echo "\t\t";
            // line 212
            echo "\t";
            // line 213
            echo "
\t";
            // line 215
            echo "\t\t";
            // line 216
            echo "\t\t\t";
            // line 217
            echo "\t\t";
            // line 218
            echo "\t";
            // line 219
            echo "
";
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    public function getTemplateName()
    {
        return "white-simple/woo_ext_products_list.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  441 => 219,  439 => 218,  437 => 217,  435 => 216,  433 => 215,  430 => 213,  428 => 212,  426 => 211,  423 => 209,  421 => 208,  419 => 207,  417 => 206,  415 => 205,  412 => 203,  410 => 202,  408 => 201,  406 => 200,  404 => 199,  402 => 198,  398 => 195,  396 => 194,  394 => 193,  392 => 192,  390 => 191,  388 => 190,  385 => 188,  383 => 187,  381 => 186,  379 => 185,  377 => 184,  374 => 182,  372 => 181,  370 => 180,  367 => 178,  363 => 129,  358 => 125,  354 => 105,  343 => 101,  338 => 98,  336 => 97,  332 => 95,  327 => 92,  321 => 90,  319 => 89,  316 => 88,  312 => 86,  310 => 85,  307 => 84,  301 => 82,  299 => 81,  291 => 75,  289 => 74,  285 => 72,  281 => 70,  266 => 68,  262 => 67,  256 => 66,  253 => 65,  236 => 64,  228 => 63,  225 => 62,  223 => 61,  219 => 59,  212 => 55,  207 => 52,  205 => 51,  201 => 49,  199 => 48,  197 => 47,  195 => 46,  193 => 45,  191 => 44,  189 => 43,  187 => 42,  183 => 39,  181 => 38,  175 => 37,  168 => 33,  164 => 32,  160 => 31,  157 => 30,  154 => 29,  149 => 28,  145 => 25,  143 => 24,  141 => 23,  138 => 21,  130 => 18,  127 => 17,  125 => 16,  122 => 15,  110 => 11,  104 => 9,  102 => 8,  98 => 6,  96 => 5,  91 => 4,  88 => 2,  73 => 1,  59 => 242,  55 => 240,  51 => 237,  47 => 234,  45 => 233,  39 => 228,  35 => 227,  31 => 226,  25 => 223,  21 => 221,  19 => 220,);
    }
}
/* {% macro show_next_product_info( nextProduct, fields_to_show, attrs_to_show, field_param ) %}*/
/* */
/* 	{#  product type-product status-publish has-post-thumbnail product_cat-clothing product_cat-t-shirts first instock shipping-taxable purchasable product-type-simple  #}*/
/* 	<li class="post-{{ nextProduct['id'] }} ">*/
/* 	{% if fields_to_show.title is defined and fields_to_show.title %}*/
/* 		<div class="row">*/
/* 		<h3>*/
/* 			{% if fields_to_show.image is defined and fields_to_show.image  %}*/
/* 				<span class="fa fa-info-circle " data-toggle="tooltip" data-placement="top" title="{{ nextProduct['purchase_note'] }}"></span>&nbsp;*/
/* 			{% endif %}*/
/* 			<a href="{{ nextProduct['guid'] }}" class="woo_products_title a_link">{{ nextProduct['id'] }}->{{ nextProduct['post_title'] }}</a>*/
/* 		</h3>*/
/* 		</div>*/
/* 	{% endif %}*/
/* */
/* 	{% if fields_to_show.image is defined and fields_to_show.image  %}*/
/* 		<div class="row">*/
/* 			<a href="{{ nextProduct['guid'] }}" class="woo_products_image a_link">{{ nextProduct['image_url'] }}</a>*/
/* 		</div>*/
/* 	{% endif %}*/
/* */
/* 		{#<div class="star-rating" title="Rated 5 out of 5">#}*/
/* 			{#<span style="width:100%"><strong class="rating">5</strong> out of 5</span>#}*/
/* 		{#</div>#}*/
/* */
/* */
/* 	{#{% if fields_to_show.brand is defined and fields_to_show.brand %}#}*/
/* 		{% for next_attr_to_show in attrs_to_show %}*/
/* 			{% if nextProduct[next_attr_to_show]  is defined and nextProduct[next_attr_to_show]!= "" %}*/
/* 			<div class="row">*/
/* 				<span class="woo_products_label">{{ next_attr_to_show }}:</span>*/
/* 				<span class="woo_products_{{ next_attr_to_show }}">*/
/* 					{{ nextProduct[next_attr_to_show] }}*/
/* 				</span>*/
/* 			</div>*/
/* 			{% endif %}*/
/* 		{% endfor %}*/
/* 	{#{% endif %}#}*/
/* */
/* */
/* 	{#{% if fields_to_show.brand is defined and fields_to_show.brand %}#}*/
/* 		{#<div class="row">#}*/
/* 			{#<span class="woo_products_label">Brand:</span>#}*/
/* 			{#<span class="woo_products_brand">#}*/
/* 				{#{{ nextProduct['brand'] }}#}*/
/* 			{#</span>#}*/
/* 		{#</div>#}*/
/* 	{#{% endif %}#}*/
/* */
/* */
/* 	{% if fields_to_show.sku is defined and fields_to_show.sku  %}*/
/* 		<div class="row">*/
/* 			<span class="woo_products_label">Sku:</span>*/
/* 			<span class="woo_products_sku">*/
/* 				{{ nextProduct['sku'] }}*/
/* 			</span>*/
/* 		</div>*/
/* 	{% endif %}*/
/* */
/* */
/* 	{% if fields_to_show.categories is defined and fields_to_show.categories %}*/
/* 		<div class="row">*/
/* 			<span class="woo_products_label">Categor{% if nextProduct['categories_list'] | length > 1 %}ies{% else %}y{% endif %}:</span>*/
/* 			{% for next_category_title in nextProduct['categories_list']['ret_titles'] %}*/
/* 			<span class="woo_products_categories">*/
/* 				<a href="{{ nextProduct['categories_list']['ret_slugs'][loop.index0] | make_woo_product_category_url }}" class="a_link">{{ next_category_title }}</a>*/
/* 				{% if not loop.last %}, {% endif %}*/
/* 			</span>*/
/* 			{% endfor %}*/
/* 		</div>*/
/* 	{% endif %}*/
/* */
/* */
/* 	{% if ( fields_to_show.sale_price is defined  and fields_to_show.sale_price ) or ( fields_to_show.regular_price is defined and fields_to_show.regular_price ) %}*/
/* 		<div class="row">*/
/* 			<span class="woo_products_label">Price:</span>*/
/* */
/* */
/* 			<span class="woo_products_sale_price">*/
/* */
/* 				{% if nextProduct['regular_price'] is defined and nextProduct['regular_price'] > 0 %}*/
/* 				<strike><span class="woo_products_currencySymbol">${{ nextProduct['regular_price'] }}</span></strike>*/
/* 				{% endif %}*/
/* */
/* 				{% if ( nextProduct['regular_price'] is defined and nextProduct['regular_price'] > 0 ) and ( nextProduct['sale_price'] is defined and nextProduct['sale_price'] >0 ) %}*/
/* 				&nbsp;-&nbsp;*/
/* 				{% endif %}*/
/* */
/* 				{% if nextProduct['sale_price'] is defined and nextProduct['sale_price'] > 0 %}*/
/* 				<span class="woo_products_currencySymbol">$</span>{{ nextProduct['sale_price'] }}*/
/* 				{% endif %}*/
/* 			</span>*/
/* 		</div>*/
/* 	{% endif %}*/
/* */
/* */
/* 		{% if fields_to_show.is_in_stock is defined and fields_to_show.is_in_stock  %}*/
/* 			<div class="row">*/
/* 				<span class="woo_products_label">In Stock:</span>*/
/* 			<span class="woo_products_is_in_stock">*/
/* 				<b>{% if nextProduct['is_in_stock'] %}Yes{% else %}No{% endif %}</b>*/
/* 			</span>*/
/* 			</div>*/
/* 		{% endif %}*/
/* */
/* */
/* 		{#            [categories_list] => Array*/
/* 						(*/
/* 							[field_values] => Computer Accessories,Computer Components,PC Gaming*/
/* 							[ret_keys] => Array*/
/* 								(*/
/* 									[0] => 8*/
/* 									[1] => 9*/
/* 									[2] => 10*/
/* 								)*/
/* */
/* 							[ret_titles] => Array*/
/* 								(*/
/* 									[0] => Computer Accessories*/
/* 									[1] => Computer Components*/
/* 									[2] => PC Gaming*/
/* 								)*/
/* */
/* 						)#}*/
/* */
/* */
/* */
/* 		{#<a rel="nofollow" href="/product-category/clothing/?add-to-cart={{ nextProduct['id'] }}" data-quantity="1" data-product_id="{{ nextProduct['id'] }}" data-product_sku="" class="button product_type_simple add_to_cart_button ajax_add_to_cart">Add to cart</a>#}*/
/* 	</li>*/
/* */
/* 	{#*/
/*             [post_title] => A4 Tech Laser Gaming Mouse*/
/*             [post_excerpt] =>*/
/*             [post_status] => publish*/
/*             [comment_status] => open*/
/*             [ping_status] => closed*/
/*             [post_password] =>*/
/*             [post_name] => a4-tech-laser-gaming-mouse*/
/*             [to_ping] =>*/
/*             [pinged] =>*/
/*             [post_modified] => 2016-08-29 13:45:49*/
/*             [post_modified_gmt] => 2016-08-29 13:45:49*/
/*             [post_content_filtered] =>*/
/*             [post_parent] => 0*/
/*             [guid] => http://local-wp-plugings.com/?post_type=product&p=14*/
/*             [menu_order] => 0*/
/*             [post_type] => product*/
/*             [post_mime_type] =>*/
/*             [comment_count] => 0*/
/*             [filter] => raw*/
/*             [image_url] => 1*/
/*             [brands_list] => Array*/
/*                 (*/
/*                     [0] => A4Tech*/
/*                 )*/
/* */
/*             [categories_list] => Array*/
/*                 (*/
/*                     [field_values] => Computer Accessories,Computer Components,PC Gaming*/
/*                     [ret_keys] => Array*/
/*                         (*/
/*                             [0] => 8*/
/*                             [1] => 9*/
/*                             [2] => 10*/
/*                         )*/
/* */
/*                     [ret_titles] => Array*/
/*                         (*/
/*                             [0] => Computer Accessories*/
/*                             [1] => Computer Components*/
/*                             [2] => PC Gaming*/
/*                         )*/
/* */
/*                 )*/
/* */
/*             [sale_price] => 25*/
/*             [regular_price] => 29.70#}*/
/* */
/* 	{#<div class="img_banner"  >#}*/
/* 		{#<img src="static/images/{{ current_skin_name }}/spotlight.png">#}*/
/* 	{#</div>#}*/
/* */
/* 	{#<div class="next_hostel_image" >#}*/
/* 		{#<a href="{{ base_url }}{{ nextProduct.name | make_woo_product_url(nextProduct) | raw }}">#}*/
/* 			{#<div class="back_image image_border" style="background-image: url('{{ nextProduct.default_image }}'); width:{{ nextProduct['width' ~ field_param] }}px; height:{{ nextProduct['height' ~ field_param] }}px;"   id="spotlight_img_hostel_{{ nextProduct.id }}"></div>#}*/
/* 		{#</a>#}*/
/* 	{#</div>#}*/
/* */
/* 	{#<h3 class="site_content_title next_hostel_title text_overflowed" id="div_show_name_{{ nextProduct.id }}">#}*/
/* 		{#<span class="glyphicon glyphicon-step-forward pull-left style_display_none margin_top_sm" id="span_show_name_{{ nextProduct.id }}"  data-toggle="tooltip" data-placement="bottom" title="{{ nextProduct.id }}->{{ nextProduct.name }} "  ></span>&nbsp;#}*/
/* 		{#<a href="{{ base_url }}{{ nextProduct.name | make_woo_product_url(nextProduct) | raw }}"  data-toggle="tooltip" data-placement="bottom" title="{{ nextProduct.id }}->{{ nextProduct.name }} " >#}*/
/* 			{#{{ nextProduct.id }}->{{ nextProduct.name }}#}*/
/* 		{#</a>#}*/
/* 	{#</h3>#}*/
/* */
/* */
/* 	{#<div class="row next_hostel_subtitle" >#}*/
/* 		{#<h4 class="site_content_subtitle text_overflowed" title="{{ nextProduct.state_name }}" id="div_show_state_name_{{ nextProduct.id }}" >#}*/
/* 			{#<span class="glyphicon glyphicon glyphicon-step-forward pull-left style_display_none" id="span_show_state_name_{{ nextProduct.id }}" data-toggle="tooltip" data-placement="bottom" title="{{ nextProduct.id }}->{{ nextProduct.state_name }}" ></span>&nbsp;#}*/
/* 			{#<span data-toggle="tooltip" data-placement="bottom" title="{{ nextProduct.id }}->{{ nextProduct.state_name }} " >{{ nextProduct.id }}->{{ nextProduct.state_name }} </span>#}*/
/* 		{#</h4>#}*/
/* 	{#</div>#}*/
/* */
/* 	{#<div class="row next_hostel_descr" >#}*/
/* 		{#<h5 class="site_content_text" class="debug" >#}*/
/* 			{#{{ nextProduct.short_descr | concat_str( 100 ) |raw }}#}*/
/* 		{#</h5>#}*/
/* 	{#</div>#}*/
/* */
/* 	{#&#123;&#35;<div class="row next_hostel_stars" >&#35;&#125;#}*/
/* 		{#&#123;&#35;<img src="{{ base_url }}{{ images_dir }}/{{ current_skin_name }}/stars/stars{{ nextProduct.reviews_avg_rating }}.png"  alt="{{ nextProduct.reviews_avg_rating | show_hostel_review_stars_rating_type_label_label }}" title="{{ nextProduct.reviews_avg_rating | show_hostel_review_stars_rating_type_label_label }}" class="pull-left"><span class="pull-right">{{ nextProduct.price | show_formatted_price( "AU$", "text_money" ) | raw }}</span>&#35;&#125;#}*/
/* 	{#&#123;&#35;</div>&#35;&#125;#}*/
/* */
/* 	{#<div class="row next_hostel_btn" >#}*/
/* 		{#<a href="{{ base_url }}{{ nextProduct.name | make_woo_product_url(nextProduct) | raw }}">#}*/
/* 			{#<button class="item_details">View Details</button>#}*/
/* 		{#</a>#}*/
/* 	{#</div>#}*/
/* */
/* {% endmacro %}  {# show_next_product_info END #}*/
/* */
/* <h2 class="site_content_title margin_top_lg">*/
/* 	<span class="fa fa-building-o fa-2x middle_icon" ></span>&nbsp;Found Products ({{ products_list | length }})*/
/* </h2>*/
/* */
/* attrs_to_show::{{ dump(attrs_to_show) }}*/
/* {% for nextProduct in products_list %}*/
/* 	<div class="col-xs-12 col-sm-6 next_hostel">*/
/* 		<ul class="products ul_multiline">*/
/* */
/*                         <span class="visible-xs">*/
/* 	                        {#nextProduct::{{ dump(nextProduct) }}#}*/
/* 	                        {# { _self.show_next_product_info( nextProduct, fields_to_show, attrs_to_show,  "_xs" ) } #}*/
/* 	                    </span>*/
/* 	                    <span class="visible-sm">*/
/* 	                        {#{{ _self.show_next_product_info( nextProduct, fields_to_show, attrs_to_show, "_sm" ) }}#}*/
/* 	                    </span>*/
/* 	                    <span class="visible-md">*/
/* 	                        {#{{ _self.show_next_product_info( nextProduct, fields_to_show, attrs_to_show,  "_md" ) }}#}*/
/* 	                    </span>*/
/* 	                    <span class="visible-lg">*/
/* 	                        {{ _self.show_next_product_info( nextProduct, fields_to_show, attrs_to_show, "_md" ) }}*/
/* 	                    </span>*/
/* 		</ul>*/
/* */
/* 	</div>*/
/* {% endfor %}*/
