<?php

/* white-simple/filter_brand_box.twig */
class __TwigTemplate_ff45d63d33c7b09d882a39226601fb6fda3bf3a79f757343a1303cad1b686692 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo $this->getAttribute((isset($context["library"]) ? $context["library"] : null), "header_block_start", array(0 => "Brands Filter", 1 => "", 2 => "fa fa-search-plus"), "method");
        echo "
<div class=\"row\">
\t";
        // line 3
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["brands_list"]) ? $context["brands_list"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["next_brand"]) {
            // line 4
            echo "\t\t<input type=\"checkbox\" id=\"next_brand_";
            echo $this->getAttribute($context["next_brand"], "ID", array());
            echo "\" value=\"";
            echo $this->getAttribute($context["next_brand"], "ID", array());
            echo "\" class=\"cbx_brand_selection_filters\" ";
            if ($this->getAttribute($context["loop"], "last", array())) {
                echo "checked ";
            }
            echo ">&nbsp;";
            echo $this->getAttribute($context["next_brand"], "name", array());
            echo " <b>(";
            echo $this->getAttribute($context["next_brand"], "products_count", array());
            echo ")</b><br>
\t";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['next_brand'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 6
        echo "</div>
";
        // line 7
        echo $this->getAttribute((isset($context["library"]) ? $context["library"] : null), "header_block_end", array(0 => "Brands Filter Block End"), "method");
    }

    public function getTemplateName()
    {
        return "white-simple/filter_brand_box.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  71 => 7,  68 => 6,  41 => 4,  24 => 3,  19 => 1,);
    }
}
/* {{ library.header_block_start("Brands Filter", "", "fa fa-search-plus") }}*/
/* <div class="row">*/
/* 	{% for next_brand in brands_list %}*/
/* 		<input type="checkbox" id="next_brand_{{ next_brand.ID }}" value="{{ next_brand.ID }}" class="cbx_brand_selection_filters" {% if loop.last %}checked {%  endif %}>&nbsp;{{ next_brand.name }} <b>({{ next_brand.products_count }})</b><br>*/
/* 	{% endfor %}*/
/* </div>*/
/* {{ library.header_block_end("Brands Filter Block End") }}*/
