<?php

/* white-simple/filter_categories_box.twig */
class __TwigTemplate_dd1b5983a63ade11d32317eeccd57938f9a491e6e87d0f0290efeb2aa44de66a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo $this->getAttribute((isset($context["library"]) ? $context["library"] : null), "header_block_start", array(0 => "Categories Filter", 1 => "", 2 => "fa fa-search-plus"), "method");
        echo "
<div class=\"row\">
\t";
        // line 3
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["categories_list"]) ? $context["categories_list"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["next_category"]) {
            // line 4
            echo "\t    <input type=\"checkbox\" id=\"next_category_";
            echo $this->getAttribute($context["next_category"], "ID", array());
            echo "\" value=\"";
            echo $this->getAttribute($context["next_category"], "ID", array());
            echo "\" class=\"cbx_category_selection_filters\" ";
            if ($this->getAttribute($context["loop"], "last", array())) {
                echo "checked ";
            }
            echo ">&nbsp;";
            echo $this->getAttribute($context["next_category"], "name", array());
            echo " <b>(";
            echo $this->getAttribute($context["next_category"], "products_count", array());
            echo ")</b><br>
\t";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['next_category'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 6
        echo "</div>
";
        // line 7
        echo $this->getAttribute((isset($context["library"]) ? $context["library"] : null), "header_block_end", array(0 => "Categories Filter Block End"), "method");
        echo "
";
    }

    public function getTemplateName()
    {
        return "white-simple/filter_categories_box.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  71 => 7,  68 => 6,  41 => 4,  24 => 3,  19 => 1,);
    }
}
/* {{ library.header_block_start("Categories Filter", "", "fa fa-search-plus") }}*/
/* <div class="row">*/
/* 	{% for next_category in categories_list %}*/
/* 	    <input type="checkbox" id="next_category_{{ next_category.ID }}" value="{{ next_category.ID }}" class="cbx_category_selection_filters" {% if loop.last %}checked {%  endif %}>&nbsp;{{ next_category.name }} <b>({{ next_category.products_count }})</b><br>*/
/* 	{% endfor %}*/
/* </div>*/
/* {{ library.header_block_end("Categories Filter Block End") }}*/
/* */
