<?php

/* white-simple/lib/library.twig */
class __TwigTemplate_4d523f7cfb698eacbe9eeb1b646c2d6aa3713488b1d6f2b91e2e09c7aaa73f80 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 9
        echo " ";
        // line 10
        echo "
";
        // line 14
        echo "


";
        // line 26
        echo " ";
        // line 27
        echo "

";
        // line 32
        echo " ";
    }

    // line 1
    public function getheader_block_start($__header_title__ = null, $__row_class__ = null, $__glyphicon__ = null, $__show_top_margin_sm__ = null, ...$__varargs__)
    {
        $context = $this->env->mergeGlobals(array(
            "header_title" => $__header_title__,
            "row_class" => $__row_class__,
            "glyphicon" => $__glyphicon__,
            "show_top_margin_sm" => $__show_top_margin_sm__,
            "varargs" => $__varargs__,
        ));

        $blocks = array();

        ob_start();
        try {
            // line 2
            echo "\t<div class=\"block_title\" > ";
            // line 3
            echo "\t\t";
            if ( !twig_test_empty((isset($context["glyphicon"]) ? $context["glyphicon"] : null))) {
                // line 4
                echo "\t\t\t<span class=\"glyphicon ";
                echo (isset($context["glyphicon"]) ? $context["glyphicon"] : null);
                echo "\" class=\"text-center\"></span>&nbsp;
\t\t";
            }
            // line 6
            echo "\t\t";
            echo (isset($context["header_title"]) ? $context["header_title"] : null);
            echo "
\t</div>
    <section class=\"block_content ";
            // line 8
            echo (isset($context["row_class"]) ? $context["row_class"] : null);
            echo " block_padding_sm\">
";
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    // line 11
    public function getheader_block_end($__end_marker__ = null, ...$__varargs__)
    {
        $context = $this->env->mergeGlobals(array(
            "end_marker" => $__end_marker__,
            "varargs" => $__varargs__,
        ));

        $blocks = array();

        ob_start();
        try {
            // line 12
            echo "\t</section> <!-- ";
            echo (isset($context["end_marker"]) ? $context["end_marker"] : null);
            echo " -->
";
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    // line 17
    public function gettable_block_start($__header_title__ = null, $__glyphicon__ = null, $__show_top_margin_sm__ = null, ...$__varargs__)
    {
        $context = $this->env->mergeGlobals(array(
            "header_title" => $__header_title__,
            "glyphicon" => $__glyphicon__,
            "show_top_margin_sm" => $__show_top_margin_sm__,
            "varargs" => $__varargs__,
        ));

        $blocks = array();

        ob_start();
        try {
            // line 18
            echo "<section class=\"table_wrapper  \" >
\t<div class=\"block_title ";
            // line 19
            if (((isset($context["show_top_margin_sm"]) ? $context["show_top_margin_sm"] : null) == true)) {
                echo "top_margin_sm";
            }
            echo "\" >
\t\t";
            // line 20
            if ( !twig_test_empty((isset($context["glyphicon"]) ? $context["glyphicon"] : null))) {
                // line 21
                echo "\t\t\t<span class=\"glyphicon ";
                echo (isset($context["glyphicon"]) ? $context["glyphicon"] : null);
                echo "\" class=\"text-center\"></span>&nbsp;
\t\t";
            }
            // line 23
            echo "\t\t";
            echo (isset($context["header_title"]) ? $context["header_title"] : null);
            echo "
\t</div>
\t<div class=\"block_content\"> ";
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    // line 29
    public function gettable_block_end($__end_marker__ = null, ...$__varargs__)
    {
        $context = $this->env->mergeGlobals(array(
            "end_marker" => $__end_marker__,
            "varargs" => $__varargs__,
        ));

        $blocks = array();

        ob_start();
        try {
            // line 30
            echo "\t</div>
</section><!-- ";
            // line 31
            echo (isset($context["end_marker"]) ? $context["end_marker"] : null);
            echo " -->
";
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    public function getTemplateName()
    {
        return "white-simple/lib/library.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  171 => 31,  168 => 30,  156 => 29,  141 => 23,  135 => 21,  133 => 20,  127 => 19,  124 => 18,  110 => 17,  96 => 12,  84 => 11,  71 => 8,  65 => 6,  59 => 4,  56 => 3,  54 => 2,  39 => 1,  35 => 32,  31 => 27,  29 => 26,  24 => 14,  21 => 10,  19 => 9,);
    }
}
/* {% macro header_block_start(header_title, row_class, glyphicon, show_top_margin_sm) %}*/
/* 	<div class="block_title" > {# {% if show_top_margin_sm == true %}big_top_EEEEEmargin{% endif %} #}*/
/* 		{% if (glyphicon is not empty) %}*/
/* 			<span class="glyphicon {{ glyphicon }}" class="text-center"></span>&nbsp;*/
/* 		{% endif %}*/
/* 		{{ header_title }}*/
/* 	</div>*/
/*     <section class="block_content {{ row_class }} block_padding_sm">*/
/* {% endmacro %} {# header_block_start #}*/
/* */
/* {% macro header_block_end(end_marker) %}*/
/* 	</section> <!-- {{ end_marker }} -->*/
/* {% endmacro %}*/
/* */
/* */
/* */
/* {% macro table_block_start(header_title, glyphicon, show_top_margin_sm) %}*/
/* <section class="table_wrapper  " >*/
/* 	<div class="block_title {% if show_top_margin_sm == true %}top_margin_sm{% endif %}" >*/
/* 		{% if (glyphicon is not empty) %}*/
/* 			<span class="glyphicon {{ glyphicon }}" class="text-center"></span>&nbsp;*/
/* 		{% endif %}*/
/* 		{{ header_title }}*/
/* 	</div>*/
/* 	<div class="block_content"> {#row #}*/
/* {% endmacro %} {# table_block_start #}*/
/* */
/* */
/* {% macro table_block_end(end_marker) %}*/
/* 	</div>*/
/* </section><!-- {{ end_marker }} -->*/
/* {% endmacro %} {# table_block_end #}*/
/* */
