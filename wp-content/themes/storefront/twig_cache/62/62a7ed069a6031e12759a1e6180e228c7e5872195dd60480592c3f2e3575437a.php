<?php

/* woo_ext_search_form.twig */
class __TwigTemplate_1b7de65fc0be18c2d97b7f499436fa1a7c558fe9a824b5a233f05f2f6e30784d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.twig", "woo_ext_search_form.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "\t";
        // line 4
        echo "
\t<script type=\"text/javascript\">
\t\t/*<![CDATA[*/
\t\tjQuery(document).ready(function (\$) {
\t\t\tappInit();
//        showHostelLongText()
//        showTourLongText()
//        showBlogArticleLongText()
\t\t\tradio_grouponChanged( \$(\"#radio_search_state_region\") )
\t\t});

\t\t//span_show_blog_article_title_

\t\tfunction showBlogArticleLongText() {
\t\t\tvar BlogArticlesList = ";
        // line 18
        echo twig_jsonencode_filter((isset($context["BlogArticlesList"]) ? $context["BlogArticlesList"] : null));
        echo ";
\t\t\tfor(i= 0; i< BlogArticlesList.length; i++) {
\t\t\t\tvar blog_article_id= BlogArticlesList[i].id
//            alert( \"blog_article_id::\"+blog_article_id+\"  BlogArticlesList[i]::\"+var_dump(BlogArticlesList[i]) )
\t\t\t\tvar div_el = \$('#div_show_article_title_'+blog_article_id);
//            alert( BlogArticlesList[i].id + \"  \"+BlogArticlesList[i].title+ \"  div_el[0].scrollHeight::\"+var_dump(div_el[0].scrollHeight) + \"  div_el.height()::\"+div_el.height() +\"   ??::\" + \$(\"#span_show_name_\"+blog_article_id).css(\"display\"))
//            alert( BlogArticlesList[i].id + \"  \"+BlogArticlesList[i].title+ \"  div_el.css('lineHeight')::\"+parseInt(div_el.css('lineHeight')) + \"  div_el.height()::\"+div_el.height() +\"   ??::\" + \$(\"#span_show_name_\"+blog_article_id).css(\"display\"))
\t\t\t\tif(div_el[0].scrollHeight > ( div_el.height()+10) ) {

//            alert( \"div_el[0].scrollHeight::\"+var_dump(div_el[0].scrollHeight) + \"  div_el.css('lineHeight')::\" + div_el.css('lineHeight'))
//            if(div_el[0].scrollHeight > parseInt(div_el.css('lineHeight'))) {
//            if( parseInt(div_el.css('lineHeight')) > ( div_el.height()+10) ) {
\t\t\t\t\t\$(\"#span_show_blog_article_title_\"+blog_article_id).css(\"display\", \"inline-block\")
//                alert( \"ToLong::\"+BlogArticlesList[i].id+\"    \"+var_dump(BlogArticlesList[i].title) )
\t\t\t\t}
\t\t\t}
\t\t} // function showBlogArticleLongText() {

\t\tfunction showHostelLongText() {
\t\t\tvar HostelsList = ";
        // line 37
        echo twig_jsonencode_filter((isset($context["HostelsList"]) ? $context["HostelsList"] : null));
        echo ";
\t\t\tfor(i= 0; i< HostelsList.length; i++) {
\t\t\t\tvar hostel_id= HostelsList[i].id
//            alert( \"hostel_id::\"+hostel_id+\"  HostelsList[i]::\"+var_dump(HostelsList[i]) )
\t\t\t\tvar div_el = \$('#div_show_name_'+hostel_id);
//            alert( HostelsList[i].id + \"  \"+HostelsList[i].name+ \"  div_el[0].scrollHeight::\"+var_dump(div_el[0].scrollHeight) + \"  div_el.height()::\"+div_el.height() +\"   ??::\" + \$(\"#span_show_name_\"+hostel_id).css(\"display\"))
//            alert( HostelsList[i].id + \"  \"+HostelsList[i].name+ \"  div_el.css('lineHeight')::\"+parseInt(div_el.css('lineHeight')) + \"  div_el.height()::\"+div_el.height() +\"   ??::\" + \$(\"#span_show_name_\"+hostel_id).css(\"display\"))
\t\t\t\tif(div_el[0].scrollHeight > ( div_el.height()+10) ) {

//            alert( \"div_el[0].scrollHeight::\"+var_dump(div_el[0].scrollHeight) + \"  div_el.css('lineHeight')::\" + div_el.css('lineHeight'))
//            if(div_el[0].scrollHeight > parseInt(div_el.css('lineHeight'))) {
//            if( parseInt(div_el.css('lineHeight')) > ( div_el.height()+10) ) {
\t\t\t\t\t\$(\"#span_show_name_\"+hostel_id).css(\"display\", \"inline-block\")
//                alert( \"ToLong::\"+HostelsList[i].id+\"    \"+var_dump(HostelsList[i].name) )
\t\t\t\t}

\t\t\t\tvar div_el = \$('#div_show_state_name_'+hostel_id);
//            alert( HostelsList[i].id + \"  \"+HostelsList[i].name+ \"  div_el[0].scrollHeight::\"+var_dump(div_el[0].scrollHeight) + \"  div_el.height()::\"+div_el.height() +\"   ??::\" + \$(\"#span_show_name_\"+hostel_id).css(\"display\"))
//            alert( HostelsList[i].id + \"  \"+HostelsList[i].name+ \"  div_el.css('lineHeight')::\"+parseInt(div_el.css('lineHeight')) + \"  div_el.height()::\"+div_el.height() +\"   ??::\" + \$(\"#span_show_name_\"+hostel_id).css(\"display\"))
\t\t\t\tif(div_el[0].scrollHeight > ( div_el.height()+10) ) {

//            alert( \"div_el[0].scrollHeight::\"+var_dump(div_el[0].scrollHeight) + \"  div_el.css('lineHeight')::\" + div_el.css('lineHeight'))
//            if(div_el[0].scrollHeight > parseInt(div_el.css('lineHeight'))) {
//            if( parseInt(div_el.css('lineHeight')) > ( div_el.height()+10) ) {
\t\t\t\t\t\$(\"#span_show_state_name_\"+hostel_id).css(\"display\", \"inline-block\")
//                alert( \"ToLong::\"+HostelsList[i].id+\"    \"+var_dump(HostelsList[i].name) )
\t\t\t\t}
\t\t\t}
\t\t} // function showHostelLongText() {



\t\tfunction showTourLongText() {
\t\t\tvar ToursList = ";
        // line 70
        echo twig_jsonencode_filter((isset($context["ToursList"]) ? $context["ToursList"] : null));
        echo ";
\t\t\tfor(i= 0; i< ToursList.length; i++) {
\t\t\t\tvar hostel_id= ToursList[i].id
//            alert( \"hostel_id::\"+hostel_id+\"  ToursList[i]::\"+var_dump(ToursList[i]) )
\t\t\t\tvar div_el = \$('#div_show_name_'+hostel_id);
//            alert( ToursList[i].id + \"  \"+ToursList[i].name+ \"  div_el[0].scrollHeight::\"+var_dump(div_el[0].scrollHeight) + \"  div_el.height()::\"+div_el.height() +\"   ??::\" + \$(\"#span_show_name_\"+hostel_id).css(\"display\"))
//            alert( ToursList[i].id + \"  \"+ToursList[i].name+ \"  div_el.css('lineHeight')::\"+parseInt(div_el.css('lineHeight')) + \"  div_el.height()::\"+div_el.height() +\"   ??::\" + \$(\"#span_show_name_\"+hostel_id).css(\"display\"))
\t\t\t\tif(div_el[0].scrollHeight > ( div_el.height()+10) ) {

//            alert( \"div_el[0].scrollHeight::\"+var_dump(div_el[0].scrollHeight) + \"  div_el.css('lineHeight')::\" + div_el.css('lineHeight'))
//            if(div_el[0].scrollHeight > parseInt(div_el.css('lineHeight'))) {
//            if( parseInt(div_el.css('lineHeight')) > ( div_el.height()+10) ) {
\t\t\t\t\t\$(\"#span_show_name_\"+hostel_id).css(\"display\", \"inline-block\")
//                alert( \"ToLong::\"+ToursList[i].id+\"    \"+var_dump(ToursList[i].name) )
\t\t\t\t}

\t\t\t\tvar div_el = \$('#div_show_state_name_'+hostel_id);
//            alert( ToursList[i].id + \"  \"+ToursList[i].name+ \"  div_el[0].scrollHeight::\"+var_dump(div_el[0].scrollHeight) + \"  div_el.height()::\"+div_el.height() +\"   ??::\" + \$(\"#span_show_name_\"+hostel_id).css(\"display\"))
//            alert( ToursList[i].id + \"  \"+ToursList[i].name+ \"  div_el.css('lineHeight')::\"+parseInt(div_el.css('lineHeight')) + \"  div_el.height()::\"+div_el.height() +\"   ??::\" + \$(\"#span_show_name_\"+hostel_id).css(\"display\"))
\t\t\t\tif(div_el[0].scrollHeight > ( div_el.height()+10) ) {

//            alert( \"div_el[0].scrollHeight::\"+var_dump(div_el[0].scrollHeight) + \"  div_el.css('lineHeight')::\" + div_el.css('lineHeight'))
//            if(div_el[0].scrollHeight > parseInt(div_el.css('lineHeight'))) {
//            if( parseInt(div_el.css('lineHeight')) > ( div_el.height()+10) ) {
\t\t\t\t\t\$(\"#span_show_state_name_\"+hostel_id).css(\"display\", \"inline-block\")
//                alert( \"ToLong::\"+ToursList[i].id+\"    \"+var_dump(ToursList[i].name) )
\t\t\t\t}
\t\t\t}


\t\t} // function showTourLongText() {




\t\tfunction onChange_select_search_state() {
\t\t\tvar state_id = \$(\"#select_search_state option:selected\").val()
\t\t\tvar HRef = \"";
        // line 107
        echo (isset($context["base_url"]) ? $context["base_url"] : null);
        echo "main/get_regions_list_by_state_id/state-id/\" + state_id + \"/format/html\";
\t\t\t// alert( \"HRef::\" + HRef )
\t\t\t\$.getJSON(HRef, function (data) {
\t\t\t\t// alert( \"data::\" + var_dump(data) )
\t\t\t\tif (data.ErrorCode == 0) {
\t\t\t\t\t\$(\"#select_search_region\").html(data.html);
\t\t\t\t\t//\$(\"#select_search_region\").selectpicker('refresh');
\t\t\t\t}
\t\t\t});
\t\t}



\t\tfunction ActiveToursSearchOnClick() {
\t\t\tvar filter_checked_category_selection = ''
\t\t\t\$('input.cbx_category_selection_filters:checked').each(function () {
\t\t\t\tfilter_checked_category_selection = filter_checked_category_selection + \$(this).val() + ','
\t\t\t});
\t\t\tvar HRef = \"";
        // line 125
        echo (isset($context["base_url"]) ? $context["base_url"] : null);
        echo "tours/search_tours_by_state/category-id/\" + encodeURIComponent(filter_checked_category_selection)
\t\t\tdocument.location = HRef
\t\t}


\t\tfunction SearchOnToursClick() {
\t\t\tvar tour_input_search_state= \$.trim( \$(\"#tour_input_search_state\").val() )
\t\t\tif ( tour_input_search_state == \"\" ) {
\t\t\t\talert(\"Enter State !\")
\t\t\t\t\$(\"#tour_input_search_state\").focus()
\t\t\t\treturn;
\t\t\t}
\t\t\tvar HRef = \"";
        // line 137
        echo (isset($context["base_url"]) ? $context["base_url"] : null);
        echo "tours/search_tours_by_state/state/\" + tour_input_search_state
\t\t\tdocument.location = HRef
\t\t\treturn;
\t\t}

\t\tfunction SearchOnHostelsClick() {
\t\t\tif (document.getElementById(\"radio_search_state\").checked) {
\t\t\t\tvar input_search_state = \$(\"#input_search_state\").val()
\t\t\t\tif (input_search_state == \"\" || input_search_state == \"Search for a location or a Hostel\") {
\t\t\t\t\talert(\"Enter State, City or Region !\")
\t\t\t\t\t\$(\"#input_search_state\").focus()
\t\t\t\t\treturn;
\t\t\t\t}
\t\t\t\tdocument.location = \"";
        // line 150
        echo (isset($context["base_url"]) ? $context["base_url"] : null);
        echo "hostels/search_hostels_by_state/state/\" + \$(\"#input_search_state\").val()
\t\t\t\treturn;
\t\t\t}
//   alert( \"-1  document.getElementById(radio_search_state_region).checked::\" + document.getElementById(\"radio_search_state_region\").checked )
\t\t\tif (document.getElementById(\"radio_search_state_region\").checked) {
\t\t\t\tvar state_id = \$(\"#select_search_state option:selected\").val()
\t\t\t\tvar region_id = \$(\"#select_search_region option:selected\").val()
\t\t\t\tif (state_id == '' || region_id == '') {
\t\t\t\t\talert(\"Select state and region !\")
\t\t\t\t\t\$(\"#select_search_state\").focus()
\t\t\t\t\treturn;
\t\t\t\t}
\t\t\t\tvar HRef = \"";
        // line 162
        echo (isset($context["base_url"]) ? $context["base_url"] : null);
        echo "hostels/get_state_region_slugs/state-id/\" + state_id + \"/region-id/\" + region_id;
\t\t\t\t\$.getJSON(HRef, function (data) {
\t\t\t\t\tif (data.ErrorCode == 0) {
\t\t\t\t\t\tvar HRef = \"";
        // line 165
        echo (isset($context["base_url"]) ? $context["base_url"] : null);
        echo "hostels/search_hostels_by_region\" + '/state-name/' + data.state_slug + \"/state-id/\" + state_id + '/region-name/' + data.region_slug + \"/region-id/\" + region_id
\t\t\t\t\t\t//alert( \"-1 ::\" + HRef)
\t\t\t\t\t\tdocument.location = HRef
\t\t\t\t\t}
\t\t\t\t});
\t\t\t}
\t\t}
\t\t/*]]>*/
\t</script>

\t<div class=\"row\">  <!-- Breadcrumbs row start -->
\t\t";
        // line 176
        echo $this->getAttribute((isset($context["components"]) ? $context["components"] : null), "showBreadcrumbs", array(0 => (isset($context["breadcrumbsList"]) ? $context["breadcrumbsList"] : null), 1 => "div_breadcrumbs"), "method");
        echo "
\t</div>              <!-- Breadcrumbs row end -->

\t<div class=\"row\"> <!-- search by State page start -->

\t\t<div class=\"col-xs-12 col-sm-4 block_left_separator_sm\">
\t\t\t";
        // line 182
        $context["show_top_margin_sm"] = true;
        // line 183
        echo "\t\t\t";
        $this->loadTemplate("/plain/search_box.twig", "woo_ext_search_form.twig", 183)->display($context);
        // line 184
        echo "\t\t\t";
        $this->loadTemplate("/plain/subscribe to_newsletter.twig", "woo_ext_search_form.twig", 184)->display($context);
        // line 185
        echo "\t\t\t";
        $this->loadTemplate("/plain/banners_list.twig", "woo_ext_search_form.twig", 185)->display($context);
        // line 186
        echo "\t\t\t";
        $this->loadTemplate("/plain/votes_list.twig", "woo_ext_search_form.twig", 186)->display($context);
        // line 187
        echo "\t\t</div>


\t\t<div class=\"col-xs-12 col-sm-8 block_right_separator_sm\">
\t\t\t<div class=\"row\">
\t\t\t\t";
        // line 192
        $this->loadTemplate("/plain/spotlight_hostels.twig", "woo_ext_search_form.twig", 192)->display($context);
        // line 193
        echo "\t\t\t</div>
\t\t\t<div class=\"row\">
\t\t\t\t";
        // line 195
        $this->loadTemplate("/plain/spotlight_tours.twig", "woo_ext_search_form.twig", 195)->display($context);
        // line 196
        echo "\t\t\t</div>
\t\t\t<div class=\"row\">
\t\t\t\t";
        // line 198
        if ((isset($context["RESOLUTION_DEBUG"]) ? $context["RESOLUTION_DEBUG"] : null)) {
            // line 199
            echo "\t\t\t\t\t<span >";
            echo $this->getAttribute((isset($context["components"]) ? $context["components"] : null), "set_page_anchors", array(0 => false, 1 => true, 2 => false), "method");
            echo "</span>
\t\t\t\t\t<span >";
            // line 200
            echo $this->getAttribute((isset($context["components"]) ? $context["components"] : null), "set_page_anchors_links", array(0 => true, 1 => false, 2 => true), "method");
            echo "</span>
\t\t\t\t";
        }
        // line 202
        echo "\t\t\t\t";
        $this->loadTemplate("/plain/blog_entries.twig", "woo_ext_search_form.twig", 202)->display($context);
        // line 203
        echo "\t\t\t</div>
\t\t</div>

\t</div> <!-- <div class=\"row\"> --> <!-- search by State page end -->


";
    }

    public function getTemplateName()
    {
        return "woo_ext_search_form.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  291 => 203,  288 => 202,  283 => 200,  278 => 199,  276 => 198,  272 => 196,  270 => 195,  266 => 193,  264 => 192,  257 => 187,  254 => 186,  251 => 185,  248 => 184,  245 => 183,  243 => 182,  234 => 176,  220 => 165,  214 => 162,  199 => 150,  183 => 137,  168 => 125,  147 => 107,  107 => 70,  71 => 37,  49 => 18,  33 => 4,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends "base.twig" %}*/
/* {% block content %}*/
/* 	{# library.twig woo_ext_search_form  #}*/
/* */
/* 	<script type="text/javascript">*/
/* 		/*<![CDATA[*//* */
/* 		jQuery(document).ready(function ($) {*/
/* 			appInit();*/
/* //        showHostelLongText()*/
/* //        showTourLongText()*/
/* //        showBlogArticleLongText()*/
/* 			radio_grouponChanged( $("#radio_search_state_region") )*/
/* 		});*/
/* */
/* 		//span_show_blog_article_title_*/
/* */
/* 		function showBlogArticleLongText() {*/
/* 			var BlogArticlesList = {{ BlogArticlesList|json_encode|raw }};*/
/* 			for(i= 0; i< BlogArticlesList.length; i++) {*/
/* 				var blog_article_id= BlogArticlesList[i].id*/
/* //            alert( "blog_article_id::"+blog_article_id+"  BlogArticlesList[i]::"+var_dump(BlogArticlesList[i]) )*/
/* 				var div_el = $('#div_show_article_title_'+blog_article_id);*/
/* //            alert( BlogArticlesList[i].id + "  "+BlogArticlesList[i].title+ "  div_el[0].scrollHeight::"+var_dump(div_el[0].scrollHeight) + "  div_el.height()::"+div_el.height() +"   ??::" + $("#span_show_name_"+blog_article_id).css("display"))*/
/* //            alert( BlogArticlesList[i].id + "  "+BlogArticlesList[i].title+ "  div_el.css('lineHeight')::"+parseInt(div_el.css('lineHeight')) + "  div_el.height()::"+div_el.height() +"   ??::" + $("#span_show_name_"+blog_article_id).css("display"))*/
/* 				if(div_el[0].scrollHeight > ( div_el.height()+10) ) {*/
/* */
/* //            alert( "div_el[0].scrollHeight::"+var_dump(div_el[0].scrollHeight) + "  div_el.css('lineHeight')::" + div_el.css('lineHeight'))*/
/* //            if(div_el[0].scrollHeight > parseInt(div_el.css('lineHeight'))) {*/
/* //            if( parseInt(div_el.css('lineHeight')) > ( div_el.height()+10) ) {*/
/* 					$("#span_show_blog_article_title_"+blog_article_id).css("display", "inline-block")*/
/* //                alert( "ToLong::"+BlogArticlesList[i].id+"    "+var_dump(BlogArticlesList[i].title) )*/
/* 				}*/
/* 			}*/
/* 		} // function showBlogArticleLongText() {*/
/* */
/* 		function showHostelLongText() {*/
/* 			var HostelsList = {{ HostelsList|json_encode|raw }};*/
/* 			for(i= 0; i< HostelsList.length; i++) {*/
/* 				var hostel_id= HostelsList[i].id*/
/* //            alert( "hostel_id::"+hostel_id+"  HostelsList[i]::"+var_dump(HostelsList[i]) )*/
/* 				var div_el = $('#div_show_name_'+hostel_id);*/
/* //            alert( HostelsList[i].id + "  "+HostelsList[i].name+ "  div_el[0].scrollHeight::"+var_dump(div_el[0].scrollHeight) + "  div_el.height()::"+div_el.height() +"   ??::" + $("#span_show_name_"+hostel_id).css("display"))*/
/* //            alert( HostelsList[i].id + "  "+HostelsList[i].name+ "  div_el.css('lineHeight')::"+parseInt(div_el.css('lineHeight')) + "  div_el.height()::"+div_el.height() +"   ??::" + $("#span_show_name_"+hostel_id).css("display"))*/
/* 				if(div_el[0].scrollHeight > ( div_el.height()+10) ) {*/
/* */
/* //            alert( "div_el[0].scrollHeight::"+var_dump(div_el[0].scrollHeight) + "  div_el.css('lineHeight')::" + div_el.css('lineHeight'))*/
/* //            if(div_el[0].scrollHeight > parseInt(div_el.css('lineHeight'))) {*/
/* //            if( parseInt(div_el.css('lineHeight')) > ( div_el.height()+10) ) {*/
/* 					$("#span_show_name_"+hostel_id).css("display", "inline-block")*/
/* //                alert( "ToLong::"+HostelsList[i].id+"    "+var_dump(HostelsList[i].name) )*/
/* 				}*/
/* */
/* 				var div_el = $('#div_show_state_name_'+hostel_id);*/
/* //            alert( HostelsList[i].id + "  "+HostelsList[i].name+ "  div_el[0].scrollHeight::"+var_dump(div_el[0].scrollHeight) + "  div_el.height()::"+div_el.height() +"   ??::" + $("#span_show_name_"+hostel_id).css("display"))*/
/* //            alert( HostelsList[i].id + "  "+HostelsList[i].name+ "  div_el.css('lineHeight')::"+parseInt(div_el.css('lineHeight')) + "  div_el.height()::"+div_el.height() +"   ??::" + $("#span_show_name_"+hostel_id).css("display"))*/
/* 				if(div_el[0].scrollHeight > ( div_el.height()+10) ) {*/
/* */
/* //            alert( "div_el[0].scrollHeight::"+var_dump(div_el[0].scrollHeight) + "  div_el.css('lineHeight')::" + div_el.css('lineHeight'))*/
/* //            if(div_el[0].scrollHeight > parseInt(div_el.css('lineHeight'))) {*/
/* //            if( parseInt(div_el.css('lineHeight')) > ( div_el.height()+10) ) {*/
/* 					$("#span_show_state_name_"+hostel_id).css("display", "inline-block")*/
/* //                alert( "ToLong::"+HostelsList[i].id+"    "+var_dump(HostelsList[i].name) )*/
/* 				}*/
/* 			}*/
/* 		} // function showHostelLongText() {*/
/* */
/* */
/* */
/* 		function showTourLongText() {*/
/* 			var ToursList = {{ ToursList|json_encode|raw }};*/
/* 			for(i= 0; i< ToursList.length; i++) {*/
/* 				var hostel_id= ToursList[i].id*/
/* //            alert( "hostel_id::"+hostel_id+"  ToursList[i]::"+var_dump(ToursList[i]) )*/
/* 				var div_el = $('#div_show_name_'+hostel_id);*/
/* //            alert( ToursList[i].id + "  "+ToursList[i].name+ "  div_el[0].scrollHeight::"+var_dump(div_el[0].scrollHeight) + "  div_el.height()::"+div_el.height() +"   ??::" + $("#span_show_name_"+hostel_id).css("display"))*/
/* //            alert( ToursList[i].id + "  "+ToursList[i].name+ "  div_el.css('lineHeight')::"+parseInt(div_el.css('lineHeight')) + "  div_el.height()::"+div_el.height() +"   ??::" + $("#span_show_name_"+hostel_id).css("display"))*/
/* 				if(div_el[0].scrollHeight > ( div_el.height()+10) ) {*/
/* */
/* //            alert( "div_el[0].scrollHeight::"+var_dump(div_el[0].scrollHeight) + "  div_el.css('lineHeight')::" + div_el.css('lineHeight'))*/
/* //            if(div_el[0].scrollHeight > parseInt(div_el.css('lineHeight'))) {*/
/* //            if( parseInt(div_el.css('lineHeight')) > ( div_el.height()+10) ) {*/
/* 					$("#span_show_name_"+hostel_id).css("display", "inline-block")*/
/* //                alert( "ToLong::"+ToursList[i].id+"    "+var_dump(ToursList[i].name) )*/
/* 				}*/
/* */
/* 				var div_el = $('#div_show_state_name_'+hostel_id);*/
/* //            alert( ToursList[i].id + "  "+ToursList[i].name+ "  div_el[0].scrollHeight::"+var_dump(div_el[0].scrollHeight) + "  div_el.height()::"+div_el.height() +"   ??::" + $("#span_show_name_"+hostel_id).css("display"))*/
/* //            alert( ToursList[i].id + "  "+ToursList[i].name+ "  div_el.css('lineHeight')::"+parseInt(div_el.css('lineHeight')) + "  div_el.height()::"+div_el.height() +"   ??::" + $("#span_show_name_"+hostel_id).css("display"))*/
/* 				if(div_el[0].scrollHeight > ( div_el.height()+10) ) {*/
/* */
/* //            alert( "div_el[0].scrollHeight::"+var_dump(div_el[0].scrollHeight) + "  div_el.css('lineHeight')::" + div_el.css('lineHeight'))*/
/* //            if(div_el[0].scrollHeight > parseInt(div_el.css('lineHeight'))) {*/
/* //            if( parseInt(div_el.css('lineHeight')) > ( div_el.height()+10) ) {*/
/* 					$("#span_show_state_name_"+hostel_id).css("display", "inline-block")*/
/* //                alert( "ToLong::"+ToursList[i].id+"    "+var_dump(ToursList[i].name) )*/
/* 				}*/
/* 			}*/
/* */
/* */
/* 		} // function showTourLongText() {*/
/* */
/* */
/* */
/* */
/* 		function onChange_select_search_state() {*/
/* 			var state_id = $("#select_search_state option:selected").val()*/
/* 			var HRef = "{{ base_url }}main/get_regions_list_by_state_id/state-id/" + state_id + "/format/html";*/
/* 			// alert( "HRef::" + HRef )*/
/* 			$.getJSON(HRef, function (data) {*/
/* 				// alert( "data::" + var_dump(data) )*/
/* 				if (data.ErrorCode == 0) {*/
/* 					$("#select_search_region").html(data.html);*/
/* 					//$("#select_search_region").selectpicker('refresh');*/
/* 				}*/
/* 			});*/
/* 		}*/
/* */
/* */
/* */
/* 		function ActiveToursSearchOnClick() {*/
/* 			var filter_checked_category_selection = ''*/
/* 			$('input.cbx_category_selection_filters:checked').each(function () {*/
/* 				filter_checked_category_selection = filter_checked_category_selection + $(this).val() + ','*/
/* 			});*/
/* 			var HRef = "{{ base_url }}tours/search_tours_by_state/category-id/" + encodeURIComponent(filter_checked_category_selection)*/
/* 			document.location = HRef*/
/* 		}*/
/* */
/* */
/* 		function SearchOnToursClick() {*/
/* 			var tour_input_search_state= $.trim( $("#tour_input_search_state").val() )*/
/* 			if ( tour_input_search_state == "" ) {*/
/* 				alert("Enter State !")*/
/* 				$("#tour_input_search_state").focus()*/
/* 				return;*/
/* 			}*/
/* 			var HRef = "{{ base_url }}tours/search_tours_by_state/state/" + tour_input_search_state*/
/* 			document.location = HRef*/
/* 			return;*/
/* 		}*/
/* */
/* 		function SearchOnHostelsClick() {*/
/* 			if (document.getElementById("radio_search_state").checked) {*/
/* 				var input_search_state = $("#input_search_state").val()*/
/* 				if (input_search_state == "" || input_search_state == "Search for a location or a Hostel") {*/
/* 					alert("Enter State, City or Region !")*/
/* 					$("#input_search_state").focus()*/
/* 					return;*/
/* 				}*/
/* 				document.location = "{{ base_url }}hostels/search_hostels_by_state/state/" + $("#input_search_state").val()*/
/* 				return;*/
/* 			}*/
/* //   alert( "-1  document.getElementById(radio_search_state_region).checked::" + document.getElementById("radio_search_state_region").checked )*/
/* 			if (document.getElementById("radio_search_state_region").checked) {*/
/* 				var state_id = $("#select_search_state option:selected").val()*/
/* 				var region_id = $("#select_search_region option:selected").val()*/
/* 				if (state_id == '' || region_id == '') {*/
/* 					alert("Select state and region !")*/
/* 					$("#select_search_state").focus()*/
/* 					return;*/
/* 				}*/
/* 				var HRef = "{{ base_url }}hostels/get_state_region_slugs/state-id/" + state_id + "/region-id/" + region_id;*/
/* 				$.getJSON(HRef, function (data) {*/
/* 					if (data.ErrorCode == 0) {*/
/* 						var HRef = "{{ base_url }}hostels/search_hostels_by_region" + '/state-name/' + data.state_slug + "/state-id/" + state_id + '/region-name/' + data.region_slug + "/region-id/" + region_id*/
/* 						//alert( "-1 ::" + HRef)*/
/* 						document.location = HRef*/
/* 					}*/
/* 				});*/
/* 			}*/
/* 		}*/
/* 		/*]]>*//* */
/* 	</script>*/
/* */
/* 	<div class="row">  <!-- Breadcrumbs row start -->*/
/* 		{{ components.showBreadcrumbs( breadcrumbsList, 'div_breadcrumbs' ) }}*/
/* 	</div>              <!-- Breadcrumbs row end -->*/
/* */
/* 	<div class="row"> <!-- search by State page start -->*/
/* */
/* 		<div class="col-xs-12 col-sm-4 block_left_separator_sm">*/
/* 			{%  set show_top_margin_sm = true %}*/
/* 			{% include '/plain/search_box.twig' %}*/
/* 			{% include '/plain/subscribe to_newsletter.twig' %}*/
/* 			{% include '/plain/banners_list.twig' %}*/
/* 			{% include '/plain/votes_list.twig' %}*/
/* 		</div>*/
/* */
/* */
/* 		<div class="col-xs-12 col-sm-8 block_right_separator_sm">*/
/* 			<div class="row">*/
/* 				{% include '/plain/spotlight_hostels.twig' %}*/
/* 			</div>*/
/* 			<div class="row">*/
/* 				{% include '/plain/spotlight_tours.twig' %}*/
/* 			</div>*/
/* 			<div class="row">*/
/* 				{% if RESOLUTION_DEBUG %}*/
/* 					<span >{{ components.set_page_anchors( false, true, false ) }}</span>*/
/* 					<span >{{ components.set_page_anchors_links( true, false , true ) }}</span>*/
/* 				{% endif %}*/
/* 				{% include '/plain/blog_entries.twig' %}*/
/* 			</div>*/
/* 		</div>*/
/* */
/* 	</div> <!-- <div class="row"> --> <!-- search by State page end -->*/
/* */
/* */
/* {% endblock content %}*/
