<?php

/* white-simple/filter_attrs_box.twig */
class __TwigTemplate_fd929e810e0a3f0e0b1c75a5bb64817cb16bf88d3b5575d81a55192bca4324dc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["attrs_options_list"]) ? $context["attrs_options_list"] : $this->getContext($context, "attrs_options_list")));
        foreach ($context['_seq'] as $context["_key"] => $context["attrs_option"]) {
            // line 2
            echo "
\t";
            // line 3
            echo $this->getAttribute((isset($context["library"]) ? $context["library"] : $this->getContext($context, "library")), "header_block_start", array(0 => ("Filter on " . $this->getAttribute($context["attrs_option"], "attr_name", array(), "array")), 1 => "", 2 => "fa fa-search-plus"), "method");
            echo "
\t<div class=\"row\">
\t\t";
            // line 5
            $context["attrs_list"] = $this->getAttribute($context["attrs_option"], "attrs_list", array(), "array");
            // line 6
            echo "\t\t";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["attrs_list"]) ? $context["attrs_list"] : $this->getContext($context, "attrs_list")));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["next_attr"]) {
                // line 7
                echo "\t\t\t<input type=\"checkbox\" id=\"next_attr_";
                echo $this->getAttribute($context["next_attr"], "ID", array());
                echo "\" value=\"";
                echo $this->getAttribute($context["next_attr"], "ID", array());
                echo "\" class=\"cbx_";
                echo $this->getAttribute($context["attrs_option"], "attr_name", array(), "array");
                echo "_selection_filters\" ";
                if ($this->getAttribute($context["loop"], "last", array())) {
                    echo "checked ";
                }
                echo ">&nbsp;";
                echo $this->getAttribute($context["next_attr"], "name", array());
                echo " <b>(";
                echo $this->getAttribute($context["next_attr"], "products_count", array());
                echo ")</b><br>
\t\t";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['next_attr'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 9
            echo "\t</div>
\t";
            // line 10
            echo $this->getAttribute((isset($context["library"]) ? $context["library"] : $this->getContext($context, "library")), "header_block_end", array(0 => (("Filter on " . $this->getAttribute($context["attrs_option"], "attr_name", array(), "array")) . " Block End")), "method");
            echo "

";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['attrs_option'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    public function getTemplateName()
    {
        return "white-simple/filter_attrs_box.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  83 => 10,  80 => 9,  51 => 7,  33 => 6,  31 => 5,  26 => 3,  23 => 2,  19 => 1,);
    }
}
/* {% for attrs_option in attrs_options_list %}*/
/* */
/* 	{{ library.header_block_start("Filter on " ~ attrs_option['attr_name'], "", "fa fa-search-plus") }}*/
/* 	<div class="row">*/
/* 		{% set attrs_list= attrs_option['attrs_list'] %}*/
/* 		{% for next_attr in attrs_list %}*/
/* 			<input type="checkbox" id="next_attr_{{ next_attr.ID }}" value="{{ next_attr.ID }}" class="cbx_{{ attrs_option['attr_name'] }}_selection_filters" {% if loop.last %}checked {%  endif %}>&nbsp;{{ next_attr.name }} <b>({{ next_attr.products_count }})</b><br>*/
/* 		{% endfor %}*/
/* 	</div>*/
/* 	{{ library.header_block_end( "Filter on " ~ attrs_option['attr_name'] ~ " Block End") }}*/
/* */
/* {% endfor %}*/
