<?php

/* base.twig */
class __TwigTemplate_4e1d687e30b34b30adc9f10afaa0cabf11a6ca4a2d3840c6256a0a2eb2237ce3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'header' => array($this, 'block_header'),
            'attached_media' => array($this, 'block_attached_media'),
            'content' => array($this, 'block_content'),
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["components"] = $this->loadTemplate("lib/library.twig", "base.twig", 1);
        // line 2
        echo "<!DOCTYPE html>
<html lang=\"en\">
<title>";
        // line 4
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
";
        // line 5
        $this->displayBlock('header', $context, $blocks);
        // line 152
        echo "
<div id=\"content\">";
        // line 153
        $this->displayBlock('content', $context, $blocks);
        echo "</div>

";
        // line 155
        $this->displayBlock('footer', $context, $blocks);
    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        if ( !twig_test_empty((isset($context["pageTitle"]) ? $context["pageTitle"] : null))) {
        }
        echo (isset($context["siteName"]) ? $context["siteName"] : null);
        echo " : ";
        echo (isset($context["pageTitle"]) ? $context["pageTitle"] : null);
    }

    // line 5
    public function block_header($context, array $blocks = array())
    {
        // line 6
        echo "
<head>
    ";
        // line 8
        $this->displayBlock('attached_media', $context, $blocks);
        // line 83
        echo "

</head>
<body>

<div class=\"container \">
    ";
        // line 89
        if ((isset($context["show_header_template"]) ? $context["show_header_template"] : null)) {
            // line 90
            echo "    <header>

        <section class=\"row centered\">   <!-- Top Menu of App Block Start -->
            <h1>© 2016 ";
            // line 93
            echo (isset($context["siteName"]) ? $context["siteName"] : null);
            echo ":&nbsp;";
            echo (isset($context["pageTitle"]) ? $context["pageTitle"] : null);
            echo "</h1>
            <div class=\"top_navigation\">

                <ul class=\"nav navbar-nav\">
                    <li class=\"top_margin_sm\" >
                        <span >";
            // line 98
            echo $context["components"]->getresponsive_helper();
            echo "</span>
                    </li>

                    <li class=\"dropdown-toggle\">
                        <img src=\"";
            // line 102
            echo (isset($context["base_url"]) ? $context["base_url"] : null);
            echo "static/images/";
            echo (isset($context["current_skin_name"]) ? $context["current_skin_name"] : null);
            echo "/logo_48.png\" class=\"img_logo\">
                    </li>
                    <li class=\" dropdown-toggle   ";
            // line 104
            if (((isset($context["admin_link"]) ? $context["admin_link"] : null) == "")) {
                echo "active";
            }
            echo "\"><a href=\"";
            echo (isset($context["base_url"]) ? $context["base_url"] : null);
            echo "/home\" >Home</a></li>

                    ";
            // line 106
            echo $context["components"]->getmenu_item("Calendar", "calendar", array(0 => array("url" => "calendar/calendar#calendar1", "title" => "Calendar 1"), 1 => array("url" => "calendar/calendar#calendar2", "title" => "Calendar 2"), 2 => array("url" => "calendar/calendar#calendar3", "title" => "Calendar 3")), (isset($context["base_url"]) ? $context["base_url"] : null));
            echo "

                    ";
            // line 108
            echo $context["components"]->getmenu_item("Hostels", "hostels", array(0 => array("url" => "hostels/search_hostels_by_state", "title" => "Hostels"), 1 => array("url" => "hostels/search_hostels_by_state/state/australian-capital-territory", "title" => "Hostels/Australian Capital Territory"), 2 => array("url" => "hostels/search_hostels_by_state/state/new-south-wales", "title" => "New South Wales"), 3 => array("url" => "/hostel/victoria/melbourne-region/inner-city/standard-hostel", "title" => "Hostel For test")), (isset($context["base_url"]) ? $context["base_url"] : null));
            echo "

                    ";
            // line 110
            if ((($this->getAttribute((isset($context["userdata"]) ? $context["userdata"] : null), "is_logged", array(), "any", true, true) && ($this->getAttribute((isset($context["userdata"]) ? $context["userdata"] : null), "is_logged", array()) == 1)) && ((isset($context["user_group"]) ? $context["user_group"] : null) == "Admin"))) {
                // line 111
                echo "                        ";
                // line 112
                echo "                        ";
                $context["logout_label"] = (((("Signed in as " . $this->getAttribute((isset($context["userdata"]) ? $context["userdata"] : null), "logged_name", array(), "array")) . "(") . $this->getAttribute((isset($context["userdata"]) ? $context["userdata"] : null), "user_groups_name", array(), "array")) . ")");
                // line 113
                echo "                        ";
                echo $context["components"]->getmenu_item((isset($context["logout_label"]) ? $context["logout_label"] : null), "profile", array(0 => array("url" => "myprofile/change_password", "title" => "Change Password"), 1 => array("url" => "myprofile/index", "title" => "My Profile"), 2 => array("url" => "admin/dashboard/index", "title" => "Dashboard"), 3 => array("url" => "main/logout", "title" => "  Logout"), 4 => array("url" => "main/test", "title" => "  Test"), 5 => array("url" => "main/cleared_test", "title" => "  Cleared Test")), (isset($context["base_url"]) ? $context["base_url"] : null));
                echo "
                    ";
                // line 115
                echo "                        <li class=\"dropdown-toggle\">
                            <a href=\"";
                // line 116
                echo (isset($context["base_url"]) ? $context["base_url"] : null);
                echo "login\" >Login</a>
                        </li>
                    ";
            }
            // line 119
            echo "
                    ";
            // line 121
            echo "\t                    ";
            // line 122
            echo "                        ";
            // line 123
            echo "                    ";
            // line 124
            echo "

                    ";
            // line 126
            if ((isset($context["RESOLUTION_DEBUG"]) ? $context["RESOLUTION_DEBUG"] : null)) {
                // line 127
                echo "                    <li>
                        <span >";
                // line 128
                echo $context["components"]->gettest_device();
                echo "</span>
                    </li>
                    ";
            }
            // line 131
            echo "
                    ";
            // line 132
            if ((isset($context["RESOLUTION_DEBUG"]) ? $context["RESOLUTION_DEBUG"] : null)) {
                // line 133
                echo "                    <li>
                        <span >";
                // line 134
                echo $context["components"]->getset_page_anchors_links(false, true, true);
                echo "</span>
                        <span >";
                // line 135
                echo $context["components"]->getset_page_anchors(true, false, false);
                echo "</span>
                    </li>
                    ";
            }
            // line 138
            echo "                    ";
            // line 139
            echo "                        ";
            // line 140
            echo "                    ";
            // line 141
            echo "
                </ul>

            </div>
        </section> <!-- <section class=\"row\"> --> <!-- Top Menu of App Block End -->
    </header>
    ";
        }
        // line 148
        echo "    <main>


    ";
    }

    // line 8
    public function block_attached_media($context, array $blocks = array())
    {
        // line 9
        echo "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    <!-- Bootstrap -->
    <meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" />

    ";
        // line 14
        echo "    ";
        // line 15
        echo "    ";
        // line 16
        echo "

    <script src=\"";
        // line 18
        echo (isset($context["base_url"]) ? $context["base_url"] : null);
        echo "static/js/jquery/jquery-3.1.0.min.js\"></script>
    <link rel=\"stylesheet\" href=\"";
        // line 19
        echo (isset($context["base_url"]) ? $context["base_url"] : null);
        echo "static/css/jquery-ui.min.css\"/>
    <script src=\"";
        // line 20
        echo (isset($context["base_url"]) ? $context["base_url"] : null);
        echo "static/js/jquery/jquery-ui-1.12.0.min.js\"></script>


    ";
        // line 24
        echo "    ";
        // line 25
        echo "    ";
        // line 26
        echo "

    <link rel=\"stylesheet\" href=\"";
        // line 28
        echo (isset($context["base_url"]) ? $context["base_url"] : null);
        echo "static/bootstrap/css/bootstrap.min.css\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 29
        echo (isset($context["base_url"]) ? $context["base_url"] : null);
        echo "static/bootstrap/css/blueimp-gallery.min.css\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 30
        echo (isset($context["base_url"]) ? $context["base_url"] : null);
        echo "static/css/chosen.css\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 31
        echo (isset($context["base_url"]) ? $context["base_url"] : null);
        echo "static/css/MonthPicker.css\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 32
        echo (isset($context["base_url"]) ? $context["base_url"] : null);
        echo "static/css/jquery-confirm.min.css\"/>

    <link rel=\"stylesheet\" href=\"";
        // line 34
        echo (isset($context["base_url"]) ? $context["base_url"] : null);
        echo "static/css/fontello/css/animation.css\"/>
    ";
        // line 36
        echo "
    ";
        // line 38
        echo "    ";
        // line 39
        echo "

    ";
        // line 42
        echo "
    ";
        // line 44
        echo "    <link rel=\"stylesheet/less\" type=\"text/css\" href=\"";
        echo (isset($context["base_url"]) ? $context["base_url"] : null);
        echo "static/less/";
        echo (isset($context["current_skin_name"]) ? $context["current_skin_name"] : null);
        echo "/styles.less\"/>

    ";
        // line 47
        echo "    <link rel=\"stylesheet/less\" type=\"text/css\" href=\"";
        echo (isset($context["base_url"]) ? $context["base_url"] : null);
        echo "static/less/";
        echo (isset($context["current_skin_name"]) ? $context["current_skin_name"] : null);
        echo "/style_xs_320.less\" media=\"only screen and (min-width: 320px) and (max-width: 479px) \" />

    ";
        // line 50
        echo "    <link rel=\"stylesheet/less\" type=\"text/css\" href=\"";
        echo (isset($context["base_url"]) ? $context["base_url"] : null);
        echo "static/less/";
        echo (isset($context["current_skin_name"]) ? $context["current_skin_name"] : null);
        echo "/style_xs_480.less\" media=\"only screen and (min-width: 480px)  and (max-width: 599px) \" />

    ";
        // line 53
        echo "    <link rel=\"stylesheet/less\" type=\"text/css\" href=\"";
        echo (isset($context["base_url"]) ? $context["base_url"] : null);
        echo "static/less/";
        echo (isset($context["current_skin_name"]) ? $context["current_skin_name"] : null);
        echo "/style_xs_600.less\" media=\"only screen and (min-width: 600px)  and (max-width: 767px) \" />

    ";
        // line 56
        echo "    <link rel=\"stylesheet/less\" type=\"text/css\" href=\"";
        echo (isset($context["base_url"]) ? $context["base_url"] : null);
        echo "static/less/";
        echo (isset($context["current_skin_name"]) ? $context["current_skin_name"] : null);
        echo "/style_sm.less\" media=\"only screen and (min-width: 768px)  and (max-width: 1023px) \" />

    ";
        // line 59
        echo "    <link rel=\"stylesheet/less\" type=\"text/css\" href=\"";
        echo (isset($context["base_url"]) ? $context["base_url"] : null);
        echo "static/less/";
        echo (isset($context["current_skin_name"]) ? $context["current_skin_name"] : null);
        echo "/style_md.less\" media=\"only screen and (min-width: 1024px) and (max-width: 1279px) \" />

    ";
        // line 62
        echo "    <link rel=\"stylesheet/less\" type=\"text/css\" href=\"";
        echo (isset($context["base_url"]) ? $context["base_url"] : null);
        echo "static/less/";
        echo (isset($context["current_skin_name"]) ? $context["current_skin_name"] : null);
        echo "/style_lg.less\" media=\"only screen and (min-width: 1280px)\" />


    <script type=\"text/javascript\" src=\"";
        // line 65
        echo (isset($context["base_url"]) ? $context["base_url"] : null);
        echo "static/js/less.js\"></script> ";
        // line 66
        echo "

    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 68
        echo (isset($context["base_url"]) ? $context["base_url"] : null);
        echo "static/css/font-awesome.min.css\" />

    <script type=\"text/javascript\" src=\"";
        // line 70
        echo (isset($context["base_url"]) ? $context["base_url"] : null);
        echo "static/bootstrap/js/bootstrap.min.js\"></script>
    <!-- <script type=\"text/javascript\" src=\"";
        // line 71
        echo (isset($context["base_url"]) ? $context["base_url"] : null);
        echo "static/bootstrap/js/bootstrap-image-gallery.min.js\"></script>  -->
    <script type=\"text/javascript\" src=\"";
        // line 72
        echo (isset($context["base_url"]) ? $context["base_url"] : null);
        echo "static/bootstrap/js/jquery.blueimp-gallery.min.js\"></script>

    <script type=\"text/javascript\" src=\"";
        // line 74
        echo (isset($context["base_url"]) ? $context["base_url"] : null);
        echo "static/js/jquery/chosen.jquery.js\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 75
        echo (isset($context["base_url"]) ? $context["base_url"] : null);
        echo "static/bootstrap/js/jquery.rcrumbs.min.js\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 76
        echo (isset($context["base_url"]) ? $context["base_url"] : null);
        echo "static/js/jquery/MonthPicker.js\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 77
        echo (isset($context["base_url"]) ? $context["base_url"] : null);
        echo "static/js/jquery/jquery-confirm.min.js\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 78
        echo (isset($context["base_url"]) ? $context["base_url"] : null);
        echo "static/js/jquery/jquery.cookie.js\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 79
        echo (isset($context["base_url"]) ? $context["base_url"] : null);
        echo "static/js/funcs.js\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 80
        echo (isset($context["base_url"]) ? $context["base_url"] : null);
        echo "static/js/";
        echo (isset($context["current_skin_name"]) ? $context["current_skin_name"] : null);
        echo "/app.js\"></script>

    ";
    }

    // line 153
    public function block_content($context, array $blocks = array())
    {
    }

    // line 155
    public function block_footer($context, array $blocks = array())
    {
        // line 156
        echo "    ";
        if ((isset($context["show_footer_template"]) ? $context["show_footer_template"] : null)) {
            // line 157
            echo "    ";
            $this->loadTemplate("/plain/footer.twig", "base.twig", 157)->display($context);
            // line 158
            echo "    ";
        }
    }

    public function getTemplateName()
    {
        return "base.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  415 => 158,  412 => 157,  409 => 156,  406 => 155,  401 => 153,  392 => 80,  388 => 79,  384 => 78,  380 => 77,  376 => 76,  372 => 75,  368 => 74,  363 => 72,  359 => 71,  355 => 70,  350 => 68,  346 => 66,  343 => 65,  334 => 62,  326 => 59,  318 => 56,  310 => 53,  302 => 50,  294 => 47,  286 => 44,  283 => 42,  279 => 39,  277 => 38,  274 => 36,  270 => 34,  265 => 32,  261 => 31,  257 => 30,  253 => 29,  249 => 28,  245 => 26,  243 => 25,  241 => 24,  235 => 20,  231 => 19,  227 => 18,  223 => 16,  221 => 15,  219 => 14,  213 => 9,  210 => 8,  203 => 148,  194 => 141,  192 => 140,  190 => 139,  188 => 138,  182 => 135,  178 => 134,  175 => 133,  173 => 132,  170 => 131,  164 => 128,  161 => 127,  159 => 126,  155 => 124,  153 => 123,  151 => 122,  149 => 121,  146 => 119,  140 => 116,  137 => 115,  132 => 113,  129 => 112,  127 => 111,  125 => 110,  120 => 108,  115 => 106,  106 => 104,  99 => 102,  92 => 98,  82 => 93,  77 => 90,  75 => 89,  67 => 83,  65 => 8,  61 => 6,  58 => 5,  48 => 4,  44 => 155,  39 => 153,  36 => 152,  34 => 5,  30 => 4,  26 => 2,  24 => 1,);
    }
}
/* {% import 'lib/library.twig' as components %}*/
/* <!DOCTYPE html>*/
/* <html lang="en">*/
/* <title>{% block title %}{% if pageTitle is not empty %}{% endif %}{{ siteName }} : {{ pageTitle }}{% endblock title %}</title>*/
/* {% block header %}*/
/* */
/* <head>*/
/*     {% block attached_media %}*/
/*     <meta name="viewport" content="width=device-width, initial-scale=1.0">*/
/*     <!-- Bootstrap -->*/
/*     <meta http-equiv="content-type" content="text/html; charset=utf-8" />*/
/* */
/*     {#{% if is_developer_comp is empty %}#}*/
/*     {#<link href='https://fonts.googleapis.com/css?family=Roboto:400,700,400italic,700italic' rel='stylesheet' type='text/css'>#}*/
/*     {#{% endif %}#}*/
/* */
/* */
/*     <script src="{{ base_url }}static/js/jquery/jquery-3.1.0.min.js"></script>*/
/*     <link rel="stylesheet" href="{{ base_url }}static/css/jquery-ui.min.css"/>*/
/*     <script src="{{ base_url }}static/js/jquery/jquery-ui-1.12.0.min.js"></script>*/
/* */
/* */
/*     {#<script src="{{ base_url }}static/js/jquery/jquery-1.11.1.min.js"></script>#}*/
/*     {#<link rel="stylesheet" href="{{ base_url }}static/css/jquery-ui.min.css"/>#}*/
/*     {#<script src="{{ base_url }}static/js/jquery/jquery-ui-1.11.1.min.js"></script>#}*/
/* */
/* */
/*     <link rel="stylesheet" href="{{ base_url }}static/bootstrap/css/bootstrap.min.css"/>*/
/*     <link rel="stylesheet" href="{{ base_url }}static/bootstrap/css/blueimp-gallery.min.css"/>*/
/*     <link rel="stylesheet" href="{{ base_url }}static/css/chosen.css"/>*/
/*     <link rel="stylesheet" href="{{ base_url }}static/css/MonthPicker.css"/>*/
/*     <link rel="stylesheet" href="{{ base_url }}static/css/jquery-confirm.min.css"/>*/
/* */
/*     <link rel="stylesheet" href="{{ base_url }}static/css/fontello/css/animation.css"/>*/
/*     {#<link rel="stylesheet" href="{{ base_url }}static/css/{{ current_skin_name }}/jquery.rcrumbs.min.css"/>#}*/
/* */
/*     {#<link rel="stylesheet" href="{{ base_url }}static/css/{{ current_skin_name }}/common_styles.css"/>#}*/
/*     {#<link rel="stylesheet" href="{{ base_url }}static/css/{{ current_skin_name }}/styles.css"/>#}*/
/* */
/* */
/*     {#iPhone portrait 320 x 480 #}*/
/* */
/*     {#<link rel="stylesheet/less" type="text/css" href="{{ base_url }}static/less/{{ current_skin_name }}/common_styles.less"/>#}*/
/*     <link rel="stylesheet/less" type="text/css" href="{{ base_url }}static/less/{{ current_skin_name }}/styles.less"/>*/
/* */
/*     {#iPhone portrait 320 x 480 #}*/
/*     <link rel="stylesheet/less" type="text/css" href="{{ base_url }}static/less/{{ current_skin_name }}/style_xs_320.less" media="only screen and (min-width: 320px) and (max-width: 479px) " />*/
/* */
/*     {#iPhone landscape 480 x 320#}*/
/*     <link rel="stylesheet/less" type="text/css" href="{{ base_url }}static/less/{{ current_skin_name }}/style_xs_480.less" media="only screen and (min-width: 480px)  and (max-width: 599px) " />*/
/* */
/*     {#Kindle portrait 600 x 1024#}*/
/*     <link rel="stylesheet/less" type="text/css" href="{{ base_url }}static/less/{{ current_skin_name }}/style_xs_600.less" media="only screen and (min-width: 600px)  and (max-width: 767px) " />*/
/* */
/*     {#iPad portrait 768 x 1024#}*/
/*     <link rel="stylesheet/less" type="text/css" href="{{ base_url }}static/less/{{ current_skin_name }}/style_sm.less" media="only screen and (min-width: 768px)  and (max-width: 1023px) " />*/
/* */
/*     {#iPad landscape 1024 x 768#}*/
/*     <link rel="stylesheet/less" type="text/css" href="{{ base_url }}static/less/{{ current_skin_name }}/style_md.less" media="only screen and (min-width: 1024px) and (max-width: 1279px) " />*/
/* */
/*     {#Macbook 1280 x 800#}*/
/*     <link rel="stylesheet/less" type="text/css" href="{{ base_url }}static/less/{{ current_skin_name }}/style_lg.less" media="only screen and (min-width: 1280px)" />*/
/* */
/* */
/*     <script type="text/javascript" src="{{ base_url }}static/js/less.js"></script> {#  MUST BE AT END OF LESS FILES  #}*/
/* */
/* */
/*     <link rel="stylesheet" type="text/css" href="{{ base_url }}static/css/font-awesome.min.css" />*/
/* */
/*     <script type="text/javascript" src="{{ base_url }}static/bootstrap/js/bootstrap.min.js"></script>*/
/*     <!-- <script type="text/javascript" src="{{ base_url }}static/bootstrap/js/bootstrap-image-gallery.min.js"></script>  -->*/
/*     <script type="text/javascript" src="{{ base_url }}static/bootstrap/js/jquery.blueimp-gallery.min.js"></script>*/
/* */
/*     <script type="text/javascript" src="{{ base_url }}static/js/jquery/chosen.jquery.js"></script>*/
/*     <script type="text/javascript" src="{{ base_url }}static/bootstrap/js/jquery.rcrumbs.min.js"></script>*/
/*     <script type="text/javascript" src="{{ base_url }}static/js/jquery/MonthPicker.js"></script>*/
/*     <script type="text/javascript" src="{{ base_url }}static/js/jquery/jquery-confirm.min.js"></script>*/
/*     <script type="text/javascript" src="{{ base_url }}static/js/jquery/jquery.cookie.js"></script>*/
/*     <script type="text/javascript" src="{{ base_url }}static/js/funcs.js"></script>*/
/*     <script type="text/javascript" src="{{ base_url }}static/js/{{ current_skin_name }}/app.js"></script>*/
/* */
/*     {% endblock attached_media %}*/
/* */
/* */
/* </head>*/
/* <body>*/
/* */
/* <div class="container ">*/
/*     {% if ( show_header_template ) %}*/
/*     <header>*/
/* */
/*         <section class="row centered">   <!-- Top Menu of App Block Start -->*/
/*             <h1>© 2016 {{ siteName }}:&nbsp;{{ pageTitle }}</h1>*/
/*             <div class="top_navigation">*/
/* */
/*                 <ul class="nav navbar-nav">*/
/*                     <li class="top_margin_sm" >*/
/*                         <span >{{ components.responsive_helper() }}</span>*/
/*                     </li>*/
/* */
/*                     <li class="dropdown-toggle">*/
/*                         <img src="{{ base_url }}static/images/{{ current_skin_name }}/logo_48.png" class="img_logo">*/
/*                     </li>*/
/*                     <li class=" dropdown-toggle   {%  if admin_link==''%}active{% endif %}"><a href="{{ base_url }}/home" >Home</a></li>*/
/* */
/*                     {{ components.menu_item( "Calendar", 'calendar', [{'url':'calendar/calendar#calendar1', 'title':'Calendar 1'}, {'url':'calendar/calendar#calendar2', 'title':'Calendar 2'}, {'url':'calendar/calendar#calendar3', 'title':'Calendar 3'} ], base_url  ) }}*/
/* */
/*                     {{ components.menu_item( "Hostels", 'hostels', [{'url':'hostels/search_hostels_by_state', 'title':'Hostels'}, {'url':'hostels/search_hostels_by_state/state/australian-capital-territory', 'title':'Hostels/Australian Capital Territory'}, {'url':'hostels/search_hostels_by_state/state/new-south-wales', 'title':'New South Wales'}, {'url':'/hostel/victoria/melbourne-region/inner-city/standard-hostel', 'title':'Hostel For test'} ], base_url  ) }}*/
/* */
/*                     {% if userdata.is_logged is defined and userdata.is_logged == 1 and user_group == "Admin" %}*/
/*                         {#{% set logout_label = "Signed in as <strong>" ~ userdata['logged_name'] ~ "</strong>(" ~ userdata['user_groups_name'] ~ ")" %}#}*/
/*                         {% set logout_label = "Signed in as " ~ userdata['logged_name'] ~ "(" ~ userdata['user_groups_name'] ~ ")" %}*/
/*                         {{ components.menu_item( logout_label, 'profile', [{'url':'myprofile/change_password', 'title':'Change Password'}, {'url':'myprofile/index', 'title':'My Profile'}, {'url':'admin/dashboard/index', 'title':'Dashboard'}, {'url':'main/logout', 'title':'  Logout'} , {'url':'main/test', 'title':'  Test'} , {'url':'main/cleared_test', 'title':'  Cleared Test'} ], base_url  ) }}*/
/*                     {#{% else %} UNCOMMENT#}*/
/*                         <li class="dropdown-toggle">*/
/*                             <a href="{{ base_url }}login" >Login</a>*/
/*                         </li>*/
/*                     {% endif %}*/
/* */
/*                     {#<li>#}*/
/* 	                    {#{{ components.menu_item( "Shop", 'shop', menu_good_groups_list, base_url  ) }}#}*/
/*                         {#<span >{{ components.test_device() }}</span>#}*/
/*                     {#</li>#}*/
/* */
/* */
/*                     {% if RESOLUTION_DEBUG %}*/
/*                     <li>*/
/*                         <span >{{ components.test_device() }}</span>*/
/*                     </li>*/
/*                     {% endif %}*/
/* */
/*                     {% if RESOLUTION_DEBUG %}*/
/*                     <li>*/
/*                         <span >{{ components.set_page_anchors_links( false, true, true ) }}</span>*/
/*                         <span >{{ components.set_page_anchors( true, false, false ) }}</span>*/
/*                     </li>*/
/*                     {% endif %}*/
/*                     {#<li>#}*/
/*                         {#&nbsp;<b>{{ pageTitle }}</b>#}*/
/*                     {#</li>#}*/
/* */
/*                 </ul>*/
/* */
/*             </div>*/
/*         </section> <!-- <section class="row"> --> <!-- Top Menu of App Block End -->*/
/*     </header>*/
/*     {% endif %}*/
/*     <main>*/
/* */
/* */
/*     {% endblock header %}*/
/* */
/* <div id="content">{% block content %}{% endblock content %}</div>*/
/* */
/* {% block footer %}*/
/*     {% if ( show_footer_template ) %}*/
/*     {% include '/plain/footer.twig' %}*/
/*     {% endif %}*/
/* {% endblock footer %}*/
