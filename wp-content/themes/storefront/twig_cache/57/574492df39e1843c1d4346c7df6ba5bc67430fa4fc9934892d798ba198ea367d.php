<?php

/* white-simple/woo_ext_search_form.twig */
class __TwigTemplate_9e083d5a31126fabc5c2f1bdb1f0383d9f9df57a719b9cdd06ecba1dcce4dc8a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("white-simple/base.twig", "white-simple/woo_ext_search_form.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "white-simple/base.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "\t";
        // line 4
        echo "
\t<script type=\"text/javascript\">
\t\t/*<![CDATA[*/
\t\tvar frontendFuncsObj;
\t\tjQuery(document).ready(function (\$) {

\t\t\tvar app_params = ";
        // line 10
        echo twig_jsonencode_filter((isset($context["app_params"]) ? $context["app_params"] : $this->getContext($context, "app_params")));
        echo ";
//\t\t\talert( \"app_params::\"+var_dump( app_params ) )
\t\t\tfrontendFuncsObj = new frontendFuncs( app_params );

\t\t\tvar categories_list = {}
\t\t\t\$('input.cbx_category_selection_filters:checked').each(function () {
\t\t\t\tcategories_list[categories_list.length-1] = \$(this).val()
\t\t\t});


\t\t\tvar brands_list = {}
\t\t\t\$('input.cbx_brand_selection_filters:checked').each(function () {
\t\t\t\tbrands_list[brands_list.length-1] = \$(this).val()
\t\t\t});


\t\t\tvar prices_list = {}
\t\t\t\$('input.cbx_price_selection_filters:checked').each(function () {
\t\t\t\tprices_list[prices_list.length-1] = \$(this).val()
\t\t\t});


\t\t\tfrontendFuncsObj.nsn_woo_ext_search_loadWooProducts( categories_list, brands_list, prices_list )

\t\t\t\$('[data-toggle=\"tooltip\"]').tooltip()
//\t\t\talert( \"AGTER::\"+var_dump(1) )
\t\t});

\t\t/*]]>*/
\t</script>


\t<div class=\"row\">  <!-- Breadcrumbs row start -->
\t\t";
        // line 44
        echo "\t</div>              <!-- Breadcrumbs row end -->

\t<div class=\"row\"> <!-- search by State page start -->

\t\t<div class=\"col-xs-12 col-sm-4 block_left_separator_sm\">
\t\t\t";
        // line 49
        $context["show_top_margin_sm"] = true;
        // line 50
        echo "\t\t\t";
        $this->loadTemplate("white-simple/filter_categories_box.twig", "white-simple/woo_ext_search_form.twig", 50)->display($context);
        // line 51
        echo "\t\t\t";
        $this->loadTemplate("white-simple/filter_attrs_box.twig", "white-simple/woo_ext_search_form.twig", 51)->display($context);
        echo " ";
        // line 52
        echo "\t\t\t";
        $this->loadTemplate("white-simple/filter_price_box.twig", "white-simple/woo_ext_search_form.twig", 52)->display($context);
        // line 53
        echo "\t\t\t";
        $this->loadTemplate("white-simple/filter_controls_box.twig", "white-simple/woo_ext_search_form.twig", 53)->display($context);
        // line 54
        echo "\t\t\t";
        // line 55
        echo "\t\t\t";
        // line 56
        echo "\t\t\t";
        // line 57
        echo "\t\t</div>


\t\t<div class=\"col-xs-12 col-sm-8 block_right_separator_sm\">
\t\t\t<div class=\"row\">
\t\t\t\t<div id=\"div_products_list\"></div>
\t\t\t\t";
        // line 64
        echo "\t\t\t</div>
\t\t\t<div class=\"row\">
\t\t\t\t";
        // line 67
        echo "\t\t\t</div>
\t\t\t<div class=\"row\">
\t\t\t\t";
        // line 70
        echo "\t\t\t</div>
\t\t</div>

\t</div> <!-- <div class=\"row\"> --> <!-- search by State page end -->


";
    }

    public function getTemplateName()
    {
        return "white-simple/woo_ext_search_form.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  121 => 70,  117 => 67,  113 => 64,  105 => 57,  103 => 56,  101 => 55,  99 => 54,  96 => 53,  93 => 52,  89 => 51,  86 => 50,  84 => 49,  77 => 44,  41 => 10,  33 => 4,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends "white-simple/base.twig" %}*/
/* {% block content %}*/
/* 	{# library.twig woo_ext_search_form  #}*/
/* */
/* 	<script type="text/javascript">*/
/* 		/*<![CDATA[*//* */
/* 		var frontendFuncsObj;*/
/* 		jQuery(document).ready(function ($) {*/
/* */
/* 			var app_params = {{ app_params|json_encode|raw }};*/
/* //			alert( "app_params::"+var_dump( app_params ) )*/
/* 			frontendFuncsObj = new frontendFuncs( app_params );*/
/* */
/* 			var categories_list = {}*/
/* 			$('input.cbx_category_selection_filters:checked').each(function () {*/
/* 				categories_list[categories_list.length-1] = $(this).val()*/
/* 			});*/
/* */
/* */
/* 			var brands_list = {}*/
/* 			$('input.cbx_brand_selection_filters:checked').each(function () {*/
/* 				brands_list[brands_list.length-1] = $(this).val()*/
/* 			});*/
/* */
/* */
/* 			var prices_list = {}*/
/* 			$('input.cbx_price_selection_filters:checked').each(function () {*/
/* 				prices_list[prices_list.length-1] = $(this).val()*/
/* 			});*/
/* */
/* */
/* 			frontendFuncsObj.nsn_woo_ext_search_loadWooProducts( categories_list, brands_list, prices_list )*/
/* */
/* 			$('[data-toggle="tooltip"]').tooltip()*/
/* //			alert( "AGTER::"+var_dump(1) )*/
/* 		});*/
/* */
/* 		/*]]>*//* */
/* 	</script>*/
/* */
/* */
/* 	<div class="row">  <!-- Breadcrumbs row start -->*/
/* 		{#{{ components.showBreadcrumbs( breadcrumbsList, 'div_breadcrumbs' ) }}#}*/
/* 	</div>              <!-- Breadcrumbs row end -->*/
/* */
/* 	<div class="row"> <!-- search by State page start -->*/
/* */
/* 		<div class="col-xs-12 col-sm-4 block_left_separator_sm">*/
/* 			{%  set show_top_margin_sm = true %}*/
/* 			{% include 'white-simple/filter_categories_box.twig' %}*/
/* 			{% include 'white-simple/filter_attrs_box.twig' %} {# $attrs_options_list#}*/
/* 			{% include 'white-simple/filter_price_box.twig' %}*/
/* 			{% include 'white-simple/filter_controls_box.twig' %}*/
/* 			{#{% include 'subscribe to_newsletter.twig' %}#}*/
/* 			{#{% include 'banners_list.twig' %}#}*/
/* 			{#{% include 'votes_list.twig' %}#}*/
/* 		</div>*/
/* */
/* */
/* 		<div class="col-xs-12 col-sm-8 block_right_separator_sm">*/
/* 			<div class="row">*/
/* 				<div id="div_products_list"></div>*/
/* 				{#{% include 'white-simple/products_list.twig' %}#}*/
/* 			</div>*/
/* 			<div class="row">*/
/* 				{#% include 'spotlight_tours.twig' %#}*/
/* 			</div>*/
/* 			<div class="row">*/
/* 				{#{% include 'blog_entries.twig' %}#}*/
/* 			</div>*/
/* 		</div>*/
/* */
/* 	</div> <!-- <div class="row"> --> <!-- search by State page end -->*/
/* */
/* */
/* {% endblock content %}*/
