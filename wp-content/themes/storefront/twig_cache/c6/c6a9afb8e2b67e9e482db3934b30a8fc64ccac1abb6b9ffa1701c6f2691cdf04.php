<?php

/* white-simple/filter_controls_box.twig */
class __TwigTemplate_3e06e65773f9a667cf77c50e4a4037993761b6ec4603cbe5c7b2e34633b78f3c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo $this->getAttribute((isset($context["library"]) ? $context["library"] : $this->getContext($context, "library")), "header_block_start", array(0 => "Prices Filter", 1 => "", 2 => "fa fa-search-plus"), "method");
        echo "


<div class=\"btn-group search_btn_group\" > ";
        // line 5
        echo "\t<button type=\"button\" onclick=\"javascript:frontendFuncsObj.nsn_woo_ext_search_runFilterSearch();return false;\" class=\"btn_run_filter\">Search</button>

\t<section class=\"alert alert-info row\">
\t\t";
        // line 9
        echo "\t\t";
        // line 10
        echo "\t\t<a class=\"a_link pull-right\" onclick=\"javascript:frontendFuncsObj.nsn_woo_ext_search_showBookmarks();return false;\">
\t\t\tShow Bookmarks
\t\t</a>
\t\t<span aria-hidden=\"true\" class=\"glyphicon glyphicon-bookmark\"></span>
\t\t<span class=\"glyphicon-class\">glyphicon glyphicon-bookmark</span>
\t\t<p class=\"text-info\">
\t\t\t<i class=\"glyphicon glyphicon-bookmark\"  >123</i>&nbsp;Apply Filter

\t\t\tYou have 6 saved Bookmarks
\t\t</p>
\t</section>




\t";
        // line 26
        echo "\t";
        // line 27
        echo "</div>

";
        // line 29
        echo $this->getAttribute((isset($context["library"]) ? $context["library"] : $this->getContext($context, "library")), "header_block_end", array(0 => "Prices Filter Block End"), "method");
    }

    public function getTemplateName()
    {
        return "white-simple/filter_controls_box.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  55 => 29,  51 => 27,  49 => 26,  32 => 10,  30 => 9,  25 => 5,  19 => 1,);
    }
}
/* {{ library.header_block_start("Prices Filter", "", "fa fa-search-plus") }}*/
/* */
/* */
/* <div class="btn-group search_btn_group" > {# bottom_margin_md top_margin_md#}*/
/* 	<button type="button" onclick="javascript:frontendFuncsObj.nsn_woo_ext_search_runFilterSearch();return false;" class="btn_run_filter">Search</button>*/
/* */
/* 	<section class="alert alert-info row">*/
/* 		{#<button type="button" onclick="javascript:frontendFuncsObj.nsn_woo_ext_search_showBookmarks();return false;" class="btn_run_filter">Show Bookmarks</button>#}*/
/* 		{# glyphicon glyphicon-bookmark#}*/
/* 		<a class="a_link pull-right" onclick="javascript:frontendFuncsObj.nsn_woo_ext_search_showBookmarks();return false;">*/
/* 			Show Bookmarks*/
/* 		</a>*/
/* 		<span aria-hidden="true" class="glyphicon glyphicon-bookmark"></span>*/
/* 		<span class="glyphicon-class">glyphicon glyphicon-bookmark</span>*/
/* 		<p class="text-info">*/
/* 			<i class="glyphicon glyphicon-bookmark"  >123</i>&nbsp;Apply Filter*/
/* */
/* 			You have 6 saved Bookmarks*/
/* 		</p>*/
/* 	</section>*/
/* */
/* */
/* */
/* */
/* 	{#<button type="button" class="btn btn-info btn-lg btn-block">You can save current filters as Bookmark</button>#}*/
/* 	{#<button type="button" onclick="javascript:frontendFuncsObj.nsn_woo_ext_search_addBookmark();return false;" class="btn_run_filter">Add Bookmark</button>#}*/
/* </div>*/
/* */
/* {{ library.header_block_end("Prices Filter Block End") }}*/
