<?php

/* white-simple/woo_ext_products_list.twig */
class __TwigTemplate_45c7090ac5903272a9d24309dbb6a7743dbda9b6ab14a8062f567d50a25ae9fe extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 139
        echo "  ";
        // line 140
        echo "
<h2 class=\"site_content_title margin_top_lg\">
\t<span class=\"fa fa-building-o fa-2x middle_icon\" ></span>&nbsp;Found Products (";
        // line 142
        echo twig_length_filter($this->env, (isset($context["products_list"]) ? $context["products_list"] : $this->getContext($context, "products_list")));
        echo ")
</h2>

";
        // line 147
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["products_list"]) ? $context["products_list"] : $this->getContext($context, "products_list")));
        foreach ($context['_seq'] as $context["_key"] => $context["nextProduct"]) {
            // line 148
            echo "\t<div class=\"col-xs-12 col-sm-6 next_woo_product\">
\t\t<ul class=\"products ul_multiline\">

\t\t\t";
            // line 152
            echo "            <span class=\"visible-xs\">
\t            ";
            // line 153
            echo $this->getAttribute($this, "show_next_product_info", array(0 => $context["nextProduct"], 1 => (isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")), 2 => (isset($context["attrs_to_show"]) ? $context["attrs_to_show"] : $this->getContext($context, "attrs_to_show")), 3 => "_xs"), "method");
            echo "
\t        </span>
\t        <span class=\"visible-sm\">
\t            ";
            // line 156
            echo $this->getAttribute($this, "show_next_product_info", array(0 => $context["nextProduct"], 1 => (isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")), 2 => (isset($context["attrs_to_show"]) ? $context["attrs_to_show"] : $this->getContext($context, "attrs_to_show")), 3 => "_sm"), "method");
            echo "
\t        </span>
\t        <span class=\"visible-md\">
\t            ";
            // line 159
            echo $this->getAttribute($this, "show_next_product_info", array(0 => $context["nextProduct"], 1 => (isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")), 2 => (isset($context["attrs_to_show"]) ? $context["attrs_to_show"] : $this->getContext($context, "attrs_to_show")), 3 => "_md"), "method");
            echo "
\t        </span>
\t        <span class=\"visible-lg\">
\t            ";
            // line 162
            echo $this->getAttribute($this, "show_next_product_info", array(0 => $context["nextProduct"], 1 => (isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")), 2 => (isset($context["attrs_to_show"]) ? $context["attrs_to_show"] : $this->getContext($context, "attrs_to_show")), 3 => "_md"), "method");
            echo "
\t        </span>

\t\t</ul>

\t</div>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['nextProduct'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    // line 1
    public function getshow_next_product_info($__nextProduct__ = null, $__fields_to_show__ = null, $__attrs_to_show__ = null, $__field_param__ = null, ...$__varargs__)
    {
        $context = $this->env->mergeGlobals(array(
            "nextProduct" => $__nextProduct__,
            "fields_to_show" => $__fields_to_show__,
            "attrs_to_show" => $__attrs_to_show__,
            "field_param" => $__field_param__,
            "varargs" => $__varargs__,
        ));

        $blocks = array();

        ob_start();
        try {
            // line 2
            echo "
\t<li class=\"post-";
            // line 3
            echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "id", array(), "array");
            echo " \">
\t";
            // line 4
            if (($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "title", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")), "title", array()))) {
                // line 5
                echo "\t\t<h3 class=\"row woo_product_next_row pull-left\">
\t\t\t";
                // line 6
                if (($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "image", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")), "image", array()))) {
                    // line 7
                    echo "\t\t\t\t<span class=\"fa fa-info-circle pull-left\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"";
                    echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "purchase_note", array(), "array");
                    echo "\"></span>&nbsp;
\t\t\t";
                }
                // line 9
                echo "\t\t\t<a href=\"";
                echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "guid", array(), "array");
                echo "\" class=\"woo_products_title woo_products_field_value a_link\">";
                echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "id", array(), "array");
                echo "->";
                echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "post_title", array(), "array");
                echo "</a>
\t\t</h3>
\t";
            }
            // line 12
            echo "
\t";
            // line 13
            if (($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "image", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")), "image", array()))) {
                // line 14
                echo "\t\t<div class=\"row \">
\t\t\t";
                // line 15
                if (($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "is_in_stock", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")), "is_in_stock", array()))) {
                    // line 16
                    echo "\t\t\t<div class=\"img_banner\"  >
\t\t\t\t";
                    // line 18
                    echo "\t\t\t\t<img src=\"";
                    echo (isset($context["site_url"]) ? $context["site_url"] : $this->getContext($context, "site_url"));
                    echo "/wp-content/plugins/wooExtSearch/images/delete.png\">
\t\t\t</div>
\t\t\t";
                }
                // line 21
                echo "
\t\t\t<div class=\"next_hostel_image\" >
\t\t\t\t<a href=\"";
                // line 23
                echo "\">
\t\t\t\t\t<div class=\"back_image image_border\" style=\"background-image: url('";
                // line 24
                echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "image_url", array(), "array");
                echo "'); width:180px; height:180px;\"   id=\"spotlight_img_hostel_";
                echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "id", array(), "array");
                echo "\"></div>
\t\t\t\t</a>
\t\t\t</div>
\t\t</div>
\t";
            }
            // line 29
            echo "
\t\t";
            // line 31
            echo "\t\t\t";
            // line 32
            echo "\t\t";
            // line 33
            echo "

\t\t";
            // line 36
            echo "\t\t";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["attrs_to_show"]) ? $context["attrs_to_show"] : $this->getContext($context, "attrs_to_show")));
            foreach ($context['_seq'] as $context["_key"] => $context["next_attr_to_show"]) {
                // line 37
                echo "\t\t\t";
                // line 38
                echo "\t\t\t";
                $context["is_field_used"] = false;
                // line 39
                echo "
\t\t\t";
                // line 40
                if ((($this->getAttribute($context["next_attr_to_show"], "attr_name", array(), "array") == "rating") && (($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "rating_average", array()) > 0) || ($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "comments_count", array()) > 0)))) {
                    // line 41
                    echo "\t\t\t\t<div class=\"row woo_product_next_row\">
\t\t\t\t\t<img src=\"";
                    // line 42
                    echo (isset($context["site_url"]) ? $context["site_url"] : $this->getContext($context, "site_url"));
                    echo "/wp-content/plugins/wooExtSearch/images/stars/stars";
                    echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "rating_average", array());
                    echo ".png\" title=\"Rated ";
                    echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "rating_average", array());
                    echo " out of 5\" class=\"pull-left\">&nbsp;
\t\t\t\t\t<span class=\"pull-right\"> (<span class=\"rating\">";
                    // line 43
                    echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "comments_count", array());
                    echo "</span> customer reviews)</span>
\t\t\t\t\t";
                    // line 44
                    $context["is_field_used"] = true;
                    // line 45
                    echo "\t\t\t\t</div>
\t\t\t";
                }
                // line 47
                echo "
\t\t\t";
                // line 48
                if ((twig_in_filter($this->getAttribute($context["next_attr_to_show"], "attr_name", array(), "array"), twig_get_array_keys_filter((isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")))) && ((isset($context["is_field_used"]) ? $context["is_field_used"] : $this->getContext($context, "is_field_used")) == false))) {
                    // line 49
                    echo "
\t\t\t\t";
                    // line 50
                    if (($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), $this->getAttribute($context["next_attr_to_show"], "attr_name", array(), "array"), array(), "array", true, true) && ($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), $this->getAttribute($context["next_attr_to_show"], "attr_name", array(), "array"), array(), "array") != ""))) {
                        // line 51
                        echo "\t\t\t\t<div class=\"row woo_product_next_row\">
\t\t\t\t\t<span class=\"woo_products_label\">";
                        // line 52
                        echo twig_title_string_filter($this->env, $this->getAttribute($context["next_attr_to_show"], "attr_name", array(), "array"));
                        echo ":</span>
\t\t\t\t\t<span class=\"woo_products_";
                        // line 53
                        echo $this->getAttribute($context["next_attr_to_show"], "attr_name", array(), "array");
                        echo " woo_products_field_value\">
\t\t\t\t\t\t";
                        // line 55
                        echo "\t\t\t\t\t\t";
                        echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), $this->getAttribute($context["next_attr_to_show"], "attr_name", array(), "array"), array(), "array");
                        echo "
\t\t\t\t\t</span>
\t\t\t\t</div>
\t\t\t\t";
                    }
                    // line 59
                    echo "
\t\t\t";
                }
                // line 61
                echo "
\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['next_attr_to_show'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 63
            echo "\t\t";
            // line 64
            echo "

\t";
            // line 66
            if (($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "post_date", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")), "post_date", array()))) {
                // line 67
                echo "\t\t<div class=\"row woo_product_next_row\">
\t\t\t<span class=\"woo_products_label\">";
                // line 68
                echo twig_title_string_filter($this->env, "Publication");
                echo ":</span>
\t\t\t<span class=\"woo_products_post_date woo_products_field_value\">
\t\t\t\t";
                // line 70
                echo call_user_func_array($this->env->getFilter('date_time')->getCallable(), array($this->getAttribute($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "post", array()), "post_date", array())));
                echo "
\t\t\t</span>
\t\t</div>
\t";
            }
            // line 74
            echo "

\t";
            // line 76
            if (($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "categories", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")), "categories", array()))) {
                // line 77
                echo "\t\t<div class=\"row woo_product_next_row\">
\t\t\t<span class=\"woo_products_label\">Categor";
                // line 78
                if ((twig_length_filter($this->env, $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "categories_list", array(), "array")) > 1)) {
                    echo "ies";
                } else {
                    echo "y";
                }
                echo ":</span>
\t\t\t";
                // line 79
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "categories_list", array(), "array"), "ret_titles", array(), "array"));
                $context['loop'] = array(
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                );
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["_key"] => $context["next_category_title"]) {
                    // line 80
                    echo "\t\t\t<span class=\"woo_products_categories woo_products_field_value\">
\t\t\t\t<a href=\"";
                    // line 81
                    echo call_user_func_array($this->env->getFilter('make_woo_product_category_url')->getCallable(), array($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "categories_list", array(), "array"), "ret_slugs", array(), "array"), $this->getAttribute($context["loop"], "index0", array()), array(), "array")));
                    echo "\" class=\"a_link\">";
                    echo $context["next_category_title"];
                    echo "</a>
\t\t\t\t";
                    // line 82
                    if ( !$this->getAttribute($context["loop"], "last", array())) {
                        echo ", ";
                    }
                    // line 83
                    echo "\t\t\t</span>
\t\t\t";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['next_category_title'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 85
                echo "\t\t</div>
\t";
            }
            // line 87
            echo "

\t";
            // line 89
            if ((($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "sale_price", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")), "sale_price", array())) || ($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "regular_price", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")), "regular_price", array())))) {
                // line 90
                echo "\t\t<div class=\"row woo_product_next_row\">
\t\t\t<span class=\"woo_products_label\">Price:</span>

\t\t\t<span class=\"woo_products_sale_price woo_products_field_value\">
\t\t\t\t";
                // line 94
                if ((($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), "regular_price", array(), "array", true, true) && ($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "regular_price", array(), "array") > 0)) && ($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "regular_price", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")), "regular_price", array())))) {
                    // line 95
                    echo "\t\t\t\t\t";
                    echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "formatted_regular_price", array(), "array");
                    echo "
\t\t\t\t";
                }
                // line 97
                echo "
\t\t\t\t";
                // line 98
                if ((((($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), "regular_price", array(), "array", true, true) && ($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "regular_price", array(), "array") > 0)) && ($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "regular_price", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")), "regular_price", array()))) && ($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), "sale_price", array(), "array", true, true) && ($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "sale_price", array(), "array") > 0))) && ($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "sale_price", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")), "sale_price", array())))) {
                    // line 99
                    echo "\t\t\t\t&nbsp;-&nbsp;
\t\t\t\t";
                }
                // line 101
                echo "
\t\t\t\t";
                // line 102
                if ((($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), "sale_price", array(), "array", true, true) && ($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "sale_price", array(), "array") > 0)) && ($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "sale_price", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")), "sale_price", array())))) {
                    // line 103
                    echo "\t\t\t\t\t";
                    echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "formatted_sale_price", array(), "array");
                    echo "
\t\t\t\t";
                }
                // line 105
                echo "\t\t\t</span>

\t\t</div>
\t";
            }
            // line 109
            echo "

\t\t";
            // line 111
            if (($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "is_in_stock", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")), "is_in_stock", array()))) {
                // line 112
                echo "\t\t\t<div class=\"row woo_product_next_row\">
\t\t\t\t<span class=\"woo_products_label\">In Stock:</span>
\t\t\t<span class=\"woo_products_is_in_stock woo_products_field_value\">
\t\t\t\t";
                // line 115
                if ($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "is_in_stock", array(), "array")) {
                    echo "Yes";
                } else {
                    echo "No";
                }
                // line 116
                echo "\t\t\t</span>
\t\t\t</div>
\t\t";
            }
            // line 119
            echo "
\t</li>


\t";
            // line 124
            echo "\t\t";
            // line 125
            echo "\t\t\t";
            // line 126
            echo "\t\t";
            // line 127
            echo "\t";
            // line 128
            echo "
\t";
            // line 130
            echo "\t\t";
            // line 131
            echo "\t";
            // line 132
            echo "
\t";
            // line 134
            echo "\t\t";
            // line 135
            echo "\t\t\t";
            // line 136
            echo "\t\t";
            // line 137
            echo "\t";
            // line 138
            echo "
";
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    public function getTemplateName()
    {
        return "white-simple/woo_ext_products_list.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  435 => 138,  433 => 137,  431 => 136,  429 => 135,  427 => 134,  424 => 132,  422 => 131,  420 => 130,  417 => 128,  415 => 127,  413 => 126,  411 => 125,  409 => 124,  403 => 119,  398 => 116,  392 => 115,  387 => 112,  385 => 111,  381 => 109,  375 => 105,  369 => 103,  367 => 102,  364 => 101,  360 => 99,  358 => 98,  355 => 97,  349 => 95,  347 => 94,  341 => 90,  339 => 89,  335 => 87,  331 => 85,  316 => 83,  312 => 82,  306 => 81,  303 => 80,  286 => 79,  278 => 78,  275 => 77,  273 => 76,  269 => 74,  262 => 70,  257 => 68,  254 => 67,  252 => 66,  248 => 64,  246 => 63,  239 => 61,  235 => 59,  227 => 55,  223 => 53,  219 => 52,  216 => 51,  214 => 50,  211 => 49,  209 => 48,  206 => 47,  202 => 45,  200 => 44,  196 => 43,  188 => 42,  185 => 41,  183 => 40,  180 => 39,  177 => 38,  175 => 37,  170 => 36,  166 => 33,  164 => 32,  162 => 31,  159 => 29,  149 => 24,  146 => 23,  142 => 21,  135 => 18,  132 => 16,  130 => 15,  127 => 14,  125 => 13,  122 => 12,  111 => 9,  105 => 7,  103 => 6,  100 => 5,  98 => 4,  94 => 3,  91 => 2,  76 => 1,  61 => 162,  55 => 159,  49 => 156,  43 => 153,  40 => 152,  35 => 148,  31 => 147,  25 => 142,  21 => 140,  19 => 139,);
    }
}
/* {% macro show_next_product_info( nextProduct, fields_to_show, attrs_to_show, field_param ) %}*/
/* */
/* 	<li class="post-{{ nextProduct['id'] }} ">*/
/* 	{% if fields_to_show.title is defined and fields_to_show.title %}*/
/* 		<h3 class="row woo_product_next_row pull-left">*/
/* 			{% if fields_to_show.image is defined and fields_to_show.image  %}*/
/* 				<span class="fa fa-info-circle pull-left" data-toggle="tooltip" data-placement="top" title="{{ nextProduct['purchase_note'] }}"></span>&nbsp;*/
/* 			{% endif %}*/
/* 			<a href="{{ nextProduct['guid'] }}" class="woo_products_title woo_products_field_value a_link">{{ nextProduct['id'] }}->{{ nextProduct['post_title'] }}</a>*/
/* 		</h3>*/
/* 	{% endif %}*/
/* */
/* 	{% if fields_to_show.image is defined and fields_to_show.image  %}*/
/* 		<div class="row ">*/
/* 			{% if fields_to_show.is_in_stock is defined and fields_to_show.is_in_stock  %}*/
/* 			<div class="img_banner"  >*/
/* 				{#<img src="{{ site_url }}/wp-content/plugins/wooExtSearch/images/in_stock.png">#}*/
/* 				<img src="{{ site_url }}/wp-content/plugins/wooExtSearch/images/delete.png">*/
/* 			</div>*/
/* 			{% endif %}*/
/* */
/* 			<div class="next_hostel_image" >*/
/* 				<a href="{#{ nextProduct['guid'] }#}">*/
/* 					<div class="back_image image_border" style="background-image: url('{{ nextProduct['image_url'] }}'); width:180px; height:180px;"   id="spotlight_img_hostel_{{ nextProduct['id'] }}"></div>*/
/* 				</a>*/
/* 			</div>*/
/* 		</div>*/
/* 	{% endif %}*/
/* */
/* 		{#<div class="star-rating" title="Rated 5 out of 5">#}*/
/* 			{#<span style="width:100%"><strong class="rating">5</strong> out of 5</span>#}*/
/* 		{#</div>#}*/
/* */
/* */
/* 		{#==================== START ==============#}*/
/* 		{% for next_attr_to_show in attrs_to_show %}*/
/* 			{#<b>next_attr_to_show</b>{{ next_attr_to_show['attr_name'] | d }}#}*/
/* 			{% set is_field_used = false %}*/
/* */
/* 			{% if next_attr_to_show['attr_name'] == "rating" and ( nextProduct.rating_average > 0 or nextProduct.comments_count > 0 ) %}*/
/* 				<div class="row woo_product_next_row">*/
/* 					<img src="{{ site_url }}/wp-content/plugins/wooExtSearch/images/stars/stars{{ nextProduct.rating_average }}.png" title="Rated {{ nextProduct.rating_average }} out of 5" class="pull-left">&nbsp;*/
/* 					<span class="pull-right"> (<span class="rating">{{ nextProduct.comments_count }}</span> customer reviews)</span>*/
/* 					{% set is_field_used = true %}*/
/* 				</div>*/
/* 			{% endif %}*/
/* */
/* 			{% if next_attr_to_show['attr_name'] in fields_to_show | keys and is_field_used == false %}*/
/* */
/* 				{% if nextProduct[next_attr_to_show['attr_name']]  is defined and nextProduct[next_attr_to_show['attr_name']]!= "" %}*/
/* 				<div class="row woo_product_next_row">*/
/* 					<span class="woo_products_label">{{ next_attr_to_show['attr_name'] | title }}:</span>*/
/* 					<span class="woo_products_{{ next_attr_to_show['attr_name'] }} woo_products_field_value">*/
/* 						{#next_attr_to_show]['attr_name']::{{ next_attr_to_show['attr_name'] }}#}*/
/* 						{{ nextProduct[ next_attr_to_show['attr_name'] ] }}*/
/* 					</span>*/
/* 				</div>*/
/* 				{% endif %}*/
/* */
/* 			{% endif %}*/
/* */
/* 		{% endfor %}*/
/* 		{#==================== END ==============#}*/
/* */
/* */
/* 	{% if fields_to_show.post_date is defined and fields_to_show.post_date %}*/
/* 		<div class="row woo_product_next_row">*/
/* 			<span class="woo_products_label">{{ "Publication" | title }}:</span>*/
/* 			<span class="woo_products_post_date woo_products_field_value">*/
/* 				{{ nextProduct.post.post_date | date_time }}*/
/* 			</span>*/
/* 		</div>*/
/* 	{% endif %}*/
/* */
/* */
/* 	{% if fields_to_show.categories is defined and fields_to_show.categories %}*/
/* 		<div class="row woo_product_next_row">*/
/* 			<span class="woo_products_label">Categor{% if nextProduct['categories_list'] | length > 1 %}ies{% else %}y{% endif %}:</span>*/
/* 			{% for next_category_title in nextProduct['categories_list']['ret_titles'] %}*/
/* 			<span class="woo_products_categories woo_products_field_value">*/
/* 				<a href="{{ nextProduct['categories_list']['ret_slugs'][loop.index0] | make_woo_product_category_url }}" class="a_link">{{ next_category_title }}</a>*/
/* 				{% if not loop.last %}, {% endif %}*/
/* 			</span>*/
/* 			{% endfor %}*/
/* 		</div>*/
/* 	{% endif %}*/
/* */
/* */
/* 	{% if ( fields_to_show.sale_price is defined and fields_to_show.sale_price ) or ( fields_to_show.regular_price is defined and fields_to_show.regular_price ) %}*/
/* 		<div class="row woo_product_next_row">*/
/* 			<span class="woo_products_label">Price:</span>*/
/* */
/* 			<span class="woo_products_sale_price woo_products_field_value">*/
/* 				{% if ( nextProduct['regular_price'] is defined and nextProduct['regular_price'] > 0 ) and ( fields_to_show.regular_price is defined and fields_to_show.regular_price ) %}*/
/* 					{{ nextProduct['formatted_regular_price'] }}*/
/* 				{% endif %}*/
/* */
/* 				{% if ( nextProduct['regular_price'] is defined and nextProduct['regular_price'] > 0 ) and ( fields_to_show.regular_price is defined and fields_to_show.regular_price ) and ( nextProduct['sale_price'] is defined and nextProduct['sale_price'] >0 ) and ( fields_to_show.sale_price is defined and fields_to_show.sale_price ) %}*/
/* 				&nbsp;-&nbsp;*/
/* 				{% endif %}*/
/* */
/* 				{% if ( nextProduct['sale_price'] is defined and nextProduct['sale_price'] > 0 ) and ( fields_to_show.sale_price is defined and fields_to_show.sale_price )%}*/
/* 					{{ nextProduct['formatted_sale_price'] }}*/
/* 				{% endif %}*/
/* 			</span>*/
/* */
/* 		</div>*/
/* 	{% endif %}*/
/* */
/* */
/* 		{% if fields_to_show.is_in_stock is defined and fields_to_show.is_in_stock  %}*/
/* 			<div class="row woo_product_next_row">*/
/* 				<span class="woo_products_label">In Stock:</span>*/
/* 			<span class="woo_products_is_in_stock woo_products_field_value">*/
/* 				{% if nextProduct['is_in_stock'] %}Yes{% else %}No{% endif %}*/
/* 			</span>*/
/* 			</div>*/
/* 		{% endif %}*/
/* */
/* 	</li>*/
/* */
/* */
/* 	{#<div class="row next_hostel_descr" >#}*/
/* 		{#<h5 class="site_content_text" class="debug" >#}*/
/* 			{#{{ nextProduct.short_descr | concat_str( 100 ) |raw }}#}*/
/* 		{#</h5>#}*/
/* 	{#</div>#}*/
/* */
/* 	{#&#123;&#35;<div class="row next_hostel_stars" >&#35;&#125;#}*/
/* 		{#&#123;&#35;<img src="{{ base_url }}{{ images_dir }}/{{ current_skin_name }}/stars/stars{{ nextProduct.reviews_avg_rating }}.png"  alt="{{ nextProduct.reviews_avg_rating | show_hostel_review_stars_rating_type_label_label }}" title="{{ nextProduct.reviews_avg_rating | show_hostel_review_stars_rating_type_label_label }}" class="pull-left"><span class="pull-right">{{ nextProduct.price | show_formatted_price( "AU$", "text_money" ) | raw }}</span>&#35;&#125;#}*/
/* 	{#&#123;&#35;</div>&#35;&#125;#}*/
/* */
/* 	{#<div class="row next_hostel_btn" >#}*/
/* 		{#<a href="{{ base_url }}{{ nextProduct.name | make_woo_product_url(nextProduct) | raw }}">#}*/
/* 			{#<button class="item_details">View Details</button>#}*/
/* 		{#</a>#}*/
/* 	{#</div>#}*/
/* */
/* {% endmacro %}  {# show_next_product_info END #}*/
/* */
/* <h2 class="site_content_title margin_top_lg">*/
/* 	<span class="fa fa-building-o fa-2x middle_icon" ></span>&nbsp;Found Products ({{ products_list | length }})*/
/* </h2>*/
/* */
/* {#attrs_to_show::{{ attrs_to_show | d }}#}*/
/* {#fields_to_show::{{ fields_to_show | d }}#}*/
/* {% for nextProduct in products_list %}*/
/* 	<div class="col-xs-12 col-sm-6 next_woo_product">*/
/* 		<ul class="products ul_multiline">*/
/* */
/* 			{#nextProduct::{{ nextProduct | d }}#}*/
/*             <span class="visible-xs">*/
/* 	            {{ _self.show_next_product_info( nextProduct, fields_to_show, attrs_to_show,  "_xs" ) }}*/
/* 	        </span>*/
/* 	        <span class="visible-sm">*/
/* 	            {{ _self.show_next_product_info( nextProduct, fields_to_show, attrs_to_show, "_sm" ) }}*/
/* 	        </span>*/
/* 	        <span class="visible-md">*/
/* 	            {{ _self.show_next_product_info( nextProduct, fields_to_show, attrs_to_show,  "_md" ) }}*/
/* 	        </span>*/
/* 	        <span class="visible-lg">*/
/* 	            {{ _self.show_next_product_info( nextProduct, fields_to_show, attrs_to_show, "_md" ) }}*/
/* 	        </span>*/
/* */
/* 		</ul>*/
/* */
/* 	</div>*/
/* {% endfor %}*/
