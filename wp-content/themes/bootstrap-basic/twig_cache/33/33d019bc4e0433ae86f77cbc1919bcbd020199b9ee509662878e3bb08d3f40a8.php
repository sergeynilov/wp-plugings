<?php

/* white-simple/filter_price_box.twig */
class __TwigTemplate_99dc0b5e1452c072d3fe6691a84b554ff8572d3674f01f483fbed107f42484a5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo $this->getAttribute((isset($context["library"]) ? $context["library"] : $this->getContext($context, "library")), "header_block_start", array(0 => "Prices Filter", 1 => "", 2 => "fa fa-search-plus"), "method");
        echo "
<div class=\"row\">
\t";
        // line 3
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["prices_list"]) ? $context["prices_list"] : $this->getContext($context, "prices_list")));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["next_price"]) {
            // line 4
            echo "\t\t<input type=\"checkbox\" id=\"next_price_";
            echo $this->getAttribute($context["next_price"], "min", array());
            echo "_";
            echo $this->getAttribute($context["next_price"], "max", array());
            echo "\" value=\"";
            echo $this->getAttribute($context["next_price"], "min", array());
            echo "\" class=\"cbx_price_selection_filters\" ";
            if ($this->getAttribute($context["loop"], "last", array())) {
                echo "checked ";
            }
            echo ">&nbsp;";
            echo $this->getAttribute($context["next_price"], "min", array());
            echo "&nbsp;-&nbsp;";
            echo $this->getAttribute($context["next_price"], "max", array());
            echo "
\t\t<b>(";
            // line 5
            if ($this->getAttribute($context["next_price"], "products_count", array(), "any", true, true)) {
                echo $this->getAttribute($context["next_price"], "products_count", array());
            } else {
                echo "0";
            }
            echo ")</b><br>
\t";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['next_price'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 7
        echo "</div>
";
        // line 8
        echo $this->getAttribute((isset($context["library"]) ? $context["library"] : $this->getContext($context, "library")), "header_block_end", array(0 => "Prices Filter Block End"), "method");
    }

    public function getTemplateName()
    {
        return "white-simple/filter_price_box.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  81 => 8,  78 => 7,  58 => 5,  41 => 4,  24 => 3,  19 => 1,);
    }
}
/* {{ library.header_block_start("Prices Filter", "", "fa fa-search-plus") }}*/
/* <div class="row">*/
/* 	{% for next_price in prices_list %}*/
/* 		<input type="checkbox" id="next_price_{{ next_price.min }}_{{ next_price.max }}" value="{{ next_price.min }}" class="cbx_price_selection_filters" {% if loop.last %}checked {%  endif %}>&nbsp;{{ next_price.min }}&nbsp;-&nbsp;{{	next_price.max }}*/
/* 		<b>({% if next_price.products_count is defined %}{{ next_price.products_count }}{% else %}0{% endif %})</b><br>*/
/* 	{% endfor %}*/
/* </div>*/
/* {{ library.header_block_end("Prices Filter Block End") }}*/
