<?php

/* white-simple/filter_attrs_box.twig */
class __TwigTemplate_6fb06877f5b2ad294d40c726c9e4a4b29e34ff2fd9a565df89478051c44e0c5d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["attrs_options_list"]) ? $context["attrs_options_list"] : $this->getContext($context, "attrs_options_list")));
        foreach ($context['_seq'] as $context["_key"] => $context["attrs_option"]) {
            // line 3
            echo "
\t";
            // line 4
            echo $this->getAttribute((isset($context["library"]) ? $context["library"] : $this->getContext($context, "library")), "header_block_start", array(0 => ("Filter on " . $this->getAttribute($context["attrs_option"], "attr_name", array(), "array")), 1 => "", 2 => "fa fa-search-plus"), "method");
            echo "
\t<div class=\"row\">
\t\t";
            // line 6
            $context["is_block_used"] = false;
            // line 7
            echo "\t\t";
            if (($this->getAttribute($context["attrs_option"], "attr_name", array(), "array") == "rating")) {
                // line 8
                echo "
\t\t\t1111<div id=\"slider_rating\" ></div>22222
\t\t\t<div class=\"alert alert-info btn-sm\"  id=\"div_slider_rating_range\" role=\"alert\" >333</div>4444
\t\t\t";
                // line 11
                $context["is_block_used"] = true;
                // line 12
                echo "\t\t";
            }
            // line 13
            echo "
\t\t";
            // line 14
            if (($this->getAttribute($context["attrs_option"], "attr_name", array(), "array") == "sku")) {
                // line 15
                echo "
\t\t\t<div class=\"btn-group search_btn_group\" >

\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<label for=\"nsn_woo_ext_search_cbx_partial_search\" class=\"col-xs-12 col-sm-5 control-label\">Partial search</label>
\t\t\t\t\t\t<div class=\"col-xs-12 col-sm-7 \">
\t\t\t\t\t\t\t<input type=\"checkbox\" value=\"1\" id=\"nsn_woo_ext_search_cbx_partial_search\" class=\" \">
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<label for=\"extra_details_smoke_free\" class=\"col-xs-12 col-sm-5 control-label\">Enter sku</label>
\t\t\t\t\t\t<div class=\"col-xs-12 col-sm-7 padding_right_sm padding_left_sm\">
\t\t\t\t\t\t\t<input type=\"text\" value=\"\" id=\"nsn_woo_ext_search_cbx_partial_search\" class=\"form-control\">
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>



\t\t\t</div>
\t\t\t";
                // line 39
                $context["is_block_used"] = true;
                // line 40
                echo "
\t\t";
            }
            // line 42
            echo "
\t\t";
            // line 43
            if (((isset($context["is_block_used"]) ? $context["is_block_used"] : $this->getContext($context, "is_block_used")) != true)) {
                // line 44
                echo "\t\t\t";
                $context["attrs_list"] = $this->getAttribute($context["attrs_option"], "attrs_list", array(), "array");
                // line 45
                echo "\t\t\t";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["attrs_list"]) ? $context["attrs_list"] : $this->getContext($context, "attrs_list")));
                $context['loop'] = array(
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                );
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["_key"] => $context["next_attr"]) {
                    // line 46
                    echo "\t\t\t\t<input type=\"checkbox\" id=\"next_attr_";
                    echo $this->getAttribute($context["next_attr"], "ID", array());
                    echo "\" value=\"";
                    echo $this->getAttribute($context["next_attr"], "name", array());
                    echo "\" class=\"cbx_";
                    echo $this->getAttribute($context["attrs_option"], "attr_name", array(), "array");
                    echo "_selection_filters\" ";
                    if ($this->getAttribute($context["loop"], "last", array())) {
                        echo "checked ";
                    }
                    echo ">&nbsp;";
                    echo $this->getAttribute($context["next_attr"], "name", array());
                    echo " <b>(";
                    if ($this->getAttribute($context["next_attr"], "products_count", array(), "any", true, true)) {
                        echo $this->getAttribute($context["next_attr"], "products_count", array());
                    } else {
                        echo "0";
                    }
                    echo ")</b><br>
\t\t\t";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['next_attr'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 48
                echo "\t\t";
            }
            // line 49
            echo "\t</div>
\t";
            // line 50
            echo $this->getAttribute((isset($context["library"]) ? $context["library"] : $this->getContext($context, "library")), "header_block_end", array(0 => (("Filter on " . $this->getAttribute($context["attrs_option"], "attr_name", array(), "array")) . " Block End")), "method");
            echo "

";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['attrs_option'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 53
        echo "





";
    }

    public function getTemplateName()
    {
        return "white-simple/filter_attrs_box.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  157 => 53,  148 => 50,  145 => 49,  142 => 48,  109 => 46,  91 => 45,  88 => 44,  86 => 43,  83 => 42,  79 => 40,  77 => 39,  51 => 15,  49 => 14,  46 => 13,  43 => 12,  41 => 11,  36 => 8,  33 => 7,  31 => 6,  26 => 4,  23 => 3,  19 => 2,);
    }
}
/* {#attrs_options_list::{{ attrs_options_list | d }}#}*/
/* {% for attrs_option in attrs_options_list %}*/
/* */
/* 	{{ library.header_block_start("Filter on " ~ attrs_option['attr_name'], "", "fa fa-search-plus") }}*/
/* 	<div class="row">*/
/* 		{% set is_block_used = false %}*/
/* 		{% if attrs_option['attr_name'] == 'rating' %}*/
/* */
/* 			1111<div id="slider_rating" ></div>22222*/
/* 			<div class="alert alert-info btn-sm"  id="div_slider_rating_range" role="alert" >333</div>4444*/
/* 			{% set is_block_used = true %}*/
/* 		{% endif %}*/
/* */
/* 		{% if attrs_option['attr_name'] == 'sku' %}*/
/* */
/* 			<div class="btn-group search_btn_group" >*/
/* */
/* 				<div class="row">*/
/* 					<div class="form-group">*/
/* 						<label for="nsn_woo_ext_search_cbx_partial_search" class="col-xs-12 col-sm-5 control-label">Partial search</label>*/
/* 						<div class="col-xs-12 col-sm-7 ">*/
/* 							<input type="checkbox" value="1" id="nsn_woo_ext_search_cbx_partial_search" class=" ">*/
/* 						</div>*/
/* 					</div>*/
/* 				</div>*/
/* */
/* 				<div class="row">*/
/* 					<div class="form-group">*/
/* 						<label for="extra_details_smoke_free" class="col-xs-12 col-sm-5 control-label">Enter sku</label>*/
/* 						<div class="col-xs-12 col-sm-7 padding_right_sm padding_left_sm">*/
/* 							<input type="text" value="" id="nsn_woo_ext_search_cbx_partial_search" class="form-control">*/
/* 						</div>*/
/* 					</div>*/
/* 				</div>*/
/* */
/* */
/* */
/* 			</div>*/
/* 			{% set is_block_used = true %}*/
/* */
/* 		{% endif %}*/
/* */
/* 		{% if is_block_used != true %}*/
/* 			{% set attrs_list= attrs_option['attrs_list'] %}*/
/* 			{% for next_attr in attrs_list %}*/
/* 				<input type="checkbox" id="next_attr_{{ next_attr.ID }}" value="{{ next_attr.name }}" class="cbx_{{ attrs_option['attr_name'] }}_selection_filters" {% if loop.last %}checked {%  endif %}>&nbsp;{{ next_attr.name }} <b>({% if next_attr.products_count is defined %}{{ next_attr.products_count }}{% else %}0{% endif %})</b><br>*/
/* 			{% endfor %}*/
/* 		{% endif %}*/
/* 	</div>*/
/* 	{{ library.header_block_end( "Filter on " ~ attrs_option['attr_name'] ~ " Block End") }}*/
/* */
/* {% endfor %}*/
/* */
/* */
/* */
/* */
/* */
/* */
/* */
