<?php

/* white-simple/filter_controls_box.twig */
class __TwigTemplate_ea98be810a09917ba5084a21ae506ba7970e244d5ed1920f15162f27fb15f4e6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo $this->getAttribute((isset($context["library"]) ? $context["library"] : $this->getContext($context, "library")), "header_block_start", array(0 => "Filter Controls", 1 => "", 2 => "fa fa-search-plus"), "method");
        echo "


<div class=\"btn-group search_btn_group\" > ";
        // line 5
        echo "
\t<section class=\"alert alert-info row\">

\t\t";
        // line 9
        echo "\t\t\t";
        // line 10
        echo "
\t\t\t<h5>Search Criteria:</h5>
\t\t\t<input type=\"checkbox\" value=\"1\" id=\"nsn_woo_ext_search_cbx_only_in_stock\">Show only in stock.<br>

\t\t\torder by:
\t\t\t<select name=\"nsn_woo_ext_search_orderby\" id=\"nsn_woo_ext_search_orderby\" class=\"orderby\">
\t\t\t\t<option value=\"menu_order\" selected=\"selected\">Default sorting</option>
\t\t\t\t<option value=\"popularity\">Sort by popularity</option>
\t\t\t\t<option value=\"rating\">Sort by average rating</option>
\t\t\t\t<option value=\"date\">Sort by newness</option>
\t\t\t\t<option value=\"price\">Sort by price: low to high</option>
\t\t\t\t<option value=\"price-desc\">Sort by price: high to low</option>
\t\t\t</select>

\t\t";
        // line 25
        echo "
\t\t<button type=\"button\" class=\"search-submit\" onclick=\"javascript:frontendFuncsObj.nsn_woo_ext_search_runFilterSearch();return false;\" >Search</button>

\t\t<p class=\"text-info\">
\t\t\t<span aria-hidden=\"true\" class=\"glyphicon glyphicon-bookmark\"></span>
\t\t\tYou have 6 saved Bookmarks
\t\t\t<a onclick=\"javascript:frontendFuncsObj.nsn_woo_ext_search_showBookmarks();return false;\" class=\"pull-right a_link\">&nbsp;Show Bookmarks</a>
\t\t</p>

\t\t<p class=\"text-info\">
\t\t\t<span aria-hidden=\"true\" class=\"glyphicon glyphicon-bookmark\"></span>
\t\t\tYou can save current filters as Bookmark
\t\t\t<a type=\"button\" onclick=\"javascript:frontendFuncsObj.nsn_woo_ext_search_addBookmark();return false;\" class=\"pull-right a_link\">&nbsp;Save</a>
\t\t</p>
\t</section>


</div>

";
        // line 44
        echo $this->getAttribute((isset($context["library"]) ? $context["library"] : $this->getContext($context, "library")), "header_block_end", array(0 => "Filter Controls Block End"), "method");
    }

    public function getTemplateName()
    {
        return "white-simple/filter_controls_box.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  69 => 44,  48 => 25,  32 => 10,  30 => 9,  25 => 5,  19 => 1,);
    }
}
/* {{ library.header_block_start("Filter Controls", "", "fa fa-search-plus") }}*/
/* */
/* */
/* <div class="btn-group search_btn_group" > {# bottom_margin_md top_margin_md#}*/
/* */
/* 	<section class="alert alert-info row">*/
/* */
/* 		{#<fieldset>#}*/
/* 			{#<legend><h5>Search Criteria:</h5></legend>#}*/
/* */
/* 			<h5>Search Criteria:</h5>*/
/* 			<input type="checkbox" value="1" id="nsn_woo_ext_search_cbx_only_in_stock">Show only in stock.<br>*/
/* */
/* 			order by:*/
/* 			<select name="nsn_woo_ext_search_orderby" id="nsn_woo_ext_search_orderby" class="orderby">*/
/* 				<option value="menu_order" selected="selected">Default sorting</option>*/
/* 				<option value="popularity">Sort by popularity</option>*/
/* 				<option value="rating">Sort by average rating</option>*/
/* 				<option value="date">Sort by newness</option>*/
/* 				<option value="price">Sort by price: low to high</option>*/
/* 				<option value="price-desc">Sort by price: high to low</option>*/
/* 			</select>*/
/* */
/* 		{#</fieldset>#}*/
/* */
/* 		<button type="button" class="search-submit" onclick="javascript:frontendFuncsObj.nsn_woo_ext_search_runFilterSearch();return false;" >Search</button>*/
/* */
/* 		<p class="text-info">*/
/* 			<span aria-hidden="true" class="glyphicon glyphicon-bookmark"></span>*/
/* 			You have 6 saved Bookmarks*/
/* 			<a onclick="javascript:frontendFuncsObj.nsn_woo_ext_search_showBookmarks();return false;" class="pull-right a_link">&nbsp;Show Bookmarks</a>*/
/* 		</p>*/
/* */
/* 		<p class="text-info">*/
/* 			<span aria-hidden="true" class="glyphicon glyphicon-bookmark"></span>*/
/* 			You can save current filters as Bookmark*/
/* 			<a type="button" onclick="javascript:frontendFuncsObj.nsn_woo_ext_search_addBookmark();return false;" class="pull-right a_link">&nbsp;Save</a>*/
/* 		</p>*/
/* 	</section>*/
/* */
/* */
/* </div>*/
/* */
/* {{ library.header_block_end("Filter Controls Block End") }}*/
