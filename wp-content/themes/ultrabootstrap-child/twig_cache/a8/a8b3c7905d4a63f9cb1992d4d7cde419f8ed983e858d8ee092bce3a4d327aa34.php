<?php

/* white-simple/new_bookmark_dialog.twig */
class __TwigTemplate_f797f7519eb6dbe72e96a067d326f491251ac3127e5453c9891eadd4007be10c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"btn-group  pull-right editor_btn_group \" role=\"group\" aria-label=\"group button\"><div class=\"modal fade\" id=\"";
        echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : null);
        echo "show_new_bookmark_dialog\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"modalLabel\" aria-hidden=\"true\">
\t<div class=\"nsn_woo_ext_search_modal-dialog\">
\t\t<div class=\"nsn_woo_ext_search_modal-content\">
\t\t\t<section class=\"modal-header\">
\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\"><span aria-hidden=\"true\">×</span><span class=\"sr-only\">Close</span></button>
\t\t\t\t<div class=\"nsn_woo_ext_search_modal-title\">New&nbsp;Bookmark&nbsp;Dialog&nbsp;Setup</div>
\t\t\t</section>

\t\t\t<section class=\"nsn_woo_ext_search_modal-body nsn_woo_ext_search_block_padding_lg\">
\t\t\t\t<form role=\"form\" class=\"form-horizontal\" >

\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-xs-12 col-sm-offset-1 col-sm-3 control-label \" for=\"";
        // line 14
        echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : null);
        echo "bookmark_title\">Bookmark Title</label>
\t\t\t\t\t\t\t<div class=\"col-xs-12 col-sm-6 col-sm-offset-right-2\">
\t\t\t\t\t\t\t\t<input class=\"form-control\" value=\"\" id=\"";
        // line 16
        echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : null);
        echo "bookmark_title\" type=\"text\">
\t\t\t\t\t\t\t\t<small> Enter name under which you want to save your bookmark</small>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-xs-12 col-sm-offset-1 col-sm-3 control-label \" for=\"";
        // line 24
        echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : null);
        echo "bookmark_title\">Save Bookmark as default</label>
\t\t\t\t\t\t\t<div class=\"col-xs-12 col-sm-6 col-sm-offset-right-2\">
\t\t\t\t\t\t\t\t<select name=\"";
        // line 26
        echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : null);
        echo "is_default\" id=\"";
        echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : null);
        echo "is_default\" class=\"is_default form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"no\">No</option>
\t\t\t\t\t\t\t\t\t<option value=\"yes\">Yes</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t<small> If \"Yes\" selected, this bookmark will be loaded on page loaded.</small>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t</form>
\t\t\t</section>

\t\t\t<section class=\"modal-footer\">
\t\t\t\t<div class=\"btn-group  pull-right nsn_woo_ext_search_editor_btn_group \" role=\"group\" aria-label=\"group button\">
\t\t\t\t\t<button type=\"button\" id=\"saveImage\" class=\"btn btn-primary\" onclick=\"javascript:nsn_woo_ext_search_frontendFuncsObj.makeSaveBookmark();return false;\" role=\"button\">Save</button>
\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary btn nsn_woo_ext_search_btn-cancel-action\" data-dismiss=\"modal\"  role=\"button\">Cancel</button>
\t\t\t\t</div>
\t\t\t</section>
\t\t</div>
\t</div>
</div>";
    }

    public function getTemplateName()
    {
        return "white-simple/new_bookmark_dialog.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 26,  52 => 24,  41 => 16,  36 => 14,  19 => 1,);
    }
}
/* <div class="btn-group  pull-right editor_btn_group " role="group" aria-label="group button"><div class="modal fade" id="{{ plugin_prefix }}show_new_bookmark_dialog" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">*/
/* 	<div class="nsn_woo_ext_search_modal-dialog">*/
/* 		<div class="nsn_woo_ext_search_modal-content">*/
/* 			<section class="modal-header">*/
/* 				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>*/
/* 				<div class="nsn_woo_ext_search_modal-title">New&nbsp;Bookmark&nbsp;Dialog&nbsp;Setup</div>*/
/* 			</section>*/
/* */
/* 			<section class="nsn_woo_ext_search_modal-body nsn_woo_ext_search_block_padding_lg">*/
/* 				<form role="form" class="form-horizontal" >*/
/* */
/* 					<div class="row">*/
/* 						<div class="form-group">*/
/* 							<label class="col-xs-12 col-sm-offset-1 col-sm-3 control-label " for="{{ plugin_prefix }}bookmark_title">Bookmark Title</label>*/
/* 							<div class="col-xs-12 col-sm-6 col-sm-offset-right-2">*/
/* 								<input class="form-control" value="" id="{{ plugin_prefix }}bookmark_title" type="text">*/
/* 								<small> Enter name under which you want to save your bookmark</small>*/
/* 							</div>*/
/* 						</div>*/
/* 					</div>*/
/* */
/* 					<div class="row">*/
/* 						<div class="form-group">*/
/* 							<label class="col-xs-12 col-sm-offset-1 col-sm-3 control-label " for="{{ plugin_prefix }}bookmark_title">Save Bookmark as default</label>*/
/* 							<div class="col-xs-12 col-sm-6 col-sm-offset-right-2">*/
/* 								<select name="{{ plugin_prefix }}is_default" id="{{ plugin_prefix }}is_default" class="is_default form-control">*/
/* 									<option value="no">No</option>*/
/* 									<option value="yes">Yes</option>*/
/* 								</select>*/
/* 								<small> If "Yes" selected, this bookmark will be loaded on page loaded.</small>*/
/* 							</div>*/
/* 						</div>*/
/* 					</div>*/
/* */
/* 				</form>*/
/* 			</section>*/
/* */
/* 			<section class="modal-footer">*/
/* 				<div class="btn-group  pull-right nsn_woo_ext_search_editor_btn_group " role="group" aria-label="group button">*/
/* 					<button type="button" id="saveImage" class="btn btn-primary" onclick="javascript:nsn_woo_ext_search_frontendFuncsObj.makeSaveBookmark();return false;" role="button">Save</button>*/
/* 					<button type="button" class="btn btn-primary btn nsn_woo_ext_search_btn-cancel-action" data-dismiss="modal"  role="button">Cancel</button>*/
/* 				</div>*/
/* 			</section>*/
/* 		</div>*/
/* 	</div>*/
/* </div>*/
