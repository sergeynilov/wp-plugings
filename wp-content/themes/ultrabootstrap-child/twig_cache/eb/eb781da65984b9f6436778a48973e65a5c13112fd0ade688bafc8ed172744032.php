<?php

/* white-simple/filter_controls_box.twig */
class __TwigTemplate_6676c67562aa0474ab3c6ac397b9fcc82d95f8c36a049462d20f6834ae36f20c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo $this->getAttribute((isset($context["library"]) ? $context["library"] : $this->getContext($context, "library")), "header_block_start", array(0 => "Filter Controls", 1 => "", 2 => "fa fa-search-plus", 3 => ((isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : $this->getContext($context, "plugin_prefix")) . "block_controls")), "method");
        echo "


\t<div class=\"row nsn_woo_ext_search_block_padding_sm\">
\t\t<button type=\"button\" class=\"search-submit  pull-left\" onclick=\"javascript:nsn_woo_ext_search_frontendFuncsObj.clearAllInputs();return false;\" >Clear All Inputs</button>
\t\t";
        // line 6
        if ($this->getAttribute((isset($context["woo_ext_search_options"]) ? $context["woo_ext_search_options"] : null), "service_post_for_help_text", array(), "any", true, true)) {
            // line 7
            echo "\t\t\t<span aria-hidden=\"true\" class=\"glyphicon glyphicon-info-sign pull-right\">
\t\t\t\t<a class=\"nsn_woo_ext_search_a_link\" target=\"_blank\" href=\"";
            // line 8
            echo call_user_func_array($this->env->getFilter('make_post_url')->getCallable(), array($this->getAttribute((isset($context["woo_ext_search_options"]) ? $context["woo_ext_search_options"] : $this->getContext($context, "woo_ext_search_options")), "service_post_for_help_text", array())));
            echo "\">Help</a>
\t\t\t</span>
\t\t";
        }
        // line 11
        echo "\t\t";
        echo $this->getAttribute((isset($context["library"]) ? $context["library"] : $this->getContext($context, "library")), "responsive_helper", array(), "method");
        echo "
\t</div>



\t";
        // line 16
        if (($this->getAttribute((isset($context["woo_ext_search_options"]) ? $context["woo_ext_search_options"] : $this->getContext($context, "woo_ext_search_options")), "show_in_extended_search_post_title", array()) == "yes")) {
            // line 17
            echo "\t\t<div class=\"row nsn_woo_ext_search_block_padding_sm\">
\t\t\t<div class=\"form-group\">
\t\t\t\t<label for=\"";
            // line 19
            echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : $this->getContext($context, "plugin_prefix"));
            echo "post_title\" class=\"col-xs-12 col-sm-5 control-label\">Title/Content</label>
\t\t\t\t<div class=\"col-xs-12 col-sm-7 padding_right_sm padding_left_sm\">
\t\t\t\t\t<input type=\"text\" value=\"\" id=\"";
            // line 21
            echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : $this->getContext($context, "plugin_prefix"));
            echo "post_title\" size=\"20\" maxlength=\"100\" class=\"form-control\" onkeyup=\"javascript:nsn_woo_ext_search_frontendFuncsObj.post_title_onChange();\">
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<label for=\"";
            // line 25
            echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : $this->getContext($context, "plugin_prefix"));
            echo "cbx_partial_title\" class=\"col-xs-12 col-sm-5 control-label\">Partial</label>
\t\t\t\t<div class=\"col-xs-12 col-sm-7 \">
\t\t\t\t\t<input type=\"checkbox\" value=\"1\" id=\"";
            // line 27
            echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : $this->getContext($context, "plugin_prefix"));
            echo "cbx_partial_title\" checked>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t";
        }
        // line 32
        echo "

\t";
        // line 34
        if (($this->getAttribute((isset($context["woo_ext_search_options"]) ? $context["woo_ext_search_options"] : $this->getContext($context, "woo_ext_search_options")), "show_in_extended_search_sku", array()) == "yes")) {
            // line 35
            echo "
\t\t<div class=\"row nsn_woo_ext_search_block_padding_sm\">
\t\t\t<div class=\"form-group\">
\t\t\t\t<label for=\"";
            // line 38
            echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : $this->getContext($context, "plugin_prefix"));
            echo "input_sku\" class=\"col-xs-12 col-sm-5 control-label\">Enter sku</label>
\t\t\t\t<div class=\"col-xs-12 col-sm-7 padding_right_sm padding_left_sm\">
\t\t\t\t\t<input type=\"text\" value=\"\" id=\"";
            // line 40
            echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : $this->getContext($context, "plugin_prefix"));
            echo "input_sku\" size=\"20\" maxlength=\"100\" class=\"form-control\" onkeyup=\"javascript:nsn_woo_ext_search_frontendFuncsObj.sku_onChange();\">
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<label for=\"";
            // line 44
            echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : $this->getContext($context, "plugin_prefix"));
            echo "cbx_partial_sku\" class=\"col-xs-12 col-sm-5 control-label\">Partial</label>
\t\t\t\t<div class=\"col-xs-12 col-sm-7 \">
\t\t\t\t\t<input type=\"checkbox\" value=\"1\" id=\"";
            // line 46
            echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : $this->getContext($context, "plugin_prefix"));
            echo "cbx_partial_sku\" class=\"\" >
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>

\t";
        }
        // line 52
        echo "

\t<div class=\"row nsn_woo_ext_search_block_padding_sm\">
\t\t<div class=\"form-group\">
\t\t\t<label for=\"";
        // line 56
        echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : $this->getContext($context, "plugin_prefix"));
        echo "cbx_only_in_stock\" class=\"col-xs-12 col-sm-5 control-label\">Only in stock</label>
\t\t\t<div class=\"col-xs-12 col-sm-7 \">
\t\t\t\t<input type=\"checkbox\" value=\"1\" id=\"";
        // line 58
        echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : $this->getContext($context, "plugin_prefix"));
        echo "cbx_only_in_stock\" >
\t\t\t</div>
\t\t</div>
\t</div>

\t<div class=\"row nsn_woo_ext_search_block_padding_sm\">
\t\t<div class=\"form-group\">
\t\t\t<label for=\"";
        // line 65
        echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : $this->getContext($context, "plugin_prefix"));
        echo "orderby\" class=\"col-xs-12 col-sm-5 control-label\">Order by</label>
\t\t\t<div class=\"col-xs-12 col-sm-7 padding_right_sm padding_left_sm\">
\t\t\t\t<select name=\"";
        // line 67
        echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : $this->getContext($context, "plugin_prefix"));
        echo "orderby\" id=\"";
        echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : $this->getContext($context, "plugin_prefix"));
        echo "orderby\" class=\"form-control\">
\t\t\t\t\t<option value=\"popularity\">By popularity</option>
\t\t\t\t\t<option value=\"rating\">By average rating</option>
\t\t\t\t\t<option value=\"date\" selected=\"selected\">By newness</option>
\t\t\t\t\t<option value=\"price\">By price: low to high</option>
\t\t\t\t\t<option value=\"price-desc\">By price: high to low</option>
\t\t\t\t</select>
\t\t\t</div>
\t\t</div>
\t</div>




";
        // line 81
        if ((array_key_exists("products_pagination_per_page_list", $context) && (twig_length_filter($this->env, (isset($context["products_pagination_per_page_list"]) ? $context["products_pagination_per_page_list"] : $this->getContext($context, "products_pagination_per_page_list"))) > 0))) {
            // line 82
            echo "\t<div class=\"row nsn_woo_ext_search_block_padding_sm\">
\t\t<div class=\"form-group\">
\t\t\t<label for=\"";
            // line 84
            echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : $this->getContext($context, "plugin_prefix"));
            echo "items_per_page\" class=\"col-xs-12 col-sm-5 control-label\">Products per page </label>
\t\t\t<div class=\"col-xs-12 col-sm-7 padding_right_sm padding_left_sm\">
\t\t\t\t<select name=\"";
            // line 86
            echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : $this->getContext($context, "plugin_prefix"));
            echo "items_per_page\" id=\"";
            echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : $this->getContext($context, "plugin_prefix"));
            echo "items_per_page\" class=\"form-control\">
\t\t\t\t\t<option value=\"\">  -Select items per page-  </option>
\t\t\t\t\t";
            // line 88
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["products_pagination_per_page_list"]) ? $context["products_pagination_per_page_list"] : $this->getContext($context, "products_pagination_per_page_list")));
            foreach ($context['_seq'] as $context["_key"] => $context["next_products_pagination_per_page"]) {
                // line 89
                echo "\t\t\t\t\t\t<option value=\"";
                echo $context["next_products_pagination_per_page"];
                echo "\" ";
                if (($this->getAttribute((isset($context["woo_ext_search_options"]) ? $context["woo_ext_search_options"] : $this->getContext($context, "woo_ext_search_options")), "products_per_page", array()) == $context["next_products_pagination_per_page"])) {
                    echo "selected";
                }
                echo " >By ";
                echo $context["next_products_pagination_per_page"];
                // line 90
                echo "</option>
\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['next_products_pagination_per_page'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 92
            echo "\t\t\t\t</select>
\t\t\t</div>
\t\t</div>
\t</div>
";
        }
        // line 97
        echo "



<div class=\"row nsn_woo_ext_search_block_padding_sm text-center\">
\t\t<button type=\"button\" class=\"search-submit\" onclick=\"javascript:nsn_woo_ext_search_frontendFuncsObj.runFilterSearch('loadWooProducts',1);return false;\" >Search</button>
\t\t";
        // line 104
        echo "\t</div>


\t";
        // line 107
        if (((($this->getAttribute((isset($context["woo_ext_search_options"]) ? $context["woo_ext_search_options"] : $this->getContext($context, "woo_ext_search_options")), "show_bookmark_functionality", array()) == "yes") && array_key_exists("current_user_id", $context)) && ((isset($context["current_user_id"]) ? $context["current_user_id"] : $this->getContext($context, "current_user_id")) > 0))) {
            // line 108
            echo "\t<div class=\"row\">
\t\t<fieldset class=\"nsn_woo_ext_search_thin_border nsn_woo_ext_search_block_padding_xs\">
\t\t\t<legend >Bookmarks:</legend>

\t\t\t<div class=\"nsn_woo_ext_search_block_padding_xs\">
\t\t\t\t<button onclick=\"javascript:nsn_woo_ext_search_frontendFuncsObj.showNewBookmarkDialog();return false;\" >&nbsp;Save current filters as
\t\t\t\t\tBookmark</button>
\t\t\t</div>

\t\t\t<div class=\"nsn_woo_ext_search_block_padding_xs\">
\t\t\t\t<span aria-hidden=\"true\" class=\"glyphicon glyphicon-bookmark\"></span>
\t\t\t\t<span id=\"span_have_bookmarks\"></span>
\t\t\t</div>

\t\t\t<div class=\"nsn_woo_ext_search_block_padding_xs\">
\t\t\t\t<button id=\"button_show_existing_bookmarks\" onclick=\"javascript:nsn_woo_ext_search_frontendFuncsObj.showBookmarks();return false;\" >&nbsp;
\t\t\t\t\tSelect Bookmark and fill controls</button>
\t\t\t</div>

\t\t</fieldset>
\t</div>
\t";
        }
        // line 130
        echo "
\t";
        // line 131
        if (( !array_key_exists("current_user_id", $context) || ((isset($context["current_user_id"]) ? $context["current_user_id"] : $this->getContext($context, "current_user_id")) <= 0))) {
            // line 132
            echo "\t<div class=\"row\">
\t\t<div class=\"nsn_woo_ext_search_block_padding_xs\">
\t\t\t";
            // line 135
            echo "\t\t\t<p class=\"text-info\">You must login to system to use Bookmarks</p>
\t\t</div>
\t</div>
\t";
        }
        // line 139
        echo "
\t<div class=\"row\" style=\"display: none\" id=\"div_textarea_args\">
\t\t<div class=\"nsn_woo_ext_search_block_padding_xs\">
\t\t\t";
        // line 143
        echo "\t\t\t<textarea id=\"textarea_args\" cols=\"50\" rows=\"6\"></textarea>
\t\t\t<input id=\"input_products_count\" value=\"\" size=\"20\" maxlength=\"100\" >
\t\t</div>
\t</div>


";
        // line 149
        echo $this->getAttribute((isset($context["library"]) ? $context["library"] : $this->getContext($context, "library")), "header_block_end", array(0 => "Filter Controls Block End"), "method");
    }

    public function getTemplateName()
    {
        return "white-simple/filter_controls_box.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  272 => 149,  264 => 143,  259 => 139,  253 => 135,  249 => 132,  247 => 131,  244 => 130,  220 => 108,  218 => 107,  213 => 104,  205 => 97,  198 => 92,  191 => 90,  182 => 89,  178 => 88,  171 => 86,  166 => 84,  162 => 82,  160 => 81,  141 => 67,  136 => 65,  126 => 58,  121 => 56,  115 => 52,  106 => 46,  101 => 44,  94 => 40,  89 => 38,  84 => 35,  82 => 34,  78 => 32,  70 => 27,  65 => 25,  58 => 21,  53 => 19,  49 => 17,  47 => 16,  38 => 11,  32 => 8,  29 => 7,  27 => 6,  19 => 1,);
    }
}
/* {{ library.header_block_start( "Filter Controls", "", "fa fa-search-plus", plugin_prefix ~ "block_controls" ) }}*/
/* */
/* */
/* 	<div class="row nsn_woo_ext_search_block_padding_sm">*/
/* 		<button type="button" class="search-submit  pull-left" onclick="javascript:nsn_woo_ext_search_frontendFuncsObj.clearAllInputs();return false;" >Clear All Inputs</button>*/
/* 		{% if woo_ext_search_options.service_post_for_help_text is defined %}*/
/* 			<span aria-hidden="true" class="glyphicon glyphicon-info-sign pull-right">*/
/* 				<a class="nsn_woo_ext_search_a_link" target="_blank" href="{{ woo_ext_search_options.service_post_for_help_text | make_post_url }}">Help</a>*/
/* 			</span>*/
/* 		{% endif %}*/
/* 		{{ library.responsive_helper() }}*/
/* 	</div>*/
/* */
/* */
/* */
/* 	{% if woo_ext_search_options.show_in_extended_search_post_title =='yes'  %}*/
/* 		<div class="row nsn_woo_ext_search_block_padding_sm">*/
/* 			<div class="form-group">*/
/* 				<label for="{{ plugin_prefix }}post_title" class="col-xs-12 col-sm-5 control-label">Title/Content</label>*/
/* 				<div class="col-xs-12 col-sm-7 padding_right_sm padding_left_sm">*/
/* 					<input type="text" value="" id="{{ plugin_prefix }}post_title" size="20" maxlength="100" class="form-control" onkeyup="javascript:nsn_woo_ext_search_frontendFuncsObj.post_title_onChange();">*/
/* 				</div>*/
/* 			</div>*/
/* 			<div class="form-group">*/
/* 				<label for="{{ plugin_prefix }}cbx_partial_title" class="col-xs-12 col-sm-5 control-label">Partial</label>*/
/* 				<div class="col-xs-12 col-sm-7 ">*/
/* 					<input type="checkbox" value="1" id="{{ plugin_prefix }}cbx_partial_title" checked>*/
/* 				</div>*/
/* 			</div>*/
/* 		</div>*/
/* 	{% endif %}*/
/* */
/* */
/* 	{% if woo_ext_search_options.show_in_extended_search_sku =='yes'  %}*/
/* */
/* 		<div class="row nsn_woo_ext_search_block_padding_sm">*/
/* 			<div class="form-group">*/
/* 				<label for="{{ plugin_prefix }}input_sku" class="col-xs-12 col-sm-5 control-label">Enter sku</label>*/
/* 				<div class="col-xs-12 col-sm-7 padding_right_sm padding_left_sm">*/
/* 					<input type="text" value="" id="{{ plugin_prefix }}input_sku" size="20" maxlength="100" class="form-control" onkeyup="javascript:nsn_woo_ext_search_frontendFuncsObj.sku_onChange();">*/
/* 				</div>*/
/* 			</div>*/
/* 			<div class="form-group">*/
/* 				<label for="{{ plugin_prefix }}cbx_partial_sku" class="col-xs-12 col-sm-5 control-label">Partial</label>*/
/* 				<div class="col-xs-12 col-sm-7 ">*/
/* 					<input type="checkbox" value="1" id="{{ plugin_prefix }}cbx_partial_sku" class="" >*/
/* 				</div>*/
/* 			</div>*/
/* 		</div>*/
/* */
/* 	{% endif %}*/
/* */
/* */
/* 	<div class="row nsn_woo_ext_search_block_padding_sm">*/
/* 		<div class="form-group">*/
/* 			<label for="{{ plugin_prefix }}cbx_only_in_stock" class="col-xs-12 col-sm-5 control-label">Only in stock</label>*/
/* 			<div class="col-xs-12 col-sm-7 ">*/
/* 				<input type="checkbox" value="1" id="{{ plugin_prefix }}cbx_only_in_stock" >*/
/* 			</div>*/
/* 		</div>*/
/* 	</div>*/
/* */
/* 	<div class="row nsn_woo_ext_search_block_padding_sm">*/
/* 		<div class="form-group">*/
/* 			<label for="{{ plugin_prefix }}orderby" class="col-xs-12 col-sm-5 control-label">Order by</label>*/
/* 			<div class="col-xs-12 col-sm-7 padding_right_sm padding_left_sm">*/
/* 				<select name="{{ plugin_prefix }}orderby" id="{{ plugin_prefix }}orderby" class="form-control">*/
/* 					<option value="popularity">By popularity</option>*/
/* 					<option value="rating">By average rating</option>*/
/* 					<option value="date" selected="selected">By newness</option>*/
/* 					<option value="price">By price: low to high</option>*/
/* 					<option value="price-desc">By price: high to low</option>*/
/* 				</select>*/
/* 			</div>*/
/* 		</div>*/
/* 	</div>*/
/* */
/* */
/* */
/* */
/* {% if products_pagination_per_page_list is defined and products_pagination_per_page_list | length > 0 %}*/
/* 	<div class="row nsn_woo_ext_search_block_padding_sm">*/
/* 		<div class="form-group">*/
/* 			<label for="{{ plugin_prefix }}items_per_page" class="col-xs-12 col-sm-5 control-label">Products per page </label>*/
/* 			<div class="col-xs-12 col-sm-7 padding_right_sm padding_left_sm">*/
/* 				<select name="{{ plugin_prefix }}items_per_page" id="{{ plugin_prefix }}items_per_page" class="form-control">*/
/* 					<option value="">  -Select items per page-  </option>*/
/* 					{% for next_products_pagination_per_page in products_pagination_per_page_list %}*/
/* 						<option value="{{ next_products_pagination_per_page }}" {% if woo_ext_search_options.products_per_page == next_products_pagination_per_page %}selected{% endif %} >By {{ next_products_pagination_per_page*/
/* 					}}</option>*/
/* 					{% endfor %}*/
/* 				</select>*/
/* 			</div>*/
/* 		</div>*/
/* 	</div>*/
/* {% endif %}*/
/* */
/* */
/* */
/* */
/* <div class="row nsn_woo_ext_search_block_padding_sm text-center">*/
/* 		<button type="button" class="search-submit" onclick="javascript:nsn_woo_ext_search_frontendFuncsObj.runFilterSearch('loadWooProducts',1);return false;" >Search</button>*/
/* 		{#<button type="button" class="search-submit  pull-right" onclick="javascript:nsn_woo_ext_search_frontendFuncsObj.clearAllInputs();return false;" >Clear All Inputs</button>#}*/
/* 	</div>*/
/* */
/* */
/* 	{% if woo_ext_search_options.show_bookmark_functionality =='yes' and current_user_id is defined and current_user_id > 0 %}*/
/* 	<div class="row">*/
/* 		<fieldset class="nsn_woo_ext_search_thin_border nsn_woo_ext_search_block_padding_xs">*/
/* 			<legend >Bookmarks:</legend>*/
/* */
/* 			<div class="nsn_woo_ext_search_block_padding_xs">*/
/* 				<button onclick="javascript:nsn_woo_ext_search_frontendFuncsObj.showNewBookmarkDialog();return false;" >&nbsp;Save current filters as*/
/* 					Bookmark</button>*/
/* 			</div>*/
/* */
/* 			<div class="nsn_woo_ext_search_block_padding_xs">*/
/* 				<span aria-hidden="true" class="glyphicon glyphicon-bookmark"></span>*/
/* 				<span id="span_have_bookmarks"></span>*/
/* 			</div>*/
/* */
/* 			<div class="nsn_woo_ext_search_block_padding_xs">*/
/* 				<button id="button_show_existing_bookmarks" onclick="javascript:nsn_woo_ext_search_frontendFuncsObj.showBookmarks();return false;" >&nbsp;*/
/* 					Select Bookmark and fill controls</button>*/
/* 			</div>*/
/* */
/* 		</fieldset>*/
/* 	</div>*/
/* 	{% endif %}*/
/* */
/* 	{% if current_user_id is not defined or current_user_id <= 0 %}*/
/* 	<div class="row">*/
/* 		<div class="nsn_woo_ext_search_block_padding_xs">*/
/* 			{#<button type="button" class="btn btn-sm btn-block">You must login to system to use Bookmarks</button>#}*/
/* 			<p class="text-info">You must login to system to use Bookmarks</p>*/
/* 		</div>*/
/* 	</div>*/
/* 	{% endif %}*/
/* */
/* 	<div class="row" style="display: none" id="div_textarea_args">*/
/* 		<div class="nsn_woo_ext_search_block_padding_xs">*/
/* 			{#<button type="button" class="btn btn-sm btn-block">You must login to system to use Bookmarks</button>#}*/
/* 			<textarea id="textarea_args" cols="50" rows="6"></textarea>*/
/* 			<input id="input_products_count" value="" size="20" maxlength="100" >*/
/* 		</div>*/
/* 	</div>*/
/* */
/* */
/* {{ library.header_block_end("Filter Controls Block End") }}*/
