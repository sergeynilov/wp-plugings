<?php

/* white-simple/filter_categories_box.twig */
class __TwigTemplate_bb09c827f5397d41f4c981c2aad51d1a587864c43f6e3b2eccebef2da12db4b3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((($this->getAttribute((isset($context["woo_ext_search_options"]) ? $context["woo_ext_search_options"] : null), "show_in_extended_search_categories_box", array()) == "yes") && (twig_length_filter($this->env, (isset($context["categories_list"]) ? $context["categories_list"] : null)) > 0))) {
            // line 2
            echo "\t";
            echo $this->getAttribute((isset($context["library"]) ? $context["library"] : null), "header_block_start", array(0 => "Categories Filter", 1 => "", 2 => "fa fa-search-plus", 3 => ((isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : null) . "block_categories")), "method");
            echo "
\t<div class=\"row nsn_woo_ext_search_block_padding_md\">

\t\t<ul class=\"nsn_woo_ext_search_ul_multiline left\" >
\t\t";
            // line 6
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["categories_list"]) ? $context["categories_list"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["next_category"]) {
                // line 7
                echo "
\t\t\t";
                // line 8
                if ( !(($this->getAttribute((isset($context["woo_ext_search_options"]) ? $context["woo_ext_search_options"] : null), "show_in_extended_search_hide_with_zero_products", array()) == "yes") &&  !$this->getAttribute($context["next_category"], "products_count", array(), "any", true, true))) {
                    // line 9
                    echo "\t\t\t<li>

\t\t\t\t<input type=\"checkbox\" id=\"";
                    // line 11
                    echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : null);
                    echo "next_category_";
                    echo $this->getAttribute($context["next_category"], "ID", array());
                    echo "\" value=\"";
                    echo $this->getAttribute($context["next_category"], "ID", array());
                    echo "\" class=\"";
                    echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : null);
                    echo "cbx_category_selection_filters\" >

\t\t\t\t";
                    // line 13
                    if ((($this->getAttribute((isset($context["woo_ext_search_options"]) ? $context["woo_ext_search_options"] : null), "run_search_by_click", array()) == "both") || ($this->getAttribute((isset($context["woo_ext_search_options"]) ? $context["woo_ext_search_options"] : null), "run_search_by_click", array()) == "link"))) {
                        // line 14
                        echo "\t\t\t\t\t<a class=\"nsn_woo_ext_search_a_link\" onclick=\"javascript:nsn_woo_ext_search_frontendFuncsObj.filter_elementClicked( 'category', '', '";
                        echo $this->getAttribute($context["next_category"], "ID", array());
                        echo "' );\treturn false;\">
\t\t\t\t";
                    }
                    // line 16
                    echo "\t\t\t\t&nbsp;";
                    echo $this->getAttribute($context["next_category"], "ID", array());
                    echo "-";
                    echo $this->getAttribute($context["next_category"], "name", array());
                    echo " ";
                    if ($this->getAttribute($context["next_category"], "products_count", array(), "any", true, true)) {
                        echo "<b>(";
                        echo $this->getAttribute($context["next_category"], "products_count", array());
                        echo ")</b>";
                    } else {
                    }
                    // line 17
                    echo "\t\t\t\t";
                    if ((($this->getAttribute((isset($context["woo_ext_search_options"]) ? $context["woo_ext_search_options"] : null), "run_search_by_click", array()) == "both") || ($this->getAttribute((isset($context["woo_ext_search_options"]) ? $context["woo_ext_search_options"] : null), "run_search_by_click", array()) == "link"))) {
                        // line 18
                        echo "\t\t\t\t\t</a>
\t\t\t\t";
                    }
                    // line 20
                    echo "
\t\t\t</li>
\t\t\t";
                }
                // line 23
                echo "
\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['next_category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 25
            echo "\t\t</ul>

\t</div>
\t";
            // line 28
            echo $this->getAttribute((isset($context["library"]) ? $context["library"] : null), "header_block_end", array(0 => "Categories Filter Block End"), "method");
            echo "
";
        }
    }

    public function getTemplateName()
    {
        return "white-simple/filter_categories_box.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  97 => 28,  92 => 25,  85 => 23,  80 => 20,  76 => 18,  73 => 17,  61 => 16,  55 => 14,  53 => 13,  42 => 11,  38 => 9,  36 => 8,  33 => 7,  29 => 6,  21 => 2,  19 => 1,);
    }
}
/* {% if woo_ext_search_options.show_in_extended_search_categories_box == 'yes'  and categories_list | length > 0  %}*/
/* 	{{ library.header_block_start("Categories Filter", "", "fa fa-search-plus", plugin_prefix ~ "block_categories") }}*/
/* 	<div class="row nsn_woo_ext_search_block_padding_md">*/
/* */
/* 		<ul class="nsn_woo_ext_search_ul_multiline left" >*/
/* 		{% for next_category in categories_list %}*/
/* */
/* 			{% if not ( woo_ext_search_options.show_in_extended_search_hide_with_zero_products == "yes" and next_category.products_count is not defined ) %}*/
/* 			<li>*/
/* */
/* 				<input type="checkbox" id="{{ plugin_prefix }}next_category_{{ next_category.ID }}" value="{{ next_category.ID }}" class="{{ plugin_prefix }}cbx_category_selection_filters" >*/
/* */
/* 				{% if woo_ext_search_options.run_search_by_click == 'both' or woo_ext_search_options.run_search_by_click == 'link' %}*/
/* 					<a class="nsn_woo_ext_search_a_link" onclick="javascript:nsn_woo_ext_search_frontendFuncsObj.filter_elementClicked( 'category', '', '{{ next_category.ID }}' );	return false;">*/
/* 				{% endif %}*/
/* 				&nbsp;{{ next_category.ID }}-{{ next_category.name }} {% if next_category.products_count is defined %}<b>({{ next_category.products_count }})</b>{% else %}{% endif %}*/
/* 				{% if woo_ext_search_options.run_search_by_click == 'both' or woo_ext_search_options.run_search_by_click == 'link' %}*/
/* 					</a>*/
/* 				{% endif %}*/
/* */
/* 			</li>*/
/* 			{% endif %}*/
/* */
/* 		{% endfor %}*/
/* 		</ul>*/
/* */
/* 	</div>*/
/* 	{{ library.header_block_end("Categories Filter Block End") }}*/
/* {% endif %}*/
