<?php

/* white-simple/filter_controls_box.twig */
class __TwigTemplate_a10c0c3b2c51899f725a4b6a65fcfdc35490502e87d067f89aef053fb72df99c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo $this->getAttribute((isset($context["library"]) ? $context["library"] : null), "header_block_start", array(0 => "Filter Controls", 1 => "  ", 2 => "fa fa-search-plus", 3 => ((isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : null) . "block_controls")), "method");
        echo "

<div class=\"row nsn_woo_ext_search_block_padding_md\">
\t<button type=\"button\" class=\"search-submit  pull-left\" onclick=\"javascript:nsn_woo_ext_search_frontendFuncsObj.clearAllInputs();return false;\" >Clear All Inputs</button>
\t";
        // line 5
        if ($this->getAttribute((isset($context["woo_ext_search_options"]) ? $context["woo_ext_search_options"] : null), "service_post_for_help_text", array(), "any", true, true)) {
            // line 6
            echo "\t\t<span aria-hidden=\"true\" class=\"glyphicon glyphicon-info-sign pull-right\">
\t\t\t\t\t<a class=\"nsn_woo_ext_search_a_link\" target=\"_blank\" href=\"";
            // line 7
            echo call_user_func_array($this->env->getFilter('make_post_url')->getCallable(), array($this->getAttribute((isset($context["woo_ext_search_options"]) ? $context["woo_ext_search_options"] : null), "service_post_for_help_text", array())));
            echo "\">Help</a>
\t\t\t\t</span>
\t";
        }
        // line 10
        echo "\t";
        // line 11
        echo "</div>



";
        // line 15
        if (($this->getAttribute((isset($context["woo_ext_search_options"]) ? $context["woo_ext_search_options"] : null), "show_in_extended_search_post_title", array()) == "yes")) {
            // line 16
            echo "\t<div class=\"row nsn_woo_ext_search_block_padding_sm\">
\t\t<div class=\"form-group\">
\t\t\t<label for=\"";
            // line 18
            echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : null);
            echo "post_title\" class=\"col-xs-12 col-sm-5 control-label\">Title/Content</label>
\t\t\t<div class=\"col-xs-12 col-sm-7 padding_right_sm padding_left_sm\">
\t\t\t\t<input type=\"text\" value=\"\" id=\"";
            // line 20
            echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : null);
            echo "post_title\" size=\"20\" maxlength=\"100\" class=\"form-control\" onkeyup=\"javascript:nsn_woo_ext_search_frontendFuncsObj.post_title_onChange();\">
\t\t\t</div>
\t\t</div>
\t\t<div class=\"form-group\">
\t\t\t<label for=\"";
            // line 24
            echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : null);
            echo "cbx_partial_title\" class=\"col-xs-12 col-sm-5 control-label\">Partial</label>
\t\t\t<div class=\"col-xs-12 col-sm-7 \">
\t\t\t\t<input type=\"checkbox\" value=\"1\" id=\"";
            // line 26
            echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : null);
            echo "cbx_partial_title\" checked>
\t\t\t</div>
\t\t</div>
\t</div>
";
        }
        // line 31
        echo "

\t";
        // line 33
        if (($this->getAttribute((isset($context["woo_ext_search_options"]) ? $context["woo_ext_search_options"] : null), "show_in_extended_search_sku", array()) == "yes")) {
            // line 34
            echo "
\t\t<div class=\"row nsn_woo_ext_search_block_padding_sm\">
\t\t\t<div class=\"form-group\">
\t\t\t\t<label for=\"";
            // line 37
            echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : null);
            echo "input_sku\" class=\"col-xs-12 col-sm-5 control-label\">Enter sku</label>
\t\t\t\t<div class=\"col-xs-12 col-sm-7 padding_right_sm padding_left_sm\">
\t\t\t\t\t<input type=\"text\" value=\"\" id=\"";
            // line 39
            echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : null);
            echo "input_sku\" size=\"20\" maxlength=\"100\" class=\"form-control\" onkeyup=\"javascript:nsn_woo_ext_search_frontendFuncsObj.sku_onChange();\">
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<label for=\"";
            // line 43
            echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : null);
            echo "cbx_partial_sku\" class=\"col-xs-12 col-sm-5 control-label\">Partial</label>
\t\t\t\t<div class=\"col-xs-12 col-sm-7 \">
\t\t\t\t\t<input type=\"checkbox\" value=\"1\" id=\"";
            // line 45
            echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : null);
            echo "cbx_partial_sku\" class=\"\" >
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>

\t";
        }
        // line 51
        echo "

<div class=\"row nsn_woo_ext_search_block_padding_sm\">
\t<div class=\"form-group\">
\t\t<label for=\"";
        // line 55
        echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : null);
        echo "cbx_only_in_stock\" class=\"col-xs-12 col-sm-5 control-label\">Only in stock</label>
\t\t<div class=\"col-xs-12 col-sm-7 \">
\t\t\t<input type=\"checkbox\" value=\"1\" id=\"";
        // line 57
        echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : null);
        echo "cbx_only_in_stock\" >
\t\t</div>
\t</div>
</div>

<div class=\"row nsn_woo_ext_search_block_padding_sm\">
\t<div class=\"form-group\">
\t\t<label for=\"";
        // line 64
        echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : null);
        echo "orderby\" class=\"col-xs-12 col-sm-5 control-label\">Order by</label>
\t\t<div class=\"col-xs-12 col-sm-7 padding_right_sm padding_left_sm\">
\t\t\t<select name=\"";
        // line 66
        echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : null);
        echo "orderby\" id=\"";
        echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : null);
        echo "orderby\" class=\"form-control\">
\t\t\t\t<option value=\"popularity\">By popularity</option>
\t\t\t\t<option value=\"rating\">By average rating</option>
\t\t\t\t<option value=\"date\" selected=\"selected\">By newness</option>
\t\t\t\t<option value=\"price\">By price: low to high</option>
\t\t\t\t<option value=\"price-desc\">By price: high to low</option>
\t\t\t</select>
\t\t</div>
\t</div>
</div>



";
        // line 79
        if ((array_key_exists("products_pagination_per_page_list", $context) && (twig_length_filter($this->env, (isset($context["products_pagination_per_page_list"]) ? $context["products_pagination_per_page_list"] : null)) > 0))) {
            // line 80
            echo "\t<div class=\"row nsn_woo_ext_search_block_padding_sm\">
\t\t<div class=\"form-group\">
\t\t\t<label for=\"";
            // line 82
            echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : null);
            echo "items_per_page\" class=\"col-xs-12 col-sm-5 control-label\">Products per page </label>
\t\t\t<div class=\"col-xs-12 col-sm-7 padding_right_sm padding_left_sm\">
\t\t\t\t<select name=\"";
            // line 84
            echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : null);
            echo "items_per_page\" id=\"";
            echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : null);
            echo "items_per_page\" class=\"form-control\">
\t\t\t\t\t<option value=\"\">  -Select items per page-  </option>
\t\t\t\t\t";
            // line 86
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["products_pagination_per_page_list"]) ? $context["products_pagination_per_page_list"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["next_products_pagination_per_page"]) {
                // line 87
                echo "\t\t\t\t\t\t<option value=\"";
                echo $context["next_products_pagination_per_page"];
                echo "\" ";
                if (($this->getAttribute((isset($context["woo_ext_search_options"]) ? $context["woo_ext_search_options"] : null), "products_per_page", array()) == $context["next_products_pagination_per_page"])) {
                    echo "selected";
                }
                echo " >By ";
                echo $context["next_products_pagination_per_page"];
                // line 88
                echo "</option>
\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['next_products_pagination_per_page'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 90
            echo "\t\t\t\t</select>
\t\t\t</div>
\t\t</div>
\t</div>
";
        }
        // line 95
        echo "



<div class=\"row nsn_woo_ext_search_block_padding_sm text-center\">
\t<button type=\"button\" class=\"search-submit\" onclick=\"javascript:nsn_woo_ext_search_frontendFuncsObj.runFilterSearch('loadWooProducts',1);return false;\" >Search</button>
</div>


";
        // line 104
        if (((($this->getAttribute((isset($context["woo_ext_search_options"]) ? $context["woo_ext_search_options"] : null), "show_bookmark_functionality", array()) == "yes") && array_key_exists("current_user_id", $context)) && ((isset($context["current_user_id"]) ? $context["current_user_id"] : null) > 0))) {
            // line 105
            echo "\t<div class=\"row nsn_woo_ext_search_block_padding_sm\">
\t\t<fieldset class=\"nsn_woo_ext_search_thin_border nsn_woo_ext_search_block_margin_sm nsn_woo_ext_search_block_padding_sm\">
\t\t\t<legend >Bookmarks:</legend>

\t\t\t<div class=\"nsn_woo_ext_search_block_padding_sm\">
\t\t\t\t<button onclick=\"javascript:nsn_woo_ext_search_frontendFuncsObj.showNewBookmarkDialog();return false;\" >&nbsp;Save current filters as Bookmark</button>
\t\t\t</div>

\t\t\t<div class=\"nsn_woo_ext_search_block_padding_sm\">
\t\t\t\t<span aria-hidden=\"true\" class=\"glyphicon glyphicon-bookmark\"></span>
\t\t\t\t<span id=\"span_have_bookmarks\"></span>
\t\t\t</div>

\t\t\t<div class=\"nsn_woo_ext_search_block_padding_sm\">
\t\t\t\t<button id=\"button_show_existing_bookmarks\" onclick=\"javascript:nsn_woo_ext_search_frontendFuncsObj.showBookmarks();return false;\" >&nbsp;
\t\t\t\t\tSelect Bookmark and fill controls</button>
\t\t\t</div>

\t\t</fieldset>
\t</div>
";
        }
        // line 126
        echo "
\t";
        // line 127
        if (( !array_key_exists("current_user_id", $context) || ((isset($context["current_user_id"]) ? $context["current_user_id"] : null) <= 0))) {
            // line 128
            echo "\t\t<div class=\"row \">
\t\t\t<div class=\"nsn_woo_ext_search_block_padding_sm\">
\t\t\t\t<p class=\"text-info\">You must login to system to use Bookmarks</p>
\t\t\t</div>
\t\t</div>
\t";
        }
        // line 134
        echo "
<div class=\"row \" style=\"display: none\" id=\"div_textarea_args\">
\t<div class=\"nsn_woo_ext_search_block_padding_sm\">
\t\t<textarea id=\"textarea_args\" cols=\"50\" rows=\"6\"></textarea>
\t\t<input id=\"input_products_count\" value=\"\" size=\"20\" maxlength=\"100\" >
\t</div>
</div>


";
        // line 143
        echo $this->getAttribute((isset($context["library"]) ? $context["library"] : null), "header_block_end", array(0 => "Filter Controls Block End"), "method");
    }

    public function getTemplateName()
    {
        return "white-simple/filter_controls_box.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  262 => 143,  251 => 134,  243 => 128,  241 => 127,  238 => 126,  215 => 105,  213 => 104,  202 => 95,  195 => 90,  188 => 88,  179 => 87,  175 => 86,  168 => 84,  163 => 82,  159 => 80,  157 => 79,  139 => 66,  134 => 64,  124 => 57,  119 => 55,  113 => 51,  104 => 45,  99 => 43,  92 => 39,  87 => 37,  82 => 34,  80 => 33,  76 => 31,  68 => 26,  63 => 24,  56 => 20,  51 => 18,  47 => 16,  45 => 15,  39 => 11,  37 => 10,  31 => 7,  28 => 6,  26 => 5,  19 => 1,);
    }
}
/* {{ library.header_block_start( "Filter Controls", "  ", "fa fa-search-plus", plugin_prefix ~ "block_controls" ) }}*/
/* */
/* <div class="row nsn_woo_ext_search_block_padding_md">*/
/* 	<button type="button" class="search-submit  pull-left" onclick="javascript:nsn_woo_ext_search_frontendFuncsObj.clearAllInputs();return false;" >Clear All Inputs</button>*/
/* 	{% if woo_ext_search_options.service_post_for_help_text is defined %}*/
/* 		<span aria-hidden="true" class="glyphicon glyphicon-info-sign pull-right">*/
/* 					<a class="nsn_woo_ext_search_a_link" target="_blank" href="{{ woo_ext_search_options.service_post_for_help_text | make_post_url }}">Help</a>*/
/* 				</span>*/
/* 	{% endif %}*/
/* 	{#<span >{{ library.responsive_helper() }}</span>#}*/
/* </div>*/
/* */
/* */
/* */
/* {% if woo_ext_search_options.show_in_extended_search_post_title =='yes'  %}*/
/* 	<div class="row nsn_woo_ext_search_block_padding_sm">*/
/* 		<div class="form-group">*/
/* 			<label for="{{ plugin_prefix }}post_title" class="col-xs-12 col-sm-5 control-label">Title/Content</label>*/
/* 			<div class="col-xs-12 col-sm-7 padding_right_sm padding_left_sm">*/
/* 				<input type="text" value="" id="{{ plugin_prefix }}post_title" size="20" maxlength="100" class="form-control" onkeyup="javascript:nsn_woo_ext_search_frontendFuncsObj.post_title_onChange();">*/
/* 			</div>*/
/* 		</div>*/
/* 		<div class="form-group">*/
/* 			<label for="{{ plugin_prefix }}cbx_partial_title" class="col-xs-12 col-sm-5 control-label">Partial</label>*/
/* 			<div class="col-xs-12 col-sm-7 ">*/
/* 				<input type="checkbox" value="1" id="{{ plugin_prefix }}cbx_partial_title" checked>*/
/* 			</div>*/
/* 		</div>*/
/* 	</div>*/
/* {% endif %}*/
/* */
/* */
/* 	{% if woo_ext_search_options.show_in_extended_search_sku =='yes'  %}*/
/* */
/* 		<div class="row nsn_woo_ext_search_block_padding_sm">*/
/* 			<div class="form-group">*/
/* 				<label for="{{ plugin_prefix }}input_sku" class="col-xs-12 col-sm-5 control-label">Enter sku</label>*/
/* 				<div class="col-xs-12 col-sm-7 padding_right_sm padding_left_sm">*/
/* 					<input type="text" value="" id="{{ plugin_prefix }}input_sku" size="20" maxlength="100" class="form-control" onkeyup="javascript:nsn_woo_ext_search_frontendFuncsObj.sku_onChange();">*/
/* 				</div>*/
/* 			</div>*/
/* 			<div class="form-group">*/
/* 				<label for="{{ plugin_prefix }}cbx_partial_sku" class="col-xs-12 col-sm-5 control-label">Partial</label>*/
/* 				<div class="col-xs-12 col-sm-7 ">*/
/* 					<input type="checkbox" value="1" id="{{ plugin_prefix }}cbx_partial_sku" class="" >*/
/* 				</div>*/
/* 			</div>*/
/* 		</div>*/
/* */
/* 	{% endif %}*/
/* */
/* */
/* <div class="row nsn_woo_ext_search_block_padding_sm">*/
/* 	<div class="form-group">*/
/* 		<label for="{{ plugin_prefix }}cbx_only_in_stock" class="col-xs-12 col-sm-5 control-label">Only in stock</label>*/
/* 		<div class="col-xs-12 col-sm-7 ">*/
/* 			<input type="checkbox" value="1" id="{{ plugin_prefix }}cbx_only_in_stock" >*/
/* 		</div>*/
/* 	</div>*/
/* </div>*/
/* */
/* <div class="row nsn_woo_ext_search_block_padding_sm">*/
/* 	<div class="form-group">*/
/* 		<label for="{{ plugin_prefix }}orderby" class="col-xs-12 col-sm-5 control-label">Order by</label>*/
/* 		<div class="col-xs-12 col-sm-7 padding_right_sm padding_left_sm">*/
/* 			<select name="{{ plugin_prefix }}orderby" id="{{ plugin_prefix }}orderby" class="form-control">*/
/* 				<option value="popularity">By popularity</option>*/
/* 				<option value="rating">By average rating</option>*/
/* 				<option value="date" selected="selected">By newness</option>*/
/* 				<option value="price">By price: low to high</option>*/
/* 				<option value="price-desc">By price: high to low</option>*/
/* 			</select>*/
/* 		</div>*/
/* 	</div>*/
/* </div>*/
/* */
/* */
/* */
/* {% if products_pagination_per_page_list is defined and products_pagination_per_page_list | length > 0 %}*/
/* 	<div class="row nsn_woo_ext_search_block_padding_sm">*/
/* 		<div class="form-group">*/
/* 			<label for="{{ plugin_prefix }}items_per_page" class="col-xs-12 col-sm-5 control-label">Products per page </label>*/
/* 			<div class="col-xs-12 col-sm-7 padding_right_sm padding_left_sm">*/
/* 				<select name="{{ plugin_prefix }}items_per_page" id="{{ plugin_prefix }}items_per_page" class="form-control">*/
/* 					<option value="">  -Select items per page-  </option>*/
/* 					{% for next_products_pagination_per_page in products_pagination_per_page_list %}*/
/* 						<option value="{{ next_products_pagination_per_page }}" {% if woo_ext_search_options.products_per_page == next_products_pagination_per_page %}selected{% endif %} >By {{ next_products_pagination_per_page*/
/* 							}}</option>*/
/* 					{% endfor %}*/
/* 				</select>*/
/* 			</div>*/
/* 		</div>*/
/* 	</div>*/
/* {% endif %}*/
/* */
/* */
/* */
/* */
/* <div class="row nsn_woo_ext_search_block_padding_sm text-center">*/
/* 	<button type="button" class="search-submit" onclick="javascript:nsn_woo_ext_search_frontendFuncsObj.runFilterSearch('loadWooProducts',1);return false;" >Search</button>*/
/* </div>*/
/* */
/* */
/* {% if woo_ext_search_options.show_bookmark_functionality =='yes' and current_user_id is defined and current_user_id > 0 %}*/
/* 	<div class="row nsn_woo_ext_search_block_padding_sm">*/
/* 		<fieldset class="nsn_woo_ext_search_thin_border nsn_woo_ext_search_block_margin_sm nsn_woo_ext_search_block_padding_sm">*/
/* 			<legend >Bookmarks:</legend>*/
/* */
/* 			<div class="nsn_woo_ext_search_block_padding_sm">*/
/* 				<button onclick="javascript:nsn_woo_ext_search_frontendFuncsObj.showNewBookmarkDialog();return false;" >&nbsp;Save current filters as Bookmark</button>*/
/* 			</div>*/
/* */
/* 			<div class="nsn_woo_ext_search_block_padding_sm">*/
/* 				<span aria-hidden="true" class="glyphicon glyphicon-bookmark"></span>*/
/* 				<span id="span_have_bookmarks"></span>*/
/* 			</div>*/
/* */
/* 			<div class="nsn_woo_ext_search_block_padding_sm">*/
/* 				<button id="button_show_existing_bookmarks" onclick="javascript:nsn_woo_ext_search_frontendFuncsObj.showBookmarks();return false;" >&nbsp;*/
/* 					Select Bookmark and fill controls</button>*/
/* 			</div>*/
/* */
/* 		</fieldset>*/
/* 	</div>*/
/* {% endif %}*/
/* */
/* 	{% if current_user_id is not defined or current_user_id <= 0 %}*/
/* 		<div class="row ">*/
/* 			<div class="nsn_woo_ext_search_block_padding_sm">*/
/* 				<p class="text-info">You must login to system to use Bookmarks</p>*/
/* 			</div>*/
/* 		</div>*/
/* 	{% endif %}*/
/* */
/* <div class="row " style="display: none" id="div_textarea_args">*/
/* 	<div class="nsn_woo_ext_search_block_padding_sm">*/
/* 		<textarea id="textarea_args" cols="50" rows="6"></textarea>*/
/* 		<input id="input_products_count" value="" size="20" maxlength="100" >*/
/* 	</div>*/
/* </div>*/
/* */
/* */
/* {{ library.header_block_end("Filter Controls Block End") }}*/
