<?php

/* white-simple/filter_rating_box.twig */
class __TwigTemplate_1a97e69f136b0f815baf6110ac23cb824a46796e44f7f012e3351a5fbae8fb14 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (($this->getAttribute((isset($context["woo_ext_search_options"]) ? $context["woo_ext_search_options"] : $this->getContext($context, "woo_ext_search_options")), "show_in_extended_search_rating", array()) == "yes")) {
            // line 2
            echo "\t";
            echo $this->getAttribute((isset($context["library"]) ? $context["library"] : $this->getContext($context, "library")), "header_block_start", array(0 => "Filter on rating", 1 => "", 2 => "fa fa-search-plus", 3 => ((isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : $this->getContext($context, "plugin_prefix")) . "block_rating")), "method");
            echo "
\t<div class=\"row\">

\t\t<div class=\"nsn_woo_ext_search_block_padding_sm\">
\t\t\tSelect Rating range from 1 till 5:<br>
\t\t\t<dt id=\"slider_rating\"></dt>
\t\t\t<dd>
\t\t\t\t<input type=\"text\" readonly id=\"slider_rating_range_info\" style=\"width: 90%\" >
\t\t\t</dd>

\t\t</div>

\t</div>
\t";
            // line 15
            echo $this->getAttribute((isset($context["library"]) ? $context["library"] : $this->getContext($context, "library")), "header_block_end", array(0 => "Filter on rating Block End"), "method");
            echo "
";
        }
    }

    public function getTemplateName()
    {
        return "white-simple/filter_rating_box.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  38 => 15,  21 => 2,  19 => 1,);
    }
}
/* {% if woo_ext_search_options.show_in_extended_search_rating == 'yes' %}*/
/* 	{{ library.header_block_start("Filter on rating", "", "fa fa-search-plus", plugin_prefix ~ "block_rating" ) }}*/
/* 	<div class="row">*/
/* */
/* 		<div class="nsn_woo_ext_search_block_padding_sm">*/
/* 			Select Rating range from 1 till 5:<br>*/
/* 			<dt id="slider_rating"></dt>*/
/* 			<dd>*/
/* 				<input type="text" readonly id="slider_rating_range_info" style="width: 90%" >*/
/* 			</dd>*/
/* */
/* 		</div>*/
/* */
/* 	</div>*/
/* 	{{ library.header_block_end( "Filter on rating Block End") }}*/
/* {% endif %}*/
