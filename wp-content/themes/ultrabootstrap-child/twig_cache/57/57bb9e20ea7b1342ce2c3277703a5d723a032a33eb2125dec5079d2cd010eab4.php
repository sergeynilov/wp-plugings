<?php

/* white-simple/woo_ext_search_form.twig */
class __TwigTemplate_52e5894f842d4ff4df0712805155195bd021b0be2cdc695130b04b2d5e79ea47 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("white-simple/base.twig", "white-simple/woo_ext_search_form.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "white-simple/base.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "\t";
        // line 4
        echo "
\t<script type=\"text/javascript\">
\t\t/*<![CDATA[*/
\t\tvar nsn_woo_ext_search_frontendFuncsObj;
\t\tvar nsn_woo_ext_search_attrs_options_list;
\t\tvar nsn_woo_ext_search_bookmarks_list;
\t\tvar nsn_woo_ext_search_ratingSlider;

\t\tjQuery(document).ready(function (\$) {

\t\t\tvar app_params = ";
        // line 14
        echo twig_jsonencode_filter((isset($context["app_params"]) ? $context["app_params"] : $this->getContext($context, "app_params")));
        echo ";
//\t\t\talert( \"app_params::\"+var_dump( app_params ) )
\t\t\tnsn_woo_ext_search_attrs_options_list= ";
        // line 16
        echo twig_jsonencode_filter((isset($context["attrs_options_list"]) ? $context["attrs_options_list"] : $this->getContext($context, "attrs_options_list")));
        echo "
\t\t\tvar show_bookmark_alerts= '";
        // line 17
        echo $this->getAttribute((isset($context["woo_ext_search_options"]) ? $context["woo_ext_search_options"] : $this->getContext($context, "woo_ext_search_options")), "show_bookmark_alerts", array());
        echo "'
//\t\t\talert( \"show_bookmark_alerts::\"+show_bookmark_alerts )


\t\t\tnsn_woo_ext_search_frontendFuncsObj = new nsn_woo_ext_search_frontendFuncs( app_params, nsn_woo_ext_search_attrs_options_list, show_bookmark_alerts );

\t\t\tnsn_woo_ext_search_frontendFuncsObj.initRating();

//\t\t\tnsn_woo_ext_search_frontendFuncsObj.clearAllInputs();
\t\t\tnsn_woo_ext_search_frontendFuncsObj.runFilterSearch(\"loadWooProducts\",1);
//\t\t\tnsn_woo_ext_search_ratingSlider = \$(\"#\"+this_m_plugin_prefix+\"rating_slider\").bootstrapSlider();
//\t\t\talert( \"initRating  nsn_woo_ext_search_ratingSlider::\"+var_dump(nsn_woo_ext_search_ratingSlider) )

\t\t\tvar current_user_id= parseInt('";
        // line 30
        echo (isset($context["current_user_id"]) ? $context["current_user_id"] : $this->getContext($context, "current_user_id"));
        echo "')
//\t\t\talert(\"current_user_id::\"+current_user_id)
\t\t\tvar show_bookmark_functionality= '";
        // line 32
        echo $this->getAttribute((isset($context["woo_ext_search_options"]) ? $context["woo_ext_search_options"] : $this->getContext($context, "woo_ext_search_options")), "show_bookmark_functionality", array());
        echo "'
\t\t\tif ( show_bookmark_functionality == 'yes' && current_user_id > 0 ) {
\t\t\t\tnsn_woo_ext_search_frontendFuncsObj.loadBookmarks();
\t\t\t}
\t\t\t\$('[data-toggle=\"tooltip\"]').tooltip()



//\t\t\t\$(\"#ex2\").bootstrapSlider();

//\t\t\tratingSlider.slider('setValue', 0).slider('setValue', 5);
\t\t\t//\$.fn.slider namespace is already bound. Use the \$.fn.bootstrapSlider namespace instead.

//\t\t\talert( \"AGTER::\"+var_dump(1) )
\t\t});

\t\tfunction testValue1() {
//\t\t\tvar ratingSlider = \$(\"#\"+this_m_plugin_prefix+\"rating_slider\").bootstrapSlider();
//\t\t\talert( \"testValue1 ratingSlider::\"+var_dump(ratingSlider) )
\t\t\talert( \"testValue1 nsn_woo_ext_search_ratingSlider::\"+var_dump(nsn_woo_ext_search_ratingSlider) )
//\t\t\tnsn_woo_ext_search_ratingSlider.slider('setValue', 1).slider('setValue', 5);
\t\t\tnsn_woo_ext_search_ratingSlider.slider.setValue(1)  //.slider('setValue', 5);
//\t\t\tnsn_woo_ext_search_ratingSlider.setValue(1)
\t\t}

\t\tfunction testValue2() {
//\t\t\tvar ratingSlider = \$(\"#\"+this_m_plugin_prefix+\"rating_slider\").bootstrapSlider();
//\t\t\talert( \"testValue1 ratingSlider::\"+var_dump(ratingSlider) )
\t\t\talert( \"testValue1 nsn_woo_ext_search_ratingSlider::\"+var_dump(nsn_woo_ext_search_ratingSlider) )
//\t\t\tnsn_woo_ext_search_ratingSlider.bootstrapSlider('setValue', 1).bootstrapSlider('setValue', 5);
\t\t\tnsn_woo_ext_search_ratingSlider.bootstrapSlider.setValue(1)  //.bootstrapSlider('setValue', 5);
//\t\t\tnsn_woo_ext_search_ratingSlider.setValue(1)  //.bootstrapSlider('setValue', 5);
\t\t}

\t\t/*]]>*/
\t</script>


\t";
        // line 71
        echo "\t\t";
        // line 72
        echo "\t";
        // line 73
        echo "
\t<div class=\"row\"> <!-- search by State page start -->

\t\t<div class=\"col-xs-12 col-sm-4 nsn_woo_ext_search_block_padding_sm\">
\t\t\t";
        // line 77
        $this->loadTemplate("white-simple/filter_controls_box.twig", "white-simple/woo_ext_search_form.twig", 77)->display($context);
        // line 78
        echo "\t\t\t";
        $this->loadTemplate("white-simple/filter_price_box.twig", "white-simple/woo_ext_search_form.twig", 78)->display($context);
        // line 79
        echo "\t\t\t";
        $this->loadTemplate("white-simple/filter_categories_box.twig", "white-simple/woo_ext_search_form.twig", 79)->display($context);
        // line 80
        echo "\t\t\t";
        $this->loadTemplate("white-simple/filter_attrs_box.twig", "white-simple/woo_ext_search_form.twig", 80)->display($context);
        // line 81
        echo "\t\t\t";
        $this->loadTemplate("white-simple/filter_rating_box.twig", "white-simple/woo_ext_search_form.twig", 81)->display($context);
        // line 82
        echo "            ";
        $this->loadTemplate("white-simple/filter_tags_box.twig", "white-simple/woo_ext_search_form.twig", 82)->display($context);
        // line 83
        echo "\t\t</div>


\t\t<div class=\"col-xs-12 col-sm-8 nsn_woo_ext_search_block_padding_sm\">
\t\t\t<div class=\"row\">
\t\t\t\t<div id=\"div_products_list\"></div>
\t\t\t\t";
        // line 90
        echo "\t\t\t</div>
\t\t\t";
        // line 92
        echo "\t\t\t\t";
        // line 93
        echo "\t\t\t";
        // line 94
        echo "\t\t\t";
        // line 95
        echo "\t\t\t\t";
        // line 96
        echo "\t\t\t";
        // line 97
        echo "\t\t</div>

\t</div> <!-- <div class=\"row\"> --> <!-- search by State page end -->

\t";
        // line 101
        $this->loadTemplate("white-simple/new_bookmark_dialog.twig", "white-simple/woo_ext_search_form.twig", 101)->display($context);
        // line 102
        echo "\t";
        $this->loadTemplate("white-simple/existing_bookmarks_dialog.twig", "white-simple/woo_ext_search_form.twig", 102)->display($context);
        // line 103
        echo "
";
    }

    public function getTemplateName()
    {
        return "white-simple/woo_ext_search_form.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  175 => 103,  172 => 102,  170 => 101,  164 => 97,  162 => 96,  160 => 95,  158 => 94,  156 => 93,  154 => 92,  151 => 90,  143 => 83,  140 => 82,  137 => 81,  134 => 80,  131 => 79,  128 => 78,  126 => 77,  120 => 73,  118 => 72,  116 => 71,  75 => 32,  70 => 30,  54 => 17,  50 => 16,  45 => 14,  33 => 4,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends "white-simple/base.twig" %}*/
/* {% block content %}*/
/* 	{# library.twig woo_ext_search_form  #}*/
/* */
/* 	<script type="text/javascript">*/
/* 		/*<![CDATA[*//* */
/* 		var nsn_woo_ext_search_frontendFuncsObj;*/
/* 		var nsn_woo_ext_search_attrs_options_list;*/
/* 		var nsn_woo_ext_search_bookmarks_list;*/
/* 		var nsn_woo_ext_search_ratingSlider;*/
/* */
/* 		jQuery(document).ready(function ($) {*/
/* */
/* 			var app_params = {{ app_params|json_encode|raw }};*/
/* //			alert( "app_params::"+var_dump( app_params ) )*/
/* 			nsn_woo_ext_search_attrs_options_list= {{ attrs_options_list |json_encode|raw  }}*/
/* 			var show_bookmark_alerts= '{{ woo_ext_search_options.show_bookmark_alerts }}'*/
/* //			alert( "show_bookmark_alerts::"+show_bookmark_alerts )*/
/* */
/* */
/* 			nsn_woo_ext_search_frontendFuncsObj = new nsn_woo_ext_search_frontendFuncs( app_params, nsn_woo_ext_search_attrs_options_list, show_bookmark_alerts );*/
/* */
/* 			nsn_woo_ext_search_frontendFuncsObj.initRating();*/
/* */
/* //			nsn_woo_ext_search_frontendFuncsObj.clearAllInputs();*/
/* 			nsn_woo_ext_search_frontendFuncsObj.runFilterSearch("loadWooProducts",1);*/
/* //			nsn_woo_ext_search_ratingSlider = $("#"+this_m_plugin_prefix+"rating_slider").bootstrapSlider();*/
/* //			alert( "initRating  nsn_woo_ext_search_ratingSlider::"+var_dump(nsn_woo_ext_search_ratingSlider) )*/
/* */
/* 			var current_user_id= parseInt('{{ current_user_id }}')*/
/* //			alert("current_user_id::"+current_user_id)*/
/* 			var show_bookmark_functionality= '{{ woo_ext_search_options.show_bookmark_functionality }}'*/
/* 			if ( show_bookmark_functionality == 'yes' && current_user_id > 0 ) {*/
/* 				nsn_woo_ext_search_frontendFuncsObj.loadBookmarks();*/
/* 			}*/
/* 			$('[data-toggle="tooltip"]').tooltip()*/
/* */
/* */
/* */
/* //			$("#ex2").bootstrapSlider();*/
/* */
/* //			ratingSlider.slider('setValue', 0).slider('setValue', 5);*/
/* 			//$.fn.slider namespace is already bound. Use the $.fn.bootstrapSlider namespace instead.*/
/* */
/* //			alert( "AGTER::"+var_dump(1) )*/
/* 		});*/
/* */
/* 		function testValue1() {*/
/* //			var ratingSlider = $("#"+this_m_plugin_prefix+"rating_slider").bootstrapSlider();*/
/* //			alert( "testValue1 ratingSlider::"+var_dump(ratingSlider) )*/
/* 			alert( "testValue1 nsn_woo_ext_search_ratingSlider::"+var_dump(nsn_woo_ext_search_ratingSlider) )*/
/* //			nsn_woo_ext_search_ratingSlider.slider('setValue', 1).slider('setValue', 5);*/
/* 			nsn_woo_ext_search_ratingSlider.slider.setValue(1)  //.slider('setValue', 5);*/
/* //			nsn_woo_ext_search_ratingSlider.setValue(1)*/
/* 		}*/
/* */
/* 		function testValue2() {*/
/* //			var ratingSlider = $("#"+this_m_plugin_prefix+"rating_slider").bootstrapSlider();*/
/* //			alert( "testValue1 ratingSlider::"+var_dump(ratingSlider) )*/
/* 			alert( "testValue1 nsn_woo_ext_search_ratingSlider::"+var_dump(nsn_woo_ext_search_ratingSlider) )*/
/* //			nsn_woo_ext_search_ratingSlider.bootstrapSlider('setValue', 1).bootstrapSlider('setValue', 5);*/
/* 			nsn_woo_ext_search_ratingSlider.bootstrapSlider.setValue(1)  //.bootstrapSlider('setValue', 5);*/
/* //			nsn_woo_ext_search_ratingSlider.setValue(1)  //.bootstrapSlider('setValue', 5);*/
/* 		}*/
/* */
/* 		/*]]>*//* */
/* 	</script>*/
/* */
/* */
/* 	{#<div class="row">  <!-- Breadcrumbs row start -->#}*/
/* 		{#&#123;&#35;{{ components.showBreadcrumbs( breadcrumbsList, 'div_breadcrumbs' ) }}&#35;&#125;#}*/
/* 	{#</div>              <!-- Breadcrumbs row end -->#}*/
/* */
/* 	<div class="row"> <!-- search by State page start -->*/
/* */
/* 		<div class="col-xs-12 col-sm-4 nsn_woo_ext_search_block_padding_sm">*/
/* 			{% include 'white-simple/filter_controls_box.twig' %}*/
/* 			{% include 'white-simple/filter_price_box.twig' %}*/
/* 			{% include 'white-simple/filter_categories_box.twig' %}*/
/* 			{% include 'white-simple/filter_attrs_box.twig' %}*/
/* 			{% include 'white-simple/filter_rating_box.twig' %}*/
/*             {% include 'white-simple/filter_tags_box.twig' %}*/
/* 		</div>*/
/* */
/* */
/* 		<div class="col-xs-12 col-sm-8 nsn_woo_ext_search_block_padding_sm">*/
/* 			<div class="row">*/
/* 				<div id="div_products_list"></div>*/
/* 				{#{% include 'white-simple/products_list.twig' %}#}*/
/* 			</div>*/
/* 			{#<div class="row">#}*/
/* 				{#&#123;&#35;% include 'spotlight_tours.twig' %&#35;&#125;#}*/
/* 			{#</div>#}*/
/* 			{#<div class="row">#}*/
/* 				{#&#123;&#35;{% include 'blog_entries.twig' %}&#35;&#125;#}*/
/* 			{#</div>#}*/
/* 		</div>*/
/* */
/* 	</div> <!-- <div class="row"> --> <!-- search by State page end -->*/
/* */
/* 	{% include 'white-simple/new_bookmark_dialog.twig' %}*/
/* 	{% include 'white-simple/existing_bookmarks_dialog.twig' %}*/
/* */
/* {% endblock content %}*/
