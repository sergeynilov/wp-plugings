<?php

/* white-simple/lib/library.twig */
class __TwigTemplate_949e8dd442e84feb1cbbf20f8ae3b3dc6218242b18ff1bb11e106c29b017fca8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 11
        echo "  ";
        // line 12
        echo "

";
        // line 29
        echo " ";
        // line 30
        echo "
";
        // line 34
        echo "


";
        // line 46
        echo " ";
        // line 47
        echo "

";
        // line 52
        echo " ";
    }

    // line 1
    public function getresponsive_helper(...$__varargs__)
    {
        $context = $this->env->mergeGlobals(array(
            "varargs" => $__varargs__,
        ));

        $blocks = array();

        ob_start();
        try {
            // line 2
            echo "\t<img class=\"test-device\" style=\"width: 52px; height: 32px;display: inline;\">
\t";
            // line 4
            echo "\t";
            // line 5
            echo "\t";
            // line 6
            echo "\t";
            // line 7
            echo "\t\t";
            // line 8
            echo "\t\t\t";
            // line 9
            echo "\t\t";
            // line 10
            echo "\t";
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    // line 14
    public function getheader_block_start($__header_title__ = null, $__row_class__ = null, $__glyphicon__ = null, $__block_id__ = null, ...$__varargs__)
    {
        $context = $this->env->mergeGlobals(array(
            "header_title" => $__header_title__,
            "row_class" => $__row_class__,
            "glyphicon" => $__glyphicon__,
            "block_id" => $__block_id__,
            "varargs" => $__varargs__,
        ));

        $blocks = array();

        ob_start();
        try {
            // line 15
            echo "\t<div class=\"nsn_woo_ext_search_block_title\" >
\t\t";
            // line 16
            if ( !twig_test_empty((isset($context["glyphicon"]) ? $context["glyphicon"] : null))) {
                // line 17
                echo "\t\t\t<span class=\"glyphicon ";
                echo (isset($context["glyphicon"]) ? $context["glyphicon"] : null);
                echo "\" class=\"text-center\"></span>&nbsp;
\t\t";
            }
            // line 19
            echo "\t\t";
            echo (isset($context["header_title"]) ? $context["header_title"] : null);
            echo "

\t\t";
            // line 21
            if ( !twig_test_empty((isset($context["block_id"]) ? $context["block_id"] : null))) {
                // line 22
                echo "\t\t<span class=\"glyphicon glyphicon-minus pull-right nsn_woo_ext_search_a_link\" style=\"padding-right: 10px\" id=\"span_";
                echo (isset($context["block_id"]) ? $context["block_id"] : null);
                echo "_filter_minus\" onclick=\"javascript:nsn_woo_ext_search_frontendFuncsObj.block_minusonClick('";
                echo (isset($context["block_id"]) ? $context["block_id"] : null);
                echo "', true)\"></span>
\t\t<span class=\"glyphicon glyphicon-plus pull-right nsn_woo_ext_search_a_link\" style=\"padding-right: 10px; display:none;\" id=\"span_";
                // line 23
                echo (isset($context["block_id"]) ? $context["block_id"] : null);
                echo "_filter_plus\" onclick=\"javascript:nsn_woo_ext_search_frontendFuncsObj.block_minusonClick('";
                echo (isset($context["block_id"]) ? $context["block_id"] : null);
                echo "', false)\"></span>
\t\t";
            }
            // line 25
            echo "

\t</div>
    <section class=\"nsn_woo_ext_search_block_content ";
            // line 28
            echo (isset($context["row_class"]) ? $context["row_class"] : null);
            echo " \" id=\"section_block_";
            echo (isset($context["block_id"]) ? $context["block_id"] : null);
            echo "\">
";
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    // line 31
    public function getheader_block_end($__end_marker__ = null, ...$__varargs__)
    {
        $context = $this->env->mergeGlobals(array(
            "end_marker" => $__end_marker__,
            "varargs" => $__varargs__,
        ));

        $blocks = array();

        ob_start();
        try {
            // line 32
            echo "\t</section> <!-- ";
            echo (isset($context["end_marker"]) ? $context["end_marker"] : null);
            echo " -->
";
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    // line 37
    public function gettable_block_start($__header_title__ = null, $__glyphicon__ = null, $__show_top_margin_sm__ = null, ...$__varargs__)
    {
        $context = $this->env->mergeGlobals(array(
            "header_title" => $__header_title__,
            "glyphicon" => $__glyphicon__,
            "show_top_margin_sm" => $__show_top_margin_sm__,
            "varargs" => $__varargs__,
        ));

        $blocks = array();

        ob_start();
        try {
            // line 38
            echo "<section class=\"table_wrapper  \" >
\t<div class=\"nsn_woo_ext_search_block_title ";
            // line 39
            if (((isset($context["show_top_margin_sm"]) ? $context["show_top_margin_sm"] : null) == true)) {
                echo "top_margin_sm";
            }
            echo "\" >
\t\t";
            // line 40
            if ( !twig_test_empty((isset($context["glyphicon"]) ? $context["glyphicon"] : null))) {
                // line 41
                echo "\t\t\t<span class=\"glyphicon ";
                echo (isset($context["glyphicon"]) ? $context["glyphicon"] : null);
                echo "\" class=\"text-center\"></span>&nbsp;
\t\t";
            }
            // line 43
            echo "\t\t";
            echo (isset($context["header_title"]) ? $context["header_title"] : null);
            echo "
\t</div>
\t<div class=\"block_content\"> ";
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    // line 49
    public function gettable_block_end($__end_marker__ = null, ...$__varargs__)
    {
        $context = $this->env->mergeGlobals(array(
            "end_marker" => $__end_marker__,
            "varargs" => $__varargs__,
        ));

        $blocks = array();

        ob_start();
        try {
            // line 50
            echo "\t</div>
</section><!-- ";
            // line 51
            echo (isset($context["end_marker"]) ? $context["end_marker"] : null);
            echo " -->
";
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    public function getTemplateName()
    {
        return "white-simple/lib/library.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  237 => 51,  234 => 50,  222 => 49,  207 => 43,  201 => 41,  199 => 40,  193 => 39,  190 => 38,  176 => 37,  162 => 32,  150 => 31,  135 => 28,  130 => 25,  123 => 23,  116 => 22,  114 => 21,  108 => 19,  102 => 17,  100 => 16,  97 => 15,  82 => 14,  71 => 10,  69 => 9,  67 => 8,  65 => 7,  63 => 6,  61 => 5,  59 => 4,  56 => 2,  45 => 1,  41 => 52,  37 => 47,  35 => 46,  30 => 34,  27 => 30,  25 => 29,  21 => 12,  19 => 11,);
    }
}
/* {% macro responsive_helper() %}*/
/* 	<img class="test-device" style="width: 52px; height: 32px;display: inline;">*/
/* 	{#<span class="visible-xs text-danger" ><strong>xs{% if( ENVIRONMENT == 'development' ) %}<span class="text-danger" >&nbsp;D</span>{% endif %}</strong></span>#}*/
/* 	{#<span class="visible-sm text-danger" ><strong>sm{% if( ENVIRONMENT == 'development' ) %}<span class="text-danger" >&nbsp;D</span>{% endif %}</strong></span>#}*/
/* 	{#<span class="visible-md text-danger" ><strong>md{% if( ENVIRONMENT == 'development' ) %}<span class="text-danger" >&nbsp;D</span>{% endif %}</strong></span>#}*/
/* 	{#<span class="visible-lg" >#}*/
/* 		{#<strong>lg#}*/
/* 			{#{% if( ENVIRONMENT == 'development' ) %}<span  >&nbsp;D</span>{% endif %}#}*/
/* 		{#</strong>#}*/
/* 	{#</span>#}*/
/* {% endmacro %}  {# responsive_helper END #}*/
/* */
/* */
/* {% macro header_block_start(header_title, row_class, glyphicon, block_id) %}*/
/* 	<div class="nsn_woo_ext_search_block_title" >*/
/* 		{% if (glyphicon is not empty) %}*/
/* 			<span class="glyphicon {{ glyphicon }}" class="text-center"></span>&nbsp;*/
/* 		{% endif %}*/
/* 		{{ header_title }}*/
/* */
/* 		{% if block_id is not empty %}*/
/* 		<span class="glyphicon glyphicon-minus pull-right nsn_woo_ext_search_a_link" style="padding-right: 10px" id="span_{{ block_id }}_filter_minus" onclick="javascript:nsn_woo_ext_search_frontendFuncsObj.block_minusonClick('{{ block_id }}', true)"></span>*/
/* 		<span class="glyphicon glyphicon-plus pull-right nsn_woo_ext_search_a_link" style="padding-right: 10px; display:none;" id="span_{{ block_id }}_filter_plus" onclick="javascript:nsn_woo_ext_search_frontendFuncsObj.block_minusonClick('{{ block_id }}', false)"></span>*/
/* 		{% endif %}*/
/* */
/* */
/* 	</div>*/
/*     <section class="nsn_woo_ext_search_block_content {{ row_class }} " id="section_block_{{ block_id }}">*/
/* {% endmacro %} {# header_block_start #}*/
/* */
/* {% macro header_block_end(end_marker) %}*/
/* 	</section> <!-- {{ end_marker }} -->*/
/* {% endmacro %}*/
/* */
/* */
/* */
/* {% macro table_block_start(header_title, glyphicon, show_top_margin_sm) %}*/
/* <section class="table_wrapper  " >*/
/* 	<div class="nsn_woo_ext_search_block_title {% if show_top_margin_sm == true %}top_margin_sm{% endif %}" >*/
/* 		{% if (glyphicon is not empty) %}*/
/* 			<span class="glyphicon {{ glyphicon }}" class="text-center"></span>&nbsp;*/
/* 		{% endif %}*/
/* 		{{ header_title }}*/
/* 	</div>*/
/* 	<div class="block_content"> {#row #}*/
/* {% endmacro %} {# table_block_start #}*/
/* */
/* */
/* {% macro table_block_end(end_marker) %}*/
/* 	</div>*/
/* </section><!-- {{ end_marker }} -->*/
/* {% endmacro %} {# table_block_end #}*/
/* */
