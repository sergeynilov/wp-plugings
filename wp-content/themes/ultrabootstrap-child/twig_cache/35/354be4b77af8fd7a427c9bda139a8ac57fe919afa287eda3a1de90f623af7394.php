<?php

/* white-simple/filter_price_box.twig */
class __TwigTemplate_747b8385215964c9fe45483d77406c75f8efcf290712435783edbd410b9124b7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((($this->getAttribute((isset($context["woo_ext_search_options"]) ? $context["woo_ext_search_options"] : null), "show_in_extended_search_prices_box", array()) == "yes") && (twig_length_filter($this->env, (isset($context["prices_list"]) ? $context["prices_list"] : null)) > 0))) {
            // line 2
            echo "\t";
            echo $this->getAttribute((isset($context["library"]) ? $context["library"] : null), "header_block_start", array(0 => "Prices Filter", 1 => "  ", 2 => "fa fa-search-plus", 3 => ((isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : null) . "block_prices")), "method");
            echo "
\t";
            // line 4
            echo "\t<div class=\"row nsn_woo_ext_search_block_padding_md\">
\t\t<ul class=\"nsn_woo_ext_search_ul_multiline left\" >

\t\t";
            // line 7
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["prices_list"]) ? $context["prices_list"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["next_price"]) {
                // line 8
                echo "
\t\t\t";
                // line 9
                if ( !(($this->getAttribute((isset($context["woo_ext_search_options"]) ? $context["woo_ext_search_options"] : null), "show_in_extended_search_hide_with_zero_products", array()) == "yes") &&  !$this->getAttribute($context["next_price"], "products_count", array(), "any", true, true))) {
                    // line 10
                    echo "\t\t\t<li>
\t\t\t\t<input type=\"checkbox\" id=\"";
                    // line 11
                    echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : null);
                    echo "next_price_";
                    echo call_user_func_array($this->env->getFilter('float_floor')->getCallable(), array($this->getAttribute($context["next_price"], "max", array())));
                    echo "\" value=\"";
                    echo $this->getAttribute($context["next_price"], "max", array());
                    echo "\" class=\"";
                    echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : null);
                    echo "cbx_price_selection_filters\" >



\t\t\t\t";
                    // line 15
                    if ((($this->getAttribute((isset($context["woo_ext_search_options"]) ? $context["woo_ext_search_options"] : null), "run_search_by_click", array()) == "both") || ($this->getAttribute((isset($context["woo_ext_search_options"]) ? $context["woo_ext_search_options"] : null), "run_search_by_click", array()) == "link"))) {
                        // line 16
                        echo "\t\t\t\t<a class=\"nsn_woo_ext_search_a_link\" onclick=\"javascript:  nsn_woo_ext_search_frontendFuncsObj.filter_elementClicked( 'price', '', '";
                        echo $this->getAttribute($context["next_price"], "max", array());
                        echo "' );\treturn false;\t\t\">
\t\t\t\t\t";
                    }
                    // line 18
                    echo "\t\t\t\t&nbsp;";
                    echo (isset($context["currency_symbol"]) ? $context["currency_symbol"] : null);
                    echo $this->getAttribute($context["next_price"], "min", array());
                    echo "&nbsp;-&nbsp;";
                    echo $this->getAttribute($context["next_price"], "max", array());
                    echo "
\t\t\t\t";
                    // line 19
                    if ($this->getAttribute($context["next_price"], "products_count", array(), "any", true, true)) {
                        echo "<b>(";
                        echo $this->getAttribute($context["next_price"], "products_count", array());
                        echo ")</b>";
                    } else {
                    }
                    // line 20
                    echo "\t\t\t\t\t";
                    if ((($this->getAttribute((isset($context["woo_ext_search_options"]) ? $context["woo_ext_search_options"] : null), "run_search_by_click", array()) == "both") || ($this->getAttribute((isset($context["woo_ext_search_options"]) ? $context["woo_ext_search_options"] : null), "run_search_by_click", array()) == "link"))) {
                        // line 21
                        echo "\t\t\t\t</a>
\t\t\t\t";
                    }
                    // line 23
                    echo "

\t\t\t<li>
\t\t\t";
                }
                // line 27
                echo "
\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['next_price'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 29
            echo "
\t\t</ul>
\t</div>
\t";
            // line 32
            echo $this->getAttribute((isset($context["library"]) ? $context["library"] : null), "header_block_end", array(0 => "Prices Filter Block End"), "method");
            echo "
";
        }
    }

    public function getTemplateName()
    {
        return "white-simple/filter_price_box.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  104 => 32,  99 => 29,  92 => 27,  86 => 23,  82 => 21,  79 => 20,  72 => 19,  64 => 18,  58 => 16,  56 => 15,  43 => 11,  40 => 10,  38 => 9,  35 => 8,  31 => 7,  26 => 4,  21 => 2,  19 => 1,);
    }
}
/* {% if woo_ext_search_options.show_in_extended_search_prices_box == 'yes' and prices_list | length > 0 %}*/
/* 	{{ library.header_block_start("Prices Filter", "  ", "fa fa-search-plus", plugin_prefix ~ "block_prices") }}*/
/* 	{# nsn_woo_ext_search_block_padding_sm #}*/
/* 	<div class="row nsn_woo_ext_search_block_padding_md">*/
/* 		<ul class="nsn_woo_ext_search_ul_multiline left" >*/
/* */
/* 		{% for next_price in prices_list %}*/
/* */
/* 			{% if not ( woo_ext_search_options.show_in_extended_search_hide_with_zero_products == "yes" and next_price.products_count is not defined ) %}*/
/* 			<li>*/
/* 				<input type="checkbox" id="{{ plugin_prefix }}next_price_{{ next_price.max | float_floor }}" value="{{ next_price.max }}" class="{{ plugin_prefix }}cbx_price_selection_filters" >*/
/* */
/* */
/* */
/* 				{% if woo_ext_search_options.run_search_by_click == 'both' or woo_ext_search_options.run_search_by_click == 'link' %}*/
/* 				<a class="nsn_woo_ext_search_a_link" onclick="javascript:  nsn_woo_ext_search_frontendFuncsObj.filter_elementClicked( 'price', '', '{{ next_price.max }}' );	return false;		">*/
/* 					{% endif %}*/
/* 				&nbsp;{{ currency_symbol }}{{ next_price.min }}&nbsp;-&nbsp;{{	next_price.max }}*/
/* 				{% if next_price.products_count is defined %}<b>({{ next_price.products_count }})</b>{% else %}{% endif %}*/
/* 					{% if woo_ext_search_options.run_search_by_click == 'both' or woo_ext_search_options.run_search_by_click == 'link' %}*/
/* 				</a>*/
/* 				{% endif %}*/
/* */
/* */
/* 			<li>*/
/* 			{% endif %}*/
/* */
/* 		{% endfor %}*/
/* */
/* 		</ul>*/
/* 	</div>*/
/* 	{{ library.header_block_end("Prices Filter Block End") }}*/
/* {% endif %}*/
/* */
