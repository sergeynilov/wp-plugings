<?php

/* white-simple/woo_ext_search_form.twig */
class __TwigTemplate_9408df18ce5ac1210bcbd4a91b4e0625005e6bae7f2743f8c4803f7e09fda8f1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("white-simple/base.twig", "white-simple/woo_ext_search_form.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "white-simple/base.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "\t";
        // line 4
        echo "
\t<script type=\"text/javascript\">
\t\t/*<![CDATA[*/
\t\tvar nsn_woo_ext_search_frontendFuncsObj;
\t\tvar nsn_woo_ext_search_attrs_options_list;
\t\tvar nsn_woo_ext_search_bookmarks_list;
\t\tvar nsn_woo_ext_search_ratingSlider;

\t\tjQuery(document).ready(function (\$) {

\t\t\tvar app_params = ";
        // line 14
        echo twig_jsonencode_filter((isset($context["app_params"]) ? $context["app_params"] : $this->getContext($context, "app_params")));
        echo ";
//\t\t\talert( \"app_params::\"+var_dump( app_params ) )
\t\t\tnsn_woo_ext_search_attrs_options_list= ";
        // line 16
        echo twig_jsonencode_filter((isset($context["attrs_options_list"]) ? $context["attrs_options_list"] : $this->getContext($context, "attrs_options_list")));
        echo "
\t\t\tnsn_woo_ext_search_frontendFuncsObj = new nsn_woo_ext_search_frontendFuncs( app_params, nsn_woo_ext_search_attrs_options_list );

//\t\t\tnsn_woo_ext_search_frontendFuncsObj.initRating();

\t\t\tnsn_woo_ext_search_frontendFuncsObj.runFilterSearch(\"loadWooProducts\");
//\t\t\tnsn_woo_ext_search_ratingSlider = \$(\"#\"+this_m_plugin_prefix+\"rating_slider\").bootstrapSlider();
//\t\t\talert( \"initRating  nsn_woo_ext_search_ratingSlider::\"+var_dump(nsn_woo_ext_search_ratingSlider) )

\t\t\tnsn_woo_ext_search_frontendFuncsObj.loadBookmarks();
\t\t\t\$('[data-toggle=\"tooltip\"]').tooltip()



//\t\t\t\$(\"#ex2\").bootstrapSlider();

//\t\t\tratingSlider.slider('setValue', 0).slider('setValue', 5);
\t\t\t//\$.fn.slider namespace is already bound. Use the \$.fn.bootstrapSlider namespace instead.

//\t\t\talert( \"AGTER::\"+var_dump(1) )
\t\t});

\t\tfunction testValue1() {
//\t\t\tvar ratingSlider = \$(\"#\"+this_m_plugin_prefix+\"rating_slider\").bootstrapSlider();
//\t\t\talert( \"testValue1 ratingSlider::\"+var_dump(ratingSlider) )
\t\t\talert( \"testValue1 nsn_woo_ext_search_ratingSlider::\"+var_dump(nsn_woo_ext_search_ratingSlider) )
//\t\t\tnsn_woo_ext_search_ratingSlider.slider('setValue', 1).slider('setValue', 5);
\t\t\tnsn_woo_ext_search_ratingSlider.slider.setValue(1)  //.slider('setValue', 5);
//\t\t\tnsn_woo_ext_search_ratingSlider.setValue(1)
\t\t}

\t\tfunction testValue2() {
//\t\t\tvar ratingSlider = \$(\"#\"+this_m_plugin_prefix+\"rating_slider\").bootstrapSlider();
//\t\t\talert( \"testValue1 ratingSlider::\"+var_dump(ratingSlider) )
\t\t\talert( \"testValue1 nsn_woo_ext_search_ratingSlider::\"+var_dump(nsn_woo_ext_search_ratingSlider) )
//\t\t\tnsn_woo_ext_search_ratingSlider.bootstrapSlider('setValue', 1).bootstrapSlider('setValue', 5);
\t\t\tnsn_woo_ext_search_ratingSlider.bootstrapSlider.setValue(1)  //.bootstrapSlider('setValue', 5);
//\t\t\tnsn_woo_ext_search_ratingSlider.setValue(1)  //.bootstrapSlider('setValue', 5);
\t\t}

\t\t/*]]>*/
\t</script>


\t<div class=\"row\">  <!-- Breadcrumbs row start -->
\t\t";
        // line 62
        echo "\t</div>              <!-- Breadcrumbs row end -->

\t<div class=\"row\"> <!-- search by State page start -->

\t\t<div class=\"col-xs-12 col-sm-4 block_left_separator_sm\">
\t\t\t";
        // line 67
        $this->loadTemplate("white-simple/filter_categories_box.twig", "white-simple/woo_ext_search_form.twig", 67)->display($context);
        // line 68
        echo "\t\t\t";
        $this->loadTemplate("white-simple/filter_attrs_box.twig", "white-simple/woo_ext_search_form.twig", 68)->display($context);
        // line 69
        echo "\t\t\t";
        $this->loadTemplate("white-simple/filter_price_box.twig", "white-simple/woo_ext_search_form.twig", 69)->display($context);
        // line 70
        echo "\t\t\t";
        $this->loadTemplate("white-simple/filter_controls_box.twig", "white-simple/woo_ext_search_form.twig", 70)->display($context);
        // line 71
        echo "\t\t</div>


\t\t<div class=\"col-xs-12 col-sm-8 block_right_separator_sm\">
\t\t\t<div class=\"row\">
\t\t\t\t<div id=\"div_products_list\"></div>
\t\t\t\t";
        // line 78
        echo "\t\t\t</div>
\t\t\t<div class=\"row\">
\t\t\t\t";
        // line 81
        echo "\t\t\t</div>
\t\t\t<div class=\"row\">
\t\t\t\t";
        // line 84
        echo "\t\t\t</div>
\t\t</div>

\t</div> <!-- <div class=\"row\"> --> <!-- search by State page end -->

\t";
        // line 89
        $this->loadTemplate("white-simple/new_bookmark_dialog.twig", "white-simple/woo_ext_search_form.twig", 89)->display($context);
        // line 90
        echo "\t";
        $this->loadTemplate("white-simple/existing_bookmarks_dialog.twig", "white-simple/woo_ext_search_form.twig", 90)->display($context);
        // line 91
        echo "
";
    }

    public function getTemplateName()
    {
        return "white-simple/woo_ext_search_form.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  144 => 91,  141 => 90,  139 => 89,  132 => 84,  128 => 81,  124 => 78,  116 => 71,  113 => 70,  110 => 69,  107 => 68,  105 => 67,  98 => 62,  50 => 16,  45 => 14,  33 => 4,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends "white-simple/base.twig" %}*/
/* {% block content %}*/
/* 	{# library.twig woo_ext_search_form  #}*/
/* */
/* 	<script type="text/javascript">*/
/* 		/*<![CDATA[*//* */
/* 		var nsn_woo_ext_search_frontendFuncsObj;*/
/* 		var nsn_woo_ext_search_attrs_options_list;*/
/* 		var nsn_woo_ext_search_bookmarks_list;*/
/* 		var nsn_woo_ext_search_ratingSlider;*/
/* */
/* 		jQuery(document).ready(function ($) {*/
/* */
/* 			var app_params = {{ app_params|json_encode|raw }};*/
/* //			alert( "app_params::"+var_dump( app_params ) )*/
/* 			nsn_woo_ext_search_attrs_options_list= {{ attrs_options_list |json_encode|raw  }}*/
/* 			nsn_woo_ext_search_frontendFuncsObj = new nsn_woo_ext_search_frontendFuncs( app_params, nsn_woo_ext_search_attrs_options_list );*/
/* */
/* //			nsn_woo_ext_search_frontendFuncsObj.initRating();*/
/* */
/* 			nsn_woo_ext_search_frontendFuncsObj.runFilterSearch("loadWooProducts");*/
/* //			nsn_woo_ext_search_ratingSlider = $("#"+this_m_plugin_prefix+"rating_slider").bootstrapSlider();*/
/* //			alert( "initRating  nsn_woo_ext_search_ratingSlider::"+var_dump(nsn_woo_ext_search_ratingSlider) )*/
/* */
/* 			nsn_woo_ext_search_frontendFuncsObj.loadBookmarks();*/
/* 			$('[data-toggle="tooltip"]').tooltip()*/
/* */
/* */
/* */
/* //			$("#ex2").bootstrapSlider();*/
/* */
/* //			ratingSlider.slider('setValue', 0).slider('setValue', 5);*/
/* 			//$.fn.slider namespace is already bound. Use the $.fn.bootstrapSlider namespace instead.*/
/* */
/* //			alert( "AGTER::"+var_dump(1) )*/
/* 		});*/
/* */
/* 		function testValue1() {*/
/* //			var ratingSlider = $("#"+this_m_plugin_prefix+"rating_slider").bootstrapSlider();*/
/* //			alert( "testValue1 ratingSlider::"+var_dump(ratingSlider) )*/
/* 			alert( "testValue1 nsn_woo_ext_search_ratingSlider::"+var_dump(nsn_woo_ext_search_ratingSlider) )*/
/* //			nsn_woo_ext_search_ratingSlider.slider('setValue', 1).slider('setValue', 5);*/
/* 			nsn_woo_ext_search_ratingSlider.slider.setValue(1)  //.slider('setValue', 5);*/
/* //			nsn_woo_ext_search_ratingSlider.setValue(1)*/
/* 		}*/
/* */
/* 		function testValue2() {*/
/* //			var ratingSlider = $("#"+this_m_plugin_prefix+"rating_slider").bootstrapSlider();*/
/* //			alert( "testValue1 ratingSlider::"+var_dump(ratingSlider) )*/
/* 			alert( "testValue1 nsn_woo_ext_search_ratingSlider::"+var_dump(nsn_woo_ext_search_ratingSlider) )*/
/* //			nsn_woo_ext_search_ratingSlider.bootstrapSlider('setValue', 1).bootstrapSlider('setValue', 5);*/
/* 			nsn_woo_ext_search_ratingSlider.bootstrapSlider.setValue(1)  //.bootstrapSlider('setValue', 5);*/
/* //			nsn_woo_ext_search_ratingSlider.setValue(1)  //.bootstrapSlider('setValue', 5);*/
/* 		}*/
/* */
/* 		/*]]>*//* */
/* 	</script>*/
/* */
/* */
/* 	<div class="row">  <!-- Breadcrumbs row start -->*/
/* 		{#{{ components.showBreadcrumbs( breadcrumbsList, 'div_breadcrumbs' ) }}#}*/
/* 	</div>              <!-- Breadcrumbs row end -->*/
/* */
/* 	<div class="row"> <!-- search by State page start -->*/
/* */
/* 		<div class="col-xs-12 col-sm-4 block_left_separator_sm">*/
/* 			{% include 'white-simple/filter_categories_box.twig' %}*/
/* 			{% include 'white-simple/filter_attrs_box.twig' %}*/
/* 			{% include 'white-simple/filter_price_box.twig' %}*/
/* 			{% include 'white-simple/filter_controls_box.twig' %}*/
/* 		</div>*/
/* */
/* */
/* 		<div class="col-xs-12 col-sm-8 block_right_separator_sm">*/
/* 			<div class="row">*/
/* 				<div id="div_products_list"></div>*/
/* 				{#{% include 'white-simple/products_list.twig' %}#}*/
/* 			</div>*/
/* 			<div class="row">*/
/* 				{#% include 'spotlight_tours.twig' %#}*/
/* 			</div>*/
/* 			<div class="row">*/
/* 				{#{% include 'blog_entries.twig' %}#}*/
/* 			</div>*/
/* 		</div>*/
/* */
/* 	</div> <!-- <div class="row"> --> <!-- search by State page end -->*/
/* */
/* 	{% include 'white-simple/new_bookmark_dialog.twig' %}*/
/* 	{% include 'white-simple/existing_bookmarks_dialog.twig' %}*/
/* */
/* {% endblock content %}*/
