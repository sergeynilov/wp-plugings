<?php

/* white-simple/woo_ext_products_list.twig */
class __TwigTemplate_052f0fb014f4a6b3dd328e463a66441b4fa1283586cbd34e65fb5a73e401d161 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 145
        echo "  ";
        // line 146
        echo "
<h2 class=\"site_content_title\">
\t";
        // line 148
        if ((twig_length_filter($this->env, (isset($context["products_list"]) ? $context["products_list"] : $this->getContext($context, "products_list"))) == 0)) {
            // line 149
            echo "\t\t<p class=\"text-info\" id=\"nsn_woo_ext_search_product_title\">No products found</p>
    ";
        } else {
            // line 151
            echo "\t    <p class=\"text-info\" id=\"nsn_woo_ext_search_product_title\">
\t\t\tFound ";
            // line 152
            echo (isset($context["total_products_count"]) ? $context["total_products_count"] : $this->getContext($context, "total_products_count"));
            echo " Product";
            if (((isset($context["total_products_count"]) ? $context["total_products_count"] : $this->getContext($context, "total_products_count")) > 1)) {
                echo "s";
            }
            // line 153
            echo "\t\t\t";
            if (((isset($context["total_products_count"]) ? $context["total_products_count"] : $this->getContext($context, "total_products_count")) != twig_length_filter($this->env, (isset($context["products_list"]) ? $context["products_list"] : $this->getContext($context, "products_list"))))) {
                // line 154
                echo "\t\t\t\t(";
                echo twig_length_filter($this->env, (isset($context["products_list"]) ? $context["products_list"] : $this->getContext($context, "products_list")));
                echo " product";
                if ((twig_length_filter($this->env, (isset($context["products_list"]) ? $context["products_list"] : $this->getContext($context, "products_list"))) > 1)) {
                    echo "s";
                }
                echo " on  page ";
                echo (isset($context["page"]) ? $context["page"] : $this->getContext($context, "page"));
                echo ")
\t\t\t";
            }
            // line 156
            echo "\t    </p>
\t";
        }
        // line 158
        echo "</h2>

";
        // line 162
        echo "<div class=\"row nsn_woo_ext_search_woo_product_next_row\">
";
        // line 163
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["products_list"]) ? $context["products_list"] : $this->getContext($context, "products_list")));
        foreach ($context['_seq'] as $context["_key"] => $context["nextProduct"]) {
            // line 164
            echo "\t<div class=\"row nsn_woo_ext_search_woo_product_next_row col-xs-12 col-sm-6\">
\t\t<div class=\"nsn_woo_ext_search_next_woo_product\">
\t\t\t<ul class=\"nsn_woo_ext_search_ul_multiline\">

            <span class=\"visible-xs\">
\t            ";
            // line 169
            echo $this->getAttribute($this, "show_next_product_info", array(0 => $context["nextProduct"], 1 => (isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")), 2 => (isset($context["attrs_to_show"]) ? $context["attrs_to_show"] : $this->getContext($context, "attrs_to_show")), 3 => (isset($context["service_category_for_attrs_array"]) ? $context["service_category_for_attrs_array"] : $this->getContext($context, "service_category_for_attrs_array")), 4 => "_xs"), "method");
            echo "
\t        </span>
\t        <span class=\"visible-sm\">
\t            ";
            // line 172
            echo $this->getAttribute($this, "show_next_product_info", array(0 => $context["nextProduct"], 1 => (isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")), 2 => (isset($context["attrs_to_show"]) ? $context["attrs_to_show"] : $this->getContext($context, "attrs_to_show")), 3 => (isset($context["service_category_for_attrs_array"]) ? $context["service_category_for_attrs_array"] : $this->getContext($context, "service_category_for_attrs_array")), 4 => "_sm"), "method");
            echo "
\t        </span>
\t        <span class=\"visible-md\">
\t            ";
            // line 175
            echo $this->getAttribute($this, "show_next_product_info", array(0 => $context["nextProduct"], 1 => (isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")), 2 => (isset($context["attrs_to_show"]) ? $context["attrs_to_show"] : $this->getContext($context, "attrs_to_show")), 3 => (isset($context["service_category_for_attrs_array"]) ? $context["service_category_for_attrs_array"] : $this->getContext($context, "service_category_for_attrs_array")), 4 => "_md"), "method");
            echo "
\t        </span>
\t        <span class=\"visible-lg\">
\t            ";
            // line 178
            echo $this->getAttribute($this, "show_next_product_info", array(0 => $context["nextProduct"], 1 => (isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")), 2 => (isset($context["attrs_to_show"]) ? $context["attrs_to_show"] : $this->getContext($context, "attrs_to_show")), 3 => (isset($context["service_category_for_attrs_array"]) ? $context["service_category_for_attrs_array"] : $this->getContext($context, "service_category_for_attrs_array")), 4 => "_md"), "method");
            echo "
\t        </span>

\t\t\t</ul>

\t\t</div>
\t</div>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['nextProduct'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 186
        echo "</div>

";
        // line 188
        if (((isset($context["navigationHTML"]) ? $context["navigationHTML"] : $this->getContext($context, "navigationHTML")) != "")) {
            // line 189
            echo "<div class=\"row nsn_woo_ext_search_block_padding_sm\">
\t";
            // line 190
            echo (isset($context["navigationHTML"]) ? $context["navigationHTML"] : $this->getContext($context, "navigationHTML"));
            echo "
</div>
";
        }
    }

    // line 1
    public function getshow_next_product_info($__nextProduct__ = null, $__fields_to_show__ = null, $__attrs_to_show__ = null, $__service_category_for_attrs_array__ = null, $__field_param__ = null, ...$__varargs__)
    {
        $context = $this->env->mergeGlobals(array(
            "nextProduct" => $__nextProduct__,
            "fields_to_show" => $__fields_to_show__,
            "attrs_to_show" => $__attrs_to_show__,
            "service_category_for_attrs_array" => $__service_category_for_attrs_array__,
            "field_param" => $__field_param__,
            "varargs" => $__varargs__,
        ));

        $blocks = array();

        ob_start();
        try {
            // line 2
            echo "
\t";
            // line 4
            echo "\t<li class=\"post-";
            echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "id", array(), "array");
            echo " \">
\t";
            // line 5
            if (($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "title", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")), "title", array()))) {
                // line 6
                echo "\t\t<h4 class=\"row nsn_woo_ext_search_woo_product_next_row\">
\t\t\t";
                // line 7
                if ((($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "purchase_note", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")), "purchase_note", array())) && ($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "purchase_note", array(), "array") != ""))) {
                    // line 8
                    echo "\t\t\t\t<span class=\"fa fa-info-circle pull-left\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"";
                    echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "purchase_note", array(), "array");
                    echo "\"></span>&nbsp;
\t\t\t";
                }
                // line 10
                echo "\t\t\t<a href=\"";
                echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "guid", array(), "array");
                echo "\" class=\"woo_products_title nsn_woo_ext_search_woo_products_field_value nsn_woo_ext_search_a_link\">";
                echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "id", array(), "array");
                echo "->";
                echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "post_title", array(), "array");
                echo "</a>
\t\t</h4>
\t";
            }
            // line 13
            echo "
\t";
            // line 14
            if (($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "image", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")), "image", array()))) {
                // line 15
                echo "\t\t<div class=\"row nsn_woo_ext_search_woo_product_next_row\">
\t\t\t";
                // line 16
                if ((($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "is_in_stock", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")), "is_in_stock", array())) && ($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "is_in_stock", array(), "array") == 1))) {
                    // line 17
                    echo "\t\t\t<div class=\"nsn_woo_ext_search_img_banner\"  >
\t\t\t\t<img src=\"";
                    // line 18
                    echo (isset($context["plugin_images"]) ? $context["plugin_images"] : $this->getContext($context, "plugin_images"));
                    echo "in_stock.png\">
\t\t\t</div>
\t\t\t";
                }
                // line 21
                echo "
\t\t\t<div class=\"next_hostel_image\" >
\t\t\t\t<a href=\"";
                // line 23
                echo "\">
\t\t\t\t\t<div class=\"nsn_woo_ext_search_image_border\" style=\"background-image: url('";
                // line 24
                echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "image_url", array(), "array");
                echo "'); width:180px; height:180px;\"   id=\"spotlight_img_hostel_";
                echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "id", array(), "array");
                echo "\"></div>
\t\t\t\t</a>
\t\t\t</div>
\t\t</div>
\t";
            }
            // line 29
            echo "
\t\t";
            // line 31
            echo "\t\t";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["attrs_to_show"]) ? $context["attrs_to_show"] : $this->getContext($context, "attrs_to_show")));
            foreach ($context['_seq'] as $context["_key"] => $context["next_attr_to_show"]) {
                // line 32
                echo "\t\t\t";
                // line 33
                echo "\t\t\t";
                $context["is_field_used"] = false;
                // line 34
                echo "
\t\t\t";
                // line 35
                if ((($this->getAttribute($context["next_attr_to_show"], "attr_name", array(), "array") == "rating") && (($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "rating_average", array()) > 0) || ($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "comments_count", array()) > 0)))) {
                    // line 36
                    echo "\t\t\t\t<div class=\"row nsn_woo_ext_search_woo_product_next_row\">
\t\t\t\t\t<img src=\"";
                    // line 37
                    echo (isset($context["site_url"]) ? $context["site_url"] : $this->getContext($context, "site_url"));
                    echo "/wp-content/plugins/woo-ext-search/images/stars/stars";
                    echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "rating_average", array());
                    echo ".png\" title=\"Rated ";
                    echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "rating_average", array());
                    echo " out of 5\" class=\"pull-left\">&nbsp;
\t\t\t\t\t &nbsp;(<span class=\"rating\">";
                    // line 38
                    echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "comments_count", array());
                    echo "</span> customer reviews)
\t\t\t\t\t";
                    // line 39
                    $context["is_field_used"] = true;
                    // line 40
                    echo "\t\t\t\t</div>
\t\t\t";
                }
                // line 42
                echo "
\t\t\t";
                // line 43
                if ((twig_in_filter($this->getAttribute($context["next_attr_to_show"], "attr_name", array(), "array"), twig_get_array_keys_filter((isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")))) && ((isset($context["is_field_used"]) ? $context["is_field_used"] : $this->getContext($context, "is_field_used")) == false))) {
                    // line 44
                    echo "
\t\t\t\t";
                    // line 45
                    if (($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), $this->getAttribute($context["next_attr_to_show"], "attr_name", array(), "array"), array(), "array", true, true) && ($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), $this->getAttribute($context["next_attr_to_show"], "attr_name", array(), "array"), array(), "array") != ""))) {
                        // line 46
                        echo "\t\t\t\t<div class=\"row nsn_woo_ext_search_woo_product_next_row\">
\t\t\t\t\t<span class=\"woo_products_label\">";
                        // line 47
                        echo twig_title_string_filter($this->env, $this->getAttribute($context["next_attr_to_show"], "attr_name", array(), "array"));
                        echo ":</span>
\t\t\t\t\t<span class=\"woo_products_";
                        // line 48
                        echo $this->getAttribute($context["next_attr_to_show"], "attr_name", array(), "array");
                        echo " nsn_woo_ext_search_woo_products_field_value\">
\t\t\t\t\t\t";
                        // line 50
                        echo "\t\t\t\t\t\t";
                        $context["service_post_id"] = "";
                        // line 51
                        echo "\t\t\t\t\t\t";
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable((isset($context["service_category_for_attrs_array"]) ? $context["service_category_for_attrs_array"] : $this->getContext($context, "service_category_for_attrs_array")));
                        foreach ($context['_seq'] as $context["key_attr"] => $context["next_service_category_for_attr"]) {
                            // line 52
                            echo "\t\t\t\t\t\t\t";
                            if (($this->getAttribute($context["next_attr_to_show"], "attr_name", array(), "array") == $context["key_attr"])) {
                                // line 53
                                echo "\t\t\t\t\t\t\t\t";
                                $context["attr_key_name"] = ((("nsn-woo-ext-search-service_category_attr_" . $this->getAttribute($context["next_attr_to_show"], "attr_name", array())) . "_") . call_user_func_array($this->env->getFilter('change_submit_key')->getCallable(), array($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), $this->getAttribute($context["next_attr_to_show"], "attr_name", array()), array(), "array"))));
                                // line 55
                                echo "\t\t\t\t\t\t\t\t";
                                if ($this->getAttribute($context["next_service_category_for_attr"], (isset($context["attr_key_name"]) ? $context["attr_key_name"] : $this->getContext($context, "attr_key_name")), array(), "array", true, true)) {
                                    // line 56
                                    echo "\t\t\t\t\t\t\t\t";
                                    $context["service_post_id"] = $this->getAttribute($context["next_service_category_for_attr"], (isset($context["attr_key_name"]) ? $context["attr_key_name"] : $this->getContext($context, "attr_key_name")), array(), "array");
                                    // line 57
                                    echo "   \t\t\t\t\t\t\t    ";
                                }
                                // line 58
                                echo "\t\t\t\t\t\t\t";
                            }
                            // line 59
                            echo "\t\t\t\t\t\t";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['key_attr'], $context['next_service_category_for_attr'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 60
                        echo "\t\t\t\t\t\t";
                        if ((($this->getAttribute($context["next_attr_to_show"], "show_attribute_link_to_post", array(), "array") == "yes") && ((isset($context["service_post_id"]) ? $context["service_post_id"] : $this->getContext($context, "service_post_id")) != ""))) {
                            // line 61
                            echo "\t\t\t\t\t\t<a class=\"nsn_woo_ext_search_a_link\" href=\"";
                            echo call_user_func_array($this->env->getFilter('make_post_url')->getCallable(), array((isset($context["service_post_id"]) ? $context["service_post_id"] : $this->getContext($context, "service_post_id"))));
                            echo "\">";
                            echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), $this->getAttribute($context["next_attr_to_show"], "attr_name", array(), "array"), array(), "array");
                            echo "</a>
\t\t\t\t\t\t";
                        } else {
                            // line 63
                            echo "\t\t\t\t\t\t";
                            echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), $this->getAttribute($context["next_attr_to_show"], "attr_name", array(), "array"), array(), "array");
                            echo "
\t\t\t\t\t\t";
                        }
                        // line 65
                        echo "\t\t\t\t\t</span>
\t\t\t\t</div>
\t\t\t\t";
                    }
                    // line 68
                    echo "
\t\t\t";
                }
                // line 70
                echo "
\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['next_attr_to_show'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 72
            echo "\t\t";
            // line 73
            echo "

\t";
            // line 75
            if (($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "post_date", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")), "post_date", array()))) {
                // line 76
                echo "\t\t<div class=\"row nsn_woo_ext_search_woo_product_next_row\">
\t\t\t<span class=\"woo_products_label\">";
                // line 77
                echo twig_title_string_filter($this->env, "Publication");
                echo ":</span>
\t\t\t<span class=\"woo_products_post_date nsn_woo_ext_search_woo_products_field_value\">
\t\t\t\t";
                // line 79
                echo call_user_func_array($this->env->getFilter('date_time')->getCallable(), array($this->getAttribute($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "post", array()), "post_date", array())));
                echo "
\t\t\t</span>
\t\t</div>
\t";
            }
            // line 83
            echo "

\t";
            // line 85
            if (($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "categories", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")), "categories", array()))) {
                // line 86
                echo "\t\t<div class=\"row nsn_woo_ext_search_woo_product_next_row\">
\t\t\t<span class=\"woo_products_label\">Categor";
                // line 87
                if ((twig_length_filter($this->env, $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "categories_list", array(), "array")) > 1)) {
                    echo "ies";
                } else {
                    echo "y";
                }
                echo ":</span>
\t\t\t";
                // line 88
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "categories_list", array(), "array"), "ret_titles", array(), "array"));
                $context['loop'] = array(
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                );
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["_key"] => $context["next_category_title"]) {
                    // line 89
                    echo "\t\t\t<span class=\"woo_products_categories nsn_woo_ext_search_woo_products_field_value\">
\t\t\t\t<a href=\"";
                    // line 90
                    echo call_user_func_array($this->env->getFilter('make_woo_product_category_url')->getCallable(), array($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "categories_list", array(), "array"), "ret_slugs", array(), "array"), $this->getAttribute($context["loop"], "index0", array()), array(), "array")));
                    echo "\" class=\"nsn_woo_ext_search_a_link\">";
                    echo $context["next_category_title"];
                    echo "</a>
\t\t\t\t";
                    // line 91
                    if ( !$this->getAttribute($context["loop"], "last", array())) {
                        echo ", ";
                    }
                    // line 92
                    echo "\t\t\t</span>
\t\t\t";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['next_category_title'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 94
                echo "\t\t</div>
\t";
            }
            // line 96
            echo "

\t";
            // line 98
            if ((($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "sale_price", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")), "sale_price", array())) || ($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "regular_price", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")), "regular_price", array())))) {
                // line 99
                echo "\t\t<div class=\"row nsn_woo_ext_search_woo_product_next_row\">
\t\t\t<span class=\"woo_products_label\">Price:</span>

\t\t\t<span class=\"woo_products_sale_price nsn_woo_ext_search_woo_products_field_value\">
\t\t\t\t";
                // line 103
                if ((($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), "regular_price", array(), "array", true, true) && ($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "regular_price", array(), "array") > 0)) && ($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "regular_price", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")), "regular_price", array())))) {
                    // line 104
                    echo "\t\t\t\t\t";
                    echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "formatted_regular_price", array(), "array");
                    echo "
\t\t\t\t";
                }
                // line 106
                echo "
\t\t\t\t";
                // line 107
                if ((((($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), "regular_price", array(), "array", true, true) && ($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "regular_price", array(), "array") > 0)) && ($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "regular_price", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")), "regular_price", array()))) && ($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), "sale_price", array(), "array", true, true) && ($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "sale_price", array(), "array") > 0))) && ($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "sale_price", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")), "sale_price", array())))) {
                    // line 108
                    echo "\t\t\t\t&nbsp;-&nbsp;
\t\t\t\t";
                }
                // line 110
                echo "
\t\t\t\t";
                // line 111
                if ((($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : null), "sale_price", array(), "array", true, true) && ($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "sale_price", array(), "array") > 0)) && ($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "sale_price", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")), "sale_price", array())))) {
                    // line 112
                    echo "\t\t\t\t\t";
                    echo $this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "formatted_sale_price", array(), "array");
                    echo "
\t\t\t\t";
                }
                // line 114
                echo "\t\t\t</span>

\t\t</div>
\t";
            }
            // line 118
            echo "

\t\t";
            // line 120
            if (($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "is_in_stock", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")), "is_in_stock", array()))) {
                // line 121
                echo "\t\t\t<div class=\"row nsn_woo_ext_search_woo_product_next_row\">
\t\t\t\t<span class=\"woo_products_label\">In Stock:</span>
\t\t\t<span class=\"woo_products_is_in_stock nsn_woo_ext_search_woo_products_field_value\">
\t\t\t\t";
                // line 124
                if ($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "is_in_stock", array(), "array")) {
                    echo "Yes";
                } else {
                    echo "No";
                }
                // line 125
                echo "\t\t\t</span>
\t\t\t</div>
\t\t";
            }
            // line 128
            echo "


\t\t";
            // line 131
            if (($this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : null), "tags", array(), "any", true, true) && $this->getAttribute((isset($context["fields_to_show"]) ? $context["fields_to_show"] : $this->getContext($context, "fields_to_show")), "tags", array()))) {
                // line 132
                echo "\t\t\t<div class=\"row nsn_woo_ext_search_woo_product_next_row\">
\t\t\t\t<span class=\"woo_products_label\">Tags:</span>
\t\t\t\t";
                // line 134
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["nextProduct"]) ? $context["nextProduct"] : $this->getContext($context, "nextProduct")), "tags_list", array(), "array"));
                $context['loop'] = array(
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                );
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["_key"] => $context["next_tag"]) {
                    // line 135
                    echo "\t\t\t\t\t<span class=\"woo_products_tags nsn_woo_ext_search_woo_products_field_value\">
\t\t\t\t\t\t";
                    // line 136
                    echo $this->getAttribute($context["next_tag"], "name", array());
                    if ( !$this->getAttribute($context["loop"], "last", array())) {
                        echo ", ";
                    }
                    // line 137
                    echo "\t\t\t\t\t</span>
\t\t\t\t";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['next_tag'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 139
                echo "\t\t\t</div>
\t\t";
            }
            // line 141
            echo "

\t</li>

";
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    public function getTemplateName()
    {
        return "white-simple/woo_ext_products_list.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  547 => 141,  543 => 139,  528 => 137,  523 => 136,  520 => 135,  503 => 134,  499 => 132,  497 => 131,  492 => 128,  487 => 125,  481 => 124,  476 => 121,  474 => 120,  470 => 118,  464 => 114,  458 => 112,  456 => 111,  453 => 110,  449 => 108,  447 => 107,  444 => 106,  438 => 104,  436 => 103,  430 => 99,  428 => 98,  424 => 96,  420 => 94,  405 => 92,  401 => 91,  395 => 90,  392 => 89,  375 => 88,  367 => 87,  364 => 86,  362 => 85,  358 => 83,  351 => 79,  346 => 77,  343 => 76,  341 => 75,  337 => 73,  335 => 72,  328 => 70,  324 => 68,  319 => 65,  313 => 63,  305 => 61,  302 => 60,  296 => 59,  293 => 58,  290 => 57,  287 => 56,  284 => 55,  281 => 53,  278 => 52,  273 => 51,  270 => 50,  266 => 48,  262 => 47,  259 => 46,  257 => 45,  254 => 44,  252 => 43,  249 => 42,  245 => 40,  243 => 39,  239 => 38,  231 => 37,  228 => 36,  226 => 35,  223 => 34,  220 => 33,  218 => 32,  213 => 31,  210 => 29,  200 => 24,  197 => 23,  193 => 21,  187 => 18,  184 => 17,  182 => 16,  179 => 15,  177 => 14,  174 => 13,  163 => 10,  157 => 8,  155 => 7,  152 => 6,  150 => 5,  145 => 4,  142 => 2,  126 => 1,  118 => 190,  115 => 189,  113 => 188,  109 => 186,  95 => 178,  89 => 175,  83 => 172,  77 => 169,  70 => 164,  66 => 163,  63 => 162,  59 => 158,  55 => 156,  43 => 154,  40 => 153,  34 => 152,  31 => 151,  27 => 149,  25 => 148,  21 => 146,  19 => 145,);
    }
}
/* {% macro show_next_product_info( nextProduct, fields_to_show, attrs_to_show, service_category_for_attrs_array, field_param ) %}*/
/* */
/* 	{#nextProduct::{{ nextProduct | d }}#}*/
/* 	<li class="post-{{ nextProduct['id'] }} ">*/
/* 	{% if fields_to_show.title is defined and fields_to_show.title %}*/
/* 		<h4 class="row nsn_woo_ext_search_woo_product_next_row">*/
/* 			{% if fields_to_show.purchase_note is defined and fields_to_show.purchase_note and nextProduct['purchase_note'] != "" %}*/
/* 				<span class="fa fa-info-circle pull-left" data-toggle="tooltip" data-placement="top" title="{{ nextProduct['purchase_note'] }}"></span>&nbsp;*/
/* 			{% endif %}*/
/* 			<a href="{{ nextProduct['guid'] }}" class="woo_products_title nsn_woo_ext_search_woo_products_field_value nsn_woo_ext_search_a_link">{{ nextProduct['id'] }}->{{ nextProduct['post_title'] }}</a>*/
/* 		</h4>*/
/* 	{% endif %}*/
/* */
/* 	{% if fields_to_show.image is defined and fields_to_show.image  %}*/
/* 		<div class="row nsn_woo_ext_search_woo_product_next_row">*/
/* 			{% if fields_to_show.is_in_stock is defined and fields_to_show.is_in_stock and nextProduct['is_in_stock'] == 1 %}*/
/* 			<div class="nsn_woo_ext_search_img_banner"  >*/
/* 				<img src="{{ plugin_images }}in_stock.png">*/
/* 			</div>*/
/* 			{% endif %}*/
/* */
/* 			<div class="next_hostel_image" >*/
/* 				<a href="{#{ nextProduct['guid'] }#}">*/
/* 					<div class="nsn_woo_ext_search_image_border" style="background-image: url('{{ nextProduct['image_url'] }}'); width:180px; height:180px;"   id="spotlight_img_hostel_{{ nextProduct['id'] }}"></div>*/
/* 				</a>*/
/* 			</div>*/
/* 		</div>*/
/* 	{% endif %}*/
/* */
/* 		{#==================== START ==============#}*/
/* 		{% for next_attr_to_show in attrs_to_show %}*/
/* 			{#next_attr_to_show::{{ next_attr_to_show | d }}#}*/
/* 			{% set is_field_used = false %}*/
/* */
/* 			{% if next_attr_to_show['attr_name'] == "rating" and ( nextProduct.rating_average > 0 or nextProduct.comments_count > 0 ) %}*/
/* 				<div class="row nsn_woo_ext_search_woo_product_next_row">*/
/* 					<img src="{{ site_url }}/wp-content/plugins/woo-ext-search/images/stars/stars{{ nextProduct.rating_average }}.png" title="Rated {{ nextProduct.rating_average }} out of 5" class="pull-left">&nbsp;*/
/* 					 &nbsp;(<span class="rating">{{ nextProduct.comments_count }}</span> customer reviews)*/
/* 					{% set is_field_used = true %}*/
/* 				</div>*/
/* 			{% endif %}*/
/* */
/* 			{% if next_attr_to_show['attr_name'] in fields_to_show | keys and is_field_used == false %}*/
/* */
/* 				{% if nextProduct[next_attr_to_show['attr_name']]  is defined and nextProduct[next_attr_to_show['attr_name']]!= "" %}*/
/* 				<div class="row nsn_woo_ext_search_woo_product_next_row">*/
/* 					<span class="woo_products_label">{{ next_attr_to_show['attr_name'] | title }}:</span>*/
/* 					<span class="woo_products_{{ next_attr_to_show['attr_name'] }} nsn_woo_ext_search_woo_products_field_value">*/
/* 						{#next_attr_to_show]['attr_name']::{{ next_attr_to_show['attr_name'] }}#}*/
/* 						{% set service_post_id='' %}*/
/* 						{% for key_attr, next_service_category_for_attr in service_category_for_attrs_array %}*/
/* 							{% if next_attr_to_show['attr_name'] == key_attr %}*/
/* 								{% set attr_key_name = 'nsn-woo-ext-search-service_category_attr_' ~ next_attr_to_show.attr_name ~ '_' ~ ( nextProduct[next_attr_to_show.attr_name] | change_submit_key )*/
/* 								%}*/
/* 								{% if next_service_category_for_attr[attr_key_name] is defined %}*/
/* 								{% set service_post_id= next_service_category_for_attr[attr_key_name] %}*/
/*    							    {% endif %}*/
/* 							{% endif %}*/
/* 						{% endfor %}*/
/* 						{% if next_attr_to_show['show_attribute_link_to_post'] == 'yes' and service_post_id != ""%}*/
/* 						<a class="nsn_woo_ext_search_a_link" href="{{ service_post_id | make_post_url }}">{{ nextProduct[ next_attr_to_show['attr_name'] ] }}</a>*/
/* 						{% else %}*/
/* 						{{ nextProduct[ next_attr_to_show['attr_name'] ] }}*/
/* 						{% endif %}*/
/* 					</span>*/
/* 				</div>*/
/* 				{% endif %}*/
/* */
/* 			{% endif %}*/
/* */
/* 		{% endfor %}*/
/* 		{#==================== END ==============#}*/
/* */
/* */
/* 	{% if fields_to_show.post_date is defined and fields_to_show.post_date %}*/
/* 		<div class="row nsn_woo_ext_search_woo_product_next_row">*/
/* 			<span class="woo_products_label">{{ "Publication" | title }}:</span>*/
/* 			<span class="woo_products_post_date nsn_woo_ext_search_woo_products_field_value">*/
/* 				{{ nextProduct.post.post_date | date_time }}*/
/* 			</span>*/
/* 		</div>*/
/* 	{% endif %}*/
/* */
/* */
/* 	{% if fields_to_show.categories is defined and fields_to_show.categories %}*/
/* 		<div class="row nsn_woo_ext_search_woo_product_next_row">*/
/* 			<span class="woo_products_label">Categor{% if nextProduct['categories_list'] | length > 1 %}ies{% else %}y{% endif %}:</span>*/
/* 			{% for next_category_title in nextProduct['categories_list']['ret_titles'] %}*/
/* 			<span class="woo_products_categories nsn_woo_ext_search_woo_products_field_value">*/
/* 				<a href="{{ nextProduct['categories_list']['ret_slugs'][loop.index0] | make_woo_product_category_url }}" class="nsn_woo_ext_search_a_link">{{ next_category_title }}</a>*/
/* 				{% if not loop.last %}, {% endif %}*/
/* 			</span>*/
/* 			{% endfor %}*/
/* 		</div>*/
/* 	{% endif %}*/
/* */
/* */
/* 	{% if ( fields_to_show.sale_price is defined and fields_to_show.sale_price ) or ( fields_to_show.regular_price is defined and fields_to_show.regular_price ) %}*/
/* 		<div class="row nsn_woo_ext_search_woo_product_next_row">*/
/* 			<span class="woo_products_label">Price:</span>*/
/* */
/* 			<span class="woo_products_sale_price nsn_woo_ext_search_woo_products_field_value">*/
/* 				{% if ( nextProduct['regular_price'] is defined and nextProduct['regular_price'] > 0 ) and ( fields_to_show.regular_price is defined and fields_to_show.regular_price ) %}*/
/* 					{{ nextProduct['formatted_regular_price'] }}*/
/* 				{% endif %}*/
/* */
/* 				{% if ( nextProduct['regular_price'] is defined and nextProduct['regular_price'] > 0 ) and ( fields_to_show.regular_price is defined and fields_to_show.regular_price ) and ( nextProduct['sale_price'] is defined and nextProduct['sale_price'] >0 ) and ( fields_to_show.sale_price is defined and fields_to_show.sale_price ) %}*/
/* 				&nbsp;-&nbsp;*/
/* 				{% endif %}*/
/* */
/* 				{% if ( nextProduct['sale_price'] is defined and nextProduct['sale_price'] > 0 ) and ( fields_to_show.sale_price is defined and fields_to_show.sale_price )%}*/
/* 					{{ nextProduct['formatted_sale_price'] }}*/
/* 				{% endif %}*/
/* 			</span>*/
/* */
/* 		</div>*/
/* 	{% endif %}*/
/* */
/* */
/* 		{% if fields_to_show.is_in_stock is defined and fields_to_show.is_in_stock  %}*/
/* 			<div class="row nsn_woo_ext_search_woo_product_next_row">*/
/* 				<span class="woo_products_label">In Stock:</span>*/
/* 			<span class="woo_products_is_in_stock nsn_woo_ext_search_woo_products_field_value">*/
/* 				{% if nextProduct['is_in_stock'] %}Yes{% else %}No{% endif %}*/
/* 			</span>*/
/* 			</div>*/
/* 		{% endif %}*/
/* */
/* */
/* */
/* 		{% if fields_to_show.tags is defined and fields_to_show.tags %}*/
/* 			<div class="row nsn_woo_ext_search_woo_product_next_row">*/
/* 				<span class="woo_products_label">Tags:</span>*/
/* 				{% for next_tag in nextProduct['tags_list'] %}*/
/* 					<span class="woo_products_tags nsn_woo_ext_search_woo_products_field_value">*/
/* 						{{ next_tag.name }}{% if not loop.last %}, {% endif %}*/
/* 					</span>*/
/* 				{% endfor %}*/
/* 			</div>*/
/* 		{% endif %}*/
/* */
/* */
/* 	</li>*/
/* */
/* {% endmacro %}  {# show_next_product_info END #}*/
/* */
/* <h2 class="site_content_title">*/
/* 	{% if products_list | length == 0 %}*/
/* 		<p class="text-info" id="nsn_woo_ext_search_product_title">No products found</p>*/
/*     {% else %}*/
/* 	    <p class="text-info" id="nsn_woo_ext_search_product_title">*/
/* 			Found {{ total_products_count }} Product{% if total_products_count > 1 %}s{% endif %}*/
/* 			{% if total_products_count != products_list | length %}*/
/* 				({{ products_list | length }} product{% if products_list | length > 1 %}s{% endif %} on  page {{ page }})*/
/* 			{% endif %}*/
/* 	    </p>*/
/* 	{% endif %}*/
/* </h2>*/
/* */
/* {#fields_to_show::{{ fields_to_show | d }}#}*/
/* {#attrs_to_show::{{ attrs_to_show | d }}#}*/
/* <div class="row nsn_woo_ext_search_woo_product_next_row">*/
/* {% for nextProduct in products_list %}*/
/* 	<div class="row nsn_woo_ext_search_woo_product_next_row col-xs-12 col-sm-6">*/
/* 		<div class="nsn_woo_ext_search_next_woo_product">*/
/* 			<ul class="nsn_woo_ext_search_ul_multiline">*/
/* */
/*             <span class="visible-xs">*/
/* 	            {{ _self.show_next_product_info( nextProduct, fields_to_show, attrs_to_show, service_category_for_attrs_array, "_xs" ) }}*/
/* 	        </span>*/
/* 	        <span class="visible-sm">*/
/* 	            {{ _self.show_next_product_info( nextProduct, fields_to_show, attrs_to_show, service_category_for_attrs_array, "_sm" ) }}*/
/* 	        </span>*/
/* 	        <span class="visible-md">*/
/* 	            {{ _self.show_next_product_info( nextProduct, fields_to_show, attrs_to_show, service_category_for_attrs_array, "_md" ) }}*/
/* 	        </span>*/
/* 	        <span class="visible-lg">*/
/* 	            {{ _self.show_next_product_info( nextProduct, fields_to_show, attrs_to_show, service_category_for_attrs_array, "_md" ) }}*/
/* 	        </span>*/
/* */
/* 			</ul>*/
/* */
/* 		</div>*/
/* 	</div>*/
/* {% endfor %}*/
/* </div>*/
/* */
/* {% if navigationHTML != "" %}*/
/* <div class="row nsn_woo_ext_search_block_padding_sm">*/
/* 	{{ navigationHTML }}*/
/* </div>*/
/* {% endif %}*/
/* */
