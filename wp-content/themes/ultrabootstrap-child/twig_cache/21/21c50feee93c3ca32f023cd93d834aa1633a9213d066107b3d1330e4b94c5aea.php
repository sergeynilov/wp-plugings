<?php

/* white-simple/woo_ext_search_form.twig */
class __TwigTemplate_5b1fcb3d2e5618b58757f59de1bba41de678547980dd251dd409cab3550b7b6d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("white-simple/base.twig", "white-simple/woo_ext_search_form.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "white-simple/base.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "
\t<script type=\"text/javascript\">
\t\t/*<![CDATA[*/
\t\tvar nsn_woo_ext_search_frontendFuncsObj;
\t\tvar nsn_woo_ext_search_attrs_options_list;

\t\tjQuery(document).ready(function (\$) {

\t\t\tvar app_params = ";
        // line 11
        echo twig_jsonencode_filter((isset($context["app_params"]) ? $context["app_params"] : null));
        echo ";
\t\t\tnsn_woo_ext_search_attrs_options_list= ";
        // line 12
        echo twig_jsonencode_filter((isset($context["attrs_options_list"]) ? $context["attrs_options_list"] : null));
        echo "
\t\t\tvar show_bookmark_alerts= '";
        // line 13
        echo $this->getAttribute((isset($context["woo_ext_search_options"]) ? $context["woo_ext_search_options"] : null), "show_bookmark_alerts", array());
        echo "'

\t\t\tnsn_woo_ext_search_frontendFuncsObj = new nsn_woo_ext_search_frontendFuncs( app_params, nsn_woo_ext_search_attrs_options_list, show_bookmark_alerts );
\t\t\tnsn_woo_ext_search_frontendFuncsObj.initRating();

\t\t\t//nsn_woo_ext_search_frontendFuncsObj.runFilterSearch(\"loadWooProducts\",1);

\t\t\tvar current_user_id= parseInt('";
        // line 20
        echo (isset($context["current_user_id"]) ? $context["current_user_id"] : null);
        echo "')
\t\t\tvar show_bookmark_functionality= '";
        // line 21
        echo $this->getAttribute((isset($context["woo_ext_search_options"]) ? $context["woo_ext_search_options"] : null), "show_bookmark_functionality", array());
        echo "'
\t\t\tif ( show_bookmark_functionality == 'yes' && current_user_id > 0 ) {
\t\t\t\tnsn_woo_ext_search_frontendFuncsObj.loadBookmarks();
\t\t\t}
\t\t\t\$('[data-toggle=\"tooltip\"]').tooltip()
\t\t});

\t\t/*]]>*/
\t</script>

\t<div class=\"row\"> <!-- search by State page start -->

\t\t<div class=\"col-xs-12 col-sm-4 nsn_woo_ext_search_block_padding_sm\">
\t\t\t";
        // line 34
        $this->loadTemplate("white-simple/filter_controls_box.twig", "white-simple/woo_ext_search_form.twig", 34)->display($context);
        // line 35
        echo "\t\t\t";
        $this->loadTemplate("white-simple/filter_price_box.twig", "white-simple/woo_ext_search_form.twig", 35)->display($context);
        // line 36
        echo "\t\t\t";
        $this->loadTemplate("white-simple/filter_rating_box.twig", "white-simple/woo_ext_search_form.twig", 36)->display($context);
        // line 37
        echo "\t\t\t";
        $this->loadTemplate("white-simple/filter_categories_box.twig", "white-simple/woo_ext_search_form.twig", 37)->display($context);
        // line 38
        echo "\t\t\t";
        $this->loadTemplate("white-simple/filter_attrs_box.twig", "white-simple/woo_ext_search_form.twig", 38)->display($context);
        // line 39
        echo "            ";
        $this->loadTemplate("white-simple/filter_tags_box.twig", "white-simple/woo_ext_search_form.twig", 39)->display($context);
        // line 40
        echo "\t\t</div>


\t\t<div class=\"col-xs-12 col-sm-8 nsn_woo_ext_search_block_padding_sm\">
\t\t\t<div class=\"row\" id=\"div_block_products_list\">
\t\t\t\t<div id=\"div_products_list\"></div>
\t\t\t</div>
\t\t\t<div class=\"row nsn_woo_ext_search_block_padding_lg nsn_woo_ext_search_block_margin_lg aligncenter\" id=\"div_block_loading_image\" style=\"display: none;\">
\t\t\t\t\t<img src=\"";
        // line 48
        echo (isset($context["plugin_images"]) ? $context["plugin_images"] : null);
        echo "big_loading.gif\" class=\"nsn_woo_ext_search_block_padding_lg nsn_woo_ext_search_block_margin_lg\">
\t\t\t</div>
\t\t</div>

\t</div>

\t";
        // line 54
        $this->loadTemplate("white-simple/new_bookmark_dialog.twig", "white-simple/woo_ext_search_form.twig", 54)->display($context);
        // line 55
        echo "\t";
        $this->loadTemplate("white-simple/existing_bookmarks_dialog.twig", "white-simple/woo_ext_search_form.twig", 55)->display($context);
        // line 56
        echo "
";
    }

    public function getTemplateName()
    {
        return "white-simple/woo_ext_search_form.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  120 => 56,  117 => 55,  115 => 54,  106 => 48,  96 => 40,  93 => 39,  90 => 38,  87 => 37,  84 => 36,  81 => 35,  79 => 34,  63 => 21,  59 => 20,  49 => 13,  45 => 12,  41 => 11,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends "white-simple/base.twig" %}*/
/* {% block content %}*/
/* */
/* 	<script type="text/javascript">*/
/* 		/*<![CDATA[*//* */
/* 		var nsn_woo_ext_search_frontendFuncsObj;*/
/* 		var nsn_woo_ext_search_attrs_options_list;*/
/* */
/* 		jQuery(document).ready(function ($) {*/
/* */
/* 			var app_params = {{ app_params|json_encode|raw }};*/
/* 			nsn_woo_ext_search_attrs_options_list= {{ attrs_options_list |json_encode|raw  }}*/
/* 			var show_bookmark_alerts= '{{ woo_ext_search_options.show_bookmark_alerts }}'*/
/* */
/* 			nsn_woo_ext_search_frontendFuncsObj = new nsn_woo_ext_search_frontendFuncs( app_params, nsn_woo_ext_search_attrs_options_list, show_bookmark_alerts );*/
/* 			nsn_woo_ext_search_frontendFuncsObj.initRating();*/
/* */
/* 			//nsn_woo_ext_search_frontendFuncsObj.runFilterSearch("loadWooProducts",1);*/
/* */
/* 			var current_user_id= parseInt('{{ current_user_id }}')*/
/* 			var show_bookmark_functionality= '{{ woo_ext_search_options.show_bookmark_functionality }}'*/
/* 			if ( show_bookmark_functionality == 'yes' && current_user_id > 0 ) {*/
/* 				nsn_woo_ext_search_frontendFuncsObj.loadBookmarks();*/
/* 			}*/
/* 			$('[data-toggle="tooltip"]').tooltip()*/
/* 		});*/
/* */
/* 		/*]]>*//* */
/* 	</script>*/
/* */
/* 	<div class="row"> <!-- search by State page start -->*/
/* */
/* 		<div class="col-xs-12 col-sm-4 nsn_woo_ext_search_block_padding_sm">*/
/* 			{% include 'white-simple/filter_controls_box.twig' %}*/
/* 			{% include 'white-simple/filter_price_box.twig' %}*/
/* 			{% include 'white-simple/filter_rating_box.twig' %}*/
/* 			{% include 'white-simple/filter_categories_box.twig' %}*/
/* 			{% include 'white-simple/filter_attrs_box.twig' %}*/
/*             {% include 'white-simple/filter_tags_box.twig' %}*/
/* 		</div>*/
/* */
/* */
/* 		<div class="col-xs-12 col-sm-8 nsn_woo_ext_search_block_padding_sm">*/
/* 			<div class="row" id="div_block_products_list">*/
/* 				<div id="div_products_list"></div>*/
/* 			</div>*/
/* 			<div class="row nsn_woo_ext_search_block_padding_lg nsn_woo_ext_search_block_margin_lg aligncenter" id="div_block_loading_image" style="display: none;">*/
/* 					<img src="{{ plugin_images }}big_loading.gif" class="nsn_woo_ext_search_block_padding_lg nsn_woo_ext_search_block_margin_lg">*/
/* 			</div>*/
/* 		</div>*/
/* */
/* 	</div>*/
/* */
/* 	{% include 'white-simple/new_bookmark_dialog.twig' %}*/
/* 	{% include 'white-simple/existing_bookmarks_dialog.twig' %}*/
/* */
/* {% endblock content %}*/
