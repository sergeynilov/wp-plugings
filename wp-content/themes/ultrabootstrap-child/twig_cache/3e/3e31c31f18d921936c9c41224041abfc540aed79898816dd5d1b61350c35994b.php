<?php

/* white-simple/filter_tags_box.twig */
class __TwigTemplate_5b87b4280acafe2ab27ca43d33e1c04fb2408875bfb48538428ffb56b0e9236c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((($this->getAttribute((isset($context["woo_ext_search_options"]) ? $context["woo_ext_search_options"] : null), "show_in_extended_search_tag", array()) == "yes") && (twig_length_filter($this->env, (isset($context["tags_list"]) ? $context["tags_list"] : null)) > 0))) {
            // line 2
            echo "\t";
            echo $this->getAttribute((isset($context["library"]) ? $context["library"] : null), "header_block_start", array(0 => "Tags Filter", 1 => "", 2 => "fa fa-search-plus", 3 => ((isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : null) . "block_tags")), "method");
            echo "
\t<div class=\"row nsn_woo_ext_search_block_padding_sm\">

\t\t<ul class=\"nsn_woo_ext_search_ul_multiline left\" >
\t\t";
            // line 6
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["tags_list"]) ? $context["tags_list"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["next_tag"]) {
                // line 7
                echo "\t\t\t<li>
\t\t\t";
                // line 8
                if ( !(($this->getAttribute((isset($context["woo_ext_search_options"]) ? $context["woo_ext_search_options"] : null), "show_in_extended_search_hide_with_zero_products", array()) == "yes") &&  !$this->getAttribute($context["next_tag"], "products_count", array(), "any", true, true))) {
                    // line 9
                    echo "
\t\t\t\t";
                    // line 10
                    if ((($this->getAttribute((isset($context["woo_ext_search_options"]) ? $context["woo_ext_search_options"] : null), "run_search_by_click", array()) == "both") || ($this->getAttribute((isset($context["woo_ext_search_options"]) ? $context["woo_ext_search_options"] : null), "run_search_by_click", array()) == "link"))) {
                        // line 11
                        echo "\t\t\t\t\t<a class=\"nsn_woo_ext_search_a_link\" onclick=\"javascript:nsn_woo_ext_search_frontendFuncsObj.filter_elementClicked( 'tag', '', '";
                        echo $this->getAttribute($context["next_tag"], "id", array());
                        echo "' );\treturn false;\">
\t\t\t\t";
                    }
                    // line 13
                    echo "\t\t\t\t<input type=\"checkbox\" id=\"";
                    echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : null);
                    echo "next_tag_";
                    echo $this->getAttribute($context["next_tag"], "id", array());
                    echo "\" value=\"";
                    echo $this->getAttribute($context["next_tag"], "id", array());
                    echo "\" class=\"";
                    echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : null);
                    echo "cbx_tag_selection_filters\" >
\t\t\t\t&nbsp;";
                    // line 14
                    echo $this->getAttribute($context["next_tag"], "id", array());
                    echo "-";
                    echo $this->getAttribute($context["next_tag"], "name", array());
                    echo " ";
                    if ($this->getAttribute($context["next_tag"], "products_count", array(), "any", true, true)) {
                        echo "(<b>";
                        echo $this->getAttribute($context["next_tag"], "products_count", array());
                        echo "</b>)";
                    } else {
                    }
                    // line 15
                    echo "\t\t\t\t";
                    if ((($this->getAttribute((isset($context["woo_ext_search_options"]) ? $context["woo_ext_search_options"] : null), "run_search_by_click", array()) == "both") || ($this->getAttribute((isset($context["woo_ext_search_options"]) ? $context["woo_ext_search_options"] : null), "run_search_by_click", array()) == "link"))) {
                        // line 16
                        echo "\t\t\t\t\t</a>
\t\t\t\t";
                    }
                    // line 18
                    echo "
\t\t\t";
                }
                // line 20
                echo "\t\t\t</li>
\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['next_tag'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 22
            echo "\t\t</ul>

\t</div>
\t";
            // line 25
            echo $this->getAttribute((isset($context["library"]) ? $context["library"] : null), "header_block_end", array(0 => "Tags Filter Block End"), "method");
            echo "
";
        }
    }

    public function getTemplateName()
    {
        return "white-simple/filter_tags_box.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 25,  89 => 22,  82 => 20,  78 => 18,  74 => 16,  71 => 15,  60 => 14,  49 => 13,  43 => 11,  41 => 10,  38 => 9,  36 => 8,  33 => 7,  29 => 6,  21 => 2,  19 => 1,);
    }
}
/* {% if woo_ext_search_options.show_in_extended_search_tag == 'yes'  and tags_list | length > 0 %}*/
/* 	{{ library.header_block_start("Tags Filter", "", "fa fa-search-plus", plugin_prefix ~ "block_tags") }}*/
/* 	<div class="row nsn_woo_ext_search_block_padding_sm">*/
/* */
/* 		<ul class="nsn_woo_ext_search_ul_multiline left" >*/
/* 		{% for next_tag in tags_list %}*/
/* 			<li>*/
/* 			{% if not ( woo_ext_search_options.show_in_extended_search_hide_with_zero_products == "yes" and next_tag.products_count is not defined ) %}*/
/* */
/* 				{% if woo_ext_search_options.run_search_by_click == 'both' or woo_ext_search_options.run_search_by_click == 'link' %}*/
/* 					<a class="nsn_woo_ext_search_a_link" onclick="javascript:nsn_woo_ext_search_frontendFuncsObj.filter_elementClicked( 'tag', '', '{{ next_tag.id }}' );	return false;">*/
/* 				{% endif %}*/
/* 				<input type="checkbox" id="{{ plugin_prefix }}next_tag_{{ next_tag.id }}" value="{{ next_tag.id }}" class="{{ plugin_prefix }}cbx_tag_selection_filters" >*/
/* 				&nbsp;{{ next_tag.id }}-{{ next_tag.name }} {% if next_tag.products_count is defined %}(<b>{{ next_tag.products_count }}</b>){% else %}{% endif %}*/
/* 				{% if woo_ext_search_options.run_search_by_click == 'both' or woo_ext_search_options.run_search_by_click == 'link' %}*/
/* 					</a>*/
/* 				{% endif %}*/
/* */
/* 			{% endif %}*/
/* 			</li>*/
/* 		{% endfor %}*/
/* 		</ul>*/
/* */
/* 	</div>*/
/* 	{{ library.header_block_end("Tags Filter Block End") }}*/
/* {% endif  %}*/
