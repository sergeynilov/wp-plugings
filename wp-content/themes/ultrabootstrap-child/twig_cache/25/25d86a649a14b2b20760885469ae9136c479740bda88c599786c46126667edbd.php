<?php

/* white-simple/filter_attrs_box.twig */
class __TwigTemplate_7a9ac841e66d564d4ee7e578884dc9d0c0f66422f953f6598408c2900cf4e356 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["attrs_options_list"]) ? $context["attrs_options_list"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["attrs_option"]) {
            // line 2
            echo "
\t";
            // line 3
            echo $this->getAttribute((isset($context["library"]) ? $context["library"] : null), "header_block_start", array(0 => ("Filter on " . $this->getAttribute($context["attrs_option"], "attr_name", array(), "array")), 1 => "", 2 => "fa fa-search-plus", 3 => (((isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : null) . "block_") . $this->getAttribute($context["attrs_option"], "attr_name", array(), "array"))), "method");
            echo "
\t<div class=\"row nsn_woo_ext_search_block_padding_md\">
\t\t\t";
            // line 5
            $context["attrs_list"] = $this->getAttribute($context["attrs_option"], "attrs_list", array(), "array");
            // line 6
            echo "
\t\t\t<ul class=\"nsn_woo_ext_search_ul_multiline left\" >
\t\t\t\t";
            // line 8
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["attrs_list"]) ? $context["attrs_list"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["next_attr"]) {
                // line 9
                echo "
\t\t\t\t\t";
                // line 10
                if ( !(($this->getAttribute((isset($context["woo_ext_search_options"]) ? $context["woo_ext_search_options"] : null), "show_in_extended_search_hide_with_zero_products", array()) == "yes") &&  !$this->getAttribute($context["next_attr"], "products_count", array(), "any", true, true))) {
                    // line 11
                    echo "\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<input type=\"checkbox\" id=\"";
                    // line 12
                    echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : null);
                    echo $this->getAttribute($context["attrs_option"], "attr_name", array(), "array");
                    echo "_next_attr_";
                    echo $this->getAttribute($context["next_attr"], "slug", array());
                    echo "\" value=\"";
                    echo $this->getAttribute($context["next_attr"], "slug", array());
                    echo "\" class=\"";
                    echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : null);
                    echo "cbx_";
                    echo $this->getAttribute($context["attrs_option"], "attr_name", array(), "array");
                    echo "_selection_filters\"  >

\t\t\t\t\t\t\t";
                    // line 14
                    if ((($this->getAttribute((isset($context["woo_ext_search_options"]) ? $context["woo_ext_search_options"] : null), "run_search_by_click", array()) == "both") || ($this->getAttribute((isset($context["woo_ext_search_options"]) ? $context["woo_ext_search_options"] : null), "run_search_by_click", array()) == "link"))) {
                        // line 15
                        echo "\t\t\t\t\t\t\t<a class=\"nsn_woo_ext_search_a_link\" onclick=\"javascript:  nsn_woo_ext_search_frontendFuncsObj.filter_elementClicked( 'attr', '";
                        echo $this->getAttribute($context["attrs_option"], "attr_name", array(), "array");
                        echo "', '";
                        echo $this->getAttribute($context["next_attr"], "slug", array());
                        echo "' );return false;\">
\t\t\t\t\t\t\t\t";
                    }
                    // line 17
                    echo "\t\t\t\t\t\t\t\t&nbsp;";
                    echo $this->getAttribute($context["next_attr"], "name", array());
                    if ($this->getAttribute($context["next_attr"], "products_count", array(), "any", true, true)) {
                        echo "<b>(";
                        echo $this->getAttribute($context["next_attr"], "products_count", array());
                        echo "</b>)";
                    } else {
                    }
                    // line 18
                    echo "\t\t\t\t\t\t\t\t";
                    if ((($this->getAttribute((isset($context["woo_ext_search_options"]) ? $context["woo_ext_search_options"] : null), "run_search_by_click", array()) == "both") || ($this->getAttribute((isset($context["woo_ext_search_options"]) ? $context["woo_ext_search_options"] : null), "run_search_by_click", array()) == "link"))) {
                        // line 19
                        echo "\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t";
                    }
                    // line 21
                    echo "
\t\t\t\t\t\t</li>
\t\t\t\t\t";
                }
                // line 24
                echo "
\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['next_attr'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 26
            echo "\t\t\t</ul>

\t</div>
\t";
            // line 29
            echo $this->getAttribute((isset($context["library"]) ? $context["library"] : null), "header_block_end", array(0 => (("Filter on " . $this->getAttribute($context["attrs_option"], "attr_name", array(), "array")) . " Block End")), "method");
            echo "

";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['attrs_option'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 32
        echo "





";
    }

    public function getTemplateName()
    {
        return "white-simple/filter_attrs_box.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  115 => 32,  106 => 29,  101 => 26,  94 => 24,  89 => 21,  85 => 19,  82 => 18,  73 => 17,  65 => 15,  63 => 14,  49 => 12,  46 => 11,  44 => 10,  41 => 9,  37 => 8,  33 => 6,  31 => 5,  26 => 3,  23 => 2,  19 => 1,);
    }
}
/* {% for attrs_option in attrs_options_list %}*/
/* */
/* 	{{ library.header_block_start("Filter on " ~ attrs_option['attr_name'], "", "fa fa-search-plus", plugin_prefix ~ "block_" ~ attrs_option['attr_name'] ) }}*/
/* 	<div class="row nsn_woo_ext_search_block_padding_md">*/
/* 			{% set attrs_list= attrs_option['attrs_list'] %}*/
/* */
/* 			<ul class="nsn_woo_ext_search_ul_multiline left" >*/
/* 				{% for next_attr in attrs_list %}*/
/* */
/* 					{% if not ( woo_ext_search_options.show_in_extended_search_hide_with_zero_products == "yes" and next_attr.products_count is not defined ) %}*/
/* 						<li>*/
/* 							<input type="checkbox" id="{{ plugin_prefix }}{{ attrs_option['attr_name'] }}_next_attr_{{ next_attr.slug }}" value="{{ next_attr.slug }}" class="{{ plugin_prefix }}cbx_{{ attrs_option['attr_name'] }}_selection_filters"  >*/
/* */
/* 							{% if woo_ext_search_options.run_search_by_click == 'both' or woo_ext_search_options.run_search_by_click == 'link' %}*/
/* 							<a class="nsn_woo_ext_search_a_link" onclick="javascript:  nsn_woo_ext_search_frontendFuncsObj.filter_elementClicked( 'attr', '{{ attrs_option['attr_name'] }}', '{{ next_attr.slug }}' );return false;">*/
/* 								{% endif %}*/
/* 								&nbsp;{{ next_attr.name }}{% if next_attr.products_count is defined %}<b>({{ next_attr.products_count }}</b>){% else %}{% endif %}*/
/* 								{% if woo_ext_search_options.run_search_by_click == 'both' or woo_ext_search_options.run_search_by_click == 'link' %}*/
/* 							</a>*/
/* 							{% endif %}*/
/* */
/* 						</li>*/
/* 					{% endif %}*/
/* */
/* 				{% endfor %}*/
/* 			</ul>*/
/* */
/* 	</div>*/
/* 	{{ library.header_block_end( "Filter on " ~ attrs_option['attr_name'] ~ " Block End") }}*/
/* */
/* {% endfor %}*/
/* */
/* */
/* */
/* */
/* */
/* */
/* */
