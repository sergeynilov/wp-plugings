<?php

/* white-simple/existing_bookmarks_dialog.twig */
class __TwigTemplate_1f99e1644d02ea60f7d0dabc77459ff61a40a12b35f7a010f204f05e5881a037 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"btn-group  pull-right editor_btn_group \" role=\"group\" aria-label=\"group button\"><div class=\"modal fade\" id=\"";
        echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : $this->getContext($context, "plugin_prefix"));
        echo "show_new_bookmark_dialog\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"modalLabel\" aria-hidden=\"true\">
\t\t<div class=\"modal-dialog\">
\t\t\t<div class=\"modal-content\">
\t\t\t\t<section class=\"modal-header\">
\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\"><span aria-hidden=\"true\">×</span><span class=\"sr-only\">Close</span></button>
\t\t\t\t\t<div class=\"modal-title\">Select Existing Bookmark Dialog </div>
\t\t\t\t</section>

\t\t\t\t<section class=\"modal-body block_padding_lg\">
\t\t\t\t\t<form role=\"form\" class=\"form-horizontal\" >

\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label class=\"col-xs-12 col-sm-4 control-label \" for=\"";
        // line 14
        echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : $this->getContext($context, "plugin_prefix"));
        echo "show_existing_select_bookmark\">Select existing bookmark</label>
\t\t\t\t\t\t\t\t<div class=\"col-xs-12 col-sm-8\">
\t\t\t\t\t\t\t\t\t<select name=\"";
        // line 16
        echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : $this->getContext($context, "plugin_prefix"));
        echo "show_existing_select_bookmark\" id=\"";
        echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : $this->getContext($context, "plugin_prefix"));
        echo "show_existing_select_bookmark\" class=\"form-control editable_field\" >
\t\t\t\t\t\t\t\t\t\t<option value=\"\"> -Select- </option>
\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t<small> Selecting bookmark all filter controls will be filled with values of selected bookmark. </small>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t</form>
\t\t\t\t</section>

\t\t\t\t<section class=\"modal-footer\">
\t\t\t\t\t<div class=\"btn-group  pull-right editor_btn_group \" role=\"group\" aria-label=\"group button\">
\t\t\t\t\t\t<button type=\"button\" id=\"saveImage\" class=\"btn btn-primary\" onclick=\"javascript:nsn_woo_ext_search_frontendFuncsObj.fillBookmarkInControls();return false;\" role=\"button\">Select</button>
\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-cancel-action\" data-dismiss=\"modal\"  role=\"button\">Cancel</button>
\t\t\t\t\t</div>
\t\t\t\t</section>
\t\t\t</div>
\t\t</div>
\t</div>
</div>";
    }

    public function getTemplateName()
    {
        return "white-simple/existing_bookmarks_dialog.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  41 => 16,  36 => 14,  19 => 1,);
    }
}
/* <div class="btn-group  pull-right editor_btn_group " role="group" aria-label="group button"><div class="modal fade" id="{{ plugin_prefix }}show_new_bookmark_dialog" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">*/
/* 		<div class="modal-dialog">*/
/* 			<div class="modal-content">*/
/* 				<section class="modal-header">*/
/* 					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>*/
/* 					<div class="modal-title">Select Existing Bookmark Dialog </div>*/
/* 				</section>*/
/* */
/* 				<section class="modal-body block_padding_lg">*/
/* 					<form role="form" class="form-horizontal" >*/
/* */
/* 						<div class="row">*/
/* 							<div class="form-group">*/
/* 								<label class="col-xs-12 col-sm-4 control-label " for="{{ plugin_prefix }}show_existing_select_bookmark">Select existing bookmark</label>*/
/* 								<div class="col-xs-12 col-sm-8">*/
/* 									<select name="{{ plugin_prefix }}show_existing_select_bookmark" id="{{ plugin_prefix }}show_existing_select_bookmark" class="form-control editable_field" >*/
/* 										<option value=""> -Select- </option>*/
/* 									</select>*/
/* 									<small> Selecting bookmark all filter controls will be filled with values of selected bookmark. </small>*/
/* 								</div>*/
/* 							</div>*/
/* 						</div>*/
/* */
/* 					</form>*/
/* 				</section>*/
/* */
/* 				<section class="modal-footer">*/
/* 					<div class="btn-group  pull-right editor_btn_group " role="group" aria-label="group button">*/
/* 						<button type="button" id="saveImage" class="btn btn-primary" onclick="javascript:nsn_woo_ext_search_frontendFuncsObj.fillBookmarkInControls();return false;" role="button">Select</button>*/
/* 						<button type="button" class="btn btn-cancel-action" data-dismiss="modal"  role="button">Cancel</button>*/
/* 					</div>*/
/* 				</section>*/
/* 			</div>*/
/* 		</div>*/
/* 	</div>*/
/* </div>*/
