<?php

/* white-simple/new_bookmark_dialog.twig */
class __TwigTemplate_f1036561901786ddf3140b330590f4488eec6f71c6ea6f68eab955ccd8fe9fd5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"btn-group  pull-right editor_btn_group \" role=\"group\" aria-label=\"group button\"><div class=\"modal fade\" id=\"";
        echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : $this->getContext($context, "plugin_prefix"));
        echo "show_existing_bookmarks_dialog\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"modalLabel\" aria-hidden=\"true\">
\t<div class=\"modal-dialog\">
\t\t<div class=\"modal-content\">
\t\t\t<section class=\"modal-header\">
\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\"><span aria-hidden=\"true\">×</span><span class=\"sr-only\">Close</span></button>
\t\t\t\t<div class=\"modal-title\">New&nbsp;Bookmark&nbsp;Dialog&nbsp;Setup</div>
\t\t\t</section>

\t\t\t<section class=\"modal-body block_padding_lg\">
\t\t\t\t<form role=\"form\" class=\"form-horizontal\" >

\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-xs-12 col-sm-4 control-label \" for=\"";
        // line 14
        echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : $this->getContext($context, "plugin_prefix"));
        echo "bookmark_title\">Bookmark Title</label>
\t\t\t\t\t\t\t<div class=\"col-xs-12 col-sm-8\">
\t\t\t\t\t\t\t\t<input class=\"form-control\" value=\"\" id=\"";
        // line 16
        echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : $this->getContext($context, "plugin_prefix"));
        echo "bookmark_title\" type=\"text\">
\t\t\t\t\t\t\t\t<small> Enter name under which you want to save your bookmark</small>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-xs-12 col-sm-4 control-label \" for=\"";
        // line 24
        echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : $this->getContext($context, "plugin_prefix"));
        echo "select_bookmark\">Or save new bookmark under existing name, overwriting it</label>
\t\t\t\t\t\t\t<div class=\"col-xs-12 col-sm-8\">
\t\t\t\t\t\t\t\t<select name=\"";
        // line 26
        echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : $this->getContext($context, "plugin_prefix"));
        echo "select_bookmark\" id=\"";
        echo (isset($context["plugin_prefix"]) ? $context["plugin_prefix"] : $this->getContext($context, "plugin_prefix"));
        echo "select_bookmark\" class=\"form-control editable_field\" >
\t\t\t\t\t\t\t\t\t<option value=\"\"> -Select existing bookmark- </option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t<small> Selecting bookmark from this list you will overwrite it</small>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t</form>
\t\t\t</section>

\t\t\t<section class=\"modal-footer\">
\t\t\t\t<div class=\"btn-group  pull-right editor_btn_group \" role=\"group\" aria-label=\"group button\">
\t\t\t\t\t<button type=\"button\" id=\"saveImage\" class=\"btn btn-primary\" onclick=\"javascript:nsn_woo_ext_search_frontendFuncsObj.makeSaveBookmark();return false;\" role=\"button\">Save</button>
\t\t\t\t\t<button type=\"button\" class=\"btn btn-cancel-action\" data-dismiss=\"modal\"  role=\"button\">Cancel</button>
\t\t\t\t</div>
\t\t\t</section>
\t\t</div>
\t</div>
</div>";
    }

    public function getTemplateName()
    {
        return "white-simple/new_bookmark_dialog.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 26,  52 => 24,  41 => 16,  36 => 14,  19 => 1,);
    }
}
/* <div class="btn-group  pull-right editor_btn_group " role="group" aria-label="group button"><div class="modal fade" id="{{ plugin_prefix }}show_existing_bookmarks_dialog" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">*/
/* 	<div class="modal-dialog">*/
/* 		<div class="modal-content">*/
/* 			<section class="modal-header">*/
/* 				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>*/
/* 				<div class="modal-title">New&nbsp;Bookmark&nbsp;Dialog&nbsp;Setup</div>*/
/* 			</section>*/
/* */
/* 			<section class="modal-body block_padding_lg">*/
/* 				<form role="form" class="form-horizontal" >*/
/* */
/* 					<div class="row">*/
/* 						<div class="form-group">*/
/* 							<label class="col-xs-12 col-sm-4 control-label " for="{{ plugin_prefix }}bookmark_title">Bookmark Title</label>*/
/* 							<div class="col-xs-12 col-sm-8">*/
/* 								<input class="form-control" value="" id="{{ plugin_prefix }}bookmark_title" type="text">*/
/* 								<small> Enter name under which you want to save your bookmark</small>*/
/* 							</div>*/
/* 						</div>*/
/* 					</div>*/
/* */
/* 					<div class="row">*/
/* 						<div class="form-group">*/
/* 							<label class="col-xs-12 col-sm-4 control-label " for="{{ plugin_prefix }}select_bookmark">Or save new bookmark under existing name, overwriting it</label>*/
/* 							<div class="col-xs-12 col-sm-8">*/
/* 								<select name="{{ plugin_prefix }}select_bookmark" id="{{ plugin_prefix }}select_bookmark" class="form-control editable_field" >*/
/* 									<option value=""> -Select existing bookmark- </option>*/
/* 								</select>*/
/* 								<small> Selecting bookmark from this list you will overwrite it</small>*/
/* 							</div>*/
/* 						</div>*/
/* 					</div>*/
/* */
/* 				</form>*/
/* 			</section>*/
/* */
/* 			<section class="modal-footer">*/
/* 				<div class="btn-group  pull-right editor_btn_group " role="group" aria-label="group button">*/
/* 					<button type="button" id="saveImage" class="btn btn-primary" onclick="javascript:nsn_woo_ext_search_frontendFuncsObj.makeSaveBookmark();return false;" role="button">Save</button>*/
/* 					<button type="button" class="btn btn-cancel-action" data-dismiss="modal"  role="button">Cancel</button>*/
/* 				</div>*/
/* 			</section>*/
/* 		</div>*/
/* 	</div>*/
/* </div>*/
