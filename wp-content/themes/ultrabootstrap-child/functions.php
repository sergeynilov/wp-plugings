<?php

add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
// function my_theme_enqueue_styles() {
//     wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
//}


function my_theme_enqueue_styles() {

    $parent_style = 'parent-style'; // This is 'twentyfifteen-style' for the Twenty Fifteen theme.

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );

if (defined('WP_DEBUG_LOG') && WP_DEBUG_LOG) {
    ini_set( 'error_log', WP_CONTENT_DIR . '/debug.txt' );
    ini_set( 'error_log', ABSPATH . '/log/debug.log' );
}



function nsn_woo_ext_search_exclude_service_category($query) { // define to exclude service category posts from pages
    $woo_ext_search_options = get_option('nsn-woo-ext-search-');
    $service_category_for_attrs= '';
    if( !empty($woo_ext_search_options['service_category_for_attrs']) and !is_admin()  ) {   // checks only for frontend
        $service_category_for_attrs= $woo_ext_search_options['service_category_for_attrs'];
    }
    $exclude_categories= '';
    if ( $query->is_home or $query->is_single or $query->is_preview  or $query->is_page  or $query->is_archive  or $query->is_category ) {
        $exclude_categories.= '-'.$service_category_for_attrs.',';
    }
//    echo '<pre>$query::'.print_r($query,true).'</pre>';
//    echo '<pre>nsn_woo_ext_search_exclude_service_category $exclude_categories::'.print_r($exclude_categories,true).'</pre>';
    if ( !empty($exclude_categories) ) {
        $query->set('cat', $exclude_categories);
    }
    return $query;
} //function nsn_woo_ext_search_exclude_service_category($query) { // define to exclude service category posts from pages
add_filter('pre_get_posts', 'nsn_woo_ext_search_exclude_service_category');


function nsn_woo_ext_search_exclude_widget_category($args){ /* Remove service category from category widget */
    $woo_ext_search_options = get_option('nsn-woo-ext-search-');

    $service_category_for_attrs= '';
    if( !empty($woo_ext_search_options['service_category_for_attrs']) and !is_admin() ) {    // checks only for frontend
        $service_category_for_attrs= $woo_ext_search_options['service_category_for_attrs'];
    }
    $exclude_categories= $service_category_for_attrs.',';
    $exclude = $exclude_categories; // IDs of excluding categories
    $args["exclude"] = $exclude;
    return $args;
} //function nsn_woo_ext_search_exclude_widget_category($args){ /* Remove service category from category widget */
add_filter("widget_categories_args","nsn_woo_ext_search_exclude_widget_category");

if ( ! function_exists( 'nsn_woo_ext_search_show_products_by_attribute' ) ) {
    function nsn_woo_ext_search_show_products_by_attribute( $current_attribute_name, $current_attribute_value, $post_id ) {
        $current_attribute_slug= nsnWooToolsData::get_attr_value_slug_by_value( $current_attribute_name, $current_attribute_value );
//        ); /* http://wordpress.stackexchange.com/questions/75079/nested-meta-query-with-multiple-relation-keys
        $page= 1;
        $navigationHTML= '';
        $orderby= "rating";
        $tax_args[] =  array(
            'taxonomy' => 'pa_' . $current_attribute_name,
            'field'    => 'slug',
            'terms'    => $current_attribute_slug,
        );
        $args = array(         //$filter_categories_list
            's'                   => '',
            'post_type'           => 'product',
            'post_status'         => 'publish',
            'ignore_sticky_posts' => -1,
            'numberposts'         => -1, // show all
            'orderby'             => $orderby,
            'posts_per_page'      => -1,
            'suppress_filters'    => false,
            'tax_query'           => $tax_args,
        );

        $related_product_posts= query_posts($args);
        $products_list= array();
        $additive_attrs= array( 'woocommerce_price_format'=> get_woocommerce_price_format(), 'woocommerce_currency_symbol'=> get_woocommerce_currency_symbol( get_woocommerce_currency() ), 'decimal_separator' => wc_get_price_decimal_separator(), 'thousand_separator' => wc_get_price_thousand_separator(), 'decimals' => wc_get_price_decimals(), 'date_format'=> get_option('date_format'), 'time_format'=> get_option('time_format')   );

        foreach( $related_product_posts as $next_key=>$next_raw_product_post ) {
            $woo_next_product = nsnWooToolsData::get_woo_product_props( $next_raw_product_post->ID, array( 'sale_price', 'regular_price', 'is_in_stock'), array(), $additive_attrs);
            $woo_next_product['guid'] = $next_raw_product_post->guid;
            $woo_next_product['post_title'] = $next_raw_product_post->post_title;
            $woo_next_product['permalink'] = get_permalink($next_raw_product_post);
            $products_list[]= $woo_next_product;
        }

        $data= array('products_list'=>$products_list, 'page'=> $page, 'total_products_count'=> count($products_list),  'navigationHTML' => $navigationHTML );
        $ret= twigpress_render_twig_template( $data, "white-simple/woo_ext_related_products_list.twig", false );
        echo $ret;

    }
} // if ( ! function_exists( 'nsn_woo_ext_search_show_products_by_attribute' ) ) {

if ( ! function_exists( 'nsn_woo_ext_search_show_related_products' ) ) {
    function nsn_woo_ext_search_show_related_products($post, $params)
    {
        $woo_ext_search_options = get_option('nsn-woo-ext-search-');
        $service_category_for_attrs_array = array();
        if (!empty($woo_ext_search_options['service_category_for_attrs_array']) and is_array($woo_ext_search_options['service_category_for_attrs_array'])) {
            $service_category_for_attrs_array = $woo_ext_search_options['service_category_for_attrs_array'];
        }
        foreach ($service_category_for_attrs_array as $attr_key => $next_service_category_for_attr_array) {
            foreach ($next_service_category_for_attr_array as $next_attr_key => $next_post_id) {
                if ($next_post_id == $post->ID) {

                    $current_attribute_name = $attr_key;
                    $attr_key= 'service_category_attr_' . $current_attribute_name.'_';
                    $a= preg_split('/'.$attr_key.'/', $next_attr_key);
//                    echo '<pre>$a::'.print_r($a,true).'</pre>';
                    if ( count($a) == 2 ) {
                        $current_attribute_value= $a[1];
//                        echo '<pre>$current_attribute_value::' . print_r($current_attribute_value, true) . '</pre>';
                        nsn_woo_ext_search_show_products_by_attribute($current_attribute_name, $current_attribute_value, $next_post_id);
                    }
                    break;
                }
            }
        }

    }
} // if ( ! function_exists( 'nsn_woo_ext_search_show_related_products' ) ) {
?>