<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp-plugings');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '1');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'c:t,cJ~8;RZrP/*pg$YZY(PWmOXfz.=2-pDh.uE|juW=nT~v=A[+!RA!vbI:ptv0');
define('SECURE_AUTH_KEY',  '(4^9Si/T`C0g?ORy+`6+3WhlY2(BeV7pg&s.*/8{inz?)g($}?Q@]T/CIQ{mPK:c');
define('LOGGED_IN_KEY',    '8FCU|]1?J5i], 8!meV.!M,Z{%x`Lh8=eWuk=BqhTg#s5!Ujo7I|Uh6v+vK}n[RI');
define('NONCE_KEY',        'Ou0o1u@KuY(nk3U9GKI=iQtb`6).LyBjW&s|+5~;gc_vvex&5/Oy?[J). dvyI/G');
define('AUTH_SALT',        '_;s*ZowIWo$BVc|Qw~t{-R Q}dbVQShezn|k<^E^;/B(zIIgrJe,QF(ALotnj7k[');
define('SECURE_AUTH_SALT', 'xRV>;5?v;+(3w`k/X=2)<bku:@#X)W_5KrY-0;)|~ 7Exj0^@^lg(Y6jm[F,FRt_');
define('LOGGED_IN_SALT',   '_N9z$ex;D0%3LFwy *TOhE^i#HFrMia+g#)E}x0t-Pxt.+w=m+HDO1kT>Qn1.mtr');
define('NONCE_SALT',       'i.@^3D69kEU,)BBeH# e:U$[Y5]FOoN34 f/7^M>2O}tgKQVr>M@}RDCU^SC<UcW');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
//define('WP_DEBUG', false);


/* That's all, stop editing! Happy blogging. */

if ( (!empty($_SERVER["HTTP_HOST"]) and !(strpos($_SERVER["HTTP_HOST"], "local-wp-plugings.com") === false) ) ) {
    define( 'isDeveloperComp', false );
} else {
    define( 'isDeveloperComp', false );
}
// echo '<pre>isDeveloperComp::'.print_r(isDeveloperComp,true).'</pre>';
error_reporting(E_ALL ^ E_DEPRECATED);
error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);


ini_set('output_buffering', 'on');
$ds = DIRECTORY_SEPARATOR;
# DEBUG
define( 'WP_DEBUG', isDeveloperComp );
// file: ~/WP_CONTENT_DIR/debug.log
define( 'WP_DEBUG_LOG', true );
define( 'WP_DEBUG_DISPLAY', isDeveloperComp );
define( 'SAVEQUERIES', true );
# DEBUG: MU
define( 'DIEONDBERROR', true );
//define( 'ERRORLOGFILE', WP_CONTENT_DIR.$ds.'logs'.$ds.'mu_error.log' );
define( 'ERRORLOGFILE', ABSPATH.'log'.$ds.'debug.log' );


define( 'BACKEND_SITE_DIR', ABSPATH . '/backend' );

$sqlQueriesFilename= str_replace( "/", $ds, ABSPATH. '/log'.$ds.'sql_queries_to_file' );
define( 'nsn_sql_queries_to_file', $sqlQueriesFilename );
//echo '<pre>nsn_sql_queries_to_file::'.print_r(nsn_sql_queries_to_file,true).'</pre>';

@ini_set( 'log_errors', 1 );
# PHP Error log location
//@ini_set( 'error_log', WP_CONTENT_DIR.$ds.'logs'.$ds.'php_error.log' );
//ini_set( 'error_log', WP_CONTENT_DIR . '/debug.log' );
ini_set( 'error_log', ABSPATH . 'log/debug1.log' );
/* That's all, stop editing! Happy blogging. */


/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

